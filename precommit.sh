cd src
find . -name "*.o" | xargs rm
find . -name "*.d" | xargs rm
cd ..

rm -rf ./src/ShadowHand-PalmSensor/.svn
rm -rf ./src/ShadowHand-PalmSensor/ShadowPalm/.svn
rm ./src/ShadowHand-PalmSensor/ShadowPalm/firmware/ShadowHand-PalmSensor.X/dist/PalmPCB_v3/debug/ShadowHand-PalmSensor.X.debug.elf 

rm ./src/ShadowHand-PalmSensor/ShadowPalm/firmware/ShadowHand-PalmSensor.X/dist/PalmPCB_v3/production/ShadowHand-PalmSensor.X.production.elf

rm ./src/ShadowHand-PalmSensor/ShadowPalm/firmware/ShadowHand-PalmSensor.X/.generated_files/*.flag
