/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITECSerialProtocol.c

  @Summary
    Implements functionality to do communication according to the serial protocol
    definition @ https://projects.cit-ec.uni-bielefeld.de/projects/agni-grasplab/wiki/Serial_protocol

  @Description
    For further details see the header file and link above.
 */
/* ************************************************************************** */
// Original Code by Luis Oberroehrmann
// Rewritten by Guillaume Walck

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */


#include "CITECSerialProtocolTask.h"

#include "CITECSerialProtocol.h"
#ifdef USE_RTOS
#include "task.h"
#endif


#ifndef SYS_ASSERT
#define SYS_ASSERT(test, message) if(test==false){SYS_MESSAGE(message); SYS_DEBUG_BreakPoint();}
#endif

// *****************************************************************************
// *****************************************************************************
// Section: Global used
// *****************************************************************************
// *****************************************************************************

// Global buffer for output (to not initialize a new one it in the writer function while passing to USBSend)
CDC_DataType cdcOutData;
CDC_DataType cdcInData;
// Global to avoid huge memory on the task stack
SP_RawData spRawData;   // Serial Protocol rawbuffer container
SP_Container spContainer;   // Serial Protocol main container
SourcesStruct spSource;   // Serial Protocol data source
SP_FnRunReq pa_SP_runRequestCB[SP_MAX_NUM_SENSOR]; // SP run request callback pointer list associated to each sensor
SP_FnSetPer pa_SP_setPeriodCB[SP_MAX_NUM_SENSOR]; // SP set period callback pointer list associated to each sensor



/* ************************************************************************** */
/* ************************************************************************** */
// Section: Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

// Write function to Serial Protocol
// TODO permit write to WIFI again
uint16_t CITEC_SerialProtocol_SerialWrite(uint8_t *pui8_outdata, uint32_t ui32_len)
{
    // handle source
    switch(spSource.e_communicationType)
    {//call function acording to Destination of Data
        case USB:
        {//Data will be send via USB
            if(CITEC_USBCDC_Write(pui8_outdata, ui32_len)!=CDCERROR_NoError)
            {//USB Send Faild
               SYS_ASSERT(0, "Could not send Data"); 
            } 
            break;
        }//case USB
        //you can add additional send/receive channels here,
        //just make sure they send incoming data to CITECSerialReceive_Queue
        //with all the required information (edit the enum if necessary)
//        case WIFI:
//        {//Send via WIFI
//            if (CITECWIFIProtocol_Send(msg)!=0)
//            {//Send via WIFI unsuccessful
//                SYS_ASSERT(0, "Could not send Data to queue");
//            }
//            break;    
//        }//case WIFI
        default:
        {//in errorcase
            SYS_ASSERT(0, "No Source specified");
            
            //Send via USB as default
            CITEC_USBCDC_Write(pui8_outdata, ui32_len);
        }
    }//switch(Source.Communication) 

    return 0;
}

void CITEC_SerialProtocol_SetWriteCallback(SP_WriteFunc fn)
{
    SP_SetWriteCallback(&spContainer, fn);
}

//Descriptive header can be found in the CITECSerialProtocol.h
void CITEC_SerialProtocol_Initialize(uint8_t ui8_deviceTypeID)
{
    // passing the global SP Raw storage in the structure
    spContainer.p_rawdata = &spRawData;
    // initialize SP internal variables
    SP_init(&spContainer);    
    // Initialize the devices    
    spContainer.config.ui8_deviceTypeID = ui8_deviceTypeID ;//iObjectPlus;                                //Define your Device Type    
    // Set default source
    spSource.e_communicationType = USB;
}


bool CITEC_SerialProtocol_TaskInit()
{
    // some checks on the serial protocol containers and initialization
    if (spContainer.config.ui8_sensorCount == 0)
    {
        SYS_ASSERT(0, "No sensor defined, call CITEC_SerialProtocol_RegisterSensor() before creating the task to add some!!!");  
        return false;
    }
    else
    {
        uint8_t ui8_sensorID=0;
        for(ui8_sensorID=0; ui8_sensorID < spContainer.config.ui8_sensorCount; ui8_sensorID++)
        {//check if Sensors got assigned to the right array position
            if(spContainer.config.sensorConfigs[ui8_sensorID].ui8_sensorDatagramID!=(ui8_sensorID+1))
            {//Array Position does not fit to Sensor ID
                SYS_ASSERT(0, "Please adjust Datagram IDs according to specification!!!");  
                return false;
            }
        }
        // create the SerialConfiguration confbuffer from the config
        SP_configInitFromConfig(&spContainer);
    }

#ifdef USE_RTOS
    // TODO Keep this until WiFi is handled again 
    //Initialize the receive queue, that will be fed by USB and WIFI incoming Data, and read + decoded by the main SP task.
    //SerialReceiveQueue = xQueueCreate(SerialReceiveQueueSize, sizeof(SerialDataStruct));
    /* don't proceed if queue was not created...*/
    if(qh_USBCDC_RxQueue == NULL)
    {   
        return false;
    }     
    
    qh_SerialDataUpdateQueue = xQueueCreate(DATAUPDATE_QUEUE_SIZE, (sizeof(SerialDataUpdateStruct)));
    /*don't proceed if queue was not created...*/
    if(qh_SerialDataUpdateQueue == NULL)
    {   
        return false;
    }
#endif
    
    return true;
}



//Descriptive header can be found in the CITECSerialProtocol.h
void CITEC_SerialProtocol_WelcomeTask(void* arg)
{
    bool *pb_welcomeTaskRunnig = (bool*)arg;
    CDC_DataType data;
    USBCDCErrors e_Sendstate;
    // prepare a short message testing if the port is open at all
    strncpy(cdcOutData.ca_Message, ".", 1); //Store string
    cdcOutData.u32_Length = 1;                    
    do
    {//try to send until successful
        e_Sendstate=CDCERROR_Undefined;
        if (cdcData.b_isConfigured)
        {
            e_Sendstate=CDCERROR_NoError;
            //here the output is generated manually
            e_Sendstate|=CITEC_USBCDC_Write(cdcOutData.ca_Message, cdcOutData.u32_Length);   
        }//send string       
        //retry or continue after 1000 ms
#ifdef USE_RTOS
        vTaskDelay(pdMS_TO_TICKS(1000));
#endif
    }while(e_Sendstate!=CDCERROR_NoError || cdcData.b_isConfigured != true );
    
    // prepare a welcome message
    strncpy(cdcOutData.ca_Message, "Welcome to this SerialProtocol Demo. Test SerialProtocol communication:\n"
                    "\treq config 0xF0C400C0F4\n"
                    "\tstart streaming 0xF0C400F1C5\n"
                    "\tstop streaming 0xF0C4000034\n"
                    "\tping 0xF0C400F0C4\n", 175); //Store string
    cdcOutData.u32_Length = 175;                
    do
    {// send in a loop
        e_Sendstate=CDCERROR_Undefined;
        if (cdcData.b_isConfigured)
        {
            e_Sendstate=CDCERROR_NoError;
            e_Sendstate|=CITEC_USBCDC_Write(cdcOutData.ca_Message, cdcOutData.u32_Length);
            CITEC_USBCDC_SendNow();
        }//send string       
        // continue after 1000 ms
#ifdef USE_RTOS
        vTaskDelay(pdMS_TO_TICKS(1000));
#endif
    }while(e_Sendstate==CDCERROR_NoError && cdcData.b_isConfigured == true  && *pb_welcomeTaskRunnig);
#ifdef USE_RTOS
    vTaskDelete( NULL );
#endif
}


//Descriptive header can be found in the CITECSerialProtocol.h
bool CITEC_SerialProtocol_RegisterSensor(SP_SensorConfiguration config, SP_FnRunReq fn_runRequest, SP_FnSetPer fn_setPeriod)
{
    uint8_t *pui8_SensorCount = &spContainer.config.ui8_sensorCount;
    if(*pui8_SensorCount + 1 <= SP_MAX_NUM_SENSOR)
    {
        // TODO validate the struct is not null ?
        // copy the given struct
        spContainer.config.sensorConfigs[*pui8_SensorCount] = config;
        spContainer.config.sensorConfigs[*pui8_SensorCount].ui8_sensorDatagramID = *pui8_SensorCount + 1 ;
        // store the runRequest callback
        pa_SP_runRequestCB[*pui8_SensorCount] = fn_runRequest;
        // store the setPeriod callback
        pa_SP_setPeriodCB[*pui8_SensorCount] = fn_setPeriod;
        // increment the number of registered sensors
        *pui8_SensorCount = *pui8_SensorCount + 1;
        return true;
    }
    else
    {
        SYS_ASSERT(0, "Maximum number of sensors reached");
        return false;
    }
}

#ifdef USE_RTOS
//Descriptive header can be found in the CITECSerialProtocol.h
void CITEC_SerialProtocol_Task(void)
{
    // declare variables 
    bool b_welcomeTaskRunning = false;
    //Setup Variables:   
    // TODO // mutex on serial container
    //Initialize the device for serial protocol transmissions
    while(CITEC_SerialProtocol_TaskInit()==false)
    {//do not continue if Receive_Queue does not exist / an error at initialize occurred
        vTaskDelay(pdMS_TO_TICKS(1));
        // TODO do some sleep here
    }    

    // Create a dummy standby tasks sending instruction how to talk to us
    b_welcomeTaskRunning = true;
    TaskHandle_t th_welcome = NULL;
    if(xTaskCreate((TaskFunction_t) CITEC_SerialProtocol_WelcomeTask, 
        "Welcome task",   512, &b_welcomeTaskRunning, 4, &th_welcome)==pdFAIL)
        SYS_ASSERT(0, "Could not create Task");
    while(1)
    {//start endless loop in task, to receive commands from PC and process the update queue.
        CITEC_SerialProtocol_TaskHandler(&b_welcomeTaskRunning);   
    }//while(1)
}//CITEC_SerialProtocol_Task
#endif

void CITEC_SerialProtocol_TaskHandler(bool *pb_welcomeTaskRunning)
{
#ifdef USE_RTOS 
    BaseType_t                  l_errStatus;          //Success info Send/Receive Queue
    SerialDataUpdateStruct updateData; 
    l_errStatus=xQueueReceive(qh_USBCDC_RxQueue, &cdcInData, pdPASS); //   // portMAX_DELAY
    if(l_errStatus!=pdFAIL)
    {
#else
    // call the low-level USB task handler
    CITEC_USBCDC_USBDeviceTaskHandler(&cdcInData);
    if(cdcData.b_isDataAvailable) // new data was received, process it
    {
        cdcData.b_isDataAvailable = false; // indicate the data is used
#endif
        
    //Successful Reception

        // TODO lock the structure
        // Store the data in the container
        SP_read(spContainer.p_rawdata, cdcInData.ca_Message, cdcInData.u32_Length);
        // TODO unlock

        // process it
        SP_update(&spContainer);

        if (spContainer.state.b_initialSerialDataReceived && *pb_welcomeTaskRunning)
        {
            // stop the welcome task
            *pb_welcomeTaskRunning = false;
        }            
    }//successful Transmission     

    // handle periodicity changes (user should be careful about 100us units)
    if (spContainer.state.b_periodChanged)
    {
        uint8_t ui8_sensorID=0;
        for(ui8_sensorID=0; ui8_sensorID < spContainer.config.ui8_sensorCount; ui8_sensorID++)
        {
            int32_t i32_period = SP_getPeriod(&spContainer.config,ui8_sensorID+1);
            if (i32_period > 0)
            {
                if(pa_SP_setPeriodCB[ui8_sensorID])
                    pa_SP_setPeriodCB[ui8_sensorID](i32_period);
            }
        }
        // mark change as processed
        spContainer.state.b_periodChanged = false;
    }

    if(spContainer.state.b_streaming)
    {   
        // Acquisition Task management
        uint8_t ui8_sensorID=0;
        for(ui8_sensorID=0; ui8_sensorID < spContainer.config.ui8_sensorCount; ui8_sensorID++)
        {
            // TODO handle activity of the specific task 
            // TODO handle single shot (trigger)
            if(pa_SP_runRequestCB[ui8_sensorID])
                pa_SP_runRequestCB[ui8_sensorID](true);
        }

        // receive the data to be send regularly
#ifdef USE_RTOS
        l_errStatus=xQueueReceive(qh_SerialDataUpdateQueue, &updateData, pdMS_TO_TICKS(1));
        if(l_errStatus!=pdFAIL)
        {//Successful Reception
            // prepare data for sending
            // TODO use ticks instead of internal counter xTaskGetTickCount(); 
            SP_packData(&spContainer, (void*)updateData.pui8_data, updateData.ui8_sensorDatagramID);
            // send the data
            // TODO handle errors
            SP_publish(&spContainer);
            CITEC_USBCDC_SendNow();
        }
#else
        // Packing and publishing the data happens in the user task
#endif   
    }   
    else
    {
        // Acquisition Task management
        uint8_t ui8_sensorID=0;
        for(ui8_sensorID=0; ui8_sensorID < spContainer.config.ui8_sensorCount; ui8_sensorID++)
        {
            // TODO handle activity of the specific task 
            // TODO handle single shot (trigger)
            if(pa_SP_runRequestCB[ui8_sensorID])
                pa_SP_runRequestCB[ui8_sensorID](false);
        }
    }  
}

//Descriptive header can be found in the CITECSerialProtocol.h
uint8_t CITEC_SerialProtocol_SendError(SP_Container *p_sc, uint8_t ui8_errorID, char ca_String[], SourcesStruct Source)
{ 
    // TODO use source
    //Source;
    switch(ui8_errorID)
    {
        case SP_ErrCheckusm:
        {
            SP_checksumError(p_sc);
            break;
        }
        case SP_ErrUnrecognizedCommand:
        {
            SP_cmdError(p_sc);
            break;
        }
        case SP_ErrUnrecognizedSensorID:
        {//only output the error ID
            SP_textError(p_sc, &ui8_errorID, 1);
            break;
        }
        
        case SP_ErrUnknown:
        default:
        {//need to send error string
            if(strlen(ca_String)<=SP_ErrMaxStringLen)
            {//get size of string and store to Send
                SP_textError(p_sc, ca_String, strlen(ca_String));
            }
            else
            {
                SYS_ASSERT(0, "The provided Error String is too long");
                return -1;  //TODO: Define errors to return
            }
            
            break;
        }
    }
    return 0;
}

/* *****************************************************************************
 End of File
 */
