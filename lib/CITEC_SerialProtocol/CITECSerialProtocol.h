/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITECSerialProtocol.h

  @Summary
    This header file describes all structs and Functions used to implement
    the Serial Protocol defined at:
    https://projects.cit-ec.uni-bielefeld.de/projects/agni-grasplab/wiki/Serial_protocol

 */
/* ************************************************************************** */

#ifndef _CITECSerialProtocol_H    /* Guard against multiple inclusion */
#define _CITECSerialProtocol_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include <stdbool.h>
#include <stdint.h>
//#include "queue.h"
#include "CITECSerialProtocolDefines.h"
#include "CITECSerialProtocolTypes.h"

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


// *****************************************************************************
// *****************************************************************************
// Section: Functions
// *****************************************************************************
// *****************************************************************************


// <editor-fold defaultstate="collapsed" desc="SP Initialization">
// *****************************************************************************
/**
  @Function
    void SP_init(SP_Container *sc)

  @Summary
    Initialize the SP struct

  @Description
    resets all variables in serial protocol struct

  @Precondition

  @Parameters
    @param sc: pointer to SP container

  @Returns

  @Remarks

 */
//// </editor-fold>
void SP_init(SP_Container *p_sc);

// <editor-fold defaultstate="collapsed" desc="SP_setConfig">
// *****************************************************************************
/**
  @Function
    SP_setConfig(SP_Container *sc, uint8_t* conf, unsigned int len)

  @Summary
    Prepare the configuration buffer

  @Description
    using the buffer and the length, prepared the configuration buffer with checksum

  @Precondition

  @Parameters
    @param sc: pointer to SP container
    @param conf: pointer to an array containing the configuration
    @param len: length of the configuration
  @Returns

  @Remarks

 */
//// </editor-fold>
void SP_encodeConfig(SP_Container *p_sc, uint8_t* conf, uint32_t ui32_len);

void SP_decodeConfig(SP_Container *p_sc);

// <editor-fold defaultstate="collapsed" desc="SP_MakeConfig"> 
// *****************************************************************************
/**
  @Function
    SP_SensorConfiguration SP_MakeConfig(uint8_t ui8_typeID, uint8_t ui8_chipID, uint16_t ui16_dataLen, uint16_t ui16_period);

  @Summary
    helper function to fill a Serial Protocol configuration struct from arguments

  @Description
    helper function to fill a Serial Protocol configuration struct from arguments

  @Precondition
    None.

  @Parameters
    @param uint8_t ui8_typeID: what kind of sensor (see CITECSerialProtocolDefines.h)
    @param uint8_t ui8_chipID: what chip is used (look at the accepted chips)
    @param uint16_t ui16_dataLen: length of the byte stream the sensor will produce
    @param uint16_t ui16_period: period in 100 useconds units at which the sensor should publish
 
  @Returns
   SP_SensorConfiguration struct filled (except DatagramID)

  @Remarks
    None.
 */
//// </editor-fold>
SP_SensorConfiguration SP_makeConfig(uint8_t ui8_typeID, uint8_t ui8_chipID, uint16_t ui16_dataLen, uint16_t ui16_period);

void SP_configInitFromConfig(SP_Container *p_sc);

void SP_configInitFromBuffer(SP_Container *p_sc, uint8_t* conf, uint32_t ui32_len);

// <editor-fold defaultstate="collapsed" desc="SP_getPeriod">
// *****************************************************************************
/**
  @Function
    int SP_getPeriod(SP_DeviceConfiguration *sdc, unsigned int sen_id);

  @Summary
    Return period of the selected sensor

  @Description
    Return period of the selected sensor

  @Precondition

  @Parameters
    @param sdc: pointer to SP Config
    @param sen_id: sensor id to get the period from

  @Returns
    period of the selected sensor,
    or SP_PeriodInactive if sensor is not active
    or SP_PeriodUnknownId if sensor id does not exist
  @Remarks

 */
//// </editor-fold>
int32_t SP_getPeriod(SP_DeviceConfiguration *p_sdc, uint8_t sen_id);

// <editor-fold defaultstate="collapsed" desc="SP_getDataLen">
// *****************************************************************************
/**
  @Function
    unsigned int SP_getDataLen(SP_DeviceConfiguration *sdc, unsigned int sen_id);

  @Summary
    Return data length of the selected sensor

  @Description
    Return data length of the selected sensor

  @Precondition

  @Parameters
    @param sdc: pointer to SP Config
    @param sen_id: sensor id to get the period from

  @Returns
    data lengths of the selected sensor,
    or 0 if sensor id does not exist
  @Remarks

 */
//// </editor-fold>
uint16_t SP_getDataLen(SP_DeviceConfiguration *p_sdc, uint8_t sen_id);

// <editor-fold defaultstate="collapsed" desc="SP_defaultPeriod">
// *****************************************************************************
/**
  @Function
    void SP_defaultPeriod(SP_DeviceConfiguration *sdc, uint16_t period=100)

  @Summary
    Defines default period for all sensors

  @Description
    Defines default period for all sensors

  @Precondition

  @Parameters
    @param sc: pointer to SP container
    @param period: period in 0.1 ms default 100 = 10 ms

  @Returns

  @Remarks

 */
//// </editor-fold>
void SP_defaultPeriod(SP_DeviceConfiguration *p_sdc);

// <editor-fold defaultstate="collapsed" desc="SP_findHeader">
// *****************************************************************************
/**
  @Function
    int SP_findHeader(SP_Data *sd)

  @Summary
    Finds the header

  @Description
    Finds the header

  @Precondition

  @Parameters
    @param sc: pointer to SP Data

  @Returns
    offset of the header or -1 if not found
  @Remarks

 */
//// </editor-fold>
int16_t SP_findHeader(uint8_t* buf);

// <editor-fold defaultstate="collapsed" desc="SP_read">
// *****************************************************************************
/**
  @Function
    void SP_read(SP_RawData *sd, uint8_t *data, unsigned int len)

  @Summary
    copy incoming data into inBuffer

  @Description
    copies the given data into the inBuffer to process its own copy

  @Precondition

  @Parameters
    @param sd: pointer to SP Data
    @param data: pointer to incoming data
    @param len: length of the incoming data

  @Returns

  @Remarks

 */
//// </editor-fold>
void SP_read(SP_RawData *p_sd, uint8_t *data, uint32_t ui32_len);

// <editor-fold defaultstate="collapsed" desc="SP_update">
// *****************************************************************************
/**
  @Function
    void SP_update(SP_Container *sc, int receivedBytes)

  @Summary
    update the SP struct with new data

  @Description
    update the SP struct including decoding commands, after new data was stored in incoming buffer

  @Precondition
    new data in st->inBuffer
  @Parameters
    @param sc: pointer to SP container
    @param receivedBytes: length of the data last written to the in buffer
  @Returns

  @Remarks

 */
//// </editor-fold>
void SP_update(SP_Container *p_sc);

uint16_t SP_DummyWrite(uint8_t *c, uint32_t l);

void SP_SetWriteCallback(SP_Container *p_sc, SP_WriteFunc fn);

// <editor-fold defaultstate="collapsed" desc="SP_publish">
// *****************************************************************************
/**
  @Function
    int SP_publish(SP_Container *sc)

  @Summary
    Finalize and publish the output buffer

  @Description
    Finalize and publish the output buffer using the write function

  @Precondition

  @Parameters
    @param sc: pointer to SP container

  @Returns
    length of data written
    -1 if data is there but no streaming or triggered mode is on to send it out
  @Remarks

 */
//// </editor-fold>
int32_t SP_publish(SP_Container *p_sc);

// <editor-fold defaultstate="collapsed" desc="SP_unpackCommand">
// *****************************************************************************
/**
  @Function
    void SP_unpackCommand(SP_Container *sc)

  @Summary
    Decode the incoming commands and react accordingly

  @Description
    Decode the incoming commands and react accordingly
    unpacks additional command data

  @Precondition

  @Parameters
    @param sc: pointer to SP container

  @Returns

  @Remarks

 */
//// </editor-fold>
void SP_unpackCommand(SP_Container *p_sc);

void SP_setPeriod(SP_DeviceConfiguration *p_sdc, uint8_t ui8_senID, uint16_t ui16_period);
void SP_activateSensor(SP_DeviceConfiguration *p_sdc, uint8_t ui8_senID);
void SP_setAllSensorActivity(SP_DeviceConfiguration *p_sdc, bool b_val);
bool SP_extractStreamSel(SP_Container *p_sc);
bool SP_extractPeriods(SP_Container *p_sc);

// <editor-fold defaultstate="collapsed" desc="SP_packData">
// *****************************************************************************
/**
  @Function
    int SP_packData(SP_Container *sc, void *data, uint8_t sen_id)

  @Summary
    Pack the data in the output buffer with the stored data length

  @Description
    Pack the data in the output buffer (concatenate to existing data),
    finds the correct length of the data stored in the config for sen_id
    increments the internal timestamp for the given sen_id

  @Precondition

  @Parameters
    @param sc: pointer to SP container
    @param data: pointer to data
    @param sen_id: sensor id of the data

  @Returns
    length of current buffer use after data added
    -1 if buffer cannot store the data (too big to fit)
  @Remarks

 */
//// </editor-fold>
int32_t SP_packData(SP_Container *p_sc, void *data, uint8_t ui8_senID);

// <editor-fold defaultstate="collapsed" desc="SP_packData">
// *****************************************************************************
/**
  @Function
    int SP_packDataLen(SP_Container *sc, void *data, unsigned int len, uint8_t sen_id)

  @Summary
    Pack the data in the output buffer

  @Description
    Pack the data in the output buffer (concatenate to existing data), including incrementing the internal timestamp

  @Precondition

  @Parameters
    @param sc: pointer to SP container
    @param data: pointer to data
    @param len: length of data
    @param sen_id: sensor id of the data

  @Returns
    length of current buffer use after data added
    -1 if buffer cannot store the data (too big to fit)
  @Remarks

 */
//// </editor-fold>
int32_t SP_packDataLen(SP_Container *p_sc, void *data, uint32_t ui32_len, uint8_t ui8_senID);


// <editor-fold defaultstate="collapsed" desc="SP_packDataTs">
// *****************************************************************************
/**
  @Function
    int SP_packDataTs(SP_Container *sc, void *data, unsigned int len, uint8_t sen_id, uint32_t ts)

  @Summary
    Pack the data in the output buffer, with given timestamp

  @Description
    Pack the data in the output buffer (concatenate to existing data), add the given timestamp

  @Precondition

  @Parameters
    @param sc: pointer to SP container
    @param data: pointer to data
    @param len: length of data
    @param sen_id: sensor id of the data
    @param ts: timestamp (provided by the user)

  @Returns
    length of current buffer use after data added
    -1 if buffer cannot store the data (too big to fit)
  @Remarks

 */
//// </editor-fold>
int32_t SP_packDataTs(SP_Container *p_sc, void *data, uint32_t ui32_len, uint8_t ui8_senID, uint32_t ts);
bool SP_checksum(uint8_t *pui8_buf, uint32_t ui32_len);
uint8_t SP_computeChecksum(uint8_t *pui8_buf, uint32_t ui32_len);
void SP_checksumError(SP_Container *p_sc);
//void SP_hdrError(SP_Container *p_sc);
void SP_hdrError(SP_Container *p_sc, uint8_t hdr1, uint8_t hdr2);
void SP_invalidError(SP_Container *p_sc);
void SP_cmdError(SP_Container *p_sc);
// <editor-fold defaultstate="collapsed" desc="">
// *****************************************************************************
/**
  @Function


  @Summary
    Prepare the configuration buffer

  @Description
    using the buffer and the length, prepared the configuration buffer with checksum

  @Precondition

  @Parameters
    @param sc: pointer to SP container

  @Returns

  @Remarks

 */
//// </editor-fold>
void SP_textError(SP_Container *p_sc, char *pc_text, uint16_t ui16_text_len);

//small shift operations to combine/split bytes
#define Highbyte(x) (x>>8)
#define Lowbyte(x) (x&0xff)


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _CITECSerialProtocol_H */

/* *****************************************************************************
 End of File
 */
