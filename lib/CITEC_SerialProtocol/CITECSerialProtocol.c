/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITECSerialProtocol.c

  @Summary
    Implements functionality to do communication according to the serial protocol
    definition @ https://projects.cit-ec.uni-bielefeld.de/projects/agni-grasplab/wiki/Serial_protocol

  @Description
    For further details see the header file and link above.
 */
/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

#include "CITECSerialProtocol.h"
#include "CITECSerialProtocolTypes.h"
#include <string.h>
#include <stdint.h>

#ifndef SYS_ASSERT
#define SYS_ASSERT(test, message) if(test==false){SYS_MESSAGE(message); SYS_DEBUG_BreakPoint();}
#endif


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

void SP_init(SP_Container *p_sc)
{
    p_sc->data.ui8_inDid = 0x0;
    p_sc->data.u8_cmd = 0x0;
    p_sc->data.ui8_outDid = 0x0;

    p_sc->data.ui8a_confBuffer[0] = SP_HeaderFirstByte;
    p_sc->data.ui8a_confBuffer[1] = SP_HeaderSecondByte;
    p_sc->data.ui8a_confBuffer[2] = SP_DatagramIdMaster; // master
    p_sc->data.ui8a_confBuffer[3] = SP_ProtocolVersion; // protocol version
    p_sc->data.ui8a_confBuffer[4] = 0x00; // no sensor available
    p_sc->data.ui8a_confBuffer[5] = 0x35; // checksum
    p_sc->data.ui16_confLen = 6; // default

    p_sc->data.ui8a_pingBuffer[0] = SP_HeaderFirstByte;
    p_sc->data.ui8a_pingBuffer[1] = SP_HeaderSecondByte;
    p_sc->data.ui8a_pingBuffer[2] = SP_CmdPing; // ping
    p_sc->data.ui8a_pingBuffer[3] = 0xC4; // checksum

    p_sc->p_rawdata->ui8a_outBuffer[0] = 0;
    p_sc->p_rawdata->ui8a_outBuffer[1] = 0;
    p_sc->p_rawdata->ui16_inLen = 0;
    p_sc->p_rawdata->ui16_outLen = 0;

    p_sc->data.ui8a_errBuffer[0] = SP_HeaderFirstByte;
    p_sc->data.ui8a_errBuffer[1] = SP_HeaderSecondByte;
    p_sc->data.ui8a_errBuffer[2] = SP_DatagramIdError;

    p_sc->state.b_streaming=false;
    p_sc->state.b_triggering=false;
    p_sc->state.b_triggered=false;
    p_sc->data.b_hdrFound = false;
    p_sc->state.b_initialSerialDataReceived=false;
    p_sc->data.ui8_hdrOffset = 0;

    p_sc->fn_write = &SP_DummyWrite;
}

void SP_configInitFromConfig(SP_Container *p_sc)
{
    uint32_t ui32_len = (2 + p_sc->config.ui8_sensorCount * 4);
    uint8_t ui8a_config[ui32_len];
    ui8a_config[0] = p_sc->config.ui8_deviceTypeID;
    ui8a_config[1] = p_sc->config.ui8_sensorCount;
    // loop over all defined sensors and create a buffer to initialize SP
    uint8_t ui8_sensorID = 0;
    for(ui8_sensorID=0; ui8_sensorID < p_sc->config.ui8_sensorCount; ui8_sensorID++)
    {
        //config[2+sensor_id*4]=p_sc->config.sensorConfigs[sensor_id].sensorDatagramID;
        // little endian
        ui8a_config[2 + ui8_sensorID * 4] = p_sc->config.sensorConfigs[ui8_sensorID].ui8_sensorChipID;
        ui8a_config[2 + ui8_sensorID * 4 + 1] = p_sc->config.sensorConfigs[ui8_sensorID].ui8_sensorTypeID;
        ui8a_config[2 + ui8_sensorID * 4 + 2] = Lowbyte(p_sc->config.sensorConfigs[ui8_sensorID].ui16_dataLength);
        ui8a_config[2 + ui8_sensorID * 4 + 3] = Highbyte(p_sc->config.sensorConfigs[ui8_sensorID].ui16_dataLength);
    }
    SP_encodeConfig (p_sc, ui8a_config, ui32_len);
}


void SP_configInitFromBuffer(SP_Container *p_sc, uint8_t* conf, uint32_t ui32_len)
{
    SP_encodeConfig(p_sc, conf, ui32_len);
    SP_decodeConfig(p_sc);
}

void SP_decodeConfig(SP_Container *p_sc)
{
    uint8_t ui8_numSensors = p_sc->data.ui8a_confBuffer[SP_SerialDescOffset];
    if (p_sc->config.ui8_sensorCount != ui8_numSensors && ui8_numSensors != 0)
    {
        p_sc->config.ui8_sensorCount = ui8_numSensors;
    }
    SP_defaultPeriod(&p_sc->config);
}


void SP_encodeConfig(SP_Container *p_sc, uint8_t* pui8_conf, uint32_t ui32_len)
{
    if (ui32_len  < 256 + SP_SerialDescOffset )
    {
        memcpy(p_sc->data.ui8a_confBuffer + SP_DeviceIdOffset, pui8_conf, ui32_len);
        p_sc->data.ui8a_confBuffer[SP_DeviceIdOffset + ui32_len] = SP_computeChecksum(p_sc->data.ui8a_confBuffer, SP_DeviceIdOffset + ui32_len);
        p_sc->data.ui16_confLen = SP_DeviceIdOffset + ui32_len + SP_ChecksumLen;
    }
}


SP_SensorConfiguration SP_makeConfig(uint8_t ui8_typeID, uint8_t ui8_chipID, uint16_t ui16_dataLen, uint16_t ui16_period)
{
    SP_SensorConfiguration config;
    config.ui16_dataLength        =    ui16_dataLen;
    config.ui8_sensorChipID     =    ui8_chipID;
    config.ui8_sensorTypeID      =    ui8_typeID;
    config.ui16_sensorPeriod      =    ui16_period;
    return config;
}

int32_t SP_getPeriod(SP_DeviceConfiguration *p_sdc, uint8_t ui8_senID)
{
    if (ui8_senID > 0 && ui8_senID < p_sdc->ui8_sensorCount + 1)
    {
        if(p_sdc->sensorConfigs[ui8_senID - 1].b_sensorActive)
        {
            return (int32_t)p_sdc->sensorConfigs[ui8_senID - 1].ui16_sensorPeriod;
        }
        else
            return SP_PeriodInactive;
    }
    return SP_PeriodUnknownId;
}

uint16_t SP_getDataLen(SP_DeviceConfiguration *p_sdc, uint8_t ui8_senID)
{
    if (ui8_senID > 0 && ui8_senID < p_sdc->ui8_sensorCount + 1)
    {
        return p_sdc->sensorConfigs[ui8_senID - 1].ui16_dataLength;
    }
    else
    {
        return 0;
    }
}


// return -1 if not found, 0 if found, 1 if found at offset by one
//int SP_findHeader(SP_Data *sd)
// TODO improve the searching to further and re-align the buffer
int16_t SP_findHeader(uint8_t* pui8_buf)
{
    if (pui8_buf[0] == SP_HeaderFirstByte)
    {
        if (pui8_buf[1] == SP_HeaderSecondByte)
        {
          return 0;
        }
        else
        {
            return -2;
        }
    }
    else
    {
      if (pui8_buf[1] == SP_HeaderFirstByte) // shifted by one ?
      {
        // check at least one more
        if (pui8_buf[2] == SP_HeaderSecondByte)
        {
            // shift the data by one
            return 1;
        }
        else
            return -3;
      }
      else
      {
          return -4;
      }
    }
}

void SP_read(SP_RawData *sd, uint8_t *pui8_data, uint32_t ui32_len)
{
    if (ui32_len < sizeof(sd->ui8a_inBuffer))
    {
        memcpy(sd->ui8a_inBuffer, pui8_data, ui32_len);
        sd->ui16_inLen = ui32_len;
    }
}

void SP_update(SP_Container *p_sc)
{
    // check for requests
    if (p_sc->p_rawdata->ui16_inLen >= SP_HeaderLen)
    {
        int16_t i16_hdrPos = SP_findHeader(p_sc->p_rawdata->ui8a_inBuffer);
        if (i16_hdrPos >= 0)
        {
            p_sc->data.ui8_hdrOffset = (uint8_t)i16_hdrPos;
            p_sc->data.b_hdrFound = true;
            // read additional 3 bytes if exist
            if (p_sc->p_rawdata->ui16_inLen >= p_sc->data.ui8_hdrOffset + SP_HeaderLen + SP_DatagramIdLen + SP_CommandLen + SP_ChecksumLen)
            {
                // decode request
                p_sc->data.ui8_inDid = (uint8_t)p_sc->p_rawdata->ui8a_inBuffer[p_sc->data.ui8_hdrOffset + SP_DatagramIdOffset];
                p_sc->data.u8_cmd = p_sc->p_rawdata->ui8a_inBuffer[p_sc->data.ui8_hdrOffset + SP_CommandOffset];
                if (p_sc->data.ui8_inDid == SP_DatagramIdMaster || p_sc->data.ui8_inDid == SP_DatagramIdAll) {
                     // request to master/all
                    SP_unpackCommand(p_sc);
                }
                else {
                    // request for a specific sub-device
                }
            }
            else {
                SP_cmdError(p_sc);
            }
        }
        else {
            //SP_hdrError(p_sc,p_sc->data.inBuffer[0],p_sc->data.inBuffer[1]);
            SP_hdrError(p_sc,p_sc->p_rawdata->ui8a_inBuffer[0],p_sc->p_rawdata->ui8a_inBuffer[1]);
        }
    }
}

// Dummy function not writing anything
uint16_t SP_DummyWrite(uint8_t *c, uint32_t l)
{
    return 0;
}

void SP_SetWriteCallback(SP_Container *p_sc, SP_WriteFunc fn)
{
    p_sc->fn_write = fn;
}

int32_t SP_publish(SP_Container *p_sc)
{
    int32_t written = 0;
    if(p_sc->p_rawdata->ui16_outLen > 0)
    {
        // send out
        if (p_sc->state.b_streaming || (p_sc->state.b_triggering && p_sc->state.b_triggered) )
        {
            written = (int32_t)p_sc->fn_write(p_sc->p_rawdata->ui8a_outBuffer, p_sc->p_rawdata->ui16_outLen);
            // mark output buffer as empty
            p_sc->p_rawdata->ui16_outLen = 0;

            if(p_sc->state.b_triggering && p_sc->state.b_triggered) {
                p_sc->state.b_triggered = false;
            }
        }
        else
            return -1;
    }
    return written;
}

void SP_unpackCommand(SP_Container *p_sc)
{
  if (p_sc->p_rawdata->ui16_inLen > p_sc->data.ui8_hdrOffset+SP_CommandOffset)
  {
    // do the checksum within each command according to existence of additional command bytes coming or not.
    //unsigned char cmd = (unsigned char)p_sc->data.inBuffer[p_sc->data.hdr_offset+SP_HeaderLen];
    switch(p_sc->data.u8_cmd)
    {
        case SP_CmdStopStreaming:
            if (SP_checksum(p_sc->p_rawdata->ui8a_inBuffer+p_sc->data.ui8_hdrOffset, 5))
            {
                p_sc->state.b_initialSerialDataReceived=true;
                SP_setAllSensorActivity(&p_sc->config, false);
                p_sc->state.b_streaming = false;
                p_sc->state.b_triggering = false;
            }
            else
                SP_checksumError(p_sc);
            break;
        case SP_CmdStartStreamingContinousAll:
            if (SP_checksum(p_sc->p_rawdata->ui8a_inBuffer+p_sc->data.ui8_hdrOffset, 5))
            {
                p_sc->state.b_initialSerialDataReceived=true;
                SP_setAllSensorActivity(&p_sc->config, true);
                p_sc->state.b_streaming = true;
                p_sc->state.b_triggering = false;
            }
            else
                SP_checksumError(p_sc);
            break;
        case SP_CmdStartStreamingContinousSelect:
            if(SP_extractStreamSel(p_sc))
            {
                p_sc->state.b_streaming = true;
                p_sc->state.b_triggering = false;
            }
            break;
        case SP_CmdTriggerAll:
            if (SP_checksum(p_sc->p_rawdata->ui8a_inBuffer+p_sc->data.ui8_hdrOffset, 5))
            {
                p_sc->state.b_initialSerialDataReceived=true;
                SP_setAllSensorActivity(&p_sc->config, true);
                p_sc->state.b_streaming = false;
                p_sc->state.b_triggering = true;
            }
            else
                SP_checksumError(p_sc);
            break;
        case SP_CmdPing:
            if (SP_checksum(p_sc->p_rawdata->ui8a_inBuffer+p_sc->data.ui8_hdrOffset, 5))
            {
                p_sc->state.b_initialSerialDataReceived=true;
                // send the ping buffer
                p_sc->fn_write(p_sc->data.ui8a_pingBuffer, SP_PingLen);
            }
            else
                SP_checksumError(p_sc);
            break;
        case SP_CmdSetSensorPeriodicity:
            // there is more data to read
            SP_extractPeriods(p_sc);
            p_sc->state.b_periodChanged = true;
            break;
        case SP_CmdRequestConfig:
            if (SP_checksum(p_sc->p_rawdata->ui8a_inBuffer+p_sc->data.ui8_hdrOffset, 5))
            {
                p_sc->state.b_initialSerialDataReceived=true;
                // send the configuration buffer
                p_sc->fn_write(p_sc->data.ui8a_confBuffer, p_sc->data.ui16_confLen);
            }
            else
                SP_checksumError(p_sc);
            break;
        default:
            SP_cmdError(p_sc);
            break;
    }
    // mark input buffer as read;
    p_sc->p_rawdata->ui16_inLen = 0;
  }
}

void SP_defaultPeriod(SP_DeviceConfiguration *p_sdc)
{
    // set default update rate of 10 ms for each sensor
    uint8_t i;
    for (i=0; i < p_sdc->ui8_sensorCount; i++)
    {
        p_sdc->sensorConfigs[i].ui16_sensorPeriod = 100; //  0.1 ms/unit
        p_sdc->sensorConfigs[i].b_sensorActive = false;
    }
}


// period in multiple of 0.1 ms
void SP_setPeriod(SP_DeviceConfiguration *p_sdc, uint8_t ui8_senID, uint16_t ui16_period)
{
    if (ui8_senID > 0 && ui8_senID < p_sdc->ui8_sensorCount + 1)
    {
        p_sdc->sensorConfigs[ui8_senID - 1].ui16_sensorPeriod = ui16_period;
    }
}

void SP_activateSensor(SP_DeviceConfiguration *p_sdc, uint8_t ui8_senID)
{
    if (ui8_senID > 0 && ui8_senID < p_sdc->ui8_sensorCount + 1)
    {
        p_sdc->sensorConfigs[ui8_senID - 1].b_sensorActive = true;
    }
}

void SP_setAllSensorActivity(SP_DeviceConfiguration *p_sdc, bool b_val)
{
    uint8_t i;
    for (i=0; i < p_sdc->ui8_sensorCount; i++)
    {
        p_sdc->sensorConfigs[i].b_sensorActive = b_val;
    }
}

bool SP_extractStreamSel(SP_Container *p_sc)
{
    /*unsigned int ret = Serial.readBytes(inBuffer+SP_CMD_DATA_SZ_OFFSET, 2);
    if (ret != 2)
    {
        invalid_error(st);
        return false;
    }*/
    uint8_t ui8_selLen = p_sc->p_rawdata->ui8a_inBuffer[SP_CommandDataSizeOffset+p_sc->data.ui8_hdrOffset] + p_sc->p_rawdata->ui8a_inBuffer[SP_CommandDataSizeOffset+1+p_sc->data.ui8_hdrOffset]*256;
    if (ui8_selLen > 0)
    {
        // read additional data
        /*ret = Serial.readBytes(inBuffer+SP_CMD_DATA_OFFSET, sel_len+1);
        if (ret != sel_len+1)
        {
          invalid_error();
          return false;
        }*/
        // buffer is complete now
        if (!SP_checksum(p_sc->p_rawdata->ui8a_inBuffer + p_sc->data.ui8_hdrOffset, SP_CommandDataSizeOffset + ui8_selLen + SP_ChecksumLen))
        {
            SP_checksumError(p_sc);
            return false;
        }
        p_sc->state.b_initialSerialDataReceived=true;
        // extract and set active sensors
        uint8_t i;
        for (i = 0; i < ui8_selLen; i++)
        {
            uint8_t ui8_did = p_sc->p_rawdata->ui8a_inBuffer[SP_CommandDataOffset + i + p_sc->data.ui8_hdrOffset];
            SP_activateSensor(&p_sc->config, ui8_did);
        }
    }
    else
    {
        // read pending checksum
        //ret = Serial.readBytes(inBuffer+SP_CMD_DATA_OFFSET,1);
        if (!SP_checksum(p_sc->p_rawdata->ui8a_inBuffer + p_sc->data.ui8_hdrOffset, SP_CommandDataOffset+SP_ChecksumLen))
        {
          SP_checksumError(p_sc);
          return false;
        }
    }
    return true;
}

bool SP_extractPeriods(SP_Container *p_sc)
{
    if(p_sc->p_rawdata->ui16_inLen < p_sc->data.ui8_hdrOffset + SP_CommandDataSizeOffset + SP_CommandDataSizeLen)
    {
        SP_invalidError(p_sc);
        return false;
    }
    uint16_t ui16_periodLen = p_sc->p_rawdata->ui8a_inBuffer[SP_CommandDataSizeOffset + p_sc->data.ui8_hdrOffset] + p_sc->p_rawdata->ui8a_inBuffer[SP_CommandDataSizeOffset + 1 + p_sc->data.ui8_hdrOffset]*256;
    if (ui16_periodLen > 0)
    {
        // read additional data
        if(p_sc->p_rawdata->ui16_inLen < p_sc->data.ui8_hdrOffset + SP_CommandDataOffset + ui16_periodLen * SP_PeriodDataLen + SP_ChecksumLen)
        {
            SP_invalidError(p_sc);
            return false;
        }
        // buffer is complete now
        if (!SP_checksum(p_sc->p_rawdata->ui8a_inBuffer+p_sc->data.ui8_hdrOffset, SP_CommandDataOffset+ui16_periodLen*SP_PeriodDataLen + SP_ChecksumLen))
        {
            SP_checksumError(p_sc);
            return false;
        }
        p_sc->state.b_initialSerialDataReceived=true;
        // extract and set periods
        uint8_t i;
        for (i = 0; i < ui16_periodLen; i++)
        {
            uint8_t ui8_did= p_sc->p_rawdata->ui8a_inBuffer[SP_CommandDataOffset + i*SP_PeriodDataLen + p_sc->data.ui8_hdrOffset];
            uint16_t ui16_period= p_sc->p_rawdata->ui8a_inBuffer[SP_CommandDataOffset + i*SP_PeriodDataLen + 1 + p_sc->data.ui8_hdrOffset] + 256*p_sc->p_rawdata->ui8a_inBuffer[SP_CommandDataOffset + i*SP_PeriodDataLen + 2 + p_sc->data.ui8_hdrOffset];
            SP_setPeriod(&p_sc->config, ui8_did, ui16_period);
        }
    }
    else
    {
        // read pending checksum
        //ret = Serial.readBytes(inBuffer+SP_CMD_DATA_OFFSET,1);
        if (!SP_checksum(p_sc->p_rawdata->ui8a_inBuffer+p_sc->data.ui8_hdrOffset, SP_CommandDataOffset + SP_ChecksumLen))
        {
            SP_checksumError(p_sc);
            return false;
        }
    }
    return true;
}

int32_t SP_packData(SP_Container *p_sc, void *data, uint8_t ui8_senID)
{
    uint16_t ui16_len = SP_getDataLen(&p_sc->config, ui8_senID);
    if (ui16_len > 0)
        return SP_packDataLen(p_sc , data, ui16_len, ui8_senID);
    else
        return -1;
}

int32_t SP_packDataLen(SP_Container *p_sc, void *data, uint32_t ui32_len, uint8_t ui8_senID)
{
    // concatenate data in output buffer as long as possible
    if(p_sc->p_rawdata->ui16_outLen + SP_DataOffset + SP_TimeStampLen + ui32_len + SP_ChecksumLen < SP_MAX_DATA_SIZE)
    {
        p_sc->config.sensorConfigs[ui8_senID].ui32_timestamp++;
        return SP_packDataTs(p_sc, data, ui32_len, ui8_senID, p_sc->config.sensorConfigs[ui8_senID].ui32_timestamp);
    }
    else
    {
        return -1;
    }
}

int32_t SP_packDataTs(SP_Container *p_sc, void *data, uint32_t ui32_len, uint8_t sen_id, uint32_t ui32_ts)
{
    // concatenate data in output buffer as long as possible
    if(p_sc->p_rawdata->ui16_outLen + SP_DataOffset + SP_TimeStampLen + ui32_len + SP_ChecksumLen < SP_MAX_DATA_SIZE)
    {
        uint16_t ui16_outputOffset = p_sc->p_rawdata->ui16_outLen;
        uint16_t ui16_outputLen = SP_DataOffset + SP_TimeStampLen + ui32_len + SP_ChecksumLen;
        p_sc->p_rawdata->ui16_outLen += ui16_outputLen;
        p_sc->p_rawdata->ui8a_outBuffer[ui16_outputOffset] = SP_HeaderFirstByte;
        p_sc->p_rawdata->ui8a_outBuffer[ui16_outputOffset + 1] = SP_HeaderSecondByte;
        p_sc->p_rawdata->ui8a_outBuffer[ui16_outputOffset + 2] = sen_id;
        memcpy(p_sc->p_rawdata->ui8a_outBuffer + ui16_outputOffset + SP_DataOffset, (uint8_t*)&ui32_ts, sizeof(ui32_ts));
        memcpy(p_sc->p_rawdata->ui8a_outBuffer + ui16_outputOffset + SP_DataOffset + SP_TimeStampLen, (uint8_t*)data, ui32_len);
        p_sc->p_rawdata->ui8a_outBuffer[ui16_outputOffset + SP_DataOffset + SP_TimeStampLen + ui32_len] = SP_computeChecksum(p_sc->p_rawdata->ui8a_outBuffer + ui16_outputOffset, SP_DataOffset + SP_TimeStampLen + ui32_len);
        return p_sc->p_rawdata->ui16_outLen;
    }
    else
    {
        return -1;
    }
}

bool SP_checksum(uint8_t *pui8_buf, uint32_t ui32_len)
{
    return SP_computeChecksum(pui8_buf, ui32_len-1) == (uint8_t)pui8_buf[ui32_len-1];
}

uint8_t SP_computeChecksum(uint8_t *buf, uint32_t ui32_len)
{
    uint8_t checksum=0;
    uint32_t i;
    for (i=0; i<ui32_len; i++)
    {
        checksum ^= buf[i];
    }
    return checksum;
}


void SP_checksumError(SP_Container *p_sc)
{
    p_sc->data.ui8a_errBuffer[3] = SP_ErrCheckusm;
    p_sc->data.ui8a_errBuffer[4] = SP_computeChecksum(p_sc->data.ui8a_errBuffer, SP_ErrLen);
    p_sc->fn_write(p_sc->data.ui8a_errBuffer, SP_ErrLen+SP_ChecksumLen);
}

void SP_hdrError(SP_Container *p_sc, uint8_t hdr1, uint8_t hdr2)
{
    p_sc->data.ui8a_errBuffer[3] = 15;
    uint8_t msg[15] = {"header error:  "};
    msg[13] = hdr1;
    msg[14] = hdr2;
    memcpy(p_sc->data.ui8a_errBuffer + SP_ErrTextOffset, msg, 15);
    p_sc->data.ui8a_errBuffer[SP_ErrTextOffset+15] = SP_computeChecksum(p_sc->data.ui8a_errBuffer, SP_ErrTextOffset+15);
    p_sc->fn_write(p_sc->data.ui8a_errBuffer, SP_ErrTextOffset+15+SP_ChecksumLen);
}

void SP_invalidError(SP_Container *p_sc)
{
    p_sc->data.ui8a_errBuffer[3] = 12;
    uint8_t msg[12] = {"invalid data"};
    memcpy(p_sc->data.ui8a_errBuffer + SP_ErrTextOffset, msg, 12);
    p_sc->data.ui8a_errBuffer[SP_ErrTextOffset+12] = SP_computeChecksum(p_sc->data.ui8a_errBuffer, SP_ErrTextOffset+12);
    p_sc->fn_write(p_sc->data.ui8a_errBuffer, SP_ErrTextOffset+12+SP_ChecksumLen);
}

void SP_cmdError(SP_Container *p_sc)
{
    p_sc->data.ui8a_errBuffer[3] = SP_ErrUnrecognizedCommand;
    p_sc->data.ui8a_errBuffer[4] = SP_computeChecksum(p_sc->data.ui8a_errBuffer, SP_ErrLen);
    p_sc->fn_write(p_sc->data.ui8a_errBuffer, SP_ErrLen+SP_ChecksumLen);
}

void SP_textError(SP_Container *p_sc, char *pc_text, uint16_t ui16_text_len)
{
    uint16_t ui16_len=ui16_text_len;
    if (ui16_len > SP_ErrMaxStringLen)
    {
        ui16_len = SP_ErrMaxStringLen;
    }
    p_sc->data.ui8a_errBuffer[3] = ui16_len;
    memcpy(p_sc->data.ui8a_errBuffer + SP_ErrTextOffset, pc_text, ui16_len);
    p_sc->data.ui8a_errBuffer[SP_ErrTextOffset + ui16_len] = SP_computeChecksum(p_sc->data.ui8a_errBuffer, SP_ErrTextOffset + ui16_len);
    p_sc->fn_write(p_sc->data.ui8a_errBuffer, SP_ErrTextOffset + ui16_len + SP_ChecksumLen);
}

/* *****************************************************************************
 End of File
 */
