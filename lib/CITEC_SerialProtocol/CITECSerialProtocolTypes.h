/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITECSerialProtocolTypes.h

  @Summary
    Declares the serial protocol main storage structure

  @Description
    Declares the serial protocol main storage structure
 */
/* ************************************************************************** */

#ifndef _CITECSERIALPROTOCOLTYPES_H    /* Guard against multiple inclusion */
#define _CITECSERIALPROTOCOLTYPES_H

#include "CITECSerialProtocolDefines.h"

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Defined                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  Various Variables that can be changed to adapt to new Protocol Versions
 *  or greater Data amounts.
 */

#define SP_MAX_DATA_SIZE            1000      //Max Data in in/out buffer
#define SP_MAX_CONF_SIZE            100      //Max Confsize
#define SP_MAX_NUM_SENSOR              25
#define SP_SERIAL_RECEIVE_QUEUE_SIZE 1        //for later change, right now a higher number is not supported!

//Additional Error Defines used intern only, not to be changed
//#define SP_SendErrorString         0x55    //For SendError Function to decrypt


// *****************************************************************************
// *****************************************************************************
// Section: Data Types
// *****************************************************************************
// *****************************************************************************

/*  Special Data types used to run the Serial Protocol
 */


   typedef uint16_t (*SP_WriteFunc)(uint8_t *, uint32_t);

// <editor-fold defaultstate="collapsed" desc="Struct description">
/** SP_State

  @Summary
    Structure containing the actual SerialProtocol state and input/output buffers
    whose pointer is passed around

  @Description
    The library mainly interacts with a single structure in memory, according to variable passed to it
    Helper functions permit to read/write to this structure

  @Remarks
    None.
 */
//// </editor-fold>


typedef struct
{
    uint8_t                 ui8_sensorDatagramID;   // Place of this Sensor in the streaming Datagram
    uint8_t                 ui8_sensorTypeID;       // Sensor Type identifier
    uint8_t                 ui8_sensorChipID;       // Sensor Chip identifier.
    uint16_t                ui16_dataLength;        // Number of bytes  of data the Sensor generates
    uint32_t                ui32_timestamp;         // Timestamp when the data was acquired
    bool                    b_newData;              // Not used
    uint16_t                ui16_sensorPeriod;      // Delay time between two Data Packages
    bool                    b_sensorActive;       //active status
} SP_SensorConfiguration;

typedef struct
{
    uint8_t ui8_deviceTypeID;                     //Defines Device type
    uint8_t ui8_sensorCount; // number of sensors in the current configuration
    SP_SensorConfiguration sensorConfigs[SP_MAX_NUM_SENSOR];   //Sensor configurations //TODO: This can be lowerd to Sensor Count
} SP_DeviceConfiguration;

typedef struct
{
    bool     b_hdrFound;  // if the header was found in the data (useful at start to flush previous data)
    uint8_t  ui8_hdrOffset;  // offset of the header (max 1)
    uint8_t  ui8_inDid; // current targeted sensor id
    uint8_t  u8_cmd; // current command
    uint8_t  ui8_outDid; // sensor id of the response
    uint16_t ui16_confLen;  // current configuration length
    uint8_t  ui8a_confBuffer[SP_SerialDescOffset + SP_MAX_CONF_SIZE + SP_ChecksumLen];   // configuration buffer
    uint8_t  ui8a_pingBuffer[SP_PingLen];   // ping buffer
    uint8_t  ui8a_errBuffer[SP_CommandOffset + SP_ErrMaxStringLen + SP_ChecksumLen];   // error buffer
} SP_Data;

typedef struct
{
    uint16_t    ui16_inLen; // last input length
    uint16_t    ui16_outLen; // current output length
    uint8_t     ui8a_inBuffer[SP_CommandDataOffset + SP_MAX_DATA_SIZE + SP_ChecksumLen];   // receive buffer
    uint8_t     ui8a_outBuffer[SP_DataOffset + SP_MAX_DATA_SIZE + SP_ChecksumLen];   // response/send buffer

} SP_RawData;


typedef struct
{
    bool     b_streaming;  // is streaming mode on
    bool     b_triggering;  // is trigger mode on
    bool     b_triggered;  // was it last triggered
    bool     b_initialSerialDataReceived;  // useful to know that a device want serial protocol to occur
    bool     b_periodChanged; // true if last command was a period change request
}SP_State;

typedef struct
{
    SP_WriteFunc            fn_write;
    SP_State                state;
    SP_DeviceConfiguration  config;
    bool                    ba_sensorActive[SP_MAX_NUM_SENSOR];  // vector of active state of the sensor
    SP_Data                 data;
    SP_RawData              *p_rawdata; // pointer to raw data (must be initialized externally)
} SP_Container;




//Sensor Type ID
enum SensorTypeIDEnum
{
    Undefined                       =   0x00,
    TactileSensor                   =   0x01,
    TactileSensorResistive          =   0x02,
    TactileSensorCapacitve          =   0x03,
    TactileSensorPiezo              =   0x04,
    TactileSensorPressure           =   0x05,
    TemperatureSensor               =   0x80,
    TemperaturArray                 =   0x88,
    MagneticSensor                  =   0x90,
    MagneticSensorHallEffect        =   0x91,
    Magnetometer                    =   0x98,
    PressureSensor                  =   0xA0,
    PressureSensorBarometer         =   0xA1,
    Humidity                        =   0xA4,
    LightIntensitySensor            =   0xB0,
    RangeSensor                     =   0xC0,
    RangeSensorUltrasound           =   0xC1,
    RangeSensorOptical              =   0xC2,
    RangeSensorToF                  =   0xC3,
    RangeSensor1DLidar              =   0xC4,
    RangeSensor2DLidar              =   0xC5,
    PositionSensor                  =   0xD0,
    PositionSensorLinear            =   0xD1,
    PositionSensorAngular           =   0xD2,
    VelocitySensor                  =   0xD4,
    VelocitySensorLinear            =   0xD5,
    VelocitySensorAngular           =   0xD6,
    Acceleration                    =   0xD8,
    AccelerationLinear              =   0xD9,
    AccelerationAngular             =   0xDA,
    IMU                             =   0xDC,
    VoltageSensor                   =   0xE0,
    CurrentSensor                   =   0xE2,
    ResistanceSensor                =   0xE4,
    ChargeSensor                    =   0xE6,
    ForceSensor                     =   0xF0,
    TorqueSensor                    =   0xF1,
    DOF6_ForceTorqueSensor          =   0xF6,
    StrainGaugeSingleAxis           =   0xFA,
    StrainGaugeDualAxis             =   0xFB,
    StrainGaugeTripleAxis           =   0xFC,
    StrainGaugeQuadAxis             =   0xFD,
};


//Device Type ID
enum DeviceTypeIDEnum
{
    Dummy                   =   0x00,
    TactileModule_v2        =   0x10,
    iObjectPlus             =   0x18,
    TactileBraceletv2       =   0x20,
    TactileGlove            =   0x28,
    PokingStick             =   0x30,
    ShadowPalmSensors       =   0x38,
    ActiveNail              =   0x40,
};

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _CITECSERIALPROTOCOLTYPES_H */

/* *****************************************************************************
 End of File
 */
