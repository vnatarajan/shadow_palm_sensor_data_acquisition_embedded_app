/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITECSerialProtocolDefines.h

  @Summary
    This header defines all Constants for the Serial Protocol.
    This File is an implementation of the specifications of the Protocol:
    https://projects.cit-ec.uni-bielefeld.de/projects/agni-grasplab/wiki/Serial_protocol

 */
/* ************************************************************************** */

#ifndef _CITECSerialProtocolDefines_H    /* Guard against multiple inclusion */
#define _CITECSerialProtocolDefines_H

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

#define SP_ProtocolVersion    0x01

//Basic Protocol Constants and Offsets
#define SP_HeaderFirstByte       0xF0
#define SP_HeaderSecondByte      0xC4

#define SP_HeaderLen             2
#define SP_DatagramIdOffset     (SP_HeaderLen)
#define SP_DatagramIdLen           1
#define SP_TimeStampLen          0x4
#define SP_DataOffset        (SP_DatagramIdOffset+SP_DatagramIdLen)
#define SP_VersionOffset     (SP_DatagramIdOffset+SP_DatagramIdLen)
#define SP_VersionLen           1
#define SP_DeviceIdOffset  (SP_VersionOffset+SP_VersionLen)
#define SP_DeviceIdLen           1
#define SP_SerialDescOffset (SP_DeviceIdOffset+SP_DeviceIdLen)
#define SP_ChecksumLen          1
#define SP_CommandOffset        (SP_DatagramIdOffset+SP_DatagramIdLen)
#define SP_CommandLen               1
#define SP_CommandDataSizeOffset (SP_CommandOffset+SP_CommandLen)
#define SP_CommandDataSizeLen       2
#define SP_CommandDataOffset (SP_CommandDataSizeOffset+SP_CommandDataSizeLen)
#define SP_PingLen          (SP_CommandOffset + SP_ChecksumLen)
#define SP_PeriodDataLen   3
#define SP_ErrLen          4
#define SP_ErrTextOffset   4

// Common DatagramID
#define SP_DatagramIdAll       0xFF
#define SP_DatagramIdMaster    0x00
#define SP_DatagramIdSensor1   0x01
#define SP_DatagramIdError     0xFE

// Command Types
#define SP_CmdStopStreaming                   0x00
#define SP_CmdRequestConfig                   0xC0
#define SP_CmdRequestTopology                 0xC1
#define SP_CmdRequestSerial                   0xC1
#define SP_CmdSetSensorPeriodicity            0xD0
#define SP_CmdRomRead1                        0xE0
#define SP_CmdRomRead2                        0xE1
#define SP_CmdRomWrite2                       0xEE
#define SP_CmdRomWrite1                       0xEF
#define SP_CmdPing                            0xF0
#define SP_CmdStartStreamingContinousAll      0xF1
#define SP_CmdStartStreamingContinousSelect   0xF2
#define SP_CmdTriggerAll                      0xF3

#define SP_PeriodInactive  -1
#define SP_PeriodUnknownId -2


//Error ID
#define SP_ErrUnknown              0x00
#define SP_ErrCheckusm             0xF0
#define SP_ErrUnrecognizedCommand  0xF1
#define SP_ErrUnrecognizedSensorID 0xF2

#define SP_ErrMaxStringLen        0xEF

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _CITECSerialProtocolDefines_H */

/* *****************************************************************************
 End of File
 */
