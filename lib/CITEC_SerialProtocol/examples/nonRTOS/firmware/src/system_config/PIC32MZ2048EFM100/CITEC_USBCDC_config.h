/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    _CITEC_USBCDC_config_H

  @Summary
    This configuration header hold all defines for the app
    a certain degree of security.
 */
/* ************************************************************************** */

#ifndef _CITEC_USBCDC_config_H    /* Guard against multiple inclusion */
#define _CITEC_USBCDC_config_H

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

// Section: Free RTOS Task Priorities
#define  USBDEVICETASK_PRIO                             1u
#define  USBCDCDEVICETASK_PRIO                          3u
// Section: Free RTOS Task Stack Sizes
#define  USBDEVICETASK_SIZE                             256u

#define USBCDCDeviceTask_Size                           512u   //needs to have aproximately the size of "USBCDCStringSizeMax"

// *****************************************************************************
// *****************************************************************************
// Section: Configuration specific application constants
// *****************************************************************************
// *****************************************************************************

#define USBCDC_BUFFER_SIZE   512  //max number of bytes to be send/received at once (within one tick)
#define USBCDC_BUFFER_SIZE_SENDNOW 450 // size of data considered to send now, as further data might not fit
#define USBCDC_SEND_TIMEOUT      100 // 1000 ms for automatic write buffer flushing
#ifdef USE_RTOS
#define USBCDC_WRITE_TIMEOUT      100 // 100 ms for automatic write complete trigger
#endif

//define the number of elements to be held by the queue to hold received CDC data 
#define USBCDC_RECEIVEQUEUE_SIZE  5
#define USBCDC_SENDQUEUE_SIZE  5

#define USBCDCPort         USB_DEVICE_CDC_INDEX_0
#define USBCDCBaud         115200        //Baud rate of the Serial Port
#define USBCDCDataBits          8        //number of bits per UART word
                                        //5, 6, 7, 8, 16
#define USBCDCParity            0       //Parity Type
                                        //0=None, 1=odd, 2=even, 3=Mark, 4=Space
#define USBCDCStopBits          0       //Number of stop bits:
                                        //0=1, 1=1.5, 2=2 bits per character



#include "system/debug/sys_debug.h"
// if issues with eth_HashTable_Default. deactivate this sys_assert
//#ifdef SYS_ASSERT
//#undef SYS_ASSERT
//#endif
//#define SYS_ASSERT(test, message) if(test==false){SYS_MESSAGE(message); SYS_DEBUG_BreakPoint();}


#endif /* _CITEC_USBCDC_config_H */

/* *****************************************************************************
 End of File
 */
