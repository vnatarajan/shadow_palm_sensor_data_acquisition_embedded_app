/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>

#include "app.h"
#include "CITECSerialProtocolTask.h"
#include "CITECSerialProtocol.h"

#define TIMEOUT_PERIOD(duration_ms) ((SYS_CLK_FREQ / 2/1000) * duration_ms) // in milliseconds ( MAX 42000))

extern SP_Container spContainer;

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************


bool b_runningSensor1; // data task control
bool b_runningSensor2; // data task control
static uint32_t ui32_timeStartSensor1, ui32_timeStartSensor2, TimeUsed;  // periodicity control
static uint16_t ui16_periodSensor1, ui16_periodSensor2;  //period for each task
char sensor0[120], sensor1[26]; // mock arrays for sensor data
uint16_t cnt; // data generator
bool b_welcomeTaskRunning; // welcome task handling

/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/*******************************************************************************
  Function:
    void APP_RunRequest1/2 ( bool b_running )

  Summary:
    Callback to set the streaming state of sensor 1/2
 */
void APP_RunRequest1(bool b_running)
{
    b_runningSensor1=b_running;
}
void APP_RunRequest2(bool b_running)
{
    b_runningSensor2=b_running;
}

/*******************************************************************************
  Function:
    void APP_SetPeriod1/2(uint16_t ui16_period100us)

  Summary:
    Callback to set the period of publication of sensor 1/2
  
  Remark : input is in 100us units as defined by the SerialProtocol
 */
void APP_SetPeriod1(uint16_t ui16_period100us)
{
    if (ui16_period100us > 10)
        ui16_periodSensor1 = ui16_period100us/10;
    else
        ui16_periodSensor1 = 10;
}

void APP_SetPeriod2(uint16_t ui16_period100us)
{
    if (ui16_period100us > 10)
        ui16_periodSensor2 = ui16_period100us/10;
    else
        ui16_periodSensor2 = 10;
}


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */
void APP_Initialize ( void )
{
    cnt = 0;
    b_welcomeTaskRunning = false;
    uint8_t i=0;
    for (i=0; i < 120 ; i++)
    {
        if (i < 26)
            sensor1[i] = 0;
        sensor0[i] = 0;
    }
    
    /* Initializes the USB CDC */
    CITEC_USBCDC_Initialize();

    ui32_timeStartSensor1 = _CP0_GET_COUNT();
    ui32_timeStartSensor1 = _CP0_GET_COUNT();
}

/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
void APP_Tasks ( void )
{
    // call the SerialProtocol handler (to get the latest command status)
    CITEC_SerialProtocol_TaskHandler(&b_welcomeTaskRunning);
        
    // simulate Sensor Data Updates
    if(cdcData.b_isConfigured)
    {
        // if data tasks are running / streaming
        if(b_runningSensor1 || b_runningSensor2)
        {
            if (b_runningSensor1)
            {
                // handle periodicity initialization
                if(ui16_periodSensor1 == 0) //un-initialized
                {
                    int32_t ui32_period = SP_getPeriod(&spContainer.config,1);
                    if (ui32_period > 0)
                        APP_SetPeriod1((uint16_t)ui32_period);
                    else
                        APP_SetPeriod1(1000); // 100 ms
                }
                // if period of publishing was reached
                TimeUsed = _CP0_GET_COUNT() - ui32_timeStartSensor1;
                if(TimeUsed > TIMEOUT_PERIOD(ui16_periodSensor1))
                {
                    // generate some changing "Sensor" Values
                    sensor0[cnt%120]=cnt++;
                    // do Sensor 0 (Sensor ID 1)
                    // pack the data
                    SP_packData(&spContainer, (void*)sensor0, 1); //ID 1
                    // send the data
                    // TODO handle errors
                    SP_publish(&spContainer);
                    // handle timing
                    ui32_timeStartSensor1 = _CP0_GET_COUNT();
                }
            }

            if(b_runningSensor2)
            {
                if(cnt%2)//update sensor 1 only half as often as sensor 0
                {
                    // generate some changing "Sensor" Values
                    sensor1[(cnt%26)]=(cnt*2);
                }
                // handle periodicity initialization
                if(ui16_periodSensor2 == 0) //un-initialized
                {
                    int32_t ui32_period = SP_getPeriod(&spContainer.config,2);
                    if (ui32_period > 0)
                        APP_SetPeriod2((uint16_t)ui32_period);
                    else
                        APP_SetPeriod2(10000); // 1 s
                }
                // if period of publishing was reached
                TimeUsed = _CP0_GET_COUNT() - ui32_timeStartSensor2;
                if(TimeUsed > TIMEOUT_PERIOD(ui16_periodSensor2))
                {
                    // do Sensor 1 (Sensor ID 2)
                    // pack the data
                    SP_packData(&spContainer, (void*)sensor1, 2); //ID 2
                    // send the data
                    // TODO handle errors
                    SP_publish(&spContainer);
                    // handle timing
                    ui32_timeStartSensor2 = _CP0_GET_COUNT();
                }
            }
            // take advantage of both sensors data being smaller in total than USBCDC_BUFFER_SIZE 
            // and flush the USB buffer only at the end of the packed data of 2 sensors
            CITEC_USBCDC_SendNow();
        }
        // handle output task
        CITEC_USBCDC_TxTaskHandler(NULL); // argument will not be used in non RTOS so can be null
    }
}


/*******************************************************************************
 End of File
 */
