/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>

#include "app.h"
#include "CITECSerialProtocol.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

extern SP_Container spContainer;

APP_DATA appDataSensor0;
APP_DATA appDataSensor1;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************
/*******************************************************************************
  Function:
 RunRequests and SetPeriod callbacks
 
 */

void RunRequestCB(APP_DATA *p_appData, bool b_runRequest)
{
    if(b_runRequest)
    {
        if (p_appData->state != APP_STATE_SERVICE_TASKS)
            p_appData->state = APP_STATE_SERVICE_TASKS; 
    }
    else
    {
        if (p_appData->state == APP_STATE_SERVICE_TASKS)
            p_appData->state = APP_STATE_IDLE;
    }
}

void RunRequestCB_Sensor0(bool b_runRequest)
{
     RunRequestCB(&appDataSensor0, b_runRequest);
}

void RunRequestCB_Sensor1(bool b_runRequest)
{
     RunRequestCB(&appDataSensor1, b_runRequest);
}

void SetPeriodCB(APP_DATA *p_appData, uint16_t ui16_period)
{
    //only accept period of full milliseconds or more
    if (ui16_period >= 10)
       p_appData->ui16_period = ui16_period/10;
    else
       p_appData->ui16_period = 1;
}

void SetPeriodCB_Sensor0(uint16_t ui16_period)
{
     SetPeriodCB(&appDataSensor0, ui16_period);
}

void SetPeriodCB_Sensor1(uint16_t ui16_period)
{
     SetPeriodCB(&appDataSensor1, ui16_period);
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************


/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */
void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appDataSensor0.state = APP_STATE_INIT;
    appDataSensor0.ui16_period = 1000;
    appDataSensor0.ID = 1;
    appDataSensor1.state = APP_STATE_INIT;
    appDataSensor1.ui16_period = 1000;
    appDataSensor1.ID = 2;
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks (void* p_arg)
{
    APP_DATA *p_appData = (APP_DATA*)p_arg;
    uint16_t cnt=0;
    uint16_t ui16_len = SP_getDataLen(&spContainer.config, p_appData->ID);
    char sensor[ui16_len]; //mock arrays for Sensor Data
    SerialDataUpdateStruct UpdateData; //struct for serialProtocol
    
    while(qh_SerialDataUpdateQueue==NULL)
    {//make sure required inits have run/queues are existent
        vTaskDelay(1);
    }
    
    while(1)
    {//simulate Sensor Data Updates
        /* Check the application's current state. */
        switch ( p_appData->state )
        {
            /* Application's initial state. */
            case APP_STATE_INIT:
            {
                // handle periodicity initialization
                int32_t ui32_period = SP_getPeriod(&spContainer.config, p_appData->ID);
                if (ui32_period > 0)
                    SetPeriodCB(p_appData, (uint16_t)ui32_period);

                bool appInitialized = true;

                if (appInitialized)
                {
                    p_appData->state = APP_STATE_IDLE;
                }
                break;
                
            }

            case APP_STATE_SERVICE_TASKS:
            {
                //generate some changing "Sensor" Values
                sensor[cnt%ui16_len]=cnt++;
                //do Sensor 0 (Sensor ID 1)
                //specify Serial Protocol Sensor ID
                UpdateData.ui8_sensorDatagramID = p_appData->ID;
                //link Data
                UpdateData.pui8_data=sensor;    //link only address! data needs to be available at all times
                //send updated Data link to Serial Protocol function
                xQueueSend(qh_SerialDataUpdateQueue, &UpdateData, 0);
                break;
            }
            
            case APP_STATE_IDLE:
            {
                break;
            }

            /* The default state should never be executed. */
            default:
            {
                /* TODO: Handle error in application's state machine. */
                break;
            }
        }   
        //update sensors every ui16_period
        vTaskDelay(pdMS_TO_TICKS(p_appData->ui16_period));
    }
}

 

/*******************************************************************************
 End of File
 */
