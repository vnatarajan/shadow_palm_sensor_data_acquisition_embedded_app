/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    RTOSSerialProtocol.h

  @Summary
    This header file describes all structs and Functions used to implement
    the Serial Protocol defined at:
    https://projects.cit-ec.uni-bielefeld.de/projects/agni-grasplab/wiki/Serial_protocol

 */
/* ************************************************************************** */
// Original Code by Luis Oberroehrmann
// Rewritten by Guillaume Walck

#ifndef _RTOSSerialProtocol_H    /* Guard against multiple inclusion */
#define _RTOSSerialProtocol_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

#include <stdbool.h>
#include <stdint.h>
#ifdef USE_RTOS
#include "FreeRTOS.h"
#include "queue.h"
#endif
#include "CITECSerialProtocolDefines.h"
#include "CITECSerialProtocolTypes.h"
#include "CITEC_USBCDC.h"
#include "tcpip/tcpip.h"
#include "system_config.h"
//#include "system_definitions.h"

#define MAX_SERIALDATA_SIZE            500      //Max Datasize (of one Sensor Data Package)
#define DATAUPDATE_QUEUE_SIZE             2        


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

// *****************************************************************************
// *****************************************************************************
// Section: Global used Queue Handles
// *****************************************************************************
// *****************************************************************************
#ifdef USE_RTOS
QueueHandle_t qh_SerialDataUpdateQueue;  //Queue holding updates for Sensor Data
#endif

typedef void (*SP_FnRunReq)(bool);
typedef void (*SP_FnSetPer)(uint16_t);
  
/***************

// <editor-fold defaultstate="collapsed" desc="CommunicationType">    
/** Communication_enum

  @Summary
    Enum to decide where data came from / should go to

  @Description
    If multible chanels of communication are possible for one Device
    this enum will store from wich chaneal data came

  @Remarks
    None.
 */
//// </editor-fold>     
enum CommunicationType
{
    WIFI,
    USB
};

// <editor-fold defaultstate="collapsed" desc="SourcesStruct">    
/** Sources_Struct

  @Summary
    Struct to decide where data came from / should go to

  @Description
    If multible chanels of communication are possible for one Device
    this enum will store from wich channel data came and where it needs to got to
    Aditional to the Communication_enum it will Provide Adresses and similar as well.

  @Remarks
    None.
 */
//// </editor-fold> 
typedef struct
{
    enum CommunicationType e_communicationType;
    IP_MULTI_ADDRESS IP_Address;    
}SourcesStruct;
    
// <editor-fold defaultstate="collapsed" desc="SerialDataStruct">    
/** SerialDataStruct

  @Summary
    Struct to receive/send Serial Data with

  @Description
    The Protocol will interface with the Serial Device/Functions with this
    Data. Each received Data is expected to be transvered in this type.
    As well as outgoing Data will arive in this struct and need to be handled by
    the serial communication interface

  @Remarks
    None.
 */
//// </editor-fold>    
typedef struct
{
    // TODO Do not delete, it will be used when WIFI is back
    CDC_DataType data;
    SourcesStruct source;                 //where did the Data come From/response goes to
} SerialDataStruct;

// <editor-fold defaultstate="collapsed" desc="SerialDataUpdateStruct">    
/** SerialDataStruct

  @Summary
    Struct for passing a pointer to data + datagram ID over a queue

  @Description
    Since the data sent over serial protocol have various length
    and queue can only contain structs of same size, one has to pass pointers
    for the data of various length. This struct countains a datagram id and the pointer
    size is retrieved thanks to the datagram id when sending

  @Remarks
    None.
 */
//// </editor-fold> 
typedef struct
{
    uint8_t  ui8_sensorDatagramID;      //SensorID the Data is from (ID should be defined in the CITECSerialInit())
    uint8_t* pui8_data;                  //Pointer to Current sensor data (preferably static)
} SerialDataUpdateStruct;



// *****************************************************************************
// *****************************************************************************
// Section: Functions
// *****************************************************************************
// *****************************************************************************

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
// *****************************************************************************
/**
  @Function
    bool CITEC_SerialProtocol_TaskInit();

  @Summary
    Creates necessary Queues and declares existing Sensors.

  @Description
    Used Queues are created within this Function.
    the SerialProtocol device configuration is processed there, 
    after having been registered.

  @Precondition
    None.

  @Parameters
    @param none

  @Returns
    <ul>
      <li>true   Queue creation Successful
      <li>false  Queue creation failed
    </ul>

  @Remarks

 */
//// </editor-fold>
bool CITEC_SerialProtocol_TaskInit(void);

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
// *****************************************************************************
/**
  @Function
    bool CITEC_SerialProtocol_SerialWrite(uint8_t *pui8_outdata, uint32_t ui32_len);

  @Summary
    callback for writing data through CITEC_USBCDC, used by low-level SP functions when publishing

  @Description
    call back for writing data of the low-level SP functions to the CITEC_USBCDC
    this function returns quickly, and stores/copies/caches the data 
    
  @Precondition
    Initialized USBCDC.

  @Parameters
    @param pui8_outdata : pointer to data array
    @param ui32_len : length of the data to send

  @Returns
    <ul>
      <li>true   Queue creation Successful
      <li>false  Queue creation failed
    </ul>

  @Remarks

 */
//// </editor-fold>
uint16_t CITEC_SerialProtocol_SerialWrite(uint8_t *pui8_outdata, uint32_t ui32_len);

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
// *****************************************************************************
/**
  @Function
    void CITEC_SerialProtocol_SetWriteCallback(SP_WriteFunc fn);

  @Summary
    Assigns a callback function to the SerialProtocol low-level library.

  @Description
    Assigns a callback function to the SerialProtocol low-level library.

  @Precondition
    None.

  @Parameters
    @param fn : function that should return quickly and cache the data

  @Returns
    <ul>
      <li>true   Queue creation Successful
      <li>false  Queue creation failed
    </ul>

  @Remarks
    Device and Sensor description needs to be adapted to each Program by user.
    Function does not need to be called manually.
 */
//// </editor-fold>
void CITEC_SerialProtocol_SetWriteCallback(SP_WriteFunc fn);

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
// *****************************************************************************
/**
  @Function
    bool CITEC_SerialProtocol_RegisterSensor(SP_SensorConfiguration config, RTOS_SP_FnRunReq fn_runRequest, RTOS_SP_FnSetPer fn_setPeriod);

  @Summary
    Register a sensor and its associated task management callbacks

  @Description
    Register a sensor and associated callbacks for the SerialProtocol container and task manager.

  @Precondition
    None.

  @Parameters
    @param SP_SensorConfiguration config: SensorConfiguration to register (DatagramID will be automatically generated)
    @param RTOS_SP_FnRunReq *fn_runRequest : pointer to function setting running state, can be NULL
    @param RTOS_SP_FnSetPer *fn_setPeriod: pointer to function setting periodicity, can be NULL

  @Returns
    <ul>
      <li>true   Sensor registered
      <li>false  Sensor not registered, due to maximum number of sensor reached
    </ul>

  @Remarks

 */
//// </editor-fold>
bool CITEC_SerialProtocol_RegisterSensor(SP_SensorConfiguration config, SP_FnRunReq fn_runRequest, SP_FnSetPer fn_setPeriod);

#ifdef USE_RTOS
// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
// *****************************************************************************
/**
  @Function
    void CITEC_SerialProtocol_Task(void);

  @Summary
    Task calling the Serial Protocol handler.

  @Description
    Task calling the Serial Protocol handler.

  @Precondition
    None.

  @Parameters
    None.

  @Returns
    None.

  @Remarks
    None.

  @Example
    @code
    xTaskCreate((TaskFunction_t) CITEC_SerialProtocol_Task, 
                                 "Serial Tasks",   2048, NULL, 4, NULL);
 */
//// </editor-fold>
void CITEC_SerialProtocol_Task(void);
#endif

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
// *****************************************************************************
/**
  @Function
    void CITEC_SerialProtocol_TaskHandler(bool *pb_welcomeTaskRunning);

  @Summary
    handler for all Protocol requests, also sending out SensorData from the queue.

  @Description
    This function manages the serial protocol requests form the host and sends
    data in the qh_SerialDataUpdateQueue to the host. 

  @Precondition
    Sensor data needs to be Sent to the qh_SerialDataUpdateQueue
    Task needs to be created at boot.

  @Parameters
    @param pb_welcomeTaskRunning : pointer to the welcome_task_running boolean to permit stopping it at first SP valid message

  @Returns
    None.

  @Remarks
    None.

 */
//// </editor-fold>
void CITEC_SerialProtocol_TaskHandler(bool *pb_welcomeTaskRunning);

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
// *****************************************************************************
/**
  @Function
    uint8_t CITEC_SerialProtocol_SendError(SP_Container *p_sc, uint8_t ui8_errorID, char ca_String[], SourcesStruct Source)

  @Summary
    Sends Error message to Serial host

  @Description
    Depending on ErrorID, will output Error messages to the host.

  @Precondition

  @Parameters
    @param uint8_t ui8_errorID: Code of the Error

    @param char ca_String[]: if applicable for Error, additional String
  
    @param Sources Source: Info were to send Error codes to

  @Returns
    0 if no errors, -1 if failed so send the error

 */
//// </editor-fold>
uint8_t CITEC_SerialProtocol_SendError(SP_Container *p_sc, uint8_t ui8_errorID, char ca_String[], SourcesStruct Source);

// *****************************************************************************
#ifdef __PIC32MZ__
    #define CDC_MAKE_BUFFER_DMA_READY  __attribute__((coherent)) __attribute__((aligned(16)))
#else
#define CDC_MAKE_BUFFER_DMA_READY
#endif


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _RTOSSerialProtocol_H */

/* *****************************************************************************
 End of File
 */
