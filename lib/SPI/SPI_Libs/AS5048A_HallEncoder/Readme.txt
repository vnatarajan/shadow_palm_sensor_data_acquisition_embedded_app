#####################################################################################
## REVISIONS (list newer Revisions on top)
## Date         Author             Description
## 2018-05-29   Luis Oberröhrmann  First release of the library
#####################################################################################
## Abstract
## This library interfaces with the AS5048A rotary encoder chip over the SPI-Bus.
## The interface offers continuous and singel readout of the angle value in degrees,
## as well as in raw (whereas 1 LSB equals to 0.0219°).
#####################################################################################
## Public interface:
##
## CITEC_AS5048_config.h     :	Hold various user defines to configure the library
##
## CITEC_AS5048AInitialize() : 	Initializes the API and enabels the funktions to run.
##
## AS5048A_GetAngle_RAW()    :	Funktion which uses the SPI interface to return the 
##				current RAW value of the Sensor. (Used for single readout)
##
## CITEC_AS5048ATasks()      : 	Task that needs to be created to implement continuous 
##			      	readout. Updates the: 
##
## CITEC_AS5048A_Queue 	     :	Queue which holds the Angle_Data Struct.
##				Continous data can be read from here.
##
## RotaryEncoderReceivedNew():	Function which checks if a Angle Struct is to old to 
##				be used or not.
##
## RawToDeg()/DegToRaw()     :	Conversion functions which convert the 0-0x3FFF Raw 
##				value to a 0-360° angle Value or vice versa.
##
#####################################################################################
## Usage
##
## Step 1: Add CITEC_AS5048_config.h, CITEC_AS5048A.h and CITEC_AS5048A.c to your project
##
## Step 2: In MHC config, activate the SPI Driver Instance and set the following settings:
##         (other parameters can be set at programmers will):
##	   - "Use Interrupt Mode?"
##	   - "Use Master Mode?"
##	   - "Use Standard Buffer Mode?"
##	   - Choose one according to your application:
##           "Use 8-Bit Mode?" or "Use 16-Bit Mode?"
##	   - Protocol Type: "DRV_SPI_PROTOCOL_TYPE_STANDARD"
##	   - do not exceed Baudrate: 10000000 (10 MHz)
##	   - Clock Mode: DRV_SPI_CLOCK_MODE_IDLE_LOW_EDGE_RISE
##	   - Input Phase: SPI_INPUT_SAMPLING_PHASE_AT_END
##
## Step 3: In CITEC_AS5048_config.h change the defines according to setup/requirements.
##
## Step 4: For continuous mode:
##	   -- In system_tasks.c include the CITEC_AS5048A.h and CITEC_AS5048ATasks functions.
##	   -- Data will be provided via CITEC_AS5048A_Queue which is set up there
##	   For single readout mode:
##	   -- add CITEC_AS5048AInitialize(), before your first call of AS5048A_GetAngle_RAW()
##
## Step 5: Receive most current angle data via CITEC_AS5048A_Queue ass needed.
##	   (Remember the CITEC_ENABLEMULTIREAD setting while doing so)
##	   (Case depending check the Angel Data age with RotaryEncoderReceivedNew())
##
#####################################################################################
## Example
##
## The example is written for a PIC32MZ EF Starter Kit with Crypto:
## http://www.microchip.com/DevelopmentTools/ProductDetails.aspx?PartNO=DM320007-C
##
## It makes use of the SPI1-Module, connected to the 40-pin header.
## The program will provide angular feedback with feedback over the 3 starter kit LEDs:
## Green:  0° to +-5°
## Orange: +-5° to +-90°
## Red:    when green and orange are not lit (+-90° to +-180°)
## When all LEDs are lit simultaneously, an error has occurred.
##
## To do so two tasks run with different priority, CITEC_AS5048ATasks() will continuosly
## get new angle data.
## APP_Tasks will receive the data via CITEC_AS5048A_Queue and switch the leds accordingly.
## (The Example includes the Percepio Tracealizer code to see task behavior)
##
## Pinout:
##---------------------------------------
## Function:	PIN on 40-pin Header J12:
## MOSI		19
## MISO		21
## SCK		23
## !SS		24
## GND		25
##
## The signalling LED's are already connected on EF SK:
## Red 		PORTH.0
## Orange 	PORTH.1
## Green 	PORTH.2
#####################################################################################

