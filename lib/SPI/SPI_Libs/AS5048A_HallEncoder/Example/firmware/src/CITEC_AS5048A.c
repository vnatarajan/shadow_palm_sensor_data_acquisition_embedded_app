/*******************************************************************************
 CITEC AS5048A API set

  Company:
    CITEC

  File Name:
    citec_AS5048A.c

  Summary:
    API set to make use of the AS5048A Rotary encoder

  Description:
    This file contains functions to run the AS5048A Rotary encoder with a 
    PIC32
 *******************************************************************************/


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "CITEC_AS5048A.h"
#include "task.h"
#include "driver/spi/src/dynamic/drv_spi_internal.h"
// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************
//8Bit SPI
#if CITEC_AS5048_SPI8BIT
    int8_t __attribute__ ((aligned (8))) SPItxData[(CITEC_AS5048_SPI_DataSize*2)];  //*2 because 2 transmissions equal one package
    int8_t __attribute__ ((aligned (8))) SPIrxData[(CITEC_AS5048_SPI_DataSize*2)];
//16Bit SPI
#else
    int16_t __attribute__ ((aligned (16))) SPItxData[CITEC_AS5048_SPI_DataSize];
    int16_t __attribute__ ((aligned (16))) SPIrxData[CITEC_AS5048_SPI_DataSize];
#endif



/*****************************************************
 * Initialize the application data structure. All
 * application related variables are stored in this
 * data structure.
 *****************************************************/


// *****************************************************************************
/* SPIHandle

  Summary:
    Contains the SPI Pointer 
*/
DRV_HANDLE CITEC_AS5048A_SPIHandle;   //could be devined lokaly in _Task but would be needet to be passed to every funktion (or open/close all the time)

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine
// *****************************************************************************
// *****************************************************************************

/******************************************************************************
  Function:
    void SPI_Initialize ( void )

  Remarks:
    See prototype in spi.h.
 */

void CITEC_AS5048AInitialize ( void )
{   
    //#error "Make shure the SPI is set up according to the Readme.txt! Delete this line if you already configured the SPI as described";
    
    if(configTICK_RATE_HZ>1000)
        SYS_ASSERT(false, "This code uses the pdMS_TO_TICKS Makro to provide ms timing. Not funktional for configTICK_RATE_HZ>1000");
        
    CITEC_AS5048A_SPIHandle = DRV_SPI_Open(CITEC_AS5048A_SPI_INDEX, DRV_IO_INTENT_READWRITE);             
       
    CITEC_AS5048_SPI_CS_TRIS_Set;     //set CS as output
    
    CITEC_AS5048A_Queue = xQueueCreate(CITEC_ANGELDATAQUEUESIZE, sizeof(CITEC_AngleData));
    xQueueReset(CITEC_AS5048A_Queue);
    /*don't proceed if queue was not created...*/
    while(CITEC_AS5048A_Queue == NULL)
    {   
        SYS_ASSERT(false, "AS5048ASend_Queue could not be created.");
        while(1);
    }
    
    CITEC_SPISemaphore=xSemaphoreCreateBinary();
    while(CITEC_SPISemaphore==NULL)
    {
        SYS_ASSERT(false, "CITEC_SPISemaphore could not be created.");
        while(1);
    }
    
    //clear error flag from previous transmissions
    AS5048A_ClearError(); 
    
}

/******************************************************************************
  Function:
    void AS5048A_Tasks ( void )

  Remarks:
    See prototype in spi.h.
 */

void CITEC_AS5048ATasks(void)
{   
    int16_t receiveRAW;     //temp var to read unsigend value to chatch communication errors
    CITEC_AngleData data;   
    
    CITEC_AS5048AInitialize();
    
    data.DataTicktime=xTaskGetTickCount();
    
    while(1)
    {          
        do{
            receiveRAW=AS5048A_GetAngle_RAW();
            if(receiveRAW<0)
                SYS_ASSERT(false, "An error ocured while we tried to get the Data, see data.Raw value for the error code");   //accept only valid data
        }while(receiveRAW<0);

        data.Raw=(uint16_t)receiveRAW;

        if(CITEC_AS5048A_Queue==NULL)
            SYS_ASSERT(false, "The Queue to send Data to was deleted at runtime");
        else
        {
            #if (CITEC_ENABLEMULTIREAD==0)
               if(xQueueSend(CITEC_AS5048A_Queue, &data, 0)==pdFALSE)          //add new data to the queue, discard if queue full
                   SYS_ASSERT(false, "AngleData_Queue has no free Slots left");
            #else
                if(xQueueOverwrite(CITEC_AS5048A_Queue, &data)==pdFALSE)          //ceep Queue current and place latest angle value in there
                   SYS_ASSERT(false, "Something went horribly wrong, you should never! be here");
            #endif
        }
        vTaskDelayUntil(&data.DataTicktime, pdMS_TO_TICKS(CITEC_GETANGLE_MS)); //repeat exactly every blocktime
    }//while
    
    //realise precise delay with same time as PID
}

bool RotaryEncoderReceivedNew(CITEC_AngleData* data, uint8_t div_ms)
{
    TickType_t time;
    time=xTaskGetTickCount();
    //check if data time is from accapted tick, if older return error 
    if(((signed)(time-pdMS_TO_TICKS(div_ms)))<=(signed)(data->DataTicktime))
        return true;
    return false;
}


int8_t AS5048A_ClearError(void)
{
    uint16_t err;
    
    if(AS5048A_Read(AS5048A_CLEARERRORFLAG, &err)==PreviosSendError);
        return PreviosSendError;
    
    
    if((err&0x0007)==0)
        return GeneralError;
    return (int8_t)-(err&0x0007); //return error code
}

int16_t AS5048A_GetAngle_RAW(void) 
{   
    int16_t Angle_RAW;
    
    if(CITEC_AS5048A_SPIHandle==NULL)
    {   
       SYS_ASSERT(false, "An emply SPIHandle is used, make shure CITEC_AS5048AInitialize() is called"); 
    }
    
    if(AS5048A_Read(AS5048A_ANGLE, &Angle_RAW)<0)
    {
        return AS5048A_ClearError();   //make next communication valid
        //return error code
    }
    if(Angle_RAW<=ANGLEMAXVAL_RAW)  //check if received data is Valide
        return Angle_RAW;
    else
    {
        SYS_ASSERT(false, "Invalid angle receifed from Read Function.");
        return AngleOutOfBoundsError;
    }
}

//not used
int16_t AS5048A_GetMagnitude(void) 
{   
    int16_t Magnitude;
    
    if(AS5048A_Read(AS5048A_MAGNITUDE, &Magnitude)<0)
    {
        return AS5048A_ClearError();   //make next communication valid
        //return error code
    }
    if(Magnitude<=ANGLEMAXVAL_RAW)  //check if received data is Valide (Magnitude max is same as angle max)
        return Magnitude;
    else
    {
        SYS_ASSERT(false, "Invalid angle receifed from Read Function.");
        return AngleOutOfBoundsError;
    }
}

//not used
int8_t AS5048A_SetZeroPosition(uint16_t Zero)
{
    int16_t high = Zero>>6;
    int16_t low = Zero&0x3f;
    
    if(!AS5048A_Write(AS5048A_ZEROPOSITIONHI, high))
        if(!AS5048A_Write(AS5048A_ZEROPOSITIONLOW, low))
            return NoError;   //write sucsesfull

    return GeneralError;
}

// WARNING: Use with caution, can only be executed once in a sensor lifetime!!!
// Untested!
// Motor needs to be in new Zero Position, make sure no rotation is possible while executing
int8_t AS5048A_ProgrammZeroPosition(void)
{
    CITEC_AngleData data;
    
    if(!AS5048A_SetZeroPosition(0))    // clear zero Position
    {
        do{
           data.Raw=AS5048A_GetAngle_RAW();
           if(data.Raw<0)
               SYS_ASSERT(false, "An error ocured while we tried to get the Data");   //accept only valid data
       }while(data.Raw<0);
        if(!AS5048A_SetZeroPosition(data.Raw))                         // set new zero
            if(!AS5048A_Write(AS5048A_PROGRAMMINGCONTROL, 0x0000))     // enable programming
                if(!AS5048A_Write(AS5048A_PROGRAMMINGCONTROL, 0x0004)) // burn value
                {
                    do{
                        data.Raw=AS5048A_GetAngle_RAW();
                        if(data.Raw<0)
                            SYS_ASSERT(false, "An error ocured while we tried to get the Data");   //accept only valid data
                    }while(data.Raw<0);
                    if(data.Raw == 0)                                  // verify if burn was successful
                        if(!AS5048A_Write(AS5048A_PROGRAMMINGCONTROL, 0x0040)) // check if burn was successful
                        {
                            do{
                                data.Raw=AS5048A_GetAngle_RAW();
                                if(data.Raw<0)
                                    SYS_ASSERT(false, "An error ocured while we tried to get the Data");   //accept only valid data
                            }while(data.Raw<0);
                            if(data.Raw == 0)                          // verify if verify was successful
                                return NoError;                              // all successful
                        }
                }
    }
    return GeneralError;   // error setting new zero position
}

//callback Routine to unblock the SPI module
DRV_SPI_BUFFER_EVENT_HANDLER SPICallback(void)
{
    if(xSemaphoreGiveFromISR(CITEC_SPISemaphore, pdFALSE)==pdTRUE)
    {
        portYIELD();    //exit current task and rescedule
        return;
    }
    SYS_ASSERT(false, "Semaphore could not be given!");   //accept only valid data
}

int8_t AS5048A_Read(uint16_t adr, uint16_t *data)
{
    DRV_SPI_BUFFER_HANDLE Read_Buffer_Handle;

    //check for correct address?
       
    adr&=~0xFFFFC000;      //clear transmission bytes and upper 16bit which are not used
    adr|=0x4000;      //add read byte
    if(returnEvenParity(adr))
        adr|=0x8000;  //add parity bit

//SPI in 8Bit mode    
#if CITEC_AS5048_SPI8BIT
    static uint16_t rxReturn[CITEC_AS5048_SPI_DataSize];   //help var to return a 16bit int
    
    SPItxData[0]=(uint8_t)(adr>>8);
    SPItxData[1]=(uint8_t)(adr&0xFF);    
    
    CITEC_AS5048_SPI_CS_SELECT;
    Read_Buffer_Handle=DRV_SPI_BufferAddWriteRead(CITEC_AS5048A_SPIHandle, &SPItxData[0], (2*sizeof(uint8_t)), &SPIrxData[0], (4*sizeof(uint8_t)), (DRV_SPI_BUFFER_EVENT_HANDLER) SPICallback, 0);
    if(xSemaphoreTake(CITEC_SPISemaphore, CITEC_SPI_WAIT4DONE_MS)==pdTRUE)
    {
        CITEC_AS5048_SPI_CS_DESELECT;

        rxReturn[0]=(uint16_t)((SPIrxData[0]<<8)|SPIrxData[1]);
        rxReturn[1]=(uint16_t)((SPIrxData[2]<<8)|SPIrxData[3]);

        //check for errors
        if(((rxReturn[0]>>14)&0x01)==1)
        {
            SYS_ASSERT(false, "Error detectet in previous SPI Communication");
            return PreviosSendError;   //error in previous send
        }
        //link data if read was succesfull
        if(!returnEvenParity(rxReturn[0]))      
        {
            *data=(rxReturn[0]&AS5048A_ANGLE);       //write back data without controll bits
            return NoError;                                //receive succesfull
        }
    }
    else
   {//something went wrong while communicationg via SPI
        SYS_ASSERT(fasle, "The SPI communication could not be completed within the assigend time period");
   }
    SYS_ASSERT(false, "Error detectet in current SPI Communication");
    return GeneralError;   //some errors
        
    
//spi in 16Bit mode
#else
    SPItxData[0]=adr;    
    
    CITEC_AS5048_SPI_CS_SELECT;
    DRV_SPI_BufferAddWriteRead(CITEC_AS5048A_SPIHandle, &SPItxData[0], (sizeof(int16_t)), &SPIrxData[0], 2*(sizeof(int16_t)), (DRV_SPI_BUFFER_EVENT_HANDLER) SPICallback, 0);
    if(xSemaphoreTake(CITEC_SPISemaphore, CITEC_SPI_WAIT4DONE_MS)==pdTRUE)
    {//wait for end transmission

        CITEC_AS5048_SPI_CS_DESELECT;

        //check for errors
        if(((SPIrxData[0]>>14)&0x01)==1)
        {
            SYS_ASSERT(false, "Error detectet in previous SPI Communication");
            return PreviosSendError;   //error in previous send
        }
        //link data if read was successful
        if(!returnEvenParity(SPIrxData[0])) 
        {
            *data=(SPIrxData[0]&AS5048A_ANGLE);          // write back data without control bits
            return NoError;                                 // receive successful
        }
   }
   else
   {//something went wrong while communicationg via SPI
        SYS_ASSERT(fasle, "The SPI communication could not be completed within the assigend time period");
   }
    SYS_ASSERT(false, "Error detected in current SPI Communication");
    return GeneralError;   // Return an error
#endif
                                    
}

int8_t AS5048A_Write(uint16_t adr, uint16_t data)
{
    DRV_SPI_BUFFER_HANDLE Write_Buffer_Handle;
    
    //check for correct address?
    
    adr&=~0xC000;                     // clear transmission bytes (set write mode))
    
    if(returnEvenParity(adr))
        adr|=0x8000;                  // add parity bit
    
    data&=~0xC000;                    // clear control bits
    if(returnEvenParity(data))
        data|=0x8000;                 // add parity bit

//8Bit SPI mode
#if CITEC_AS5048_SPI8BIT
    SPItxData[0]=(uint8_t)(adr>>8);
    SPItxData[1]=(uint8_t)(adr&0xFF);
    SPItxData[2]=(uint8_t)(data>>8);
    SPItxData[3]=(uint8_t)(data&0xFF);
    SPItxData[4]=(uint8_t)(AS5048A_NOP>>8);    //send a nop to verify send (see datasheet)
    SPItxData[5]=(uint8_t)(AS5048A_NOP&0xFF);         
    
    CITEC_AS5048_SPI_CS_SELECT;
    Write_Buffer_Handle = DRV_SPI_BufferAddWriteRead(CITEC_AS5048A_SPIHandle, &SPItxData[0], (6*sizeof(int16_t)), &SPIrxData[0], (8*sizeof(int16_t)), (DRV_SPI_BUFFER_EVENT_HANDLER) SPICallback, 0);
    if(xSemaphoreTake(CITEC_SPISemaphore, CITEC_SPI_WAIT4DONE_MS)==pdTRUE)
    {
        CITEC_AS5048_SPI_CS_DESELECT;

        //verify send
        if(((SPIrxData[0]>>6)&0x01)==1)
        return PreviosSendError;   //error in previous send

        //bytes sucesfully send?
        if((uint16_t)((SPIrxData[6]<<8)|SPIrxData[7])==data)
            return NoError;
    }
    else
    {//something went wrong while communicationg via SPI
        SYS_ASSERT(fasle, "The SPI communication could not be completed within the assigend time period");
    }
        return GeneralError;
    
//16Bit SPI mode
#else
    SPItxData[0]=adr;
    SPItxData[1]=data;
    SPItxData[2]=AS5048A_NOP;           //send a nop to verify send (see datasheet)
    
    CITEC_AS5048_SPI_CS_SELECT;
    Write_Buffer_Handle = DRV_SPI_BufferAddWriteRead(CITEC_AS5048A_SPIHandle, &SPItxData[0], 3*sizeof(int16_t), &SPIrxData[0], 4*sizeof(int16_t), (DRV_SPI_BUFFER_EVENT_HANDLER) SPICallback, 0);
    if(xSemaphoreTake(CITEC_SPISemaphore, CITEC_SPI_WAIT4DONE_MS)==pdTRUE)
    {
        CITEC_AS5048_SPI_CS_DESELECT;

        // verify send
        if(((SPIrxData[0]>>14)&0x01)==1)
        return PreviosSendError;

        // bytes successfully sent?
        if(SPIrxData[3]==data)
            return NoError;
    }
    else
    {//something went wrong while communicationg via SPI
        SYS_ASSERT(fasle, "The SPI communication could not be completed within the assigend time period");
    }
    return GeneralError;
#endif    
}


bool returnEvenParity(uint16_t x){
    unsigned char cnt = 0;
    unsigned char i;
    for(i = 0; i < 16; i++){
        if(x & 1)
        {   //count up if bit detected
            cnt++;
        }
        x >>= 1;    //shift whole int right once
    }
    return (bool)(cnt & 1); //if cnt.0==0 -> even parity
}

/*******************************************************************************
 End of File
 */

