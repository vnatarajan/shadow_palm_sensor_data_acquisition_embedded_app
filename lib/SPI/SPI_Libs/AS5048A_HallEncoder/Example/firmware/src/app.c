/*******************************************************************************
 CITEC AS5048A example

  Company:
    CITEC

  File Name:
    app.c

  Summary:
    Example to make use of the AS5048A Rotary encoder

  Description:
    This file contains an example Application to run the AS5048A Rotary encoder
    with a PIC32
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "task.h"


// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************

//defines to easily switch LEDs on and off
#define ON  1
#define OFF 0

#define LED_Red(x)      (PORTHbits.RH0=x)
#define LED_Orange(x)   (PORTHbits.RH1=x)
#define LED_Green(x)    (PORTHbits.RH2=x)

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    //set LEDs as output
    TRISHbits.TRISH0=0;
    TRISHbits.TRISH1=0;
    TRISHbits.TRISH2=0;
        
    do                          //can only run if Queue is initialised
    {
        vTaskDelay(1 / portTICK_PERIOD_MS);              //short wait to allow other funktions to run
    }while(CITEC_AS5048A_Queue==NULL);                    //wait untill queue handeler is initialised
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Task ( void )
{
    BaseType_t errStatus;
    CITEC_AngleData Data;
    
    APP_Initialize();
    
    while(1)
    {
        errStatus = pdTRUE; //reset eror status
        //get new Angle Data
       if(CITEC_AS5048A_Queue==NULL)
           SYS_ASSERT(false, "The Queue to send Data to was deleted at runtime");
       else
       {
            #if (CITEC_ENABLEMULTIREAD==0)
                while(xQueueReceive(CITEC_AS5048A_Queue, &Data, 0) == pdTRUE)  //read until queue is empty
                {   
                    //we do not use the extra data here,
                    //queue size could be 1 in this example, with CITEC_GETANGLE_MS=100 (blocktime of this task)
                }
            #else
                if(xQueuePeek(CITEC_AS5048A_Queue, &Data)==pdFALSE) //view the most current Data, leave Data there for other task to view
                    SYS_ASSERT(false, "No Data at the Queue, check if CITEC_AS5048ATasks() is running or if xQueueReceive(CITEC_AS5048A_Queue,..) is called anywhere");
            #endif
       }
        //check if the Angle value is older than MAXANGLEAGE_MS ms
        if(RotaryEncoderReceivedNew(&Data, CITEC_MAXANGLEAGE_MS)==false)   
        {
            SYS_ASSERT(false, "Old Angle Data received");
            errStatus=pdFALSE;
        }
        
        //obviously you can get a single value localy via the AS5048A_GetAngle_RAW(); function
        //(This contains only the raw value (or the error code if something went wrong)) (since the time const is never used the struct does not need to be used)
        //should only be done if you do not intend to use CITEC_AS5048ATasks(), and in irregular poll intervall is needed
        int16_t show;
        show=AS5048A_GetAngle_RAW();
        if(show>0)
            show=RawToDeg(show);    //the angle value can be received by using the RawToDeg funktion 
        
        //generate threshold based output on LEDs
        if(errStatus == pdFALSE)
        {   //some error occourd
            LED_Red(ON);
            LED_Orange(ON);
            LED_Green(ON);
        }
        else
        {
            if(RawToDeg(Data.Raw)<=5||RawToDeg(Data.Raw)>=355)
            {
                //if degree=0 (+-5), green led
                LED_Red(OFF);
                LED_Orange(OFF);
                LED_Green(ON);
            }
            else if(RawToDeg(Data.Raw)<=90||RawToDeg(Data.Raw)>=180)
            {   
                //if +-5�<degree<=180�
                LED_Red(OFF);
                LED_Orange(ON);
                LED_Green(OFF);
            }
            else
            {
                //if +-180�<degree
                LED_Red(ON);
                LED_Orange(OFF);
                LED_Green(OFF);
            }
        }
        
        vTaskDelay(pdMS_TO_TICKS(100));       //block for 100ms
    }
    
    
    
}


/*******************************************************************************
 End of File
 */
