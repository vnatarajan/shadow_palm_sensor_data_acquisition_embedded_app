/*******************************************************************************
  MPLAB Harmony Application

  Application Header
  
  Company:
    CITEC

  File Name:
    citec_AS5048A_confic.h

  Summary:
    User definitions to change the API behaviour and addapt to your Program
*******************************************************************************/
#include <stdint.h>



#define CITEC_AS5048A_SPI_INDEX        DRV_SPI_INDEX_0  //SPI Module to use (use harmony configurator to set up SPI)

#define CITEC_AS5048_SPI_CS_PORT_ID    PORT_CHANNEL_J   //SPIU !CS Port

#define CITEC_AS5048_SPI_CS_PORT_PIN   PORTS_BIT_POS_5  //SPI !CS Pin

#define CITEC_MAXANGLEAGE_MS        1                   //max age accaptable for the Angel value in ms (only nessecarry if RotaryEncoderReceivedNew is used)(usually you will what that to be 0)

#define CITEC_GETANGLE_MS           1                   //interval in which the angle value will be updated in ms (delay time in CITEC_AS5048ATasks)

                                                        //Change API behaviour:
#define CITEC_ENABLEMULTIREAD       0                   //0: only one Task will receive the CITEC_AS5048A_Queue, CITEC_ANGELDATAQUEUESIZE needs to be configured accordingly
                                                        //1: Multible Task need to gain the most current Angle value, Queue size will be 1, use xQueuePeek() to receive vales!!! 

#if (CITEC_ENABLEMULTIREAD==0)   
#define CITEC_ANGELDATAQUEUESIZE    110                 //Number of Angel Data that can be held by the queue at once, the timing will determain how many values are stored!                   
#endif

                                                        //choose the SPI working mode that the SPI is configured in with Harmony:
#define CITEC_AS5048_SPI8BIT        0                   //1 =  8Bit mode
                                                        //0 = 16Bit mode ("Use 16-Bit Mode?" in Harmony is ticked)


//only to be changed if other SPI communications with greater data amounts are used with the same SPI Buffer
#define CITEC_AS5048_SPI_DataSize   5                   //size of SPI buffer to send/receive data (for the 16Bit packages)

#define CITEC_SPI_WAIT4DONE_MS      pdMS_TO_TICKS(100)  //time a task will block and wait for the SPI kommunication to finish