/*******************************************************************************
  MPLAB Harmony Application

  Application Header
  
  Company:
    CITEC

  File Name:
    citec_AS5048A.c

  Summary:
    API set to make use of the AS5048A Rotary encoder

  Description:
    This file contains funktions to run the AS5048A Rotary encoder with a 
    PIC32
*******************************************************************************/


#ifndef AS5048A_H
#define AS5048A_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system/system.h"
#include "driver/spi/drv_spi.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "CITEC_AS5048_config.h"


#include "system/debug/sys_debug.h"
#ifdef SYS_ASSERT
#undef SYS_ASSERT
#endif
#define SYS_ASSERT(test, message) do{SYS_DEBUG_BreakPoint();}while(0)

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

#define CITEC_AS5048_SPI_CS_TRIS_Set   SYS_PORTS_PinDirectionSelect(PORTS_ID_0, SYS_PORTS_DIRECTION_OUTPUT, CITEC_AS5048_SPI_CS_PORT_ID, CITEC_AS5048_SPI_CS_PORT_PIN)
 
#define CITEC_AS5048_SPI_CS_SELECT     SYS_PORTS_PinClear(PORTS_ID_0,CITEC_AS5048_SPI_CS_PORT_ID,CITEC_AS5048_SPI_CS_PORT_PIN)          

#define CITEC_AS5048_SPI_CS_DESELECT   SYS_PORTS_PinSet(PORTS_ID_0,CITEC_AS5048_SPI_CS_PORT_ID,CITEC_AS5048_SPI_CS_PORT_PIN)

// define Device registers
// *****************************************************************************
#define AS5048A_NOP                 0x0000
#define AS5048A_CLEARERRORFLAG      0x0001
#define AS5048A_PROGRAMMINGCONTROL  0x0003
#define AS5048A_ZEROPOSITIONHI      0x0016
#define AS5048A_ZEROPOSITIONLOW     0x0017
#define AS5048A_AGC                 0x3FFD
#define AS5048A_MAGNITUDE           0x3FFE
#define AS5048A_ANGLE               0x3FFF

//other defines
#define ANGLEHALFVAL_RAW             0x1FFF
#define ANGLEMAXVAL_RAW              0x3FFF

#define AS5048A_MAXBAUD_HZ          10000000    //max SPI frequency of the sensor

#define NoError                      0
#define FramingError                -1
#define CommandInvlaidError         -2
#define ParityError                 -4
#define PreviosSendError            -8
#define AngleOutOfBoundsError       -16
#define GeneralError                -32


#ifndef CITEC_ANGELDATAQUEUESIZE
#define CITEC_ANGELDATAQUEUESIZE 1
#endif

/*******************************************************************************
 * Structure that holds the Angel values
 * Raw:     The bit value gained by the Sensor 0-0x3FFF
*******************************************************************************/
typedef struct
{
    uint16_t    Raw;
    TickType_t  DataTicktime;
} CITEC_AngleData;

typedef struct
{
    uint16_t    Raw;
    TickType_t  DataTicktime;
} CITEC_MagnitudeData;

//used to send all Angel data to its destination Task
QueueHandle_t CITEC_AS5048A_Queue;
SemaphoreHandle_t CITEC_SPISemaphore;
/*******************************************************************************
  Function:
    void CITEC_AS5048AInitialize ( void )

  Summary:
    Inialises the SPI Handler and neccessary Queue.

  Description:
    This routine creates the SPI Handler which is used to communicate with the device
    and sets up a Queue from type Angel_Data with the sice 1 to always bring the most
    current data to the application.

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    This routine is called from AS5048A_getAngle() routine.
 */
void CITEC_AS5048AInitialize ( void );


/*******************************************************************************
  Function:
    void CITEC_AS5048ATasks (void)

  Summary:
    The FREETROS Task to run as an independent entity

  Description:
    This routine runs as an FREERTOS Task and, for this example, adds a new
    angle Data to the queue every CITEC_GETANGLE_MS.

  Precondition:
    CITEC_AS5048AInitialize ( void );

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    None.
 */
void CITEC_AS5048ATasks(void);


/*******************************************************************************
  Function:
    int8_t AS5048A_Read (int16_t adr, int16_t *data)

  Summary:
    Reads one Register of the Sensor

  Description:
    This routine reads one register of te Sensor via SPI by 
    adding two protokoll bits to the 16bit data string and verifyes the recived
    Data.

  Precondition:
    CITEC_AS5048AInitialize() needs to be called once before

  Parameters:
    int16_t adr:   holds the Adress that is to be read
    int16_t *data: will hold the 16bit data from the Register

  Returns:
    0 on Sucsess
    1 on parity error
    2 on previous send error
  
  Remarks:
    This routine is called when needed by other funktions.
 */
int8_t AS5048A_Read(uint16_t adr, uint16_t *data);


/*******************************************************************************
  Function:
    int16_t AS5048A_GetAngle (void);

  Summary:
    Gets the current Angle

  Description:
    This routine gets the Raw and Degree Value of the current Motor state

  Precondition:
    CITEC_AS5048AInitialize() needs to be called once before

  Parameters:
    None.

  Returns:
    Raw angle Value or,
    error code on fail

  Remarks:
    None.
 */
int16_t AS5048A_GetAngle_RAW(void);

/*******************************************************************************
  Function:
    int16_t AS5048A_GetMagnitude (void)

  Summary:
    Gets the current Magnitude

  Description:
    This routine gets the Magnitude Value of the current Motor state

  Precondition:
    CITEC_AS5048AInitialize() needs to be called once before

  Parameters:
    none

  Returns:
    Magnitude or,
    error code on fail
 
  Remarks:
    None.
 */
int16_t AS5048A_GetMagnitude(void); 


/*******************************************************************************
  Function:
    int8_t AS5048A_ClearError (void)

  Summary:
    Clears the error register

  Description:
    This routine Clears the error Register and returns its value

  Precondition:
    CITEC_AS5048AInitialize() needs to be called once before

  Parameters:
    None.

  Returns:
    Error code (see data sheet)

  Remarks:
    None.
 */
int8_t AS5048A_ClearError(void);

/*******************************************************************************
  Function:
    bool has_even_parity (uint16_t x)

  Summary:
    Generates an even parity bit from a 16Bit string if needed

  Description:
    Counts the bits set to one in the given Bits. If the number of '1' Bits is 
    even the Parity is fullfilled.

  Precondition:
    None.

  Parameters:
    uint16_t x: value to generate parity for

  Returns:
    0 if string is eaven
    1 if stirng is odd (parity bit needs to be added/ communication error occured)

  Remarks:
    None.
 */
bool returnEvenParity(uint16_t x);

/*******************************************************************************
  Function:
    bool RotaryEncoderReceivedNew (CITEC_AngleData* data, uint8_t div_ms)

  Summary:
    Checks if the Angle Data is current

  Description:
    This routine compares the timestamp of the Angel_Data with the current 
    time(systemtick) and returns a true if the data is current

  Precondition:
    AS5048A_Initialize() needs to be called once before

  Parameters:
    CITEC_AngleData* data: The Angle value which should be used
    uint8_t div     : max age in ms that is accaptable for the Rotary data

  Returns:
    true if the data is current enough
    false if the data is to old

  Remarks:
    To be used after peek from Queue
  
 */
bool RotaryEncoderReceivedNew(CITEC_AngleData* data, uint8_t div_ms);

//minor Funktions to convert the Angle Values
uint16_t DegToRaw(uint16_t deg){ if((deg>360)||(deg<0)) SYS_ASSERT(false, "The Degree value is invalid"); return (deg*16383/360);}
uint16_t RawToDeg(int16_t raw) {if((raw>ANGLEMAXVAL_RAW)||(raw<0)) SYS_ASSERT(false, "The Bit value is invalid"); return((raw*360)/16383); }



// *****************************************************************************
// *****************************************************************************
// Section: Further Funktions
// Following funktions are implemented according to the datasheet but are not
// tested in any way and the funktionality needs to be verified!
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    int8_t AS5048A_Write (int16_t adr, int16_t data)

  Summary:
    Reads one Register of the Sensor

  Description:
    This routine Writes one register of te Sensor via SPI by 
    adding two protokoll bits to the 16bit data string and verifyes the
    if send was succsesfull

  Precondition:
    CITEC_AS5048AInitialize() needs to be called once before

  Parameters:
    int16_t adr:   holds the Adress that is to be writen to
    int16_t data: will hold the 16bit data for the Register

  Returns:
    0 on Sucsess
    1 on parity error
    2 on previous send error

  Remarks:
    This routine is called when needed by other funktions.
 */
int8_t AS5048A_Write(uint16_t adr, uint16_t data);

/*******************************************************************************
  Function:
    int8_t AS5048A_SetZeroPossition (int16_t Zero)

  Summary:
    Sets a user devined Zero Possition (themporeily)

  Description:
    This routine Sets a themporare user devined Zero Possition for the Motor

  Precondition:
    CITEC_AS5048AInitialize() needs to be called once before

  Parameters:
    int16_t *Zero: Holds the current read data of the new Zero Possition

  Returns:
    0 on Sucsess

  Remarks:
    None.
 */
int8_t AS5048A_SetZeroPosition(uint16_t Zero);

/*******************************************************************************
  Function:
    int8_t AS5048A_ProgrammZeroPosition (void)

  Summary:
    Programmes a user devined Zero Possition

  Description:
    This routine Programmes a user devined Zero Possition for the Motor
    which will be keept and stored as absolute zero for ever.

  Precondition:
    CITEC_AS5048AInitialize() needs to be called once before

  Parameters:
    None.

  Returns:
    0 on Sucsess

  Remarks:
    WARNING: Can be used only once!!!
 */
int8_t AS5048A_ProgrammZeroPosition(void);


#endif /* AS5048A_H */

/*******************************************************************************
 End of File
 */



