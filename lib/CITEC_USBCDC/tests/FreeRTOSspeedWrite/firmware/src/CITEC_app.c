/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_app.c

  @Summary
    Example of usage of CITEC_USBCDC in Realtime Mode.

  @Description
    Example of usage of CITEC_USBCDC in Realtime Mode.
    don't forget to define the macro USE_RTOS in the compiler settings
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "CITEC_app.h"
//#include "FreeRTOSConfig.h"
//#include "system_config.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

APP_DATA appData;

/*******************************************************************************
  Function:
    void CITECAppInit(void)

  Remarks:
    See prototype in CITEC_app.h.
 ******************************************************************************/

void APP_Initialize ( void )
{
    // Place the App state machine in its initial state
    appData.state = APP_STATE_INIT;
    appData.counter = 0;
}

/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
void APP_Tasks ( void )
{
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            // According to iObject+ USBCDC lib example: continue only if RxQueue has been created
            if (qh_USBCDC_RxQueue!=NULL)
                appData.state = APP_STATE_WAIT_FOR_GO;
        } break;

        case APP_STATE_WAIT_FOR_GO:
        {
            // Wait for any input from USB host to continue
            if (xQueueReceive(qh_USBCDC_RxQueue, &appData.USBCDCRx_Data, 0) == pdPASS)
            {
                switch(appData.USBCDCRx_Data.ca_Message[0])
                {
                    case '1':
                        appData.state = APP_STATE_SMALL_STRING_AUTO;
                        break;
                    case '2':
                        appData.state = APP_STATE_SMALL_STRING_SENDNOW;
                        break;
                    case '3':
                        appData.state = APP_STATE_LONG_STRING_AUTO;
                        break;
                    case '4':
                        appData.state = APP_STATE_LONG_STRING_SENDNOW;
                        break;
                    case '5':
                        appData.state = APP_STATE_MULTI_STRING_AUTO;
                        break;
                    case '6':
                        appData.state = APP_STATE_VERYLONG_STRING_AUTO;
                        break;
                    case '7':
                        appData.state = APP_STATE_NONFULL_TIMEOUT;
                        break;
                    default:
                        appData.state = APP_STATE_UNKNOWN;
                        break;
                }
                cdcData.b_isDataAvailable = false;
            }
        } break;
        

        case APP_STATE_SMALL_STRING_AUTO:
        {
            if(cdcData.b_isConfigured)
            {
                uint8_t ui8;
                for (ui8 = 0;ui8 < 10; ui8++) // Send out 500 characters, while not calling SendNow
                {
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, "count:%ld it:%d ", appData.counter,ui8);
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    UBaseType_t ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);                  
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    appData.counter++;
                }
            }
            if (appData.counter >= 1000)
            {
                appData.counter = 0;
                appData.state = APP_STATE_WAIT_FOR_GO;
            }

                   
        } break;

        case APP_STATE_SMALL_STRING_SENDNOW:
        {
            if(cdcData.b_isConfigured)
            {
                uint8_t ui8;
                for (ui8 = 0;ui8 < 10; ui8++) // Send out 500 characters, while calling SendNow every 50 characters.
                {
                    //CITEC_USBCDC_WriteData(makeCDCString0("A12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678\n")); // 100 characters
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, "count:%ld it:%d ", appData.counter,ui8);
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    UBaseType_t ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);                  
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    CITEC_USBCDC_SendNow();
                    appData.counter++;
                }
            }
            if (appData.counter >= 1000)
            {
                appData.counter = 0;
                appData.state = APP_STATE_WAIT_FOR_GO;
            }

                   
        } break;
                case APP_STATE_LONG_STRING_AUTO:
        {
            if(cdcData.b_isConfigured)
            {
                uint8_t ui8;
                for (ui8 = 0;ui8 < 10; ui8++) // Send out 500 characters, while not calling SendNow
                {
                    if (ui8 == 6)
                        ui8 = 6;
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, "count:%ld it:%d ", appData.counter,ui8);
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    char ca_longString[601] = {"A123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                            "B123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                            "C123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                            "D123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                            "E123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                            "F123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
                            "\n"}; // > 512 characters
                    CITEC_USBCDC_Write(ca_longString, 601);
                    UBaseType_t ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);                  
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    appData.counter++;
                }
            }
            if (appData.counter >= 100)
            {
                appData.counter = 0;
                appData.state = APP_STATE_WAIT_FOR_GO;
            }

                   
        } break;
        
          case APP_STATE_LONG_STRING_SENDNOW:
        {
            if(cdcData.b_isConfigured)
            {
                uint8_t ui8;
                for (ui8 = 0;ui8 < 10; ui8++) // Send out 500 characters, while not calling SendNow
                {
                    if (ui8 == 6)
                        ui8 = 6;
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, "count:%ld it:%d ", appData.counter,ui8);
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    char ca_longString[601] = {"A123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "B123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "C123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "D123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "E123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "F123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "\n"}; // > 512 characters
                    CITEC_USBCDC_Write(ca_longString, 601);
                    UBaseType_t ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);                  
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    CITEC_USBCDC_SendNow();
                    appData.counter++;
                }
            }
            if (appData.counter >= 100)
            {
                appData.counter = 0;
                appData.state = APP_STATE_WAIT_FOR_GO;
            }

                   
        } break;
        
        case APP_STATE_VERYLONG_STRING_AUTO:
        {
            if(cdcData.b_isConfigured)
            {
                uint8_t ui8;
                for (ui8 = 0;ui8 < 10; ui8++) // Send out 500 characters, while not calling SendNow
                {
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, "count:%ld it:%d ", appData.counter,ui8);
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    char ca_longString[1101] = {"A123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "B123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "C123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "D123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "E123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "F123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "G123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "H123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "I123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "J123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "
                            "K123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456# "                          
                            "\n"}; // 1101 characters
                    CITEC_USBCDC_Write(ca_longString, 1101);
                    UBaseType_t ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);                  
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    appData.counter++;
                }
            }
            if (appData.counter >= 100)
            {
                appData.counter = 0;
                appData.state = APP_STATE_WAIT_FOR_GO;
            }

                   
        } break;
        
        case APP_STATE_MULTI_STRING_AUTO:
        {
            if(cdcData.b_isConfigured)
            {
                uint8_t ui8;
                for (ui8 = 0;ui8 < 10; ui8++) // Send out 500 characters, in 5 set of 100 bytes
                {
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, "count:%ld it:%d ", appData.counter,ui8); // 17
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    UBaseType_t ul_spaceInQueue;
                    
                    CITEC_USBCDC_WriteData(makeCDCString0("A123456789012345678901234567890123456789012345678012345678901234567890012345678901")); // //100 90 60 50 characters
                    ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);           // 33        
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    CITEC_USBCDC_WriteData(makeCDCString0("B123456789012345678901234567890123456789012345678901234567890123456789012345678901")); // 100 characters
                     ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);           // 33        
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    CITEC_USBCDC_WriteData(makeCDCString0("C123456789012345678901234567890123456789012345678901234567890123456789012345678901")); // 100 characters
                     ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);           // 33        
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    CITEC_USBCDC_WriteData(makeCDCString0("D123456789012345678901234567890123456789012345678901234567890123456789012345678901")); // 100 characters
                     ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);           // 33        
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    CITEC_USBCDC_WriteData(makeCDCString0("E1234567890123456789012345678901234567890123456789012345678901234")); // 100 characters
                    ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);           // 33        
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    //CITEC_USBCDC_SendNow();
                    appData.counter++;
                }
            }
            if (appData.counter >= 200)
            {
                appData.counter = 0;
                appData.state = APP_STATE_WAIT_FOR_GO;
            }
//CITEC_USBCDC_WriteData(makeCDCString0("A12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678\n")); // 100 characters
                   
        } break;
        
        case APP_STATE_NONFULL_TIMEOUT:
        {
            if(cdcData.b_isConfigured)
            {
                CITEC_USBCDC_WriteData(makeCDCString0("starting sending then waiting 500 ms\n"));
                CITEC_USBCDC_SendNow();
                uint8_t ui8;
                for (ui8 = 0;ui8 < 10; ui8++) // Send out 500 characters, while calling SendNow every 50 characters.
                {
                    //CITEC_USBCDC_WriteData(makeCDCString0("A12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678\n")); // 100 characters
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, "count:%ld it:%d ", appData.counter,ui8);
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    CITEC_USBCDC_WriteData(makeCDCString0("should be out 100 ms after last message\n"));
                    UBaseType_t ul_spaceInQueue = uxQueueSpacesAvailable(qh_USBCDC_TxQueue);
                    appData.USBCDCRx_Data.u32_Length = sprintf(appData.USBCDCRx_Data.ca_Message, " ,buf %c, buflen %u, freequeue %lu\n", c_nameBufferUsed, cdcData.ui16_writebufferStart, ul_spaceInQueue);                  
                    CITEC_USBCDC_WriteData(&appData.USBCDCRx_Data);
                    
                    vTaskDelay(pdMS_TO_TICKS(500));
                    CITEC_USBCDC_WriteData(makeCDCString0("end 500 ms wait\n"));
                    CITEC_USBCDC_SendNow();
                    appData.counter++;
                }
            }
            if (appData.counter >= 20)
            {
                appData.counter = 0;
                appData.state = APP_STATE_WAIT_FOR_GO;
            }

                   
        } break;
        
        case APP_STATE_UNKNOWN:
        default:                            // The default state should never be executed
        {
            if(cdcData.b_isConfigured)
            {
                CITEC_USBCDC_WriteData(makeCDCString0("\n Choose a test with its number. Tests available are :\n"));
                CITEC_USBCDC_WriteData(makeCDCString0("\t1: SMALL_STRING_AUTO\n"));
                CITEC_USBCDC_WriteData(makeCDCString0("\t2: SMALL_STRING_SENDNOW\n"));
                CITEC_USBCDC_WriteData(makeCDCString0("\t3: LONG_STRING_AUTO\n"));
                CITEC_USBCDC_WriteData(makeCDCString0("\t4: LONG_STRING_SENDNOW\n"));
                CITEC_USBCDC_WriteData(makeCDCString0("\t5: MULTI_STRING_AUTO\n"));
                CITEC_USBCDC_WriteData(makeCDCString0("\t6: VERYLONG_STRING_AUTO\n"));
                CITEC_USBCDC_WriteData(makeCDCString0("\t7: NONFULL_TIMEOUT\n"));
                CITEC_USBCDC_SendNow();
                appData.state = APP_STATE_WAIT_FOR_GO; 
            }                   
        } break;
    }
}

/* *****************************************************************************
 End of File
 */
