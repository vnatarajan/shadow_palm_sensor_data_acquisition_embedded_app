/* ************************************************************************** */
/** Descriptive File Name

  @Company
 CITEC

  @File Name
    CITEC_app.h

  @Summary
    Definition of the app

  @Description
    Definition of the app
 */
/* ************************************************************************** */

#ifndef _CITEC_app_H    /* Guard against multiple inclusion */
#define _CITEC_app_H

#include "CITEC_USBCDC.h"

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

typedef enum
{
	/* Application's state machine's initial state. */
    APP_STATE_INIT=0,
    APP_STATE_WAIT_FOR_GO,
    APP_STATE_SMALL_STRING_AUTO,
    APP_STATE_SMALL_STRING_SENDNOW,
    APP_STATE_MULTI_STRING_AUTO,
    APP_STATE_LONG_STRING_AUTO,
    APP_STATE_LONG_STRING_SENDNOW,
    APP_STATE_VERYLONG_STRING_AUTO,
    APP_STATE_NONFULL_TIMEOUT,
    APP_STATE_UNKNOWN
} APP_STATES;

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */

typedef struct
{
    /* The application's current state */
    APP_STATES state;
    uint32_t counter;
    CDC_DataType USBCDCRx_Data;
} APP_DATA;

void APP_Initialize ( void );
void APP_Tasks ( void );

#endif /* _CITEC_app_H */

/* *****************************************************************************
 End of File
 */
