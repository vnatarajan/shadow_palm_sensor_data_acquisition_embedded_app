/*******************************************************************************
  MPLAB Harmony System Configuration Header

  File Name:
    system_config.h

  Summary:
    Build-time configuration header for the system defined by this MPLAB Harmony
    project.

  Description:
    An MPLAB Project may have multiple configurations.  This file defines the
    build-time options for a single configuration.

  Remarks:
    This configuration header must not define any prototypes or data
    definitions (or include any files that do).  It only provides macro
    definitions for build-time configuration options that are not instantiated
    until used by another MPLAB Harmony module or application.

    Created with MPLAB Harmony Version 2.06
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _SYSTEM_CONFIG_H
#define _SYSTEM_CONFIG_H

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
/*  This section Includes other configuration headers necessary to completely
    define this configuration.
*/
//#include "peripheral/ports/plib_ports.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

extern "C" {

#endif
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: System Service Configuration
// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
/* Common System Service Configuration Options
*/
#define SYS_VERSION_STR           "2.06"
#define SYS_VERSION               20600

// *****************************************************************************
/* Clock System Service Configuration Options
*/
#define SYS_CLK_FREQ                        200000000ul
#define SYS_CLK_BUS_PERIPHERAL_1            100000000ul
#define SYS_CLK_BUS_PERIPHERAL_2            100000000ul
#define SYS_CLK_BUS_PERIPHERAL_3            100000000ul
#define SYS_CLK_BUS_PERIPHERAL_4            100000000ul
#define SYS_CLK_BUS_PERIPHERAL_5            100000000ul
#define SYS_CLK_BUS_PERIPHERAL_7            200000000ul
#define SYS_CLK_BUS_PERIPHERAL_8            200000000ul
#define SYS_CLK_CONFIG_PRIMARY_XTAL         24000000ul
#define SYS_CLK_CONFIG_SECONDARY_XTAL       0ul
#define SYS_CLK_CONFIG_FREQ_ERROR_LIMIT     10
#define SYS_CLK_WAIT_FOR_SWITCH             true
#define SYS_CLK_ON_WAIT                     OSC_ON_WAIT_IDLE 
   
/*** Ports System Service Configuration ***/
#define SYS_PORT_A_ANSEL        0x3900		// DDxx xDDx DDDD DDDD = 0x3900
#define SYS_PORT_A_TRIS         0x3900		// 00xx x00x 0000 0000 = 0x3900 (was 0x3FF7 missing soft off and hibernate as out)
#define SYS_PORT_A_LAT          0xC002		// 11xx x00x 0000 0010 = 0xC002 // SCL, SDA high in case they become outputs
#define SYS_PORT_A_ODC          0xC000		// 11xx x00x 0000 0000 = 0xC000
#define SYS_PORT_A_CNPU         0xC000		// 11xx x00x 0000 0000 = 0xC000
#define SYS_PORT_A_CNPD         0x0000		// OK
#define SYS_PORT_A_CNEN         0x0000		// OK

#define SYS_PORT_B_ANSEL        0x00C1		// DDDD DDDD AADD DDDA = 0x00C1 // PGED2 and PGEC2 as analog as they cannot have pull-up's or down's activated (was 0x0001)
#define SYS_PORT_B_TRIS         0x01C3		// 0000 0001 1100 0011 = 0x01C3 (was 0x0103 and before 0xFF33 missing wlan clk and wlan cs as out + NC pins)
#define SYS_PORT_B_LAT          0xC000		// 1100 000- --00 00-- = 0xC000 (was 0x8000, idle state of SPI CLK in Mode 3 is high, before 0x0000, missing latch WLAN CS high)
#define SYS_PORT_B_ODC          0x0000		// OK
#define SYS_PORT_B_CNPU         0x0000		// OK
#define SYS_PORT_B_CNPD         0x0000 		// (was 0x0002, no USB_ON pull-down needed, as there is a physical resistor on PCB)
#define SYS_PORT_B_CNEN         0x0000		// OK

#define SYS_PORT_C_ANSEL        0x1FE1		// DDDD xxxx xxxD DDDx = 0x0FE1 (was 0x57F8 but OC and DO are not analog and other pins do not exist)
#define SYS_PORT_C_TRIS         0x1FF5		// 0001 xxxx xxx1 010x = 0x1FF5 (was 0xD7FD but DO was not out, and NC were not out)
#define SYS_PORT_C_LAT          0x0000		// 000- xxxx xxx0 0-0x = 0x0000
#define SYS_PORT_C_ODC          0x0000		// OK
#define SYS_PORT_C_CNPU         0x0000		// OK
#define SYS_PORT_C_CNPD         0x0000		// OK
#define SYS_PORT_C_CNEN         0x0000		// OK

#define SYS_PORT_D_ANSEL        0x01C0		// DDDD DDDx xxDD DDDD = 0x01C0 (was 0x0040 but N/A pins should high)
#define SYS_PORT_D_TRIS         0x41C5		// 0100 000x xx00 0101 = 0x41C5 (was 0xFB7F but WLAN MOSI BNO_nReset were not, TACT MOSI and CLK were not out)
#define SYS_PORT_D_LAT          0x3232		// 0-11 001x xx11 0-1- = 0x3232 (was 0x3230 (idle state of SPI CLK in Mode 3 is high) and before was 0x0000 but CS_E needs latch high, the same for BNO_nReset and other NC pins)
#define SYS_PORT_D_ODC          0x0000		// OK
#define SYS_PORT_D_CNPU         0x0000		// OK
#define SYS_PORT_D_CNPD         0x0000		// OK
#define SYS_PORT_D_CNEN         0x0000		// OK

#define SYS_PORT_E_ANSEL        0xFC00		// xxxx xxDD DDDD DDDD = 0xFC00 (was 0xFC10)
#define SYS_PORT_E_TRIS         0xFC00		// xxxx xx00 0000 0000 = 0xFC00 (was 0xFFF5 but NC pins were not out)
#define SYS_PORT_E_LAT          0x0000		// OK
#define SYS_PORT_E_ODC          0x0000		// OK
#define SYS_PORT_E_CNPU         0x0000		// OK
#define SYS_PORT_E_CNPD         0x0000		// OK
#define SYS_PORT_E_CNEN         0x0000		// OK

#define SYS_PORT_F_ANSEL        0xCEC0		// xxDD xxxD xxDD DDDD = 0xCEC0
#define SYS_PORT_F_TRIS         0xCEC0		// xx00 xxx0 xx00 0000 = 0xCEC0 (was 0xEFFF but NC pins were not out)
#define SYS_PORT_F_LAT          0x0000		// OK
#define SYS_PORT_F_ODC          0x0000		// OK
#define SYS_PORT_F_CNPU         0x0000		// OK
#define SYS_PORT_F_CNPD         0x0000		// OK
#define SYS_PORT_F_CNEN         0x0000		// OK

#define SYS_PORT_G_ANSEL        0x0C3C		// DDDD xxDD DDxx xxDD = 0x0C3C (was 0x0187 but value made no sense)
#define SYS_PORT_G_TRIS         0x0CBC		// 0000 xx00 10xx xx00 = 0x0CBC (was 0xFFFF, but TAC MOSI and CLK were not out + NC pins were not out)
#define SYS_PORT_G_LAT          0x0040		// 0000 xx00 01xx xx00 = 0x0040 (was 0x0000, idle state of SPI CLK in Mode 3 is high)
#define SYS_PORT_G_ODC          0x0000		// OK
#define SYS_PORT_G_CNPU         0x0000		// OK
#define SYS_PORT_G_CNPD         0x0000		// OK
#define SYS_PORT_G_CNEN         0x0000		// OK


/*** Interrupt System Service Configuration ***/
#define SYS_INT                     true
/*** Timer System Service Configuration ***/
#define SYS_TMR_POWER_STATE             SYS_MODULE_POWER_RUN_FULL
#define SYS_TMR_DRIVER_INDEX            DRV_TMR_INDEX_0
#define SYS_TMR_MAX_CLIENT_OBJECTS      5
#define SYS_TMR_FREQUENCY               1000
#define SYS_TMR_FREQUENCY_TOLERANCE     10
#define SYS_TMR_UNIT_RESOLUTION         10000
#define SYS_TMR_CLIENT_TOLERANCE        10
#define SYS_TMR_INTERRUPT_NOTIFICATION  false

// *****************************************************************************
// *****************************************************************************
// Section: Driver Configuration
// *****************************************************************************
// *****************************************************************************
/*** Timer Driver Configuration ***/
#define DRV_TMR_INTERRUPT_MODE             true
#define DRV_TMR_INSTANCES_NUMBER           1
#define DRV_TMR_CLIENTS_NUMBER             1


 
// *****************************************************************************
// *****************************************************************************
// Section: Middleware & Other Library Configuration
// *****************************************************************************
// *****************************************************************************
/*** OSAL Configuration ***/
#define OSAL_USE_RTOS          9

/*** USB Driver Configuration ***/


/* Enables Device Support */
#define DRV_USBHS_DEVICE_SUPPORT      true

/* Disable Host Support */
#define DRV_USBHS_HOST_SUPPORT      false

/* Maximum USB driver instances */
#define DRV_USBHS_INSTANCES_NUMBER    1

/* Interrupt mode enabled */
#define DRV_USBHS_INTERRUPT_MODE      true


/* Number of Endpoints used */
#define DRV_USBHS_ENDPOINTS_NUMBER    3




/*** USB Device Stack Configuration ***/










/* The USB Device Layer will not initialize the USB Driver */
#define USB_DEVICE_DRIVER_INITIALIZE_EXPLICIT

/* Maximum device layer instances */
#define USB_DEVICE_INSTANCES_NUMBER     1

/* EP0 size in bytes */
#define USB_DEVICE_EP0_BUFFER_SIZE      64










/* Maximum instances of CDC function driver */
#define USB_DEVICE_CDC_INSTANCES_NUMBER     1










/* CDC Transfer Queue Size for both read and
   write. Applicable to all instances of the
   function driver */
#define USB_DEVICE_CDC_QUEUE_DEPTH_COMBINED 3





// *****************************************************************************
// *****************************************************************************
// Section: Application Configuration
// *****************************************************************************
// *****************************************************************************
/*** Application Defined Pins ***/

/*** Functions for LED_RE pin ***/
#define LED_REToggle() PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_E, PORTS_BIT_POS_3)
#define LED_REOn() PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_E, PORTS_BIT_POS_3)
#define LED_REOff() PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_E, PORTS_BIT_POS_3)
#define LED_REStateGet() PLIB_PORTS_PinGetLatched(PORTS_ID_0, PORT_CHANNEL_E, PORTS_BIT_POS_3)

/*** Functions for LED_YE pin ***/
#define LED_YEToggle() PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_E, PORTS_BIT_POS_4)
#define LED_YEOn() PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_E, PORTS_BIT_POS_4)
#define LED_YEOff() PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_E, PORTS_BIT_POS_4)
#define LED_YEStateGet() PLIB_PORTS_PinGetLatched(PORTS_ID_0, PORT_CHANNEL_E, PORTS_BIT_POS_4)


/*** Application Instance 0 Configuration ***/

//DOM-IGNORE-BEGIN
#ifdef __cplusplus
}
#endif
//DOM-IGNORE-END

#endif // _SYSTEM_CONFIG_H
/*******************************************************************************
 End of File
*/
