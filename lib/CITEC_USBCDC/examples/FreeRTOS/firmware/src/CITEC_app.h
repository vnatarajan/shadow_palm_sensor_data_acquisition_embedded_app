/* ************************************************************************** */
/** Descriptive File Name

  @Company
 CITEC

  @File Name
    CITEC_app.h

  @Summary
    Definition of the app

  @Description
    Definition of the app
 */
/* ************************************************************************** */

#ifndef _CITEC_app_H    /* Guard against multiple inclusion */
#define _CITEC_app_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "system_config.h"
#include "system_definitions.h"
#include "CITEC_USBCDC.h"


/*******************************************************************************
  Function:
    void CITECAppInit ( void )

  Summary:
    This function Initializes the application and generates its initial state.

  Parameters:
    None.

  Returns:
    None.

 */
void CITECAppInit(void);

/*******************************************************************************
  Function:
    void CITECAppTask ( void )

  Summary:
     MPLAB Harmony application Task routine.

  Description:
    This Task is the Example task on how to operate the CDC-Funktions.
    After a first Output is generated all incomming data will be mirrored out
    without delay:

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
     This routine must be called from SYS_Tasks() routine.
*/
void CITECAppTask(void);


#endif /* _CITEC_app_H */

/* *****************************************************************************
 End of File
 */
