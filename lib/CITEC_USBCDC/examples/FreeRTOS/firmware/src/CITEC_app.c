/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_app.c

  @Summary
    Example of usage of CITEC_USBCDC in Realtime Mode.

  @Description
    Example of usage of CITEC_USBCDC in Realtime Mode.
    don't forget to define the macro USE_RTOS in the compiler settings
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "CITEC_app.h"
#include "FreeRTOSConfig.h"
#include "system_config.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************


/*******************************************************************************
  Function:
    void CITECAppTask(void)

  Remarks:
    See prototype in CITEC_app.h.
 ******************************************************************************/
void CITECAppTask(void)
{
    CDC_DataType data;
    USBCDCErrors e_Sendstate;

    // Continue only after CDC_Initialize() has finished
    while(qh_USBCDC_RxQueue==NULL)
    {
        vTaskDelay(pdMS_TO_TICKS(10)); // Re-try in 10 msec
    }

    // TODO Guillaume this first send will actually pass, even if the com port is not open at host side
    do
    {//try to send until successful
        
        e_Sendstate=CDCERROR_Undefined;
        if (cdcData.b_isConfigured)
        {
            e_Sendstate=CDCERROR_NoError;
            
            // CITEC_USBCDC_WriteData() accepts data in the CDCDataTyp format.  
            // An example how the parameter can be generated manually:
            data.u32_Length=37;                                            //save string size
            strncpy(data.ca_Message, "Welcome to this USBCDC Demo FreeRTOS\n", data.u32_Length); //Store string (This can be int values or any other kind of data as well)
            e_Sendstate|=CITEC_USBCDC_WriteData(&data);                            //send string
        }
        //retry or continue after 100 ms
        vTaskDelay(pdMS_TO_TICKS(100)); 
       
    } while(!(cdcData.b_isConfigured && e_Sendstate==CDCERROR_NoError)); // Exits the while loop when the CDC is configured and e_Sendstate returns NoError
        
    // TODO Guillaume: one cannot wait for writeComplete because the first write 
    // occurred before the com port was open, so no write event will come back
    do
    {
        e_Sendstate=CDCERROR_NoError;
        // it generates a CDCDataTyp Format from given Data:
        // an easier way to send Strings is to use the makeCDCString function,
        e_Sendstate|=CITEC_USBCDC_WriteData(makeCDCString0("Your input Text will be mirrored from now on: \n\n"));
        vTaskDelay(pdMS_TO_TICKS(10)); 
    }while(e_Sendstate!=CDCERROR_NoError);

    // TODO Guillaume: currently the first two writes mess up something in memory or on the linux com port driver
    // some read events are generated, containing some characters of the first write one did when com port was still closed
    // if one do no initial writing there is no spurious messages coming back...
    // the example always read first... we want to write first a welcome message.
    
    CITEC_USBCDC_SendNow();
    // we will first reach here after every of the above messages have been displayed
    // successfully (an error will be thrown if no terminal is connected)
    
    while(true)
    {
        // wait until data was received
        if(xQueueReceive(qh_USBCDC_RxQueue, &data, portMAX_DELAY)==pdPASS)
        {
            e_Sendstate=CDCERROR_NoError;
            // echo received data right back
            e_Sendstate|=CITEC_USBCDC_Write(data.ca_Message, data.u32_Length);
            if (e_Sendstate!=CDCERROR_NoError || e_Sendstate!=CDCERROR_NothingToWrite)
            {
               if (e_Sendstate==CDCERROR_SendBufferFull)
               {
#ifdef LED_REToggle
                   LED_REToggle();
#endif
               }
               if (e_Sendstate==CDCERROR_MessageSizeToLong)
               {
#ifdef LED_YEToggle
                   LED_YEToggle();
#endif
               } 
            }
            else
            {
                CITEC_USBCDC_WriteData(makeCDCString0("\n"));   // add a line feed for better readability
            }
            // can be omitted, there is an automatic sending after a while
            CITEC_USBCDC_SendNow();
        }    
    }
}

/* *****************************************************************************
 End of File
 */
