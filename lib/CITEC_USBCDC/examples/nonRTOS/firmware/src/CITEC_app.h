/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    CITEC_app.h

  @Summary:
    This header file provides prototypes and definitions for the application.

  @Description
    This header file provides prototypes and definitions for the application.
 */
/* ************************************************************************** */

#ifndef _CITEC_app_H    /* Guard against multiple inclusion */
#define _CITEC_app_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system_definitions.h"
#include "CITEC_USBCDC.h"


// *****************************************************************************
/* Application States

  Summary:
    Application states enumeration

  Description:
    This enumeration defines the valid application states.  These states
    determine the behavior of the application at various times.
*/

typedef enum
{
    /* Application's state machine's initial state. */
    /* Application waits for device configuration*/
    APP_STATE_INIT=0,

    /* Application sends and welcome message and waits for it to be out*/
    APP_STATE_WELCOME,

    /* The application waits for message to be all out AND for a request to come in */
    APP_STATE_MIRROR_MESSAGE,

    /* Application Error state*/
    APP_STATE_ERROR

} APP_STATES;


// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */

typedef struct
{
    /* Application's current state*/
    APP_STATES e_state;
    CDC_DataType cdcData; 


} APP_DATA;


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Summary:
     MPLAB Harmony application initialization routine.

  Description:
    This function initializes the Harmony application.  It places the 
    application in its initial state and prepares it to run so that its 
    APP_Tasks function can be called.

  Precondition:
    All other system initialization routines should be called before calling
    this routine (in "SYS_Initialize").

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    APP_Initialize();
    </code>

  Remarks:
    This routine must be called from the SYS_Initialize function.
*/

void APP_Initialize ( void );


/*******************************************************************************
  Function:
    void APP_Tasks ( void )

  Summary:
    MPLAB Harmony Demo application tasks function

  Description:
    This routine is the Harmony Demo application's tasks function.  It
    defines the application's core logic and 

  Precondition:
    The system and application initialization ("SYS_Initialize") should be
    called before calling this.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    APP_Tasks();
    </code>

  Remarks:
    This routine must be called from SYS_Tasks() routine.
 */

void APP_Tasks ( void );


#endif /* _CITEC_app_H */
/*******************************************************************************
 End of File
 */

