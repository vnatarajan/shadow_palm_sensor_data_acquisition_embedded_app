/* ************************************************************************** */

#include "CITEC_USBCDC.h"

/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_app.c

  @Summary
    Example of usage of CITEC_USBCDC in non-Realtime mode.

  @Description
    Example of usage of CITEC_USBCDC in non-Realtime mode.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
#include "CITEC_app.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/
APP_DATA appData;



// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */
void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.e_state = APP_STATE_INIT; 
}


/*****************************************************
 * This function is called in every step of the
 * application state machine.
 *****************************************************/
bool APP_StateReset(void)
{
    /* This function returns true if the device
     * was reset  */
    bool retVal;

    if(cdcData.b_isConfigured == false)
    {
        appData.e_state = APP_STATE_INIT;
        cdcData.b_isReadComplete = true;
        cdcData.b_isDataAvailable = false;
        cdcData.b_isWriteComplete = true;
        cdcData.b_wasDataWritten = false;
        retVal = true;
    }
    else
    {
        retVal = false;
    }

    return(retVal);
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
void APP_Tasks (void )
{
    USBCDCErrors e_sendstate;
    e_sendstate=CDCERROR_NoError;
    
    // call the low-level task handler
    // pass the buffer that will be filled if data is there
    CITEC_USBCDC_USBDeviceTaskHandler(&appData.cdcData);
    
    /* Update the application state machine based
     * on the current state */ 
    switch(appData.e_state)
    {
        case APP_STATE_INIT:
            /* Check if the device was configured */
            if(cdcData.b_isConfigured)
            {
                // here the output is generated manually
                // the function USBCDCSend accepts any data in the CDCDataTyp Format.  
                strncpy(appData.cdcData.ca_Message, "Welcome to this USBCDC Demo nonRTOS\n", 36); //Store string (This can be int values or any other kind of data as well)
                appData.cdcData.u32_Length=36;                                            //save string size
                e_sendstate = CDCERROR_NoError;
                e_sendstate|=CITEC_USBCDC_WriteData(&appData.cdcData);                            //send string
                if (e_sendstate == CDCERROR_NoError)
                {
                    // an easier way to send Strings is to use the makeCDCString function,
                    // it generates a CDCDataTyp Format from given Data:
                    e_sendstate|=CITEC_USBCDC_WriteData(makeCDCString0("Your input Text will be mirrored from now on: \n\n"));
                    if (e_sendstate == CDCERROR_NoError)
                        appData.e_state = APP_STATE_MIRROR_MESSAGE;
                    
                }
                CITEC_USBCDC_SendNow();
            }
            break;
                

        case APP_STATE_MIRROR_MESSAGE:

            if(APP_StateReset())
                break;
            
            if(cdcData.b_isDataAvailable)
            {
                cdcData.b_isDataAvailable = false;
                e_sendstate=CDCERROR_NoError;
                /* Do something with the data */
                e_sendstate|=CITEC_USBCDC_WriteData(&appData.cdcData);    //send received data right back

                if (e_sendstate!=CDCERROR_NoError || e_sendstate!=CDCERROR_NothingToWrite)
                {
                   if (e_sendstate==CDCERROR_SendBufferFull)
                   {
                       LED_REToggle();
                   }
                   if (e_sendstate==CDCERROR_MessageSizeToLong)
                   {
                       LED_YEToggle();
                   } 
                }
                else
                {
                    CITEC_USBCDC_WriteData(makeCDCString0("\n"));   //do formating for better readability
                }
                //CITEC_USBCDC_SendNow();
            }  
            break;
        default:
            break;
    }
    CITEC_USBCDC_TxTaskHandler(&appData.cdcData);
}

/*******************************************************************************
 End of File
 */

