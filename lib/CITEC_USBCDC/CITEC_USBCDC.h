/*******************************************************************************
  MPLAB Harmony Application Header File

  Company:
    CITEC
    Microchip Technology Inc.

  File Name:
    CITEC_USBCDC.h

  Summary:
    This header file provides prototypes and definitions for the application.

  Description:
    This header file provides function prototypes and data type definitions for
    the application.  Some of these are required by the system (such as the
    "CDC_Initialize" and "CDC_Tasks" prototypes) and some of them are only used
    internally by the application (such as the "CDC_STATES" definition).  Both
    are defined here for convenience.
*******************************************************************************/

//DOM-IGNORE-BEGIN
// Original Author Microchip
// Modified by Luis Oberroehrmann
// Rewritten by Guillaume Walck
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
//DOM-IGNORE-END

#ifndef _CITEC_USBCDC_H
#define _CITEC_USBCDC_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "usb/usb_device_cdc.h"
// The next include should be in the system_config of the project.
// if not, copy it from CITEC_USBCDC_config.h.template
#include "CITEC_USBCDC_config.h"
#include "system_config.h"

#ifndef DISABLE_USBCDC_RTOS
  #ifdef OSAL_USE_RTOS
   #define USE_RTOS
  #endif
#endif

#ifdef USE_RTOS
    #include "FreeRTOS.h"
    #include "queue.h"
    #include "task.h"
#endif

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************



// *****************************************************************************
// *****************************************************************************
// Section: Configuration specific application constants
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application States

  Summary:
    Application states 

  Description:
    This defines the valid application states.  These states
    determine the behavior of the application at various times.
*/
#define USBDEVICETASK_OPENUSB_STATE             1
#define USBDEVICETASK_ATTACHUSB_STATE           2
#define USBDEVICETASK_PROCESSUSBEVENTS_STATE    3

#define USBDEVICETASK_NO_EVENT                  0
#define USBDEVICETASK_USBPOWERED_EVENT          1
#define USBDEVICETASK_USBCONFIGURED_EVENT       2
#define USBDEVICETASK_READDONECOM_EVENT         3
#define USBDEVICETASK_WRITEDONECOM_EVENT        4

#define CITEC_USBCDC_TX_TASK_WAIT_USB_CONF      1
#define CITEC_USBCDC_TX_TASK_PROCESSING         2
#define CITEC_USBCDC_TX_TASK_WAIT_COMPLETE      3

// *****************************************************************************
// *****************************************************************************
// Section: Data Types
// *****************************************************************************

#ifdef __PIC32MZ__
    #define MAKE_BUFFER_DMA_READY  __attribute__((coherent)) __attribute__((aligned(16)))
#else
#define MAKE_BUFFER_DMA_READY
#endif

// For debugging purpose only:
char c_nameBufferUsed;

/******************************************************
 * USBCDC Errors
 ******************************************************/

typedef enum
{
    CDCERROR_NoError                    = 0,
    CDCERROR_MessageSizeToLong          = 1,
    CDCERROR_NoUSBConnection            = 2,
    CDCERROR_SendBufferFull             = 3,
    CDCERROR_Undefined                  = 4,
    CDCERROR_NothingToWrite             = 5,
    CDCERROR_InternalProblem            = 6,
}USBCDCErrors;

/******************************************************
 * USBCDC Port Object
 ******************************************************/

typedef struct
{
    /* CDC instance number */
    USB_DEVICE_CDC_INDEX cdcInstance;

    /* Set Line Coding Data */
    USB_CDC_LINE_CODING setLineCodingData;

    /* Get Line Coding Data */
    USB_CDC_LINE_CODING getLineCodingData;

    /* Control Line State */
    USB_CDC_CONTROL_LINE_STATE controlLineStateData;

    /* Break data */
    uint16_t breakData;

}CDC_COM_PORT_OBJECT;

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */



//this struct is used intern to get not only the data but the number of received characters as well
typedef struct
{
    size_t u32_Length;
    uint32_t ca_Message;
} USBCDC_intern;


//struct is used for the usb cdc setup/control
typedef struct
{
    /* Device layer handle returned by device layer open function */
    USB_DEVICE_HANDLE deviceHandle;
    /* This demo supports 1 COM port */
    CDC_COM_PORT_OBJECT cdcCOMPortObject;

    /* Application's current state*/
    uint32_t ui32_USB_DeviceTaskState;
    uint32_t ui32_USB_DeviceTaskEvent;

    uint32_t ui32_numBytesRead;
    /* Device configured state */
    bool b_isConfigured;
    /* Data status */
    bool b_isReadComplete;  // internal status
    bool b_isDataAvailable;  // external status
    bool b_isWriteComplete;  // internal status
    bool b_wasDataWritten;  // external status
    /* timeout counter */
    uint16_t ui16_sendNowTimeoutCounter;
#ifdef USE_RTOS
    uint16_t ui16_writeTimeoutCounter;
#endif
    /* Sending buffer handling */
    uint16_t ui16_writebufferStart;  // where to concatenate the next data
    uint16_t ui16_sendBufferLength;  // the final length to be sent after buffer swap
    bool b_sendNow;
    
} CDC_DATA;

//DataType in which Send and received data will be handled in
typedef struct
{
    uint32_t u32_Length;
    char MAKE_BUFFER_DMA_READY ca_Message[USBCDC_BUFFER_SIZE];
} CDC_DataType;


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */


#ifdef USE_RTOS
// The main output queue. What ever is received on the COM, will be send to this queue
QueueHandle_t qh_USBCDC_RxQueue;
QueueHandle_t qh_USBCDC_TxQueue;
QueueHandle_t USBDeviceTask_EventQueue_Handle;
#endif

volatile CDC_DATA cdcData;   //global because needed in event handler and receive Task
// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************
/* These routines are called by drivers when certain events occur.
*/
void CITEC_USBCDC_USBDeviceEventHandler(USB_DEVICE_EVENT event, void * pData,
                                                            uintptr_t context);

USB_DEVICE_CDC_EVENT_RESPONSE CITEC_USBCDC_CDCEventHandler (
            USB_DEVICE_CDC_INDEX index , USB_DEVICE_CDC_EVENT event ,void* pData,
                                                            uintptr_t userData);

	
// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void CITEC_USBCDC_Initialize ( );

  Summary:
     MPLAB Harmony application initialization routine.

  Description:
    This function initializes the USB_CDC Functionality.  It places the 
    application in its initial state and configures the com-Port.

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    CITECUSBCDCInitialize();
    </code>

  Remarks:
    This routine is called once by CITECUSBCDCReceiveTask().
*/
bool CITEC_USBCDC_Initialize ( );



#ifdef USE_RTOS
/*******************************************************************************
  Function:
    void CITEC_USBCDC_ReceiveTask(void* p_arg);

  Summary:
    Calls the Receive Task handler.

  Description:
    Calls the Receive Task handler.

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    CITEC_USBCDC_ReceiveTask();
    </code>

  Remarks:
    This routine must be called from SYS_Tasks() routine.
 */

void CITEC_USBCDC_RxTask(void* p_arg);
/*******************************************************************************
  Function:
    void CITEC_USBCDC_TxTask(void* p_arg);

  Summary:
    Calls the Send Task handler.

  Description:
    Calls the Send Task handler.

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    CITEC_USBCDC_SendTask();
    </code>

  Remarks:
    This routine must be called from SYS_Tasks() routine.
 */
void CITEC_USBCDC_TxTask(void* p_arg);
#endif

/*******************************************************************************
  Function:
    void CITEC_USBCDC_USBDeviceTaskHandler(CDC_DataType *data);

  Summary:
    Handles the USB CDC scheduling part

  Description:
    This routine is the USB_CDC task logic. It handles the USBCDC connection 
    with the Host and if connected receives the Data and places them in a queue
    for general handling.

  Precondition:
    None.

  Parameters:
    CDCDataTyp* data: Struct holding the Datastring to receive data (in non RT)

  Returns:
    None.

  Remarks:
    This routine is not a task, but should be called in one.
 */
void CITEC_USBCDC_USBDeviceTaskHandler(CDC_DataType *data);

/*******************************************************************************
  Function:
    void CITEC_USBCDC_TxTaskHandler(CDC_DataType *data);

  Summary:
    Handles the Sending to USB scheduling part

  Description:
    This routine is the Sending to USB task logic. It handles the comWriteBuffer 
 as well as a queue in RT.

  Precondition:
    None.

  Parameters:
    CDCDataTyp* data: no used in non RT, pointer to a data structure to store 
                      messages extracted from the queue in RT

  Returns:
    None.

  Remarks:
    This routine is not a task, but should be called in one.
 */
void CITEC_USBCDC_TxTaskHandler(CDC_DataType *data);


/*******************************************************************************
  Function:
    bool CITEC_USBCDC_ConcatenateToWriteBuffer(char* ca_Message, uint32_t u32_Length)

  Summary:
    Concatenates to the WriteBuffer if there is space
 
  Description:
    This function will, if possible, concatenate to the write buffer, 
    and also set the sendnow accordingly, 
    otherwise returns false. 
 
  Parameters:
    char* ca_Message : char array holding the Datastring to append 
    uint32_t u32_Length amount of Bytes to append

  Returns:
    true if data was concatenated
    false if there is no space left (data was not concatenated)

 */
bool CITEC_USBCDC_ConcatenateToWriteBuffer(char* ca_Message, uint32_t u32_Length);

/*******************************************************************************
  Function:
    bool CITEC_USBCDC_ConcatenateToBuffer(uint8_t *ui8a_buffer, uint16_t *ui16_bufferStart, char* ca_Message, uint32_t u32_Length);

  Summary:
    Concatenates to given buffer at given start, if there is space
 
  Description:
    This function will, if possible, concatenate to the given buffer, 
    otherwise returns false. 
 
  Parameters:
    uint8_t *ui8a_buffer: buffer to concatenate to
    uint16_t *ui16_bufferStart: buffer start
    char* ca_Message : char array holding the Datastring to append 
    uint32_t u32_Length : amount of Bytes to append

  Returns:
    true if data was concatenated
    false if there is no space left (data was not concatenated)

 */
bool CITEC_USBCDC_ConcatenateToBuffer(uint8_t *ui8a_buffer, uint16_t *ui16_bufferStart, char* ca_Message, uint32_t u32_Length);


/*******************************************************************************
  Function:
    int8_t CITEC_USBCDC_Write(char* ca_Message, uint32_t u32_Length);

  Summary:
    Writing to the internal buffer, does not do the actual sending

  Description:
    This function will, if possible, store the requested send, 
    either by merging the data into the comWriteBuffer if small, 
    or by queuing the data in RT mode (in non RT no queuing possible) 
 
  Precondition:
    CITECUSBCDCSendTask() must be running.

  Parameters:
    char* ca_Message, : char array holding the Datastring to send 
    uint32_t u32_Length amount of Bytes to send

  Returns:
    enum: Holding an Error of type USBCDCErrors
 */

USBCDCErrors CITEC_USBCDC_Write(char* ca_Message, uint32_t u32_Length);

/*******************************************************************************
  Function:
    USBCDCErrors CITEC_USBCDC_WriteData(CDC_DataType* msg);

  Summary:
    Convenience function of CITEC_USBCDC_Write to directly use CDC_Datatype

  Description:
    see CITEC_USBCDC_Write
 
  Precondition:
    CITECUSBCDCSendTask() must be running.

  Parameters:
    CDCDataTyp* msg: Struct holding the Datastring to send and the amount of
                     Bytes to send

  Returns:
   enum: Holding an Error of type USBCDCErrors
 */
USBCDCErrors CITEC_USBCDC_WriteData(CDC_DataType* msg);


/*******************************************************************************
  Function:
    void CITEC_USBCDC_SendNow();

  Summary:
    Tell the sending task handler to send the output buffer
    without waiting for more data 
  
  Description:
    Flips the internal sendNow flag, to tell the sending task handler
    to send the current output buffer without waiting for more data

  Precondition:

 */
void CITEC_USBCDC_SendNow();

/*******************************************************************************
  Function:
    USBCDCErrors CITEC_USBCDC_InternalWrite(uint32_t ui32_msgLength, bool b_pendingData);

  Summary:
    Internal use only, do the actual sending of comWriteBuffer 
    and send back possible errors

  Description:
    This function will, if possible, send the comWriteBuffer via USBCDC to the 
    Host. The function will return an error if sending the data was not possible

  Precondition:

  Parameters:
    size_t msgSize : size of the comWriteBuffer to send
    bool pendingData: if true, tries to send in PENDING mode

  Returns:
    enum: Holding an Error of type USBCDCErrors

 */
USBCDCErrors CITEC_USBCDC_InternalWrite(uint32_t ui32_msgLength, bool b_pendingData);

/*******************************************************************************
  Function:
    CDCDataTyp* makeCDCString0 ( char* str0 )

  Summary:
    Generates a CDCDataTyp from a String to send with USBCDCSend

  Parameters:
    char* str0: String that should be send.

  Returns:
    CDCDataTyp*: pointer to the CDCDataTyp to be used in the USBCDCSend function

 */
CDC_DataType* makeCDCString0(char* str0);

#endif /* _CITEC_USBCDC_H */
/*******************************************************************************
 End of File
 */


