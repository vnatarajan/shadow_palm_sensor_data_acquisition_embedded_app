#####################################################################################
REVISIONS (list newer Revisions on top)
Date         Author             Description
2020-10-10   Guillaume Walck    Added double buffering and improved task interactions
2020-08-20   Guillaume Walck    Fully reworked the library
2018-10-15   Luis Oberröhrmann  First draft of the library
#####################################################################################

Abstract
The present CITEC_USBCDC library establishes and maintains a USB-CDC connection with
a USB host. The library can be included in conventional ("non-RTOS") coding or in
firmwares that make use of the FreeRTOS extensions.

The library interface consists of USB-CDC read and write functions that encapsulate
the low-level USB-CDC complexity.

#####################################################################################
Usage & library interface

Prerequisites:
* Microchip MPLAB X v5.40 or later, XC32 v2.50 or later and Microchip Harmony version
  2.06 (exactly this version!) are being used. You can get them from following links:
  https://www.microchip.com/mplab/mplab-x-ide
  https://www.microchip.com/en-us/development-tools-tools-and-software/mplab-xc-compilers
  https://www.microchip.com/mplab/mplab-harmony/mplab-harmony-v2
* The CITEC_USBCDC library should be located under Microchip Harmony folder structure
  under third_party\unibi\PIC32libs\CITEC_USBCDC.
  In Windows, with the recommended default installation path, this is:
  C:\microchip\harmony\v2_06\third_party\unibi\PIC32libs\CITEC_USBCDC
* The MPLAB X PIC32 Harmony project where the CITEC_USBCDC library will be included,
  has already been created and exists on disk. Ideally, and in order to adhere to
  UniBi/CITEC rules, the project should be located under third_party\unibi\apps.
  In Windows, the absolute path would thus be:
  C:\microchip\harmony\v2_06\third_party\unibi\apps\<project_name>
* If using FreeRTOS, FreeRTOS needs to already be included in the project and the
  project settings appropriately configured to use FreeRTOS.
* Before continuing, the project should output not a single error or warning when
  running "Clean & Build Main Project" (shortcut Shift+F11 key).

Step 1: copy "CITEC_USBCDC_config.h.template" from the library folder into
        your project under system_config\<configuration_name>\ as CITEC_USBCDC_config.h
        (full path under Windows:
         C:\microchip\harmony\v2_06\third_party\unibi\apps\<project_name>\firmware\
		    src\system_config\<configuration_name>\CITEC_USBCDC_config.h )

Step 2: In MPLAB X IDE, add to your project tree
        * CITEC_USBCDC.h
        * CITEC_USBCDC_config.h, and
        * CITEC_USBCDC.c
        For this, right click "Header Files" in the project tree and choose
        "Add Existing Item...".
        Navigate to the library folder and select CITEC_USBCDC.h.
        The "Store path as:" should be selected as "Relative" and the "Copy" checkbox
		should NOT be selected. Click "Select".
        Similarly, add CITEC_USBCDC.c to "Source Files".
        Add similarly "CITEC_USBCDC_config.h" from the project
		src\system_config\<configuration_name> into the "Header Files" as well.
		
Step 3: Include the CITEC_USBCDC library folder path in the project.
        For this, open the project properties by navigating to menu
		File -> Project Properties (<project_name>)
		Navigate to "XC32" and then to "xc32-gcc".
		Under "Option categories", select "Preprocessing and messages".
		Click on the triple dots behind "Include directories".
		Click "Browse..." and open
		C:\microchip\harmony\v2_06\third_party\unibi\PIC32libs\CITEC_USBCDC
		Click "OK" to close the include directories dialog and clock "OK" again to close
		the project properties dialog.

Step 4: In the project tree, unfold "Header Files", unfold "app/system_config/<configuration_name>".
		Open system_definitions.h and add in the included files section
        #include "CITEC_USBCDC.h"

Step 5: Open Microchip Harmony Configurator (MHC) (menu Tools -> Embedded -> MPLAB Harmony
        Configurator).
		(Do note that the project needs to be set as main project in MPLAB X IDE for the
		Harmony Configurator to open. The main project ist indicated by a bold font of the
		project name in the the projects tree. In case, the current project is not yet set
		as main project, right click the project name in the tree (typically the uppermost
		tree element) and select "Set as Main Project".)
		In the Harmony Framework Configurator, activate the USB Device Stack
        (under Harmony Framework Configuration -> USB Library) and set also the
        following settings (other parameters can be set at programmers will):
        * "RTOS Configuration" (in case using FreeRTOS)
        ** "Run Library Tasks As": "Standalone"
        ** check "Use Task Delay"
        *** "Task Delay": 50
        * check "Interrupt Mode"
        * check "USB Device"
        ** "Number of Endpoints Used": 3
        *** unfold "USB Device Instance 0"
        **** unfold "Function 1"
        ***** "Device Class": CDC
        **** "Product ID Selection": cdc_com_port_single_demo
             Do NOT change Vendor ID and Product ID strings from their default values of 0x04D8 and
			 0x000A respectively!
        Verify the correct MCU clock configuration under "Clock Diagram" tag and that the USB output
        clock is configured correctly for 48 MHz.

Step 6: Call library initialization in system_init.c before the initialization of your app:
        CITEC_USBCDC_Initialize();

Step 7: Add calls to your program to run the CITEC_USBCDC library code:
        (when using FreeRTOS only):
		Add following code to system_tasks.c function SYS_Tasks(),
		before the call to vTaskStartScheduler();

	    /* Create OS Thread for CDC Tasks. */
		if(xTaskCreate((TaskFunction_t) CITEC_USBCDC_RxTask, "CITEC_USBCDC_RxTask",
                USBCDCDeviceTask_Size, NULL, USBCDCDEVICETASK_PRIO, NULL)!=pdPASS)
			SYS_ASSERT(false, "Not enough heap to create CITEC_USBCDC_RxTask!");
   
	    /* Create OS Thread for Send tasks. */
		if(xTaskCreate((TaskFunction_t) CITEC_USBCDC_TxTask, "CITEC_USBCDC_TxTask",
                USBCDCDeviceTask_Size, NULL, USBCDCDEVICETASK_PRIO, NULL)!=pdPASS)
			SYS_ASSERT(false, "Not enough heap to create CITEC_USBCDC_TxTask!"); 

	    Check and adjust if necessary, the required heap size under MHC FreeRTOS configuration.
	    The MHC default value of 10240 double words (=40960 bytes) is typically not enough!
	   

        (in non-RTOS only)
	    Add into the main loop:
	    * at the top of the loop:
	      // pass the buffer that will be filled if data is there
	      CITEC_USBCDC_USBDeviceTaskHandler(&appData.cdcData);

	    * at the end of the loop, to handle the outgoing transfers:
	      CITEC_USBCDC_TxTaskHandler(&appData.cdcData);

Step 8: Start interfacing with the USBCDC:
        * In your application, use a struct of type CDC_DataType for storing
          received and/or sending data, e.g. define:
		  CDC_DataType data;
        * Check for configuration in initialization phase
          /* Check if the device was configured before sending to the device */
          if(cdcData.b_isConfigured) { <continue code> }
      
        (in FreeRTOS only):
		** Read received data
		   Example of a blocking code:
           xQueueReceive(qh_USBCDC_RxQueue, &data, portMAX_DELAY);
		   <parse received data>
		   
		   or alternatively with a timeout (here, an example that waits up to 10 milliseconds):
           if (xQueueReceive(qh_USBCDC_RxQueue, &data, 10 / portTICK_PERIOD_MS)==pdPASS)
		      { <parse received data> }
        ** Write data (always non-blocking):
           CITEC_USBCDC_WriteData(&data); 

		(in non-RTOS only):
		** check if data was received (non-blocking)
           if(cdcData.b_isDataAvailable) { <parse received data> }
        ** write data (non-blocking):
           CITEC_USBCDC_WriteData(&data); 
        ** Flush
           // at the end of your loop or if big chuncks of data are pending, flush the send buffers
           CITEC_USBCDC_SendNow();

Remark: CITEC_USBCDC_WriteData() copies the data, and buffers it, until there is enough
        to send, or buffer is filled over a limit, or CITEC_USBCDC_SendNow() is called.
        The buffer is processed in the TxHandler, so ensure it is called with correct
        frequency and priority.

Known Issue: if during compiling, an error of type eth_HashTable_Default.h occurs,
        you could fix the Harmony v2.06 bug:
		open file <harmony dir>\framework\peripheral\eth\templates\eth_HashTable_Default.h
		and change in function ETH_HashTableSet_Default() the line
        PLIB_ASSERT(((!eth->ETHCON1.RXEN)|| (!eth->ETHRXFC,HTEN))
		to
		PLIB_ASSERT(((!eth->ETHCON1.RXEN)|| (!eth->ETHRXFC.HTEN)) 
		(to denote the bit position in the register, there should be a dot and not comma).
 
#####################################################################################
Examples

The examples (located under library subfolder "examples") are written for iObjectPlus
circuit that uses a Microchip PIC32MZ.

The folder containts examples for FreeRTOS and also for non-RTOS. They show some
usecases of the library. After an initial greeting, the example will echo the received
data back to the host. The communication port settings used in the example are:
Port:	    0
Baud:	    115200
DataBits:	8
Parity:	    None
StopBits:	1
(these can be altered by changing corresponding values in CITEC_USBCDC_config.h)

Should this example be used with a different ciruit, then the device needs to 
be adapted in the MPLAB X project and the Harmony code generation needs to be rerun.
#####################################################################################
