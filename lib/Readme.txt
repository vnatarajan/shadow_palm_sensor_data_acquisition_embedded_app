Here you can find various in CITEC developed C libraries for
Microchip PIC32 microcontrollers.

The repository is meant to be checked out in the Harmony
subfolder, under Windows by default:
C:\microchip\harmony\<Version>\PIC32libs

Every library comes with a readme-file and an example program
demonstrating its usage.

To compile, you need the following software components:
1) Microchip MPLAB X IDE ( >= v4.35)
   http://microchip.com/mplab/mplab-x-ide
2) XC32/32++ C/C++ Compiler ( >= v2.41)
   http://www.microchip.com/mplab/compilers
   In case optimized code generation is required, the
   Neuroinformatics Group has one XC32 Pro Network Server
   License available (1 shared license -> 1 person at a time
   can compile).
3) Microchip Harmony Framework incl. FreeRTOS extensions
   ( >= 2.06)
   https://www.microchip.com/mplab/mplab-harmony/mplab-harmony-v2
   http://www.microchip.com/mplab/mplab-harmony
   Note that FreeRTOS does not need to be explicitly installed,
   it is already part of Harmony installation.
   Further documentation of FreeRTOS can be reached here:
   https://www.freertos.org/Documentation/RTOS_book.html   
   
In case you find an error in the libraries included here,
or there is a requirement for update, e.g., due to newer
Harmony or FreeRTOS framework changes, please feel free to
update the files. Please include your changes and a brief
description of the changes in the Readme-file of appropriate
library.

The examples should preferrably target a commercially available
board, such as a Microchip Starter Kit.

The current preference is on PIC32MZ EF Starter Kit with Crypto:
http://www.microchip.com/DevelopmentTools/ProductDetails.aspx?PartNO=DM320007-C

When writing own libraries, please prefix all files and public
functions, queues etc., with "CITEC_" to distinguish own code from
Microchip or other foreign code.
Example "CITEC_USBCDC_Init()".