/*******************************************************************************
  USB CDC library header file to be used with MPLAB Harmony

  Company:
    Bielefeld University / CITEC

  File Name:
    CITEC_USBCDC.h

  Description:
    This header file provides function prototypes and data type definitions for
    the CITEC USB CDC library.

 
  Authorship:
    Based on Microchip USB CDC examples shipped with MPLAB Harmony
    Modified  by Luis Oberr�hrmann
    Rewritten by Guillaume Walck
    Redacted  by Risto K�iva 
 *******************************************************************************/

#ifndef _CITEC_USBCDC_H
#define _CITEC_USBCDC_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "usb/usb_device_cdc.h"
// The next include should be in the system_config of the project.
// if not, copy it from CITEC_USBCDC_config.h.template
#include "CITEC_USBCDC_config.h"
#include "system_config.h"

// By defining DISABLE_USBCDC_RTOS, you can use non-RTOS version of the library,
// also with FreeRTOS, if desired.
#ifndef DISABLE_USBCDC_RTOS
  #ifdef OSAL_USE_RTOS
   #define USE_RTOS
  #endif
#endif

#ifdef USE_RTOS
    #include "FreeRTOS.h"
    #include "queue.h"
    #include "task.h"
#endif


// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

/* USB Device Task States

  Summary:
    USB Device Task States

  Description:
    This defines the valid USB Device Task states.  These states
    determine the behavior of the CITEC_USBCDC_USBDeviceTaskHandler() at various
    times.
*/
typedef enum 
{
    USBDEVICETASK_STATES_OPENUSB,
    USBDEVICETASK_STATES_ATTACHUSB,
    USBDEVICETASK_STATES_PROCESSUSBEVENTS,
} USBDEVICETASK_STATES;

/* USB Device Task Events

  Summary:
    USB Device Task Events

  Description:
    This defines the valid USB Device Task events.  These are used for
    communicating events from CITEC_USBCDC_USBDeviceEventHandler() and
    CITEC_USBCDC_CDCEventHandler() to CITEC_USBCDC_USBDeviceTaskHandler()
    to trigger appropriate actions.
*/
typedef enum
{
    USBDEVICETASK_EVENTS_NO_EVENT,
    USBDEVICETASK_EVENTS_USBPOWERED,
    USBDEVICETASK_EVENTS_USBCONFIGURED,
    USBDEVICETASK_EVENTS_READDONE,
    USBDEVICETASK_EVENTS_WRITEDONE,
} USBDEVICETASK_EVENTS;

/* USB CDC Tx Task States

  Summary:
    CITEC USB CDC library transmit task states

  Description:
    This defines the valid CITEC USB CDC library transmission task states.
    They determine the behavior of the CITEC_USBCDC_TxTaskHandler() at various
    times.
*/
typedef enum
{
    CITEC_USBCDC_TX_TASK_WAIT_USB_CONF,
    CITEC_USBCDC_TX_TASK_PROCESSING,
    CITEC_USBCDC_TX_TASK_WAIT_COMPLETE,
} CITEC_USBCDC_TX_TASK_STATES;


/* USBCDCWriteReturnValues

  Summary:
    CITEC USB CDC library return values

  Description:
    This lists possible return values of CITEC_USBCDC_Write() and
    CITEC_USBCDC_WriteData().
*/
typedef enum
{
    CDCWrite_OK,
    CDCWrite_MessageSizeTooLong,
    CDCWrite_NoUSBConnection,
    CDCWrite_SendBufferFull,
    CDCWrite_NothingToWrite,
    CDCWrite_sendNowSemaphoreOverflow,
    CDCWrite_WrongMsgSize,
    CDCWrite_InternalProblem,
    CDCWrite_Undefined,
} USBCDCWriteReturnValues;


// *****************************************************************************
// *****************************************************************************
// Section: Data Types
// *****************************************************************************
// *****************************************************************************

#ifdef __PIC32MZ__
    #define MAKE_BUFFER_DMA_READY  __attribute__((coherent)) __attribute__((aligned(16)))
#else
#define MAKE_BUFFER_DMA_READY
#endif

/******************************************************
 * USB CDC COM Port Object
 ******************************************************/
typedef struct
{
    USB_DEVICE_CDC_INDEX cdcInstance;                   // CDC instance number
    USB_CDC_LINE_CODING setLineCodingData;              // Set Line Coding Data
    USB_CDC_LINE_CODING getLineCodingData;              // Get Line Coding Data
    USB_CDC_CONTROL_LINE_STATE controlLineStateData;    // Control Line State
    USB_DEVICE_CDC_EVENT_DATA_SEND_BREAK breakData;     // Break data
} CDC_COM_PORT_OBJECT;

/******************************************************
  USB CDC library data

  Summary:
    Holds setup and control data of USB CDC library

  Description:
    This structure holds the library's data.
 ******************************************************/
typedef struct
{
    USB_DEVICE_HANDLE deviceHandle;             // Device layer handle returned by device layer open function
    CDC_COM_PORT_OBJECT cdcCOMPortObject;       // CDC communication port object

    USBDEVICETASK_STATES USB_DeviceTaskState;   // USB device task's current state
    USBDEVICETASK_EVENTS USB_DeviceTaskEvent;   // USB device task events

    uint32_t ui32_numBytesRead;                 // saves the number of incoming bytes read in CITEC_USBCDC_CDCEventHandler())
    bool b_isConfigured;                        // States if USB device is in a configured state

    // Data status
    bool b_isReadComplete;                      // internal status
    bool b_isDataAvailable;                     // external status
    bool b_isWriteComplete;                     // internal status
    bool b_wasDataWritten;                      // external status

    uint16_t ui16_sendNowTimeoutCounter;        // timeout counter
    bool b_sendNow;                             // A flag stating if we should send out the data in the transmit buffer (alternatively it also says if we should wait and gather more data)
} CDC_DATA;

/******************************************************
  USB CDC Data type

  Summary:
    The main data type in which the received and transmitted data is passed on.

  Description:
    This structure holds the incoming and outgoing payload data.
 ******************************************************/
typedef struct
{
    uint8_t ca_Message[USBCDC_BUFFER_SIZE];        // The data array
    uint32_t u32_Length;                        // Size of data actually used in the array. Data starts always from position 0.
    char c_BufferID;                            // Identifies the buffer used. Here, characters 'A' and 'B' are implemented. Required only for debugging.
} CDC_DataType;


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

#ifdef USE_RTOS
  QueueHandle_t qh_USBCDC_RxQueue;    // The main receive queue. What ever is received over the USB CDC, will be sent to this queue
  QueueHandle_t qh_USBCDC_TxQueue;
  QueueHandle_t USBDeviceTask_EventQueue_Handle;
#endif

volatile CDC_DATA cdcData;          // The setup and control variable of CITEC USB CDC library
	

// *****************************************************************************
// *****************************************************************************
// Section: Public Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    bool CITEC_USBCDC_Initialize (void);

  Summary:
     CITEC USB-CDC library initialization routine.

  Description:
    This function initializes the USB-CDC functionality. It places the 
    application in its initial state and configures the virtual communication port.
    If FreeRTOS extensions are used, it created the necessary queues, semaphores
    and tasks.

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    true if successful, false otherwise.

  Example:
    <code>
    if (!CITECUSBCDCInitialize())
    {
       // Handle error 
    }
    </code>

  Remarks:
    This routine should be called once. When FreeRTOS extensions are used, it
    should ideally be called before the scheduler is started.
*/
bool CITEC_USBCDC_Initialize(void);

#ifdef USE_RTOS
/*******************************************************************************
  Function:
    void CITEC_USBCDC_USBDeviceTask(void* p_arg);

  Summary:
    Calls the USB Device Task handler.

  Description:
    Calls the USB Device Task handler.

  Precondition:
    None.

  Parameters:
    A parameter passed from xTaskCreate().

  Returns:
    None.

  Example:
    <code>
    CITEC_USBCDC_ReceiveTask();
    </code>

  Remarks:
    This routine must be called from SYS_Tasks() routine.
 */
void CITEC_USBCDC_USBDeviceTask(void* p_arg);

/*******************************************************************************
  Function:
    void CITEC_USBCDC_TxTask(void* p_arg);

  Summary:
    Calls indefinitely CITEC_USBCDC_TxTaskHandler().

  Description:
    Calls indefinitely CITEC_USBCDC_TxTaskHandler().

  Precondition:
    None.

  Parameters:
     A parameter passed from xTaskCreate().

  Returns:
    None.

  Example:
    non-RTOS:
    <code>
    CITEC_USBCDC_TxTask();
    </code>

  Remarks:
    non-RTOS: This routine must be called from SYS_Tasks() routine.
    FreeRTOS: The TxTask is created automatically inside library initalization.
 */
void CITEC_USBCDC_TxTask(void* p_arg);
#endif

/*******************************************************************************
  Function:
    void CITEC_USBCDC_USBDeviceTaskHandler(CDC_DataType *data);

  Summary:
    Handles the USB CDC scheduling

  Description:
    This routine is the USB CDC task logic. It handles the USB CDC connection 
    with the USB host and if connected, receives the data and places it in a
    queue for general handling.

  Precondition:
    None.

  Parameters:
    CDCDataTyp* data: structure holding the received data

  Returns:
    None.

  Remarks:
    In FreeRTOS, this routine should be called in a indefinite task.
 */
void CITEC_USBCDC_USBDeviceTaskHandler(CDC_DataType *data);

/*******************************************************************************
  Function:
    void CITEC_USBCDC_TxTaskHandler(CDC_DataType *data);

  Summary:
    Handles the sending out of data to USB

  Description:
    This routine services the outgoing USB data.
    It handles the ps_USBCDC_TxWriteOutBuffer as well as a queue in FreeRTOS.

  Precondition:
    None.

  Parameters:
    CDCDataTyp* data: used only when FreeRTOS extensions are active.
                      Is a pointer to a data structure to store messages
                      extracted from the queue.

  Returns:
    None.

  Remarks:
    In FreeRTOS, this routine should be called in a indefinite task.
 */
void CITEC_USBCDC_TxTaskHandler(CDC_DataType *data);

/*******************************************************************************
  Function:
    USBCDCWriteReturnValues CITEC_USBCDC_Write(char* ca_Message, uint32_t _u32_Length);

  Summary:
    Writing to the internal buffer, does not do the actual sending

  Description:
    This function will, if possible, store the requested send, 
    either by merging the data into the ps_USBCDC_TxPrepareBuffer if small, 
    or by queuing the data in FreeRTOS mode (in nonRTOS, no queuing is possible)
 
  Precondition:
    CITEC_USBCDC_TxTask() must be running.

  Parameters:
    char* ca_Message:     char array holding the data to send 
    uint32_t _u32_Length: amount of bytes to send

  Returns:
    enum: Holding an Error of type USBCDCWriteReturnValues
 */

USBCDCWriteReturnValues CITEC_USBCDC_Write(char* ca_Message, uint32_t _u32_Length);

/*******************************************************************************
  Function:
    USBCDCWriteReturnValues CITEC_USBCDC_WriteData(CDC_DataType* msg);

  Summary:
    Convenience function of CITEC_USBCDC_Write)= to directly use CDC_DataZype

  Description:
    see CITEC_USBCDC_Write()
 
  Precondition:
     CITEC_USBCDC_TxTask() must be running.

  Parameters:
    CDCDataTyp* msg: Structure holding the Data to send out

  Returns:
   enum: Holding an Error of type USBCDCWriteReturnValues
 */
USBCDCWriteReturnValues CITEC_USBCDC_WriteData(CDC_DataType* msg);


/*******************************************************************************
  Function:
    void CITEC_USBCDC_SendNow();

  Summary:
    Tell the sending task handler to send the output buffer
    without waiting for further data.
  
  Description:
    Swaps ps_USBCDC_TxPrepareBuffer and ps_USBCDC_TxWriteOutBuffer, sets the
    internal sendNow flag, to tell the sending task handler to send the current
    ps_USBCDC_TxWriteOutBuffer without waiting for further data.
 */
void CITEC_USBCDC_SendNow();

/*******************************************************************************
  Function:
    CDCDataTyp* Str2CDCDataType ( char* p_ca )

  Summary:
    Generates a CDC_DataType from a string.

  Parameters:
    char* p_ca: string that should be sent.

  Returns:
    CDCDataTyp*: pointer to the CDC_DataType
 */
CDC_DataType* Str2CDCDataType(char* p_ca);

#endif /* _CITEC_USBCDC_H */
/*******************************************************************************
 End of File
 */


