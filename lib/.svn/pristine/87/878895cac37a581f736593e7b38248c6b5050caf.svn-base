#####################################################################################
REVISIONS (list newer Revisions on top)
Date         Author             Description
2020-08-20   Guillaume Walck    Rewritten from scratch.
2019-10-09   Tobias Schwank     Fixed little endian order of the data in linkData().
2019-25-04   Luis Oberröhrmann  Changed output handling to enable multiple
                                communication channels, minor bugfixes.
                                Example Program should run on any device now.
2019-01-02   Luis Oberröhrmann  First release of the library.
#####################################################################################

Abstract
This library handles the Serial Protocol for devices.
The protocol is described in detail at:
https://projects.cit-ec.uni-bielefeld.de/projects/agni-grasplab/wiki/Serial_protocol
The serial communication is *NOT* part of this library. This library is designed to
work with various serial interfaces, including multiple interfaces at once.
(Only one continous streaming is possible at the moment, currently the library always
responds to senders of any command)

#####################################################################################
Usage & library interface

Step 1: Add (but do not copy) to your project tree
         CITECSerialProtocolDefines.h,
         CITECSerialProtocol.h,
         CITECSerialProtocolTypes.h
         CITEC_USBCDC.h
         CITECSerialProtocolTask.h
     and
         CITECSerialProtocol.c
         CITEC_USBCDC.c
         CITECSerialProtocolTask.c
       to your project.
     copy CITEC_USBCDC_config.h.template as CITEC_USBCDC_config.h in system_config/<profile>/
        Add
         #include "CITEC_USBCDC.h"
         #include "CITECSerialProtocol.h"
         #include "CITECSerialProtocolTask.h"
        to system_config/<profile>/system_definitions.h

Step 2: Adapt defines in CITEC_USBCDC_config.h: 
 	    If required, increase the values of USBCDC_RECEIVEQUEUE_DEPTH and USBCDC_BUFFER_SIZE. 	

Step 3: Make your application/tasks use the Serial Interface:
 - in RTOS, send data from YOUR communication to qh_SerialDataUpdateQueue()
   Use the SerialDataUpdateStruct datatype to do so, setting the ui8_sensorDatagramID to the Logical Sensor ID (see step 4) corresponding to the data being sent
   and optionally the ui32_timestamp field with YOUR own timestamp, otherwise set 0 to use internal timestamps.
   Provide a RunRequest and a SetPeriodicity function for each task/sensor to permit start/stop/change periodicity from the serial protocol directly.
 - in nonRTOS, your main loop should call in this order (see example for detail)
   CITEC_SerialProtocol_TaskHandler  // handles incoming data
   // update your data here
   SP_packData(&spContainer, (void*)POINTERTOYOURDATA, 1); //ID 1
   SP_publish(&spContainer); // add to the internal sp container output buffer and cache to CDC
   CITEC_USBCDC_SendNow(); // effective write to CDC (call only once after all your data was packed and published)
   CITEC_USBCDC_TxTaskHandler(NULL); // handles outgoing data

Step 4: Adapt the SerialProtocol to your needs in system_init.c
 - Initialize the usbcdc task
   CITEC_USBCDC_Initialize();
 - Initialize the serial protocol task giving the deviceID (ask for a new deviceID to the maintainer if no existing one is suitable)
   CITEC_SerialProtocol_Initialize(iObjectPlus);
 - Set a callback to a SerialWrite Function of your choice.
   A CITEC_SerialProtocol_SerialWrite function using CITEC_USBCDC is offered but you can write your own.
   example :
   // register the write function
   CITEC_SerialProtocol_SetWriteCallback(&CITEC_SerialProtocol_SerialWrite);
 - Register as many Logical Sensors as you want to send over the Serial Protocol, providing 
   * SensorType (the type of sensing principle)
   * ChipType   (the correct number must be requested to the maintainer of the SerialProtocol, it identifies the correct parser to be used)
   * DataLength in bytes
   * callback to RunRequest (permits to start stop the sensor related task)
   * callback to SetPeriodicity (permits to adjust the periodicity of the task)
     example:
     // register the sensor config along with callbacks to runRequest and setPeriod of task producing data
     CITEC_SerialProtocol_RegisterSensor(SP_makeConfig(TactileSensorResistive, 0x20, 120, 0x001E), &SPI_RunRequest, &SPI_SetPeriod); // 30 x 100 us = 3 ms is the time to get all ADC for one channel
     CITEC_SerialProtocol_RegisterSensor(SP_makeConfig(IMU, 0x20, 26, 0x00F0), &I2C_BNO055_RunRequest, &I2C_BNO055_SetPeriod);
   Note: For each CITEC_SerialProtocol_RegisterSensor call, an internal Logical Sensor ID is incremented and associated to the registered sensor starting at ID 1
   In further communication (see step 3) the ID identifies which sensor data is being sent (using ui8_sensorDatagramID in the queue in RTOS or the ui8_senID argument in SP_packData in nonRTOS) 

Step 5: in RTOS only, start the USBCDC and SerialProtocol Tasks by adding the following code to SYS_Tasks()
        in system_tasks.c

    /* Create OS Thread for SerialProtocol tasks. */
    if(xTaskCreate((TaskFunction_t) CITEC_SerialProtocol_Task, "SerialProtocolTask", 1024, NULL, USBCDCDEVICETASK_PRIO+1, NULL) != pdPASS)
       SYS_ASSERT(false, "Task create for SerialProtocolTask");

#####################################################################################
Implemented commands:

- 0x00 : Stop streaming
- 0xC0 : Send System Configuration with Datagram ID 0x00
- 0xC1 : Set the Topology of the Logical Sensor IDs
- 0xC2 : Request Serial Number of the device
- 0xD0 : Set the periodicity of sensors (for a single and multiple sensors)
- 0xF0 : Ping Request
- 0xF1 : Start streaming continously for all

Implemented data output:

- 0x00 : System Configuration
- 0xDB : Debug Data
- 0xF0 : Ping Response
- 0xF1 : Serial Number
- 0XF2 : Topology Response
- 0xFE : Error output

#####################################################################################
TODO:
- Start streaming continously for selected
- Trigger (one and all)
- Extend functionality and include all commands defined in the Wiki
- Add option to stream data on multiple different channels/devices at once
#####################################################################################
Known Issues / Solutions:
1) If multiple communication channels are available, the device will always respond
   to all, therefore device b can stop a streaming to device a

2) If during compiling, an error of type eth_HashTable_Default.h occurs, please fix
   the Harmony v2.06 bug:
   open file <harmony dir>\framework\peripheral\eth\templates\eth_HashTable_Default.h
   and change in function ETH_HashTableSet_Default() the line
   PLIB_ASSERT(((!eth->ETHCON1.RXEN)|| (!eth->ETHRXFC,HTEN))
   to
   PLIB_ASSERT(((!eth->ETHCON1.RXEN)|| (!eth->ETHRXFC.HTEN)) 
   (to denote the bit position in the register, there should be a dot and not a comma).

#####################################################################################
Example:
The example should work with every PIC32, as long as a USB connection is available.
You do need to change the harmony configuration to match your controller!
(The example was written for the iObject+ Hardware (PIC32MZ2048EFM100).)
The example implements a mokery iObject+ with communication using the USBCDC Demo.
The example sensor configuration of the iObject+ is done in the init and some data is 
generated and updated every 5 and 10 milliseconds in the app_task.
see the #warnings for important changes/user addaptions.
