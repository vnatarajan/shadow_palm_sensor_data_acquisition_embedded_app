#include "imu.h"
#include "mplab_i2c_bitbanging.h"
#include "app_delay.h"

bool imu_init(void){
/* Make sure we have the right device */
      uint8_t id = BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_CHIP_ID_ADDR);
  if (id != BNO055_ID) {
    app_delay(1000); // hold on for boot
    id = BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_CHIP_ID_ADDR);
    if (id != BNO055_ID) {
      return false; // still not? ok bail
    }
  }

  
// put your setup code here, to run once:
// BNO055_SET_OPERATION_MODE(BNO055_I2C_ADDR, BNO055_OPERATION_MODE_NDOF); // Setting the BNO055 to NDOF mode (Fusion)
  BNO055_SET_OPERATION_MODE(BNO055_I2C_ADDR, BNO055_OPERATION_MODE_CONFIG);
  BNO055_WRITE_BYTE(BNO055_I2C_ADDR, BNO055_SYS_TRIGGER_ADDR, 0x20);
  app_delay(30);
    while (BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_CHIP_ID_ADDR) != BNO055_ID) {
      app_delay(10);
    }
  app_delay(50);
  BNO055_SET_OPERATION_MODE(BNO055_I2C_ADDR, BNO055_OPERATION_MODE_IMUPLUS); // setting the BNo055 to AMG mode 
  app_delay(10);
  BNO055_WRITE_BYTE(BNO055_I2C_ADDR, BNO055_PAGE_ID_ADDR, 0);
  return true;

}

struct quaternion imu_read(){
  struct quaternion q;
  q = BNO055_GET_QUATERNION();
  return q;
}


void BNO055_WRITE_BYTE(uint8_t i2c_slave_address, uint16_t register_address, uint8_t data) 
{
    i2c_beginTransmission(i2c_slave_address);
    i2c_write8(register_address);
    i2c_write8(data);
    i2c_endTransmission();
}

uint8_t BNO055_READ_BYTE(uint8_t i2c_slave_address, uint16_t register_address) 
{
    i2c_beginTransmission(i2c_slave_address);
    i2c_write8(register_address);
//    i2c_endTransmission(); COMMENTED BHARGAV
    return i2c_read8(i2c_slave_address);
}

void BNO055_SET_OPERATION_MODE(uint8_t address, uint8_t op_mode)
{
   BNO055_WRITE_BYTE(address, OPR_MODE, op_mode);
}

void readlen(uint8_t address, uint8_t *buffer_1, uint8_t len){
  i2c_beginTransmission(BNO055_I2C_ADDR);
  i2c_write8(address);
  i2c_endTransmission();
  i2c_requestFrom(BNO055_I2C_ADDR, len, buffer_1);
}


struct quaternion BNO055_GET_QUATERNION(){
  uint8_t buffer_qua[8];
  int i;
  struct quaternion q;

  for(i=0;i<8;i++) buffer_qua[i] = 0x0;
  int16_t x=0, y=0, z=0, w=0;
  readlen(BNO055_QUATERNION_DATA_W_LSB_ADDR, buffer_qua, 8);
  w = (((uint16_t)buffer_qua[1]) << 8) | ((uint16_t)buffer_qua[0]);
  x = (((uint16_t)buffer_qua[3]) << 8) | ((uint16_t)buffer_qua[2]);
  y = (((uint16_t)buffer_qua[5]) << 8) | ((uint16_t)buffer_qua[4]);
  z = (((uint16_t)buffer_qua[7]) << 8) | ((uint16_t)buffer_qua[6]);
  const float scale = (1.0/(1<<14));
  //const double scale = 1;
  q.w = scale * w;
  q.x = scale * x;
  q.y = scale * y;
  q.z = scale * z;
//  q ={ scale * w,  scale*x,  scale*y,  scale*z};
  return q;
}
void BNO055_GET_ACC(uint16_t *data){
    int i;
  if(!data){
    return;
  }
  uint8_t ACC_BUFFER[6]; 
  ACC_BUFFER[0] = BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_ACCEL_DATA_X_LSB_ADDR );
  ACC_BUFFER[1] = BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_ACCEL_DATA_X_MSB_ADDR );
  ACC_BUFFER[2] = BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_ACCEL_DATA_Y_LSB_ADDR );
  ACC_BUFFER[3] = BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_ACCEL_DATA_Y_MSB_ADDR );
  ACC_BUFFER[4] = BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_ACCEL_DATA_Z_LSB_ADDR );
  ACC_BUFFER[5] = BNO055_READ_BYTE(BNO055_I2C_ADDR, BNO055_ACCEL_DATA_Z_MSB_ADDR );

  int j = 0;
  for(i=0; i<6 ;i+=2){
    data[j] = ((ACC_BUFFER[i+1]<<8)|(ACC_BUFFER[i]&0xFF));
    j++;
  }
}

void MSB_LSB(uint8_t *p, uint8_t *q)
{
  int j = 0;
  int i;
  for(i=0; i<8 ;i+=2)
  {
    p[j] = ((q[i]<<8)|q[i+1]&0xFF);
    j++;
  }
}
