#ifndef IMU_H
#define IMU_H
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

/*operation modes of BNO055 values that can be written in OPR_MODE*/
#define BNO055_OPERATION_MODE_CONFIG 0x00
#define BNO055_OPERATION_MODE_ACCONLY 0x01
#define BNO055_OPERATION_MODE_MAGONLY 0x02
#define BNO055_OPERATION_MODE_GYRONLY 0x03
#define BNO055_OPERATION_MODE_ACCMAG 0x04
#define BNO055_OPERATION_MODE_ACCGYRO 0x05
#define BNO055_OPERATION_MODE_MAGGYRO 0x06
#define OPERATION_MODE_ANY_MOTION 0x07
#define BNO055_OPERATION_MODE_IMUPLUS 0x08
#define BNO055_OPERATION_MODE_COMPASS 0x09
#define BNO055_OPERATION_MODE_M4G 0x0A
#define BNO055_OPERATION_MODE_NDOF_FMC_OFF 0x0B
#define BNO055_OPERATION_MODE_NDOF 0x0C

/*Queternion regsister access*/
#define BNO055_QUATERNION_DATA_W_LSB_ADDR   (0X20)
#define BNO055_QUATERNION_DATA_W_MSB_ADDR   (0X21)
#define BNO055_QUATERNION_DATA_X_LSB_ADDR   (0X22)
#define BNO055_QUATERNION_DATA_X_MSB_ADDR   (0X23)
#define BNO055_QUATERNION_DATA_Y_LSB_ADDR   (0X24)
#define BNO055_QUATERNION_DATA_Y_MSB_ADDR   (0X25)
#define BNO055_QUATERNION_DATA_Z_LSB_ADDR   (0X26)
#define BNO055_QUATERNION_DATA_Z_MSB_ADDR   (0X27)

/*Array indec for reading data  */
#define BNO055_SENSOR_DATA_QUATERNION_WXYZ_W_LSB   (0)
#define BNO055_SENSOR_DATA_QUATERNION_WXYZ_W_MSB   (1)
#define BNO055_SENSOR_DATA_QUATERNION_WXYZ_X_LSB   (2)
#define BNO055_SENSOR_DATA_QUATERNION_WXYZ_X_MSB   (3)
#define BNO055_SENSOR_DATA_QUATERNION_WXYZ_Y_LSB   (4)
#define BNO055_SENSOR_DATA_QUATERNION_WXYZ_Y_MSB   (5)
#define BNO055_SENSOR_DATA_QUATERNION_WXYZ_Z_LSB   (6)
#define BNO055_SENSOR_DATA_QUATERNION_WXYZ_Z_MSB   (7)


/* Accel data register*/
#define BNO055_ACCEL_DATA_X_LSB_ADDR        (0X08)
#define BNO055_ACCEL_DATA_X_MSB_ADDR        (0X09)
#define BNO055_ACCEL_DATA_Y_LSB_ADDR        (0X0A)
#define BNO055_ACCEL_DATA_Y_MSB_ADDR        (0X0B)
#define BNO055_ACCEL_DATA_Z_LSB_ADDR        (0X0C)
#define BNO055_ACCEL_DATA_Z_MSB_ADDR        (0X0D)

/*Mag data register*/
#define BNO055_MAG_DATA_X_LSB_ADDR          (0X0E)
#define BNO055_MAG_DATA_X_MSB_ADDR          (0X0F)
#define BNO055_MAG_DATA_Y_LSB_ADDR          (0X10)
#define BNO055_MAG_DATA_Y_MSB_ADDR          (0X11)
#define BNO055_MAG_DATA_Z_LSB_ADDR          (0X12)
#define BNO055_MAG_DATA_Z_MSB_ADDR          (0X13)

/*Gyro data registers*/
#define BNO055_GYRO_DATA_X_LSB_ADDR         (0X14)
#define BNO055_GYRO_DATA_X_MSB_ADDR         (0X15)
#define BNO055_GYRO_DATA_Y_LSB_ADDR         (0X16)
#define BNO055_GYRO_DATA_Y_MSB_ADDR         (0X17)
#define BNO055_GYRO_DATA_Z_LSB_ADDR         (0X18)
#define BNO055_GYRO_DATA_Z_MSB_ADDR         (0X19)

#define BNO055_SYS_TRIGGER_ADDR 0X3F
#define BNO055_CHIP_ID_ADDR 0X00
#define BNO055_PAGE_ID_ADDR 0X07
#define BNO055_ID (0xA0)

#define OPR_MODE 0x3D // Opereation mode register 

/*i2c Slave address*/
#define BNO055_I2C_ADDR 0x28 //Primary address when COM3 is LOW

struct quaternion{
  float w;
  float x;
  float y;
  float z;
};



bool imu_init(void);
uint8_t BNO055_READ_BYTE(uint8_t i2c_slave_address, uint16_t register_address);
void BNO055_SET_OPERATION_MODE(uint8_t address, uint8_t op_mode);
void readlen(uint8_t address, uint8_t *buffer_1, uint8_t len);
void BNO055_GET_ACC(uint16_t *data);
void MSB_LSB(uint8_t *p, uint8_t *q);
void BNO055_WRITE_BYTE(uint8_t i2c_slave_address, uint16_t register_address, uint8_t data); 
struct quaternion BNO055_GET_QUATERNION();
struct quaternion imu_read();
#endif
