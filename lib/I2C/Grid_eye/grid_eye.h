#ifndef GRID_EYE_H
#define GRID_EYE_H
void grid_eye_init();
void grid_eye_read(int16_t *pixels);
#endif
