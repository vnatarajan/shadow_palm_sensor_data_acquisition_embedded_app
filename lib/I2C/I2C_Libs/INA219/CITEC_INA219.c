/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_INA219.c

  @Summary
    Interface funktions to use the INA219

 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */
#include "driver/i2c/src/drv_i2c_local.h"
#include "CITEC_INA219.h"


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */


/******************************************************************************/
//                            Public Funktions
/******************************************************************************/

/******************************************************************************
  Function:
    void CITEC_INA219_Init(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h
 */
void CITEC_Init(void)
{
    //Open the I2C Module
    INA219_I2C_Handle = DRV_I2C_Open(DRV_I2C_INA219, DRV_IO_INTENT_READWRITE);
    if(INA219_I2C_Handle==0)
    {
        SYS_ASSERT(false, "Opening the I2C Module failed");
    }
   
    CITEC_I2CSemaphore=xSemaphoreCreateBinary();
    while(CITEC_I2CSemaphore==NULL)
    {
        SYS_ASSERT(false, "CITEC_SPISemaphore could not be created.");
        while(1);
    }
}

/******************************************************************************
  Function:
    void INA219_setCalibration_32V_2A (INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
void INA219_setCalibration_32V_2A(INA219* obj)
{
  // By default we use a pretty huge range for the input voltage,
  // which probably isn't the most appropriate choice for system
  // that don't use a lot of power.  But all of the calculations
  // are shown below if you want to change the settings.  You will
  // also need to change any relevant register settings, such as
  // setting the VBUS_MAX to 16V instead of 32V, etc.

  // VBUS_MAX = 32V             (Assumes 32V, can also be set to 16V)
  // VSHUNT_MAX = 0.32          (Assumes Gain 8, 320mV, can also be 0.16, 0.08, 0.04)
  // RSHUNT = 0.1               (Resistor value in ohms)

  // 1. Determine max possible current
  // MaxPossible_I = VSHUNT_MAX / RSHUNT
  // MaxPossible_I = 3.2A

  // 2. Determine max expected current
  // MaxExpected_I = 2.0A

  // 3. Calculate possible range of LSBs (Min = 15-bit, Max = 12-bit)
  // MinimumLSB = MaxExpected_I/32767
  // MinimumLSB = 0.000061              (61uA per bit)
  // MaximumLSB = MaxExpected_I/4096
  // MaximumLSB = 0,000488              (488uA per bit)

  // 4. Choose an LSB between the min and max values
  //    (Preferrably a roundish number close to MinLSB)
  // CurrentLSB = 0.0001 (100uA per bit)

  // 5. Compute the calibration register
  // Cal = trunc (0.04096 / (Current_LSB * RSHUNT))
  // Cal = 4096 (0x1000)

  obj->calValue = 4096;

  // 6. Calculate the power LSB
  // PowerLSB = 20 * CurrentLSB
  // PowerLSB = 0.002 (2mW per bit)

  // 7. Compute the maximum current and shunt voltage values before overflow
  //
  // Max_Current = Current_LSB * 32767
  // Max_Current = 3.2767A before overflow
  //
  // If Max_Current > Max_Possible_I then
  //    Max_Current_Before_Overflow = MaxPossible_I
  // Else
  //    Max_Current_Before_Overflow = Max_Current
  // End If
  //
  // Max_ShuntVoltage = Max_Current_Before_Overflow * RSHUNT
  // Max_ShuntVoltage = 0.32V
  //
  // If Max_ShuntVoltage >= VSHUNT_MAX
  //    Max_ShuntVoltage_Before_Overflow = VSHUNT_MAX
  // Else
  //    Max_ShuntVoltage_Before_Overflow = Max_ShuntVoltage
  // End If

  // 8. Compute the Maximum Power
  // MaximumPower = Max_Current_Before_Overflow * VBUS_MAX
  // MaximumPower = 3.2 * 32V
  // MaximumPower = 102.4W

  // Set multipliers to convert raw current/power values
  obj->currentDivider_mA = 10;  // Current LSB = 100uA per bit (1000/100 = 10)
  obj->powerMultiplier_mW = 2;     // Power LSB = 1mW per bit (2/1)

  // Set Calibration register to 'Cal' calculated above
  INA219_WriteRegister(obj->addr, INA219_REG_CALIBRATION, obj->calValue);

  // Set Config register to take into account the settings above
  uint16_t config = INA219_CONFIG_BVOLTAGERANGE_32V |
                    INA219_CONFIG_GAIN_8_320MV |
                    INA219_CONFIG_BADCRES_12BIT |
                    INA219_CONFIG_SADCRES_12BIT_1S_532US |
                    INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS;
  INA219_WriteRegister(obj->addr, INA219_REG_CONFIG, config);
}

/******************************************************************************
  Function:
    void INA219_setCalibration_32V_1A (INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
void INA219_setCalibration_32V_1A(INA219* obj)
{
  // By default we use a pretty huge range for the input voltage,
  // which probably isn't the most appropriate choice for system
  // that don't use a lot of power.  But all of the calculations
  // are shown below if you want to change the settings.  You will
  // also need to change any relevant register settings, such as
  // setting the VBUS_MAX to 16V instead of 32V, etc.

  // VBUS_MAX = 32V		(Assumes 32V, can also be set to 16V)
  // VSHUNT_MAX = 0.32	(Assumes Gain 8, 320mV, can also be 0.16, 0.08, 0.04)
  // RSHUNT = 0.1			(Resistor value in ohms)

  // 1. Determine max possible current
  // MaxPossible_I = VSHUNT_MAX / RSHUNT
  // MaxPossible_I = 3.2A

  // 2. Determine max expected current
  // MaxExpected_I = 1.0A

  // 3. Calculate possible range of LSBs (Min = 15-bit, Max = 12-bit)
  // MinimumLSB = MaxExpected_I/32767
  // MinimumLSB = 0.0000305             (30.5?A per bit)
  // MaximumLSB = MaxExpected_I/4096
  // MaximumLSB = 0.000244              (244?A per bit)

  // 4. Choose an LSB between the min and max values
  //    (Preferrably a roundish number close to MinLSB)
  // CurrentLSB = 0.0000400 (40?A per bit)

  // 5. Compute the calibration register
  // Cal = trunc (0.04096 / (Current_LSB * RSHUNT))
  // Cal = 10240 (0x2800)

  obj->calValue = 10240;

  // 6. Calculate the power LSB
  // PowerLSB = 20 * CurrentLSB
  // PowerLSB = 0.0008 (800?W per bit)

  // 7. Compute the maximum current and shunt voltage values before overflow
  //
  // Max_Current = Current_LSB * 32767
  // Max_Current = 1.31068A before overflow
  //
  // If Max_Current > Max_Possible_I then
  //    Max_Current_Before_Overflow = MaxPossible_I
  // Else
  //    Max_Current_Before_Overflow = Max_Current
  // End If
  //
  // ... In this case, we're good though since Max_Current is less than MaxPossible_I
  //
  // Max_ShuntVoltage = Max_Current_Before_Overflow * RSHUNT
  // Max_ShuntVoltage = 0.131068V
  //
  // If Max_ShuntVoltage >= VSHUNT_MAX
  //    Max_ShuntVoltage_Before_Overflow = VSHUNT_MAX
  // Else
  //    Max_ShuntVoltage_Before_Overflow = Max_ShuntVoltage
  // End If

  // 8. Compute the Maximum Power
  // MaximumPower = Max_Current_Before_Overflow * VBUS_MAX
  // MaximumPower = 1.31068 * 32V
  // MaximumPower = 41.94176W

  // Set multipliers to convert raw current/power values
  obj->currentDivider_mA = 25;      // Current LSB = 40uA per bit (1000/40 = 25)
  obj->powerMultiplier_mW = 0.8f;   // Power LSB = 800uW per bit

  // Set Calibration register to 'Cal' calculated above
  INA219_WriteRegister(obj->addr, INA219_REG_CALIBRATION, obj->calValue);

  // Set Config register to take into account the settings above
  uint16_t config = INA219_CONFIG_BVOLTAGERANGE_32V |
                    INA219_CONFIG_GAIN_8_320MV |
                    INA219_CONFIG_BADCRES_12BIT |
                    INA219_CONFIG_SADCRES_12BIT_1S_532US |
                    INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS;
  INA219_WriteRegister(obj->addr, INA219_REG_CONFIG, config);
}

/******************************************************************************
  Function:
    void INA219_setCalibration_16V_400mA (INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
void INA219_setCalibration_16V_400mA(INA219* obj)
{

  // Calibration which uses the highest precision for
  // current measurement (0.1mA), at the expense of
  // only supporting 16V at 400mA max.

  // VBUS_MAX = 16V
  // VSHUNT_MAX = 0.04          (Assumes Gain 1, 40mV)
  // RSHUNT = 0.1               (Resistor value in ohms)

  // 1. Determine max possible current
  // MaxPossible_I = VSHUNT_MAX / RSHUNT
  // MaxPossible_I = 0.4A

  // 2. Determine max expected current
  // MaxExpected_I = 0.4A

  // 3. Calculate possible range of LSBs (Min = 15-bit, Max = 12-bit)
  // MinimumLSB = MaxExpected_I/32767
  // MinimumLSB = 0.0000122              (12uA per bit)
  // MaximumLSB = MaxExpected_I/4096
  // MaximumLSB = 0.0000977              (98uA per bit)

  // 4. Choose an LSB between the min and max values
  //    (Preferrably a roundish number close to MinLSB)
  // CurrentLSB = 0.00005 (50uA per bit)

  // 5. Compute the calibration register
  // Cal = trunc (0.04096 / (Current_LSB * RSHUNT))
  // Cal = 8192 (0x2000)

  obj->calValue = 8192;

  // 6. Calculate the power LSB
  // PowerLSB = 20 * CurrentLSB
  // PowerLSB = 0.001 (1mW per bit)

  // 7. Compute the maximum current and shunt voltage values before overflow
  //
  // Max_Current = Current_LSB * 32767
  // Max_Current = 1.63835A before overflow
  //
  // If Max_Current > Max_Possible_I then
  //    Max_Current_Before_Overflow = MaxPossible_I
  // Else
  //    Max_Current_Before_Overflow = Max_Current
  // End If
  //
  // Max_Current_Before_Overflow = MaxPossible_I
  // Max_Current_Before_Overflow = 0.4
  //
  // Max_ShuntVoltage = Max_Current_Before_Overflow * RSHUNT
  // Max_ShuntVoltage = 0.04V
  //
  // If Max_ShuntVoltage >= VSHUNT_MAX
  //    Max_ShuntVoltage_Before_Overflow = VSHUNT_MAX
  // Else
  //    Max_ShuntVoltage_Before_Overflow = Max_ShuntVoltage
  // End If
  //
  // Max_ShuntVoltage_Before_Overflow = VSHUNT_MAX
  // Max_ShuntVoltage_Before_Overflow = 0.04V

  // 8. Compute the Maximum Power
  // MaximumPower = Max_Current_Before_Overflow * VBUS_MAX
  // MaximumPower = 0.4 * 16V
  // MaximumPower = 6.4W

  // Set multipliers to convert raw current/power values
  obj->currentDivider_mA = 20;    // Current LSB = 50uA per bit (1000/50 = 20)
  obj->powerMultiplier_mW = 1.0f; // Power LSB = 1mW per bit

  // Set Calibration register to 'Cal' calculated above
  INA219_WriteRegister(obj->addr, INA219_REG_CALIBRATION, obj->calValue);

  // Set Config register to take into account the settings above
  uint16_t config = INA219_CONFIG_BVOLTAGERANGE_16V |
                    INA219_CONFIG_GAIN_1_40MV |
                    INA219_CONFIG_BADCRES_12BIT |
                    INA219_CONFIG_SADCRES_12BIT_1S_532US |
                    INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS;
  INA219_WriteRegister(obj->addr, INA219_REG_CONFIG, config);
}

/******************************************************************************
  Function:
    void INA219_setCalibration (INA219* obj,
                                uint8_t INA219_CONFIG_BVOLTAGERANGE,
                                uint8_t INA219_CONFIG_GAIN,
                                uint8_t INA219_CONFIG_BADCRES,
                                uint8_t INA219_CONFIG_SADCRES,
                                uint8_t INA219_CONFIG_MODE_SANDBVOLT,
                                uint32_t calValue,
                                uint32_t currentDivider_mA,
                                float powerMultiplier_mW)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
void INA219_setCalibration(INA219* obj,
                           uint8_t INA219_CONFIG_BVOLTAGERANGE,
                           uint8_t INA219_CONFIG_GAIN,
                           uint8_t INA219_CONFIG_BADCRES,
                           uint8_t INA219_CONFIG_SADCRES,
                           uint8_t INA219_CONFIG_MODE_SANDBVOLT,
                           uint32_t calValue,
                           uint32_t currentDivider_mA,
                           float powerMultiplier_mW)
{
  // Set multipliers to convert raw current/power values
  obj->calValue = calValue;
  obj->currentDivider_mA = currentDivider_mA;    // Current LSB = 50uA per bit (1000/50 = 20)
  obj->powerMultiplier_mW = powerMultiplier_mW; // Power LSB = 1mW per bit

  // Set Calibration register to 'Cal' calculated above
  INA219_WriteRegister(obj->addr, INA219_REG_CALIBRATION, obj->calValue);

  // Set Config register to take into account the settings above
  uint16_t config = INA219_CONFIG_BVOLTAGERANGE |
                    INA219_CONFIG_GAIN |
                    INA219_CONFIG_BADCRES |
                    INA219_CONFIG_SADCRES |
                    INA219_CONFIG_MODE_SANDBVOLT;
  INA219_WriteRegister(obj->addr, INA219_REG_CONFIG, config);
}

/******************************************************************************
  Function:
    float INA219_getBusVoltage_V(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
float INA219_getBusVoltage_V(INA219* obj)
{
  int16_t value = INA219_getBusVoltage_raw(obj);
  return value * 0.001;
}

/******************************************************************************
  Function:
    float INA219_getShuntVoltage_mV(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
float INA219_getShuntVoltage_mV(INA219* obj)
{
  int16_t value;
  value = INA219_getShuntVoltage_raw(obj);
  return value * 0.01;
}

/******************************************************************************
  Function:
    float INA219_getCurrent_mA(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
float INA219_getCurrent_mA(INA219* obj)
{
  float valueDec = INA219_getCurrent_raw(obj);
  valueDec /= obj->currentDivider_mA;
  return valueDec;
}

/******************************************************************************
  Function:
    float INA219_getPower_mW(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
float INA219_getPower_mW(INA219* obj)
{
  float valueDec = INA219_getPower_raw(obj);
  valueDec *= obj->powerMultiplier_mW;
  return valueDec;
}


/******************************************************************************/
//                            Support Funktions
/******************************************************************************/

/******************************************************************************
  Function:
    void INA219_WriteRegister(uint8_t addr, uint8_t reg, uint16_t value)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
void INA219_WriteRegister(uint8_t addr, uint8_t reg, uint16_t value)
{
    DRV_I2C_BUFFER_HANDLE TXHandle;
    static uint8_t TXbuffer[TXBUFFERSIZE];   
    uint8_t cnt;
    
    //map the data to the TXbuffer
    TXbuffer[0]=reg;    
    TXbuffer[1]=(value>>8);
    TXbuffer[2]=(value&0x0F);


    TXHandle=DRV_I2C_Transmit (INA219_I2C_Handle, (addr<<1), &TXbuffer[0], (sizeof(uint8_t)*3), NULL);    
//    //  A callback and blocking would be nice but is not jet implemented in the function (see docu for further info)
//    if(xSemaphoreTake(CITEC_I2CSemaphore, I2C_WAITTIME_MS)==pdTRUE)
//    {
//        //block as long as I2C running
//    }
//    else
//    {//I2C did not finish succesfully
//        SYS_ASSERT(false, "I2C could not end its communication!");
//        //return TXHandle;
//    }
    
    //return the handle to indicate fail/succsess
}

/******************************************************************************
  Function:
    void INA219_ReadRegister(uint8_t addr, uint8_t reg, uint16_t *value)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
void INA219_ReadRegister(uint8_t addr, uint8_t reg, uint16_t *value)
{
    DRV_I2C_BUFFER_HANDLE RXHandle;
    static uint8_t TXbuffer[1];
    static uint8_t RXbuffer[2];
    TXbuffer[0]=reg;
    
    RXHandle=DRV_I2C_TransmitThenReceive (INA219_I2C_Handle, (addr<<1), TXbuffer, sizeof(uint8_t), RXbuffer, (sizeof(uint8_t)*2), NULL);
////A callback to block until I2C is done
//    if(xSemaphoreTake(CITEC_I2CSemaphore, I2C_WAITTIME_MS)==pdTRUE)
//    {
//        //block as long as I2C running
//    }
//    else
//    {//I2C did not finish succesfully
//        SYS_ASSERT(false, "I2C could not end its communication!");
//        //return RXHandle;
//    }
    
    *value=(uint16_t)((RXbuffer[0]<<8)|RXbuffer[1]);
    
   //return the hadle to return fail/succsess
}

/******************************************************************************
  Function:
    int16_t INA219_getBusVoltage_raw(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
int16_t INA219_getBusVoltage_raw(INA219* obj)
{
  uint16_t value;
  INA219_ReadRegister(obj->addr, INA219_REG_BUSVOLTAGE, &value);

  // Shift to the right 3 to drop CNVR and OVF and multiply by LSB
  return (int16_t)((value >> 3) * 4);
}

/******************************************************************************
  Function:
    int16_t INA219_getShuntVoltage_raw(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
int16_t INA219_getShuntVoltage_raw(INA219* obj)
{
  uint16_t value;
  INA219_ReadRegister(obj->addr, INA219_REG_SHUNTVOLTAGE, &value);
  return (int16_t)value;
}

/******************************************************************************
  Function:
    int16_t INA219_getCurrent_raw(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
int16_t INA219_getCurrent_raw(INA219* obj)
{
  uint16_t value;

  // Sometimes a sharp load will reset the INA219, which will
  // reset the cal register, meaning CURRENT and POWER will
  // not be available ... avoid this by always setting a cal
  // value even if it's an unfortunate extra step
  INA219_WriteRegister(obj->addr, INA219_REG_CALIBRATION, obj->calValue);

  // Now we can safely read the CURRENT register!
  INA219_ReadRegister(obj->addr, INA219_REG_CURRENT, &value);

  return (int16_t)value;
}

/******************************************************************************
  Function:
    int16_t INA219_getPower_raw(INA219* obj)

  Remarks:
    See prototype in CITEC_INA219.h.
 */
int16_t INA219_getPower_raw(INA219* obj)
{
  uint16_t value;

  // Sometimes a sharp load will reset the INA219, which will
  // reset the cal register, meaning CURRENT and POWER will
  // not be available ... avoid this by always setting a cal
  // value even if it's an unfortunate extra step
  INA219_WriteRegister(obj->addr, INA219_REG_CALIBRATION, obj->calValue);

  // Now we can safely read the POWER register!
  INA219_ReadRegister(obj->addr, INA219_REG_POWER, &value);

  return (int16_t)value;
}

/* *****************************************************************************
 End of File
 */
