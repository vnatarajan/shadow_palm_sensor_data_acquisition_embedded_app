#####################################################################################
## REVISIONS (list newer Revisions on top)
## Date         Author             Description
## 2018-10-15   Luis Oberröhrmann  First release of the library
#####################################################################################
## Abstract
## This library provides funktions to configure and read values from the INA219 Sensor
##
##
#####################################################################################
## Usage & library interface
##
## Step 1: CITEC_INA219_config.h, CITEC_INA219.h and CITEC_INA219.h to your project
##	   and add #include "CITEC_INA219.h" to system_config.h
##
## Step 2: In MHC config, activate the I2C Module (Drivers) and set the following settings:
##         (other parameters can be set at programmers will):
##	   - Operation Mode "DRV_I2C_MODE_MASTER"
##	   - I2C CLOCK FREQUENCY (Hz): (free* may not exeed 3400000)	
##
## Step 3: In CITEC_AS5048_config.h change the defines according to setup/requirements.
##
## Step 4: Generate a Sersor Object and assign Data to it:

    INA219 INA219Data;
    INA219Data.addr=INA219_ADDRESS;
    INA219Data.shunt_R=SHUNTVALUE_R;        
    INA219Data.calValue=0;
    INA219Data.currentDivider_mA=0;
    INA219Data.powerMultiplier_mW=0;

##Step 5: Calibrate the Sensor to your needs using one of the INA219_setCalibration*
##	  functions
##
##Step 6: Read data whenever needed using the INA219_get* functions
##
#####################################################################################
## Example
## 
## The example is written for a PIC32MZ Starter Kit with I/O Expansion Board.
## The Sensor used Sensor was on a Adafrit Board:
## https://www.adafruit.com/product/904
## To connect bouth following Pinout is used:

Funktion	uC-Pin		Adapter-Pin168	Adapter132	Expansion bord Pin

SDA1		96		126		86		J11/37
SCL1		95		124		84		J11/35

VCC (5V)	--		--		--		+5V
GND		--		--		--		GND

## In the example the Sensor is configured to use measure up to 16V and 400mA.
## All available results will be read and stored in a dedicated Variable, that can be
## viewed in the Debug Mode.
## Therefore the example is only usefull to test its functions with static DC currents!
## 
#####################################################################################

