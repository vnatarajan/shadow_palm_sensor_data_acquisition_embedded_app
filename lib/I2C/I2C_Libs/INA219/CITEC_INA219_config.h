/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_INA219_Config.h

  @Summary
    Configuration Headder for the Project

  @Description
    In this Headder all used Pins are assigned by a simple Define.
    With this the Library can easily be ported to any environment.
 */
/* ************************************************************************** */

#ifndef _CITEC_INA219_Config_H    /* Guard against multiple inclusion */
#define _CITEC_INA219_Config_H


#error "Make shure the I2C is configured and connected corectly"

#define SHUNTVALUE_R    0.1

#define INA219_ADDRESS 0b1000000           //choose the used I2C_ADDR (7Bit according to the A0/A1 Wirering
                                            //see INA219 Datasheed for more Info about the I2C_adressing


#define DRV_I2C_INA219 DRV_I2C_INDEX_0      //Index of the used Harmony module numer


#define TXBUFFERSIZE 100                    //size of the I2C TXbuffer

#define I2C_WAITTIME_MS     pdMS_TO_TICKS(1000)            //if a I2C communication is not returned valid after this time an error ocures




#define INA219_MAXFREQUENZY  3400000          //I2C frequency should not be higher then this



//provide simple means to debug the application
#include "system/debug/sys_debug.h"
#ifdef SYS_ASSERT
#undef SYS_ASSERT
#endif
#define SYS_ASSERT(test, message) do{SYS_DEBUG_BreakPoint();}while(0)



#ifdef __cplusplus
}
#endif

#endif /* _CITEC_Config_H */

/* *****************************************************************************
 End of File
 */
