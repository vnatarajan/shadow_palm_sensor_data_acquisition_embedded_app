/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_INA219.h

  @Summary
    Interface funktions to use the INA219.
    This Funktions are c addaptions of:
        * /*!
        * @file Adafruit_INA219.h
        *
        * This is a library for the Adafruit INA219 breakout board
        * ----> https://www.adafruit.com/products/904
        *
        * Adafruit invests time and resources providing this open source code,
        * please support Adafruit and open-source hardware by purchasing
        * products from Adafruit!
        *
        * Written by Kevin "KTOWN" Townsend for Adafruit Industries.
        *
        * BSD license, all text here must be included in any redistribution.
        *
       //*
 */
/* ************************************************************************** */
#include "CITEC_INA219_Config.h"
#include "semphr.h"

#ifndef _CITECINA219_I2C_H    /* Guard against multiple inclusion */
#define _CITECINA219_I2C_H

/*******************************************************************************
********************************************************************************/
/**************************************************************************/
/*!
    Defines to acces the INA219 Sensor
*/
/**************************************************************************/
#define INA219_REG_CONFIG                      (0x00)
#define INA219_CONFIG_RESET                    (0x8000)  // Reset Bit
#define INA219_CONFIG_BVOLTAGERANGE_MASK       (0x2000)  // Bus Voltage Range Mask
#define INA219_CONFIG_GAIN_MASK                (0x1800)  // Gain Mask
#define INA219_CONFIG_BADCRES_MASK             (0x0780)  // Bus ADC Resolution Mask
#define INA219_CONFIG_SADCRES_MASK             (0x0078)  // Shunt ADC Resolution and Averaging Mask
#define INA219_CONFIG_MODE_MASK                (0x0007)  // Operating Mode Mask
#define INA219_REG_SHUNTVOLTAGE                (0x01)
#define INA219_REG_BUSVOLTAGE                  (0x02)
#define INA219_REG_POWER                       (0x03)
#define INA219_REG_CURRENT                     (0x04)
#define INA219_REG_CALIBRATION                 (0x05)


/**************************************************************************/
/*!
 Various enums to configure the INA219 behaviour
*/
/**************************************************************************/
enum BusVoltage
{
    INA219_CONFIG_BVOLTAGERANGE_16V =        (0x0000),  // 0-16V Range
    INA219_CONFIG_BVOLTAGERANGE_32V =        (0x2000),  // 0-32V Range
};

enum GainBits
{
    INA219_CONFIG_GAIN_1_40MV        =       (0x0000),  // Gain 1, 40mV Range
    INA219_CONFIG_GAIN_2_80MV        =       (0x0800),  // Gain 2, 80mV Range
    INA219_CONFIG_GAIN_4_160MV       =       (0x1000),  // Gain 4, 160mV Range
    INA219_CONFIG_GAIN_8_320MV       =       (0x1800),  // Gain 8, 320mV Range
};

enum BusResolution
{
    INA219_CONFIG_BADCRES_9BIT       =       (0x0000),  // 9-bit bus res = 0..511
    INA219_CONFIG_BADCRES_10BIT      =       (0x0080),  // 10-bit bus res = 0..1023
    INA219_CONFIG_BADCRES_11BIT      =       (0x0100),  // 11-bit bus res = 0..2047
    INA219_CONFIG_BADCRES_12BIT       =      (0x0180),  // 12-bit bus res = 0..4097
};

enum ShuntResolution
{
    INA219_CONFIG_SADCRES_9BIT_1S_84US     = (0x0000),  // 1 x 9-bit shunt sample
    INA219_CONFIG_SADCRES_10BIT_1S_148US   = (0x0008),  // 1 x 10-bit shunt sample
    INA219_CONFIG_SADCRES_11BIT_1S_276US   = (0x0010),  // 1 x 11-bit shunt sample
    INA219_CONFIG_SADCRES_12BIT_1S_532US   = (0x0018),  // 1 x 12-bit shunt sample
    INA219_CONFIG_SADCRES_12BIT_2S_1060US  = (0x0048),	 // 2 x 12-bit shunt samples averaged together
    INA219_CONFIG_SADCRES_12BIT_4S_2130US  = (0x0050),  // 4 x 12-bit shunt samples averaged together
    INA219_CONFIG_SADCRES_12BIT_8S_4260US  = (0x0058),  // 8 x 12-bit shunt samples averaged together
    INA219_CONFIG_SADCRES_12BIT_16S_8510US = (0x0060),  // 16 x 12-bit shunt samples averaged together
    INA219_CONFIG_SADCRES_12BIT_32S_17MS   = (0x0068),  // 32 x 12-bit shunt samples averaged together
    INA219_CONFIG_SADCRES_12BIT_64S_34MS   = (0x0070),  // 64 x 12-bit shunt samples averaged together
    INA219_CONFIG_SADCRES_12BIT_128S_69MS =  (0x0078),  // 128 x 12-bit shunt samples averaged together
};

enum OperatingMode
{
    INA219_CONFIG_MODE_POWERDOWN          =  (0x0000),
    INA219_CONFIG_MODE_SVOLT_TRIGGERED    =  (0x0001),
    INA219_CONFIG_MODE_BVOLT_TRIGGERED    =  (0x0002),
    INA219_CONFIG_MODE_SANDBVOLT_TRIGGERED = (0x0003),
    INA219_CONFIG_MODE_ADCOFF             =  (0x0004),
    INA219_CONFIG_MODE_SVOLT_CONTINUOUS  =  (0x0005),
    INA219_CONFIG_MODE_BVOLT_CONTINUOUS   =  (0x0006),
    INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS = (0x0007),
};


/**************************************************************************/
/*!
    INA219 "Object" to hold al variables needet to use all funktions
    This is used to enable the simple use of multiple INA219 Sensors within one
    Program/bus
*/
/**************************************************************************/
typedef struct
{
  uint8_t addr;
  uint32_t calValue;
  // The following multipliers are used to convert raw current and power
  // values to mA and mW, taking into account the current config settings
  float shunt_R;
  uint32_t currentDivider_mA;
  float    powerMultiplier_mW;
}INA219;


//Global defines to accsess the I2C Bus
DRV_HANDLE INA219_I2C_Handle;
SemaphoreHandle_t CITEC_I2CSemaphore;



/******************************************************************************/
//                            Public Funktions
/******************************************************************************/

/*******************************************************************************
  Function:
    void CITEC_Init ( void )

  Summary:
    Initialisation for the INA219 Communication, readdys the I2C-Bus

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

 */
void CITEC_Init(void);

/*******************************************************************************
  Function:
    void INA219_setCalibration_32V_2A ( INA219* obj )
    void INA219_setCalibration_32V_1A ( INA219* obj )
    void INA219_setCalibration_16V_400mA ( INA219* obj )

  Summary:
    Example Calibrations of the INA219 that supports:
    * 32V and 2A
    * 32V and 1A
    * 16V and 400mA
    The calibration affects the accuracy/resolutuin of the Measurement and
    should be done individually for your usecase.

  Precondition:
    I2C Bus needs to be running

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    None.

 */
void INA219_setCalibration_32V_2A(INA219* obj);
void INA219_setCalibration_32V_1A(INA219* obj);
void INA219_setCalibration_16V_400mA(INA219* obj);

/*******************************************************************************
  Function:
    void INA219_setCalibration(INA219* obj,
                               uint8_t INA219_CONFIG_BVOLTAGERANGE,
                               uint8_t INA219_CONFIG_GAIN,
                               uint8_t INA219_CONFIG_BADCRES,
                               uint8_t INA219_CONFIG_SADCRES,
                               uint8_t INA219_CONFIG_MODE_SANDBVOLT,
                               uint32_t calValue,
                               uint32_t currentDivider_mA,
                               float powerMultiplier_mW);)

  Summary:
    General Calibration function wich enables you to set each Parameter
    individualy to optimise results.
    Refer to the datasheet to see how to choos each value

  Precondition:
    I2C Bus needs to be running

  Parameters:
    INA219* obj:                            INA219 Object pointer to identify sensor to be used
    uint8_t INA219_CONFIG_BVOLTAGERANGE:    config Bus Voltage (BusVoltage enum)
    uint8_t INA219_CONFIG_GAIN:             config Gain (GainBits enum)
    uint8_t INA219_CONFIG_BADCRES:          config Bus resolution (BusResolution enum)
    uint8_t INA219_CONFIG_SADCRES:          config shunt resulution (ShuntResolution enum)
    uint8_t INA219_CONFIG_MODE_SANDBVOLT:   config operation mode (OperatingMode enum)
    uint32_t calValue:                      needs to be calculated (see datasheet)
    uint32_t currentDivider_mA:             multiplier to convert raw current/power values             
    float powerMultiplier_mW:               multiplier to convert raw current/power values

  Returns:
    None.

 */
void INA219_setCalibration(INA219* obj,
                           uint8_t INA219_CONFIG_BVOLTAGERANGE,
                           uint8_t INA219_CONFIG_GAIN,
                           uint8_t INA219_CONFIG_BADCRES,
                           uint8_t INA219_CONFIG_SADCRES,
                           uint8_t INA219_CONFIG_MODE_SANDBVOLT,
                           uint32_t calValue,
                           uint32_t currentDivider_mA,
                           float powerMultiplier_mW);

/*******************************************************************************
  Function:
    float INA219_getBusVoltage_V ( INA219* obj )

  Summary:
    Read the Bus Voltage from the Sensor and converts it to Volt

  Precondition:
    Calibration needs to be done once before

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    Bus Voltage in Volt

 */
float INA219_getBusVoltage_V(INA219* obj);

/*******************************************************************************
  Function:
    float INA219_getShuntVoltage_mV ( INA219* obj )

  Summary:
    Read the shunt Voltage from the Sensor and converts it to Volt

  Precondition:
    Calibration needs to be done once before

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    Shunt Voltage in Volt

 */
float INA219_getShuntVoltage_mV(INA219* obj);

/*******************************************************************************
  Function:
    float INA219_getCurrent_mA ( INA219* obj )

  Summary:
    Read the current from the Sensor and converts it to mA

  Precondition:
    Calibration needs to be done once before

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    Shunt current in mA

 */
float INA219_getCurrent_mA(INA219* obj);

/*******************************************************************************
  Function:
    float INA219_getPower_mW ( INA219* obj )

  Summary:
    Read the Power from the Sensor and converts it to mW

  Precondition:
    Calibration needs to be done once before

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    Load Power in mA

 */
float INA219_getPower_mW(INA219* obj);



/******************************************************************************/
//                            Support Funktions
/******************************************************************************/

/*******************************************************************************
  Function:
    void INA219_WriteRegister(uint8_t addr, uint8_t reg, uint16_t value);

  Summary:
    Writes 16Bit Data to a INA219 Sensors Register

  Precondition:
    I2C-Bus needs to be initilised.

  Parameters:
    uint8_t addr:   7Bit Adress of the INA219 Sensor
    uint8_t reg.    Register Adress that should be writen to
    uint16_t value: Vale to write to the Register

  Returns:
    None.

 */
void INA219_WriteRegister(uint8_t addr, uint8_t reg, uint16_t value);

/*******************************************************************************
  Function:
    void INA219_WriteRegister(uint8_t addr, uint8_t reg, uint16_t value);

  Summary:
    Reads 16Bit Data from a INA219 Sensors Register

  Precondition:
    I2C-Bus needs to be initilised.

  Parameters:
    uint8_t addr:   7Bit Adress of the INA219 Sensor
    uint8_t reg.    Register Adress that should be read from
    uint16_t value: Address wher to store read value

  Returns:
    None.

 */
void INA219_ReadRegister(uint8_t addr, uint8_t reg, uint16_t *value);


/*******************************************************************************
  Function:
    int16_t INA219_getBusVoltage_raw ( INA219* obj )

  Summary:
    Read the Bus Voltage form the Sensor

  Precondition:
    Calibration needs to be done once before

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    Register Value for the Bus Voltage

 */
int16_t INA219_getBusVoltage_raw(INA219* obj);

/*******************************************************************************
  Function:
    int16_t INA219_getShuntVoltage_raw ( INA219* obj )

  Summary:
    Read the shunt Voltage form the Sensor

  Precondition:
    Calibration needs to be done once before

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    Register Value for the shunt Voltage

 */
int16_t INA219_getShuntVoltage_raw(INA219* obj);

/*******************************************************************************
  Function:
    int16_t INA219_getCurrent_raw ( INA219* obj )

  Summary:
    Read the shunt curent form the Sensor

  Precondition:
    Calibration needs to be done once before

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    Register Value for the shunt curent

 */
int16_t INA219_getCurrent_raw(INA219* obj);

/*******************************************************************************
  Function:
    int16_t INA219_getPower_raw ( INA219* obj )

  Summary:
    Read the load Power form the Sensor

  Precondition:
    Calibration needs to be done once before

  Parameters:
    INA219* obj: INA219 Object pointer to identify sensor to be used

  Returns:
    Register Value for the Power Register

 */
int16_t INA219_getPower_raw(INA219* obj);



        
#endif //_CITECINA219_I2C_H