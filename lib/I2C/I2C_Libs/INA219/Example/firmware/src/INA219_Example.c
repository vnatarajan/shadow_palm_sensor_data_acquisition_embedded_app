/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    CITEC
  
  File Name:
    INA219_Example.c

  Summary:
    This file contains the example Source code to run the INA219 Sensor

  Description:
    //todo
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END



// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************
#include "INA219_Example.h"

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************


/******************************************************************************
  Function:
    void INA219_EXAMPLE_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
void INA219_EXAMPLE_Tasks( void )
{
    float shuntvoltage = 0;
    float busvoltage = 0;
    float current_mA = 0;
    float loadvoltage = 0;
    float power_mW = 0;
     
    //Generate on INA219 Object and assign the right adress and shunt value
    INA219 INA219Data;
    INA219Data.addr=INA219_ADDRESS;
    INA219Data.shunt_R=SHUNTVALUE_R;        
    INA219Data.calValue=0;
    INA219Data.currentDivider_mA=0;
    INA219Data.powerMultiplier_mW=0;
    
    //initialise the I2C Module
    CITEC_Init();
    
    //calibrate the Sensor acording to your needs
    //a general Calibration function is available
    //please reffer to the datasheet for informations on how to determain the right values
    INA219_setCalibration_16V_400mA(&INA219Data);
    
    while(1)
    {                
        //get all available data form the generated INA219 Object
        shuntvoltage = INA219_getShuntVoltage_mV(&INA219Data);
        busvoltage = INA219_getBusVoltage_V(&INA219Data);
        current_mA = INA219_getCurrent_mA(&INA219Data);
        power_mW = INA219_getPower_mW(&INA219Data);
        loadvoltage = busvoltage + (shuntvoltage / 1000);
    

        vTaskDelay(pdMS_TO_TICKS(100));
    }

}