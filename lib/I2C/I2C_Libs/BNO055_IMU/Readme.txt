##############################################################################
## REVISIONS (newer Revisions on top)
##
## Date         Author             Description
## 2018-09-18   <loberroehrmann>   Initial release of the Library
##############################################################################
## Abstract
##
## This library interfaces with the BNO055 IMU Sensor.
## A fast and easy way to configure and access the Sensors Data is given.
## The Library is written to be used in a freeRTOS environment, but should with
## minor changes be usable without it.
##############################################################################
## Usage & library interface
## 
## To use the library follow this Steps:
##
## Step 1: Add "CITEC_bno055_Config.h", "CITEC_bno055.h", "CITEC_bno055.c",
##	   "Bosch_bno055.h" and "Bosch_bno055.c" to your project.
##	   And include the "CITEC_bno055.h" in "system_definitions.h".
##
## Step 2: In MHC config:
##	   - Activate I2C drivers: tick "Use I2C Driver?"
##	   - "Driver Implementation Dynamic"
##	   - choose your used "I2C Module ID"
##	   - Operation Mode: "DRV_I2C_MODE_MASTER"
##	   - choose: "I2C Clock FREQUENCY (Hz)" (with 400000 as max Value)
##	   Configure others as required for your Project.
##
## Step 3: In "CITEC_Config.h" change at least the "DRV_I2C_BNO055" and
##	   "BNO055_I2C_ADDR" defines to match your setup. Other configuration
##	   can be made as required.
##
## Step 4: Until the Harmony funktion supports callback funktions for the I2C
##	   driver a workaround with the interruptroutine is used. Therefore
##	   substitute the Harmony generated interruptroutine in 
##	   "system_interrupt.c" with the following code:
##
void IntHandlerDrvI2CMasterInstance0(void)
{
    DRV_I2C_Tasks(sysObj.drvI2C0);
    
    if(I2C2STATbits.P==1&&I2C2STATbits.D_A==1)
    {//enable funktion again
        if(xSemaphoreGiveFromISR(CITEC_I2CSemaphore, pdFALSE)==pdTRUE)
        {
            portYIELD();    //exit current task and rescedule
            return;
        }
        SYS_ASSERT(false, "Semaphore could not be given!");   //accept only valid data
    
    }
}
##
## Step 5: Start Using the Library with:
##	   - Create a variable from type: "struct bno055_t YOURNAME;"
##	   - Call the "CITEC_BNO055_Init(YOURNAME);"
##	   - Use the bno055-Funktions to acces the Sensor
## 
##############################################################################
## Example
## 
## The example is written for a DataBusBoard_v2.6:
## (see: https://svn.techfak.uni-bielefeld.de/citec/projects/TactileModules/
##  electronics/BottomDataBusBoard/v2_EmbeddedUSB)
## The Board has a BNO055 embedded and therefore nothing more is required
## than a USB-Cable to use this example.
##
## In the example all available Data that will be read from the Sensor. 
## All Data as well as some aditional converted Data is repetitively send 
## to the PC Using a UCBCDC Example Code with following settings:
## Baud: 	9600
## DataBits:	8
## Parity:	None
## StopBits:	1
##
## Should this example be used with a different setup the SPI and USB Modules
## need to be reconfigured in the Harmoy settings and in the "CITEC_Config.h".
##
## (The Example includes the Percepio Tracealizer code to see task behavior)
##############################################################################
