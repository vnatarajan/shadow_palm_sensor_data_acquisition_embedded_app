/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_Config.h

  @Summary
    Configuration Headder for the Project

  @Description
    In this Headder all used Pins are assigned by a simple Define.
    With this the Library can easily be ported to any environment.
 */
/* ************************************************************************** */

#ifndef _CITEC_Config_H    /* Guard against multiple inclusion */
#define _CITEC_Config_H

#include "Bosch_bno055.h"


#define DRV_I2C_BNO055 DRV_I2C_INDEX_0      //Index of the used Harmony module numer

#define BNO055_I2C_ADDR BNO055_I2C_ADDR2    //choose the used I2C_ADDR according to your com3 Setting
                                            //com3=LOW => ADDR1 com3=HIGH => ADDR2
                                            //(com3 == config pin on bno005)

#define TXBUFFERSIZE 100                    //size of the I2C TXbuffer

#define I2C_WAITTIME_MS     1000            //if a I2C communication is not returned valid after this time an error ocures




#define BNO055MAXFREQUENZY  400000          //I2C frequency should not be higher then this



//provide simple means to debug the application
#include "system/debug/sys_debug.h"
#ifdef SYS_ASSERT
#undef SYS_ASSERT
#endif
#define SYS_ASSERT(test, message) do{SYS_DEBUG_BreakPoint();}while(0)



#ifdef __cplusplus
}
#endif

#endif /* _CITEC_Config_H */

/* *****************************************************************************
 End of File
 */
