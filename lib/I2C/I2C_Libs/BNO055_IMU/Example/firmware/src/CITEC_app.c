/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    CITEC
  
  File Name:
    CITEC_app.c

  Summary:
    This file contains the example Source code to run the BNO055 Sensor

  Description:
    //todo
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "CITEC_app.h"
#include "CITEC_bno055.h"
#include "USB_CDC_Example.h"

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************
#include <stdio.h>

typedef struct
{
    char Text[APP_Write_BUFFER_SIZE];
    uint8_t size;
}OutString;


//allows a define value to be insertet in a string
OutString makeString0(char* str1)
{
    OutString ret;
    
    ret.size=sprintf(ret.Text, "%s", str1);
    
    if(ret.size>APP_Write_BUFFER_SIZE) // puts string into buffer
        SYS_ASSERT(false, "Given Error Message is to long");
    
    return ret;
}

OutString makeString1(char* str1, int zahl1, char* str2)
{
    OutString ret;
    
    ret.size=sprintf(ret.Text, "%s%i%s", str1, zahl1, str2);
    
    if(ret.size>APP_Write_BUFFER_SIZE) // puts string into buffer
        SYS_ASSERT(false, "Given Error Message is to long");
    
    return ret;
}

OutString makeString1f(char* str1, double zahl1, char* str2)
{
    OutString ret;
    
    ret.size=sprintf(ret.Text, "%s%f%s", str1, zahl1, str2);
    
    if(ret.size>APP_Write_BUFFER_SIZE) // puts string into buffer
        SYS_ASSERT(false, "Given Error Message is to long");
    
    return ret;
}



void sendText(OutString msg)
{
    USB_DEVICE_CDC_TRANSFER_HANDLE Write_Handle;
    Write_Handle = USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID;
    
/* Write Data Buffer. Size is same as read buffer size */
    static uint8_t APP_MAKE_BUFFER_DMA_READY WriteBuffer[APP_Write_BUFFER_SIZE] ;    
    
    uint8_t i=0;
    while((i<(APP_Write_BUFFER_SIZE))&&(i<msg.size)) //check for end of line
    {
        WriteBuffer[(i)]=(uint8_t)msg.Text[i];
        i++;
    }
    
    USB_DEVICE_CDC_Write(0, &Write_Handle,
                            WriteBuffer, i, 0);
    
    vTaskDelay(pdMS_TO_TICKS(1));   //give usb time to send/prevent write buffer overflow
}

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void CITEC_APP_Initialize (Bosch_bno055* bno055 )
{
    #error "Make shure the I2C is set up according to the Readme.txt! Delete this line if you already configured the I2C as described";
    
    
    //be shure USB is initialised
    vTaskDelay(pdMS_TO_TICKS(10));
    
    
    
    if(configTICK_RATE_HZ>1000)
        SYS_ASSERT(false, "This code uses the pdMS_TO_TICKS Makro to provide ms timing. Not funktional for configTICK_RATE_HZ>1000");
    
    CITEC_BNO055_Init(bno055);
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void CITEC_APP_Tasks ( void )
{
    uint32_t cnt=0;
    struct bno055_t bno055;

    /* Variable used to return value of
	communication routine*/
	s32 comres = BNO055_ERROR;
	/* variable used to set the power mode of the sensor*/
	u8 power_mode = BNO055_INIT_VALUE;
	/*********read raw accel data***********/
	/* variable used to read the accel x data */
	s16 accel_datax = BNO055_INIT_VALUE;
	 /* variable used to read the accel y data */
	s16 accel_datay = BNO055_INIT_VALUE;
	/* variable used to read the accel z data */
	s16 accel_dataz = BNO055_INIT_VALUE;
	/* variable used to read the accel xyz data */
	struct bno055_accel_t accel_xyz;

	/*********read raw mag data***********/
	/* variable used to read the mag x data */
	s16 mag_datax  = BNO055_INIT_VALUE;
	/* variable used to read the mag y data */
	s16 mag_datay  = BNO055_INIT_VALUE;
	/* variable used to read the mag z data */
	s16 mag_dataz  = BNO055_INIT_VALUE;
	/* structure used to read the mag xyz data */
	struct bno055_mag_t mag_xyz;

	/***********read raw gyro data***********/
	/* variable used to read the gyro x data */
	s16 gyro_datax = BNO055_INIT_VALUE;
	/* variable used to read the gyro y data */
	s16 gyro_datay = BNO055_INIT_VALUE;
	 /* variable used to read the gyro z data */
	s16 gyro_dataz = BNO055_INIT_VALUE;
	 /* structure used to read the gyro xyz data */
	struct bno055_gyro_t gyro_xyz;

	/*************read raw Euler data************/
	/* variable used to read the euler h data */
	s16 euler_data_h = BNO055_INIT_VALUE;
	 /* variable used to read the euler r data */
	s16 euler_data_r = BNO055_INIT_VALUE;
	/* variable used to read the euler p data */
	s16 euler_data_p = BNO055_INIT_VALUE;
	/* structure used to read the euler hrp data */
	struct bno055_euler_t euler_hrp;

	/************read raw quaternion data**************/
	/* variable used to read the quaternion w data */
	s16 quaternion_data_w = BNO055_INIT_VALUE;
	/* variable used to read the quaternion x data */
	s16 quaternion_data_x = BNO055_INIT_VALUE;
	/* variable used to read the quaternion y data */
	s16 quaternion_data_y = BNO055_INIT_VALUE;
	/* variable used to read the quaternion z data */
	s16 quaternion_data_z = BNO055_INIT_VALUE;
	/* structure used to read the quaternion wxyz data */
	struct bno055_quaternion_t quaternion_wxyz;

	/************read raw linear acceleration data***********/
	/* variable used to read the linear accel x data */
	s16 linear_accel_data_x = BNO055_INIT_VALUE;
	/* variable used to read the linear accel y data */
	s16 linear_accel_data_y = BNO055_INIT_VALUE;
	/* variable used to read the linear accel z data */
	s16 linear_accel_data_z = BNO055_INIT_VALUE;
	/* structure used to read the linear accel xyz data */
	struct bno055_linear_accel_t linear_acce_xyz;

	/*****************read raw gravity sensor data****************/
	/* variable used to read the gravity x data */
	s16 gravity_data_x = BNO055_INIT_VALUE;
	/* variable used to read the gravity y data */
	s16 gravity_data_y = BNO055_INIT_VALUE;
	/* variable used to read the gravity z data */
	s16 gravity_data_z = BNO055_INIT_VALUE;
	/* structure used to read the gravity xyz data */
	struct bno055_gravity_t gravity_xyz;

	/*************read accel converted data***************/
	/* variable used to read the accel x data output as m/s2 or mg */
	double d_accel_datax = BNO055_INIT_VALUE;
	/* variable used to read the accel y data output as m/s2 or mg */
	double d_accel_datay = BNO055_INIT_VALUE;
	/* variable used to read the accel z data output as m/s2 or mg */
	double d_accel_dataz = BNO055_INIT_VALUE;
	/* structure used to read the accel xyz data output as m/s2 or mg */
	struct bno055_accel_double_t d_accel_xyz;

	/******************read mag converted data********************/
	/* variable used to read the mag x data output as uT*/
	double d_mag_datax = BNO055_INIT_VALUE;
	/* variable used to read the mag y data output as uT*/
	double d_mag_datay = BNO055_INIT_VALUE;
	/* variable used to read the mag z data output as uT*/
	double d_mag_dataz = BNO055_INIT_VALUE;
	/* structure used to read the mag xyz data output as uT*/
	struct bno055_mag_double_t d_mag_xyz;

	/*****************read gyro converted data************************/
	/* variable used to read the gyro x data output as dps or rps */
	double d_gyro_datax = BNO055_INIT_VALUE;
	/* variable used to read the gyro y data output as dps or rps */
	double d_gyro_datay = BNO055_INIT_VALUE;
	/* variable used to read the gyro z data output as dps or rps */
	double d_gyro_dataz = BNO055_INIT_VALUE;
	/* structure used to read the gyro xyz data output as dps or rps */
	struct bno055_gyro_double_t d_gyro_xyz;

	/*******************read euler converted data*******************/
	/* variable used to read the euler h data output
	as degree or radians*/
	double d_euler_data_h = BNO055_INIT_VALUE;
	/* variable used to read the euler r data output
	as degree or radians*/
	double d_euler_data_r = BNO055_INIT_VALUE;
	/* variable used to read the euler p data output
	as degree or radians*/
	double d_euler_data_p = BNO055_INIT_VALUE;
	/* structure used to read the euler hrp data output
	as as degree or radians */
	struct bno055_euler_double_t d_euler_hpr;

	/*********read linear acceleration converted data**********/
	/* variable used to read the linear accel x data output as m/s2*/
	double d_linear_accel_datax = BNO055_INIT_VALUE;
	/* variable used to read the linear accel y data output as m/s2*/
	double d_linear_accel_datay = BNO055_INIT_VALUE;
	/* variable used to read the linear accel z data output as m/s2*/
	double d_linear_accel_dataz = BNO055_INIT_VALUE;
	/* structure used to read the linear accel xyz data output as m/s2*/
	struct bno055_linear_accel_double_t d_linear_accel_xyz;

	/********************Gravity converted data**********************/
	/* variable used to read the gravity sensor x data output as m/s2*/
	double d_gravity_data_x = BNO055_INIT_VALUE;
	/* variable used to read the gravity sensor y data output as m/s2*/
	double d_gravity_data_y = BNO055_INIT_VALUE;
	/* variable used to read the gravity sensor z data output as m/s2*/
	double d_gravity_data_z = BNO055_INIT_VALUE;
	/* structure used to read the gravity xyz data output as m/s2*/
	struct bno055_gravity_double_t d_gravity_xyz;
    
    
    CITEC_APP_Initialize(&bno055);
    
    comres=bno055_init(&bno055);
    /*	For initializing the BNO sensor it is required to the operation mode
	of the sensor as NORMAL
	Normal mode can set from the register
	Page - page0
	register - 0x3E
	bit positions - 0 and 1*/
	power_mode = BNO055_POWER_MODE_NORMAL;
	/* set the power mode as NORMAL*/
	comres += bno055_set_power_mode(power_mode);
    
    while(true)
    {     
        /*	Using BNO055 sensor we can read the following sensor data and
        virtual sensor data
        Sensor data:
            Accel
            Mag
            Gyro
        Virtual sensor data
            Euler
            Quaternion
            Linear acceleration
            Gravity sensor */
    /*	For reading sensor raw data it is required to set the
        operation modes of the sensor
        operation mode can set from the register
        page - page0
        register - 0x3D
        bit - 0 to 3
        for sensor data read following operation mode have to set
         * SENSOR MODE
            *0x01 - BNO055_OPERATION_MODE_ACCONLY
            *0x02 - BNO055_OPERATION_MODE_MAGONLY
            *0x03 - BNO055_OPERATION_MODE_GYRONLY
            *0x04 - BNO055_OPERATION_MODE_ACCMAG
            *0x05 - BNO055_OPERATION_MODE_ACCGYRO
            *0x06 - BNO055_OPERATION_MODE_MAGGYRO
            *0x07 - BNO055_OPERATION_MODE_AMG
            based on the user need configure the operation mode*/
        comres += bno055_set_operation_mode(BNO055_OPERATION_MODE_AMG);
    /*	Raw accel X, Y and Z data can read from the register
        page - page 0
        register - 0x08 to 0x0D*/
        comres += bno055_read_accel_x(&accel_datax);
        comres += bno055_read_accel_y(&accel_datay);
        comres += bno055_read_accel_z(&accel_dataz);
        comres += bno055_read_accel_xyz(&accel_xyz);
    /*	Raw mag X, Y and Z data can read from the register
        page - page 0
        register - 0x0E to 0x13*/
        comres += bno055_read_mag_x(&mag_datax);
        comres += bno055_read_mag_y(&mag_datay);
        comres += bno055_read_mag_z(&mag_dataz);
        comres += bno055_read_mag_xyz(&mag_xyz);
    /*	Raw gyro X, Y and Z data can read from the register
        page - page 0
        register - 0x14 to 0x19*/
        comres += bno055_read_gyro_x(&gyro_datax);
        comres += bno055_read_gyro_y(&gyro_datay);
        comres += bno055_read_gyro_z(&gyro_dataz);
        comres += bno055_read_gyro_xyz(&gyro_xyz);

    /************************* END READ RAW SENSOR DATA****************/

    /************************* START READ RAW FUSION DATA ********
        For reading fusion data it is required to set the
        operation modes of the sensor
        operation mode can set from the register
        page - page0
        register - 0x3D
        bit - 0 to 3
        for sensor data read following operation mode have to set
        *FUSION MODE
            *0x08 - BNO055_OPERATION_MODE_IMUPLUS
            *0x09 - BNO055_OPERATION_MODE_COMPASS
            *0x0A - BNO055_OPERATION_MODE_M4G
            *0x0B - BNO055_OPERATION_MODE_NDOF_FMC_OFF
            *0x0C - BNO055_OPERATION_MODE_NDOF
            based on the user need configure the operation mode*/
        comres += bno055_set_operation_mode(BNO055_OPERATION_MODE_NDOF);
    /*	Raw Euler H, R and P data can read from the register
        page - page 0
        register - 0x1A to 0x1E */
        comres += bno055_read_euler_h(&euler_data_h);
        comres += bno055_read_euler_r(&euler_data_r);
        comres += bno055_read_euler_p(&euler_data_p);
        comres += bno055_read_euler_hrp(&euler_hrp);
    /*	Raw Quaternion W, X, Y and Z data can read from the register
        page - page 0
        register - 0x20 to 0x27 */
        comres += bno055_read_quaternion_w(&quaternion_data_w);
        comres += bno055_read_quaternion_x(&quaternion_data_x);
        comres += bno055_read_quaternion_y(&quaternion_data_y);
        comres += bno055_read_quaternion_z(&quaternion_data_z);
        comres += bno055_read_quaternion_wxyz(&quaternion_wxyz);
    /*	Raw Linear accel X, Y and Z data can read from the register
        page - page 0
        register - 0x28 to 0x2D */
        comres += bno055_read_linear_accel_x(&linear_accel_data_x);
        comres += bno055_read_linear_accel_y(&linear_accel_data_y);
        comres += bno055_read_linear_accel_z(&linear_accel_data_z);
        comres += bno055_read_linear_accel_xyz(&linear_acce_xyz);
    /*	Raw Gravity sensor X, Y and Z data can read from the register
        page - page 0
        register - 0x2E to 0x33 */
        comres += bno055_read_gravity_x(&gravity_data_x);
        comres += bno055_read_gravity_y(&gravity_data_y);
        comres += bno055_read_gravity_z(&gravity_data_z);
        comres += bno055_read_gravity_xyz(&gravity_xyz);
    /************************* END READ RAW FUSION DATA  ************/

    /******************START READ CONVERTED SENSOR DATA****************/
    /*	API used to read accel data output as double  - m/s2 and mg
        float functions also available in the BNO055 API */
        comres += bno055_convert_double_accel_x_msq(&d_accel_datax);
        comres += bno055_convert_double_accel_x_mg(&d_accel_datax);
        comres += bno055_convert_double_accel_y_msq(&d_accel_datay);
        comres += bno055_convert_double_accel_y_mg(&d_accel_datay);
        comres += bno055_convert_double_accel_z_msq(&d_accel_dataz);
        comres += bno055_convert_double_accel_z_mg(&d_accel_dataz);
        comres += bno055_convert_double_accel_xyz_msq(&d_accel_xyz);
        comres += bno055_convert_double_accel_xyz_mg(&d_accel_xyz);

    /*	API used to read mag data output as double  - uT(micro Tesla)
        float functions also available in the BNO055 API */
        comres += bno055_convert_double_mag_x_uT(&d_mag_datax);
        comres += bno055_convert_double_mag_y_uT(&d_mag_datay);
        comres += bno055_convert_double_mag_z_uT(&d_mag_dataz);
        comres += bno055_convert_double_mag_xyz_uT(&d_mag_xyz);

    /*	API used to read gyro data output as double  - dps and rps
        float functions also available in the BNO055 API */
        comres += bno055_convert_double_gyro_x_dps(&d_gyro_datax);
        comres += bno055_convert_double_gyro_y_dps(&d_gyro_datay);
        comres += bno055_convert_double_gyro_z_dps(&d_gyro_dataz);
        comres += bno055_convert_double_gyro_x_rps(&d_gyro_datax);
        comres += bno055_convert_double_gyro_y_rps(&d_gyro_datay);
        comres += bno055_convert_double_gyro_z_rps(&d_gyro_dataz);
        comres += bno055_convert_double_gyro_xyz_dps(&d_gyro_xyz);
        comres += bno055_convert_double_gyro_xyz_rps(&d_gyro_xyz);

    /*	API used to read Euler data output as double  - degree and radians
        float functions also available in the BNO055 API */
        comres += bno055_convert_double_euler_h_deg(&d_euler_data_h);
        comres += bno055_convert_double_euler_r_deg(&d_euler_data_r);
        comres += bno055_convert_double_euler_p_deg(&d_euler_data_p);
        comres += bno055_convert_double_euler_h_rad(&d_euler_data_h);
        comres += bno055_convert_double_euler_r_rad(&d_euler_data_r);
        comres += bno055_convert_double_euler_p_rad(&d_euler_data_p);
        comres += bno055_convert_double_euler_hpr_deg(&d_euler_hpr);
        comres += bno055_convert_double_euler_hpr_rad(&d_euler_hpr);

    /*	API used to read Linear acceleration data output as m/s2
        float functions also available in the BNO055 API */
        comres += bno055_convert_double_linear_accel_x_msq(
        &d_linear_accel_datax);
        comres += bno055_convert_double_linear_accel_y_msq(
        &d_linear_accel_datay);
        comres += bno055_convert_double_linear_accel_z_msq(
        &d_linear_accel_dataz);
        comres += bno055_convert_double_linear_accel_xyz_msq(
        &d_linear_accel_xyz);

    /*	API used to read Gravity sensor data output as m/s2
        float functions also available in the BNO055 API */
        comres += bno055_convert_gravity_double_x_msq(&d_gravity_data_x);
        comres += bno055_convert_gravity_double_y_msq(&d_gravity_data_y);
        comres += bno055_convert_gravity_double_z_msq(&d_gravity_data_z);
        comres += bno055_convert_double_gravity_xyz_msq(&d_gravity_xyz);
        

        
        
        /*********************************************************************/
        /*                     Output some of the Values                     */
        /*********************************************************************/
        sendText(makeString1("Dataset Number: ", cnt, ":\n"));
        sendText(makeString0("############################################################\n\n"));
//        raw x-y-zAchsis values of the accelaration, magnet, and gyro sensor
 //       accel_xyz
                sendText(makeString1("Accel X_Raw: ", accel_xyz.x, "\n"));
                sendText(makeString1("Accel Y_Raw: ", accel_xyz.y, "\n"));
                sendText(makeString1("Accel Z_Raw: ", accel_xyz.z, "\n\n"));
 //       mag_xyz
                sendText(makeString1("Magnet X_Raw: ", mag_xyz.x, "\n"));
                sendText(makeString1("Magnet Y_Raw: ", mag_xyz.y, "\n"));
                sendText(makeString1("Magnet Z_Raw: ", mag_xyz.z, "\n\n"));
  //      gyro_xyz
                sendText(makeString1("Gyro X_Raw: ", gyro_xyz.x, "\n"));
                sendText(makeString1("Gyro Y_Raw: ", gyro_xyz.y, "\n"));
                sendText(makeString1("Gyro Z_Raw: ", gyro_xyz.z, "\n\n"));
 //       raw euler value of x-y-z-
  //      combined values as raw
   //     euler_hrp
                sendText(makeString1("Euler H_Raw: ", euler_hrp.h, "\n"));
                sendText(makeString1("Euler R_Raw: ", euler_hrp.r, "\n"));
                sendText(makeString1("Euler P_Raw: ", euler_hrp.p, "\n\n"));
   //     quaternion_wxyz
                sendText(makeString1("Quaternion W_Raw: ", quaternion_wxyz.w, "\n"));
                sendText(makeString1("quaternion X_Raw: ", quaternion_wxyz.x, "\n"));
                sendText(makeString1("quaternion Y_Raw: ", quaternion_wxyz.y, "\n"));
                sendText(makeString1("quaternion Z_Raw: ", quaternion_wxyz.z, "\n\n"));
    //    linear_acce_xyz
                sendText(makeString1("linear_acceleration X_Raw: ", linear_acce_xyz.x, "\n"));
                sendText(makeString1("linear_acceleration Y_Raw: ", linear_acce_xyz.y, "\n"));
                sendText(makeString1("linear_acceleration Z_Raw: ", linear_acce_xyz.z, "\n\n"));
    //    gravity_xyz
                sendText(makeString1("gravity X_Raw: ", gravity_xyz.x, "\n"));
                sendText(makeString1("gravity Y_Raw: ", gravity_xyz.y, "\n"));
                sendText(makeString1("gravity Z_Raw: ", gravity_xyz.z, "\n\n"));
     //   converted VAlues
    //    d_accel_xyz             //axel values as mg
                sendText(makeString1f("accelleration Double X_Value: ", d_accel_xyz.x, "\n"));
                sendText(makeString1f("accelleration Double Y_Value: ", d_accel_xyz.y, "\n"));
                sendText(makeString1f("accelleration Double Z_Value: ", d_accel_xyz.z, "\n\n"));
     //   d_mag_xyz               //mag as double value
                sendText(makeString1f("Magnet Double X_Value: ", d_mag_xyz.x, "\n"));
                sendText(makeString1f("Magnet Double Y_Value: ", d_mag_xyz.y, "\n"));
                sendText(makeString1f("Magnet Double Z_Value: ", d_mag_xyz.z, "\n\n"));
     //   d_gyro_xyz              //gyro as rps value
                sendText(makeString1f("Gyro Double X_Value: ", d_gyro_xyz.x, "\n"));
                sendText(makeString1f("Gyro Double Y_Value: ", d_gyro_xyz.y, "\n"));
                sendText(makeString1f("Gyro Double Z_Value: ", d_gyro_xyz.z, "\n\n"));     
    //    d_euler_hpr             //euler as radiant
                sendText(makeString1f("Euler Double H_Value: ", d_euler_hpr.h, "\n"));
                sendText(makeString1f("Euler Double P_Value: ", d_euler_hpr.p, "\n"));
                sendText(makeString1f("Euler Double R_Value: ", d_euler_hpr.r, "\n\n"));
    //    d_linear_accel_xyz      //linear accel values as double
                sendText(makeString1f("linear acceleration Double X_Value: ", d_linear_accel_xyz.x, "\n"));
                sendText(makeString1f("linear acceleration Double Y_Value: ", d_linear_accel_xyz.y, "\n"));
                sendText(makeString1f("linear acceleration Double Z_Value: ", d_linear_accel_xyz.z, "\n\n"));     
   //     d_gravity_xyz           //gravity as double values
                sendText(makeString1f("gravity Double X_Value: ", d_gravity_xyz.x, "\n"));
                sendText(makeString1f("gravity Double Y_Value: ", d_gravity_xyz.y, "\n"));
                sendText(makeString1f("gravity Double Z_Value: ", d_gravity_xyz.z, "\n\n"));
                
                
        sendText(makeString0("############################################################\n\n"));
                
        cnt++;
                
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

/*******************************************************************************
 End of File
 */
