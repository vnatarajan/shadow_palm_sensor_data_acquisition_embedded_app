/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_bno055.h

  @Summary
    Interface funktions to use the bno055 api-set provided by Bosch
 */
/* ************************************************************************** */
#include "Bosch_bno055.h"
#include "CITEC_bno055_Config.h"

#ifndef _CITECbno055_I2C_H    /* Guard against multiple inclusion */
#define _CITECbno055_I2C_H

/*******************************************************************************
  Function:
    void CITEC_BNO055_Init (Bosch_bno055 *bno055)

  Summary:
    Init function to enable the Driver and link nessecary Data to the
    Sensor-Funktions.

  Description:
    This function enables the User to use the bno055_Funktions from Bosch
    by linking Funktions and Data to a strct that is used within the Funktions.

  Precondition:
    None.

  Parameters:
    Bosch_bno055 *bno055: Holds all Data cncerning one bno055 Sensor and how to
    address one.

  Returns:
    None.

  Example:
    <code>
    APP_Initialize();
    </code>

  Remarks:
    This routine must be called from the SYS_Initialize function.
*/
void CITEC_BNO055_Init(Bosch_bno055 *bno055);

/*******************************************************************************
  Function:
    s8 CITEC_BNO055_I2C_WD (u8 dev_addr, u8 reg_addr, u8* reg_data, u8 wr_len)

  Summary:
    I2C-Write-Funktion implemented in a way needed by the bno055 api

  Description:
    This function interfaces between the Bosch bno055 api and the PIC32 I2C Module.
    It implements a write, using the Data given by the api.
    The funktion is implemented non blocking

  Precondition:
    CITEC_BNO055_Init (Bosch_bno055 *bno055) needs to be called first.

  Parameters:
    u8 dev_addr  : 7Bit Device Address of the I2C-Slave
    u8 reg_addr  : 8Bit REgister address of the Register to write to
    u8* reg_data : Poiter to the Data array wich should be send
    u8 wr_len    : Number of Bytes held by the Data array

  Returns:
    s8 converted DRV_I2C_BUFFER_HANDLE of the Write opperation.

  Remarks:
    This routine is called by the bno055 api and does not need to be called by 
    the user itselsf
*/
s8 CITEC_BNO055_I2C_WD(u8 dev_addr, u8 reg_addr, u8* reg_data, u8 wr_len);

/*******************************************************************************
  Function:
    s8 CITEC_BNO055_I2C_RD(u8 dev_addr, u8 reg_addr, u8* reg_data, u8 r_len)

  Summary:
    I2C-Read-Funktion implemented in a way needed by the bno055 api

  Description:
    This function interfaces between the Bosch bno055 api and the PIC32 I2C Module.
    It implements a read, using the Data given by the api.
    The funktion is implemented non blocking

  Precondition:
    CITEC_BNO055_Init (Bosch_bno055 *bno055) needs to be called first.

  Parameters:
    u8 dev_addr  : 7Bit Device Address of the I2C-Slave
    u8 reg_addr  : 8Bit Register address of the Register to read from
    u8* reg_data : Poiter to the Data array wich should store the read data
    u8 r_len     : Number of Bytes to read.

  Returns:
    s8 converted DRV_I2C_BUFFER_HANDLE of the write_read opperation.

  Remarks:
    This routine is called by the bno055 api and does not need to be called by 
    the user itselsf
*/
s8 CITEC_BNO055_I2C_RD(u8 dev_addr, u8 reg_addr, u8* reg_data, u8 r_len);


/*******************************************************************************
  Function:
    void bno055Dealy (u32 time_MS)

  Summary:
    Delay Funktion needed by the bno055 api. Blocks the running Task.

  Precondition:
    None.

  Parameters:
    u32 time_MS: time to Delay in ms.

  Returns:
    None.
*/
void bno055Dealy(u32 time_MS);



    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /*_CITECbno055_I2C_H*/

/* *****************************************************************************
 End of File
 */
