/* ************************************************************************** */
/** Descriptive File Name

  @Company
    CITEC

  @File Name
    CITEC_bno055.c

  @Summary
    Interface funktions to use the bno055 api-set provided by Bosch

 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */
#include "driver/i2c/src/drv_i2c_local.h"
#include "CITEC_app.h"
#include "CITEC_bno055.h"


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

// *****************************************************************************
void CITEC_BNO055_Init(Bosch_bno055 *bno055)
{
    //Open the I2C Module
    BNO055_I2C_Handle = DRV_I2C_Open(DRV_I2C_BNO055, DRV_IO_INTENT_READWRITE);
    if(BNO055_I2C_Handle==0)
    {
        SYS_ASSERT(false, "Opening the I2C Module failed");
    }
   
    CITEC_I2CSemaphore=xSemaphoreCreateBinary();
    while(CITEC_I2CSemaphore==NULL)
    {
        SYS_ASSERT(false, "CITEC_SPISemaphore could not be created.");
        while(1);
    }
     
    //link the I2C functions and nessesely Data to the struct
    bno055->bus_write=CITEC_BNO055_I2C_WD;
    bno055->bus_read=CITEC_BNO055_I2C_RD;
    bno055->delay_msec=bno055Dealy;
    bno055->dev_addr=BNO055_I2C_ADDR;
    
    //you could add an additional sensor initialisation (Powermode etc.) here
}


/** 
  @Function
    int ExampleInterfaceFunctionName ( int param1, int param2 ) 

  @Summary
    Brief one-line description of the function.

  @Remarks
    Refer to the example_file.h interface header for function usage details.
 */
s8 CITEC_BNO055_I2C_WD(u8 dev_addr, u8 reg_addr, u8* reg_data, u8 wr_len)
{
    DRV_I2C_BUFFER_HANDLE TXHandle;
    static u8 TXbuffer[TXBUFFERSIZE];   
    u8 cnt;
    
    dev_addr=dev_addr<<1;
    
    if(wr_len>=(TXBUFFERSIZE-1))
    {
        SYS_ASSERT(false, "The number of bytes to write was greater than expected");
        return 1;
    }
    //map the data to the TXbuffer
    TXbuffer[0]=reg_addr;    
    for(cnt=0; cnt<wr_len; cnt++)
    {
        TXbuffer[(cnt+1)]=*(reg_data+cnt);
    }


    TXHandle=DRV_I2C_Transmit (  BNO055_I2C_Handle, dev_addr, &TXbuffer[0], (sizeof(u8)*(wr_len+1)), NULL);    
    //  A callback and blocking would be nice but is not jet implemented in the function (see docu for further info)
    if(xSemaphoreTake(CITEC_I2CSemaphore, I2C_WAITTIME_MS)==pdTRUE)
    {
        //block as long as I2C running
    }
    else
    {//I2C did not finish succesfully
        SYS_ASSERT(false, "I2C could not end its communication!");
        return TXHandle;
    }
    
    //return the handle to indicate fail/succsess
    return TXHandle;
}

s8 CITEC_BNO055_I2C_RD(u8 dev_addr, u8 reg_addr, u8* reg_data, u8 r_len)
{
    DRV_I2C_BUFFER_HANDLE RXHandle;
    
    dev_addr=dev_addr<<1;
        
    RXHandle=DRV_I2C_TransmitThenReceive ( BNO055_I2C_Handle, dev_addr, &reg_addr, (sizeof(u8)), reg_data, (sizeof(u8)*r_len), NULL);
//A callback to block until I2C is done
    if(xSemaphoreTake(CITEC_I2CSemaphore, I2C_WAITTIME_MS)==pdTRUE)
    {
        //block as long as I2C running
    }
    else
    {//I2C did not finish succesfully
        SYS_ASSERT(false, "I2C could not end its communication!");
        return RXHandle;
    }
 
   //return the hadle to return fail/succsess
    return RXHandle;   //everything went well
}



void bno055Dealy(u32 time_MS)
{   
    if(time_MS>0)
        vTaskDelay(pdMS_TO_TICKS(time_MS));
    else
        SYS_ASSERT(false, "The delay time can not be negative!");
}


/* *****************************************************************************
 End of File
 */
