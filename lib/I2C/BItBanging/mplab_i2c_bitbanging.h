#ifndef MPLAB_ARDUINO_I2C
#define MPLAB_ARDUINO_I2C

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_definitions.h"

//Public
extern void     i2c_beginTransmission(uint8_t i2c_address);
extern void     i2c_endTransmission(void);
extern void     i2c_restartTransmission(uint8_t i2c_address);
extern void     i2c_write8(uint8_t data);
extern void     i2c_requestFrom(uint8_t i2c_address, uint8_t n_bytes, uint8_t *data);
extern void     i2c_begin();
extern void     i2c_init();
extern void     i2c_write16(uint16_t data);
extern uint8_t  i2c_read8(uint8_t address);

#define I2CDELAYINUSSHORT   3
#define I2CDELAYINUSLONG    3



//Private
bool i2c_read_ack(void);
void i2c_write_bit(bool bit);
bool i2c_read_bit(void);
void i2c_beginTransmission_common();
#endif
