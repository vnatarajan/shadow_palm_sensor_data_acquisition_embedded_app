#include "mplab_i2c_bitbanging.h"
#include "app_delay.h"
#include "system_config.h"

static uint8_t raw_i2c_read8(bool ack);

void i2c_init()
{
    i2c_begin();   
    app_delay(100);
}

void I2C_delay(bool bLong)
{
    if (bLong)
        DelayUs(I2CDELAYINUSLONG);
    else
        DelayUs(I2CDELAYINUSSHORT);
}

void i2c_beginTransmission_common(){
    set_SDA();                  // set SDA to 1
    I2C_delay(false);
    set_SCL();
    while (read_SCL() == 0)     // Clock stretching
    {
        I2C_delay(false);
    }
    I2C_delay(true);            // Repeated start setup time, minimum 4.7us

    if (read_SDA() == 0)
    {
        i2c_begin();			// Arbitration lost
    }
    clear_SDA();                  // SCL is high, set SDA from 1 to 0.
    I2C_delay(false);
    clear_SCL();
}

void i2c_beginTransmission(uint8_t i2c_address)
{
    uint8_t i2c_address_write_byte;
    i2c_beginTransmission_common();
    i2c_address_write_byte = (((i2c_address & 0x7F) << 1) & 0xFE);
    i2c_write8(i2c_address_write_byte);
}

void i2c_restartTransmission(uint8_t i2c_address)
{
    //i2c_endTransmission();
    i2c_beginTransmission(i2c_address);
}

void i2c_endTransmission()
{
    // set SDA to 0
    clear_SDA();
    I2C_delay(false);
    set_SCL();

    while (read_SCL() == 0)         // Clock stretching
    {
        I2C_delay(false);
    }
    I2C_delay(true);                // Stop bit setup time, minimum 4us
    set_SDA();                      // SCL is high, set SDA from 0 to 1
    I2C_delay(false);

    if (read_SDA() == 0) {
        i2c_begin();                // Arbitration lost
    }

}

bool i2c_read_ack(void)
{
    bool bit;
    set_SDA();                      // Let the slave drive data
    I2C_delay(true);                // Wait for SDA value to be written by slave, minimum of 4us for standard mode
    set_SCL();                      // Set SCL high to indicate a new valid SDA value is available
    while (read_SCL() == 0)         // Clock stretching
    { 
        I2C_delay(false);
    }
    I2C_delay(true);                // Wait for SDA value to be written by slave, minimum of 4us for standard mode
    bit = read_SDA();               // SCL is high, read out bit
    clear_SCL();                    // Set SCL low in preparation for next operation
    return bit;
}


static uint8_t raw_i2c_read8(bool ack)
{
    unsigned char byte = 0;
    unsigned char bit;

    for (bit = 0; bit < 8; ++bit)
    {
        byte = (byte << 1) | i2c_read_bit();
    }

    if(ack){
        i2c_write_bit(false);
    }else{
        i2c_write_bit(true);
        i2c_endTransmission();
    }

    return byte;

}

uint8_t i2c_read8(uint8_t address)
{
    uint8_t buf[2];
    i2c_requestFrom(address, 1, &buf[0]);
    return buf[0];
}

void i2c_write16(uint16_t data)
{
    uint8_t msb = (data >> 8) & 0xFF;
    uint8_t lsb = data & 0xFF;
    i2c_write8(msb);
    i2c_write8(lsb);
}


void i2c_write8(uint8_t data)
{
    unsigned bit;
    bool ack;

    for (bit = 0; bit < 8; ++bit)
    {
        i2c_write_bit((data & 0x80) != 0);
        data <<= 1;
    }

    ack = i2c_read_ack();
}

void i2c_requestFrom(uint8_t i2c_address, uint8_t n_bytes, uint8_t *data)
{
    uint8_t i2c_address_read_byte;
    int i;
    
    if(data == NULL){
        //OOM
        return;
    }


    i2c_beginTransmission_common();
    
    i2c_address_read_byte = (((i2c_address & 0x7F) << 1) | 0x01);
    i2c_write8(i2c_address_read_byte);

    for(i=0; i < ( n_bytes - 1 ); i++){
        data[i] = raw_i2c_read8(true);
    }

    data[ n_bytes - 1 ] = raw_i2c_read8(false);
}

void i2c_begin()
{
    // Do nothing
    SCL1Input();
    SDA1Input();
    I2C_delay(true);
    set_SDA();
    set_SCL();
    SCL1Output();
    SDA1Output();
}

void i2c_write_bit(bool bit)
{
    if (bit)
    {
        set_SDA();
    } else
    {
        clear_SDA();
    }

    I2C_delay(false);               // SDA change propagation delay
    set_SCL();                      // Set SCL high to indicate a new valid SDA value is available
    I2C_delay(true);                // Wait for SDA value to be read by slave, minimum of 4us for standard mode
    while (read_SCL() == 0)         // Clock stretching
    {
        I2C_delay(false);
    }
    // SCL is high, now data is valid
    if (bit && (read_SDA() == 0))   // If SDA is high, check that nobody else is driving SDA
    {
		i2c_begin();                // Arbitration lost
    }
    
    clear_SCL();
}

bool i2c_read_bit(void)
{
    bool bit;
    set_SDA(); 
    I2C_delay(true);                // Wait for SDA value to be written by slave, minimum of 4us for standard mode
    set_SCL();                      // Set SCL high to indicate a new valid SDA value is available
    while (read_SCL() == 0)         // Clock stretching
    { 
        I2C_delay(false);
    }
    I2C_delay(true);                // Wait for SDA value to be written by slave, minimum of 4us for standard mode
    bit = read_SDA();               // SCL is high, read out bit
    clear_SCL();                    // Set SCL low in preparation for next operation
    return bit;
}






