#ifndef TOF_H
#define TOF_H
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

//#include "system_config.h"

#define ADDRESS_VL6180X                             0x29
#define ADDRESS_VL6180X_1                           0x35
#define ADDRESS_GPIO                                0x42



void tof_init();
double tof_read(uint8_t i2c_slave_address);
void sensor_1_init();
void sensor_2_init();
void i2c_device_address(uint8_t new_addr);
void VL6180X_WRITE_BYTE(uint8_t i2c_slave_address, uint16_t register_address, uint8_t data) ;
uint8_t VL6180X_READ_BYTE(uint8_t i2c_slave_address, uint16_t register_address) ;
#endif
