 
#include "mplab_i2c_bitbanging.h"
#include "app_delay.h"
#include "tof.h"
#include "system_config.h"
#include "CITEC_USBCDC.h"
/* I2C Slave Address*/
//#define ADDRESS_VL6180X                             0x29
//#define ADDRESS_VL6180X_1                           0x35
//#define ADDRESS_GPIO                                0x42

/* Register Address */
#define REG_ADDRESS_GPIO_DIRECTION                  0x14
#define REG_ADDRESS_GPIO_STATE                      0x12
#define REG_ADDRESS_SYS_RANGE_START                 0x018
#define REG_ADDRESS_RESULT_STATUS_INT_STATUS_GPIO   0x04f
#define REG_ADDRESS_RES_RANGE_VAL                   0x062
#define REG_ADDRESS_INTERRUPT                       0x015
#define I2C_SLAVE_DEVICE_ADDRESS                   0x212


#define VL6180X_MAX_RANGE_MM			200 /*200 mm*/
#define VL6180X_MAX_RANGE_DECIMAL		255 /*8 bit*/

#define ENABLE      0x01
#define DISABLE     0x00
#define USB_DATA_BUFFER 20
//#define STARTER_KIT 
////////////////

//typedef struct{
////    double pixels[64];
//    double tof_1;
//    double tof_2;
////	struct quaternion qn;
//}sensor_fusion_h;


//CDC_DataType data;
//char *sensor_out=NULL;
//char clubbed_sensor_data[USB_DATA_BUFFER];
    
//void clear_sens_data(sensor_fusion_h *s){
////    int i;
////    for(i=0;i<64;i++){
////        s->pixels[i] = 0.0;
////    }
////    
////    s->qn.w = 0.0;
////    s->qn.x = 0.0;
////    s->qn.y = 0.0;
////    s->qn.z = 0.0;
//    
//    s->tof_1 = 0.0;
//    s->tof_1 = 0.0;
//    
//
//}
//
//void clear_buf(char *buf, int size)
//{   
//    int i;
//    
//    for(i=0; i<size; i++){
//        buf[i] = '\0';
//    }
//
//}
//
//void append_sensor_val(char *out, double sval, uint8_t last_value)
//{
//    char tbuf[16];
//
//    if(last_value){
//        snprintf(tbuf, 16, "%f;\n", sval);
//    }else{
//        snprintf(tbuf, 16, "%f,", sval);
//    }   
//    
//    strcat(out, tbuf);
//}
//char *encode_sensor_data(sensor_fusion_h s)
//{
//    int i;
//    clear_buf(clubbed_sensor_data, USB_DATA_BUFFER);
//
//
//    append_sensor_val(clubbed_sensor_data, s.tof_1, 0); 
//    append_sensor_val(clubbed_sensor_data, s.tof_2, 0);
////    append_sensor_val(clubbed_sensor_data, s.qn.x, 0);
////    append_sensor_val(clubbed_sensor_data, s.qn.y, 0);
////    append_sensor_val(clubbed_sensor_data, s.qn.z, 0);
//
////    for(i=0;i<63;i++){
////        append_sensor_val(clubbed_sensor_data, s.pixels[i], 0);
////    }
////        append_sensor_val(clubbed_sensor_data, s.pixels[63], 1);
//    return clubbed_sensor_data;
//}





void set_pin_as_output()
{  
    i2c_beginTransmission(ADDRESS_GPIO); 
    i2c_write8(REG_ADDRESS_GPIO_DIRECTION);   
    i2c_write8(ENABLE);  
    i2c_endTransmission();

}

void enable_pin()
{
    i2c_beginTransmission(ADDRESS_GPIO); 
    i2c_write8(REG_ADDRESS_GPIO_STATE);   
    i2c_write8(ENABLE);  
    i2c_endTransmission();
}

void VL6180X_INIT_REGISTERS(uint8_t i2c_slave_address)
{

    if (VL6180X_READ_BYTE(i2c_slave_address, 0x016) == 0x01){
    //private registers
    
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0207, 0x01);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0208, 0x01);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0096, 0x00);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0097, 0xfd);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00e3, 0x01);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00e4, 0x03);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00e5, 0x02);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00e6, 0x01);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00e7, 0x03);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00f5, 0x02);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00d9, 0x05);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00db, 0xce);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00dc, 0x03);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00dd, 0xf8);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x009f, 0x00);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00a3, 0x3c);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00b7, 0x00);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00bb, 0x3c);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00b2, 0x09);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00ca, 0x09);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0198, 0x01);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x01b0, 0x17);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x01ad, 0x00);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x00ff, 0x05);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0100, 0x05);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0199, 0x05);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x01a6, 0x1b);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x01ac, 0x3e);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x01a7, 0x1f);
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0030, 0x00);
    
    //public registers
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0011, 0x10); 
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x010a, 0x30); 
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x003f, 0x46); 
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0031, 0xFF); 
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0041, 0x63); 
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x002e, 0x01); 
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x001b, 0x09); 
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x003e, 0x31); 
    VL6180X_WRITE_BYTE(i2c_slave_address, 0x0014, 0x24); 
  }  

}

void VL6180X_START_RANGE_MEASUREMENT(uint8_t i2c_slave_address)
{
    VL6180X_WRITE_BYTE(i2c_slave_address, REG_ADDRESS_SYS_RANGE_START, 0x01);   
}

void VL6180X_CLEAR_INTERRUPT(uint8_t i2c_slave_address)
{
    VL6180X_WRITE_BYTE(i2c_slave_address, REG_ADDRESS_INTERRUPT, 0x07);       
}

/* Return range value in mm */
double VL6180X_POLL_RANGE_MEASUREMENT(uint8_t i2c_slave_address)
{
    double range = 0.0;
	uint8_t reading = 0;
	uint8_t error_bit = 0;
    uint8_t reg_status = (VL6180X_READ_BYTE(i2c_slave_address, REG_ADDRESS_RESULT_STATUS_INT_STATUS_GPIO) & 0x07);

    while (reg_status != 0x04 ){
        app_delay(10);
        reg_status = (VL6180X_READ_BYTE(i2c_slave_address, REG_ADDRESS_RESULT_STATUS_INT_STATUS_GPIO)) & 0x07;
  	    
        
        error_bit = (reg_status >> 6);

        if(error_bit > 0 ){
            break;	
        }
        


    }
    
    reading = VL6180X_READ_BYTE(i2c_slave_address, REG_ADDRESS_RES_RANGE_VAL); 
    range =(double) ((double) (reading ) * ((double) VL6180X_MAX_RANGE_MM / (double)VL6180X_MAX_RANGE_DECIMAL));
    VL6180X_CLEAR_INTERRUPT(i2c_slave_address);

    return range;
}

void VL6180X_WRITE_BYTE(uint8_t i2c_slave_address, uint16_t register_address, uint8_t data) 
{
    i2c_beginTransmission(i2c_slave_address);
    i2c_write16(register_address);
    i2c_write8(data);
    i2c_endTransmission();
    app_delay(1);
}

uint8_t VL6180X_READ_BYTE(uint8_t i2c_slave_address, uint16_t register_address) 
{
    i2c_beginTransmission(i2c_slave_address);
    i2c_write16(register_address);
    i2c_endTransmission();//TODO CHECK removed False
    //i2c_beginTransmission(i2c_slave_address); 
    //i2c_restartTransmission(i2c_slave_address);
    return i2c_read8(i2c_slave_address);
}

void tof_init()
{

    TOF1_GPIO0Off();
    TOF2_GPIO0Off();
    sensor_1_init();
    sensor_2_init();
}

void sensor_1_init(){
    TOF1_GPIO0On();
    app_delay(100);
    i2c_device_address(ADDRESS_VL6180X_1);
    app_delay(100);
    VL6180X_INIT_REGISTERS(ADDRESS_VL6180X_1);
    app_delay(2000);
}

void sensor_2_init(){
    TOF1_GPIO0On();
    TOF2_GPIO0On();
    app_delay(100);
    VL6180X_INIT_REGISTERS(ADDRESS_VL6180X);
    app_delay(2000);
}

double tof_read(uint8_t i2c_slave_address)
{
    double tof_reading = 0.0;
    VL6180X_START_RANGE_MEASUREMENT(i2c_slave_address);
    tof_reading = VL6180X_POLL_RANGE_MEASUREMENT(i2c_slave_address);
    return tof_reading;
}

void i2c_device_address(uint8_t new_addr)
{
    VL6180X_WRITE_BYTE(ADDRESS_VL6180X, I2C_SLAVE_DEVICE_ADDRESS, new_addr);
}














