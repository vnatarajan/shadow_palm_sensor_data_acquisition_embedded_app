#####################################################################################
## REVISIONS (list newer Revisions on top)
## Date         Author             Description
## 2019-25-04   Luis Oberröhrmann  First release of the library
#####################################################################################
## Abstract
## This library connets to a WIFI newtwork and implements options to Send/Receive
## data within. (PIC32, Harmony, RTOS) MRF24WN Wi-Fi Device. IPv4
## Sending Data, is done with a UDP Protocol (no verification)
## Receiving Data, is done with a TCP Protocol (with verefication)
##
#####################################################################################
## Usage & library interface
##
## Step 1: Add app_wifi_mrf24wn.h, app_wifi_winc1500.h, CITEC_WIFI.h, 
## CITEC_WIFIUDPTCP.h, CITEC_WIFI.c and CITEC_WIFIUDPTCP.c to your project.
## add #include CITEC_WIFI.h and #include CITEC_WIFIUDPTCP.h  to system_config.h
##
## Step 2: Addaptions defines in CITEC_WIFIUDPTCP.h: 
##    - if included #define CITECWIFIProtocol_DestinationIP "192.168.178.23"
##      data will always be send to the specified IP adress
##    - Destination/Server Port describes on wich port data will be send to / read at
##    - UDP_DATAPACKET_SIZE decides how many bytes can be received/send
##      does influence the required storage space
##
## Step 3: Harmony Configurations:
##         Please import the WIFI.mhc file and change folloing to your specifications:
##         (a full guide for the Harmony configuration from scratch is at the end of this file)
##         Make shure SPI 0 and External Interrupt 0 are unused! To change these see full description at the bottom
##         - Harmony Framework Configuration -> Drivers -> SPI -> SPI Driver Instance 0
##            + SPI Module ID: Select SPI Module the WI-Fi Module is connected to
##         - Harmony Framework Configuration -> Drivers -> Wi-Fi
##            + All MRF24WN Pin settings acording to your Harware Setup
##            + Wi-Fi SSID: Wi-Fi Network SSID to connect to
##            + Wi-Fi WPA-PSK or WPA2-PSK Pass Phrase: Login PW for the Wi-Fi network
##         - Harmony Framework Configuration -> TCPIP Stack -> Network Configuration 0
##            + HostName:       Device Name in the network
##            + Mac Address:    Mac Address the device should have
##            + ip Address:     For static networks without DHCP server, IP Adress of Device
##            + Network Mask:   Network Mask for conigured Device ID
##            + Default Gateway:IP Adress of the Default gateway
##            + Primary DNS:    DNS server Adress to use
##
## Step 4: Include the Task calls in system_tasks.c
##
    static void _CITEC_WIFI_Tasks(void);
    
    void SYS_Tasks ( void )
    {
      //always include
      xTaskCreate((TaskFunction_t) _CITEC_WIFI_Tasks,
                  "CITEC APP WIFI Tasks",
                  1024, NULL, 4, NULL);
                  
      //if data needs to be send (UDP)
      xTaskCreate((TaskFunction_t) CITECWIFIProtocol_ClientTasks,
                  "CITECWIFIProtocol_ClientTasks",
              2024, NULL, 3, NULL);

        //if data needs to be received (TCP)
        xTaskCreate((TaskFunction_t) CITECWIFIProtocol_ServerTasks,
                  "CITECWIFIProtocol_ServerTasks",
              1024, NULL, 3, NULL);
    }//end SYS_Tasks
    
    static void _CITEC_WIFI_Tasks(void)
    {
        
        CITEC_WIFI_Initialize();
        
        while(1)
        {
            CITEC_WIFI_Tasks();       
            vTaskDelay(1 / portTICK_PERIOD_MS);
        }
    }
            
##
##
## Step 6: Link your Sensor Data to the Serial Protocol:
##	    When new Sensor Data is available do:
##	     (Make shure the Data is available until new pointer is send via queue)

            UpdateData.SensorDatagramID=2;		//Send Datagram ID of Sensor
            UpdateData.data=&Data;			    //Send Pointer to Data
            //Send Datapointer to Serial Protokol
            errStatus = xQueueSend(CITECSerialDataUpdate_Queue, &UpdateData,0);
            if (errStatus == errQUEUE_FULL)
            {
                configASSERT(0);
            } 
##
## Step 7: Interface with the Tasks to send/Receive Data
##  - To receive Data check "CITECWIFIProtocol_ReceiveQueue"
##    new data and its source will be published there
##
      CITECWIFIProtocol_DataStruct inData;  //see CITEC_WIFIUDP for struct description
      
      xQueueReceive(CITECWIFIProtocol_ReceiveQueue, &inData, 20);
      //inData.data         //holds received String
      //inData.size         //length of data string
      //inData.Source.Adress//source of data
##
##  - To send data call CITECWIFIProtocol_Send()
##    make shure the Data is static, only pointer will be used here
##
      static CITECWIFIProtocol_DataStruct outData;
      
      outData.size=19;  //size of data to send
      //get data in position
      memcpy(outData.data, "some string to send", outData.size);
      //send to source of previous received data
      retData.Source = inData.Source;
      // or send to specific ip
      TCPIP_Helper_StringToIPAddress("192.168.178.23", &outData.Source.Adress);
      //or use define in headder to always send to specified ip, then outData.Source.Adress
      //can be left blank
      
      //send data
      CITECWIFIProtocol_Send(&outData);
##
#####################################################################################
## TODO:
## - Change SPI buffer Mode to DMA to save CPU load (Got system crashes while Testing DMA)
## - add ipv6 support (shold be done with smale changes)
#####################################################################################
## Known Issues / Solutions:
## A common issue is Heap/Stack Problem.
## Make shure the project settings match with the harmony settings
## Aditional to the task heap requeirements the tcp stack uses quite some space to 
## manage connections an data.
## (configurable in Harmony Framework Configuration -> TCPIP Stack -> Select Heap Configuration)
## The TCP/IP Stack Dynamik Ram Size is essential for a working project, the Projects configured
## size should be slightly greater than the specified value, but match with required heap
##
## Sometimes conecting to the Wi-Fi does take some time, be patient or restart Debugging
## (noticed especially while stopping debug by debugging changed version, restart Program!)
##
#####################################################################################
## Example:
## The Example Programm will connect to a test WiFi:
## SSID: GraspLab Passphrase: CITEC1234567
## 
## Within this network a continously Data output to 192.168.178.23/7500 is done.
## This settings can easily be changed within Harmony (see above) and in the app.c
## (the ip is hardcoded there)
## The output contains 1 to 100 bytes(adding one each send, restart at 100), which mirrow the array position of id byte
## 
## Data will be received on 192.168.178.22/7501 (ip may change depending on Network settings!
## lookup ip within router)
## Received data will be send right back to the device wich did send the data.
##
## Test Setup:
## The Program is written for the iObject+, with a PIC32MZ2048EFM100,
## The used WIFI Module is:
## Pinout:
## CipSelect: B15
## Interrupt: B8 (INT2)
## PowerPin:  A1
## ResetPin
## SPI Module ID3 as Driver Instance 0 with Pins:
## SCK: B14
## CS:  B15
## MISO:D14
## MOSI:D15
##
## Wi-Fi Network (SSID/Pass mentioned above):
## tested with a Fritzbox 4020
## and a Netgear FWG114P
## both routers had DHCP enabled.
## Configured with WPA2-PSK security
## Use of IPv4 adresses
##
## Send/receive Test packages:
## Send TCP Packages: "Packet Sender" in TCP Send mode
## "https://packetsender.com/"
## Receive Packages either with Wireshark (with root on receiving PC)
## alternativ with Microsoft Store(Win10) "UDP - Sender/Reciever" 
## (does not always show actual Data, bit buggy! no root)
#####################################################################################
## Full manual Harmony Configuration:
## (No waranty, many cross dependancies, proceed with cautions, import .mhc if possible)
## work from top to bottom, dont change order
##
## - -> Pin Setting
##  + Setup SPI Pins (SCK/MOSI/MISO/CS(out)) to connect to the module
##  + set Wi-Fi interrupt as interrupt Pin (External Interrupt or Change Notification)
##    *If use of External Interrupt:
##      ~ -> Harmony Framework Configuration -> System Services -> Interrupts
##        °(check) Use Interrupt System Service?
##        °-> (check) Use External Interrups?
##          %->(check) External Interrupt Instance x
##            - External Interrupt Module ID: Int source from Pin setup
##            - -> (check) Generate ISR Code?
##              + Setup Leves as you see fit
##              + Polarity: Int_EDGE_TRIGGER_FALLING
##              + (check) Enabled by System Service?
## - -> Harmony Framework Configuration -> Cryptographic (Crypto) Library
##  + (check) Use Cryptographic Library
##  + -> Hashes
##    *(check) Use MD5?
##    *(check) Use SHA-1?
##    *(check) Use SHA-256?
##    *(check) Use HMAC?
##  + -> Random Numbers
##    *(check) Use Random Number Functions?
## - -> Harmony Framework Configuration -> Drivers -> Wi-Fi
##  + -> (check) Use Wi-Fi Driver?
##    * Wi-Fi Device: choose your Device (Library written witch MRF24WN, others may require different settigs and code)
##    * Use External Interrupt or Use Interrupt for Change Notification: select acording to Pin setting
##    * If use of External Interrupt:
##      ~ Wi-Fi Driver External Interrupt Instace: Number of Interrupt Instance of Wi-Fi Module configured above:
##    * Wi-Fi Driver SPI Instnace Index: Choose first free SPI Index
##    * MRF24WN XXX Pin Port Channel / Bit Position: acording to Hardware setup
##    * Wi-Fi Network Type: Infrastructure:
##    * Wi-Fi Security Mode: Addapt to Wi-Wi Settings
##    * Wi-Fi SSID: Wi-Fi Network name
##    * Wi-Fi WPA-PSK or WPA2-PSK Pass Phrase: Wi-Fi Networks Passphrase
## - -> Harmony Framework Configuration -> Drivers -> SPI
##  + -> (check) Use SPDriver?
##    * Dreiver Implementation: DYNAMIC
##  + (check) Use Interrupt Mode?
##  + (check) Use Master Mode?
##  + (check) Use Enhanced Buffer (FIFO) Mode?
##  + (check) Use 8-bit Mode?
##  + Number of SPI Driver Instances: as required, min 1
##  + Number of SPI Driver Clients: as requered, min 1
##  + -> SPI Driver Instance n (as set in Wi-Fi Driver config)
##    * SPI Module ID: Module connected with Pins
##    * (Following settings should be set automatically)
##    * (check) -> Master/Slave Mode -> Master
##    * (check) -> Data Widht -> 8-bit
##    * (check) -> Buffer Mode -> Enhanced
##    * Protocol Type DRV_SPI_PROTOCOL_TYPE_STANDARD
##    * Baud Clock Soutce: your configured clock
##    * Clock/Baud Rate-Hz: 20000000
##    * Clock Mode: DRV_SPE_CLOCK_MODE_IDLE_HIGH_EDGE_FALL
##    * Input Phase: SPI_INPUT_SAMPLING_PHASE_IN_END
## - -> Harmony Framework Configuration -> TCPIP Stack
##  + (check) -> Use TCP\IP Stack?
##    * (check) -> IPv4
##      ~ (check) Enable IPv4 Fragmentation Support?
##    * (check) Use TCP
##    * (check) Use UDP
##    * Number of Network Configurations: 1
##    * (check) -> Network Configuration 0
##      ~ Interface: MRF24WN (choose yours, lib not tested with others tho)
##      ~ HostName:       Device Name in the network
##      ~ Mac Address:    Mac Address the device should have
##      ~ ip Address:     For static networks without DHCP server, IP Adress of Device
##      ~ Network Mask:   Network Mask for conigured Device ID
##      ~ Default Gateway:IP Adress of the Default gateway
##      ~ Primary DNS:    DNS server Adress to use
##      ~ Network MAC Driver: WDRV_MRF24WN_MACObject
##    * (check) -> Select Heap Configuration
##      ~ Use h Config: TXPIP_STACK_HEAP_TYPE_INTERNAL_HEAP
##      ~ TXP/IP Stack Dynamic RAM Size: Depends on your Data Sizes to send/Receive
##                                       should be greater 8K, 40K recomandet (fragmentational looses)
##                                       (This one is a trouble maker, limits Heap Sice and influences Stack requered)
##    * (check) -> TXP/IP Stack Dynamic Allocation Debug
##    * (check) TCP/IP Stack Dynamic Allocation Trace
##    * (check) DHCP Client
##    * (check) -> DNS Client
##      ~ DNS Conntection Type: IP_ADDRESS_TYPE_IPV4
##    * (check) -> ICMPv4 Client and Server                                 
##      ~ (check) Use ICMPv4 Server
##      ~ (check) Use ICMPv4 Client
##    * (check) Use NetBIOS Name Server  
##    * (check) Use Link Local Zero Config
##    * (check) Use iperf Benchmark Tool
##    * (check) Use TCP/IP Commands
## - -> Harmony Framework Configuration -> System Services
##  + (check) -> Console -> Use Console System Service?
##    * (check) Override STDIO?
##    * Select Peripheral For Consol Instance: Use your System console
##      ~ Configure choosen Console as requeired!
##  + (check) -> Debug -> Use Debug System Service
##  + (check) -> Random -> Use Random System Service?
##  + (check) -> Timer -> Use Timer System Service?
##     * Setup to match your Timers configured at
##       -> Harmony Framework Configuration -> Drivers -> Timer -> Use Timer Driver