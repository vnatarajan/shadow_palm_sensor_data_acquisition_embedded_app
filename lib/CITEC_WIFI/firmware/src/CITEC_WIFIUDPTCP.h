/*******************************************************************************
  MPLAB Harmony Application Header File

  Company:
    Microchip Technology Inc.

  File Name:
    CITEC_WIFIUDPTCP.h

  Summary:
    This header file provides prototypes and definitions for the application.

  Description:
    This header file provides function prototypes and data type definitions for
    the application.  Some of these are required by the system (such as the
    "CITEC_WIFIUDPTCP_Initialize" and "CITEC_WIFIUDPTCP_Tasks" prototypes) and some of them are only used
    internally by the application (such as the "CITEC_WIFIUDPTCP_STATES" definition).  Both
    are defined here for convenience.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
//DOM-IGNORE-END

#ifndef _CITEC_WIFIUDPTCP_H
#define _CITEC_WIFIUDPTCP_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system_definitions.h"
#include "tcpip/tcpip.h"

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************


//Device and port Data should be send to

//if commented out The Data will always be send to the ip that send the command
//#define CITECWIFIProtocol_DestinationIP     "192.168.178.23"

//Port to send data to
#define CITECWIFIProtocol_DestinationPort   7500

//Port on wich data will be read
#define CITECWIFIProtocol_ServerPort        7501

//Max Size of one Transmission
#define UDP_DATAPACKET_SIZE                 (100)         //UDP max 65,507

//number of elements allowd in Send/Receve queues
#define CITECWIFIProtocol_SendQueueSize     5
#define CITECWIFIProtocol_ReceiveQueueSize  5

//wait and block times
#define WAITFORSENDSPACE_MS                2

// *****************************************************************************
/* Application States

  Summary:
    Application states enumeration

  Description:
    This enumeration defines the valid application states.  These states
    determine the behavior of the application at various times.
*/

typedef enum
{
    CITECWIFIProtocol_WAIT_INIT,
	/* Application's state machine's initial state. */
    CITECWIFIProtocol_WAIT_FOR_IP,
    CITECWIFIProtocol_WAITING_FOR_COMMAND,
    CITECWIFIProtocol_WAIT_ON_DNS,
    CITECWIFIProtocol_WAIT_FOR_CONNECTION,
    CITECWIFIProtocol_TCPIP_Flush,        
	/* TODO: Define states used by the application state machine. */
    CITECWIFIProtocol_WAIT_FOR_RESPONSE,
    CITECWIFIProtocol_OPENING_SERVER,
    CITECWIFIProtocol_SERVING_CONNECTION,
    CITECWIFIProtocol_CLOSING_CONNECTION,
    CITECWIFIProtocol_ERROR,
} CITECWIFIProtocol_STATES;


// <editor-fold defaultstate="collapsed" desc="Enum description"> 
// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */
//// </editor-fold>
typedef struct
{
    /* The application's current state */
    CITECWIFIProtocol_STATES ServerState;
    CITECWIFIProtocol_STATES ClientState;

    UDP_SOCKET              ServerSocket;
    UDP_SOCKET              ClientSocket;

    uint64_t    mTimeOut;

}CITECWIFIProtocol_DeviceStruct;

// <editor-fold defaultstate="collapsed" desc="Struct description">    
/** Sources_Struct

  @Summary
    Struct to decide where data came from / should go to

  @Description
    Stores the IP address with wich a communication should be done 
  @Remarks
    None.
 */
//// </editor-fold> 
typedef struct
{
    IP_MULTI_ADDRESS Adress;    
}Sources_Struct;
    
// <editor-fold defaultstate="collapsed" desc="Struct description">    
/** CITECWIFIProtocol_DataStruct

  @Summary
    Struct to receive/send Data with

  @Description
    Data in and outgoing, will be handled with this datatype.
    it contains not only the data and information about that, but
    also the adress where Data came from/should go to
 
  @Remarks
    None.
 */
//// </editor-fold>    
typedef struct
{
    uint16_t size;                  //legth of the Datastring
    char     data[UDP_DATAPACKET_SIZE+7];   //Data to Send/receive
    Sources_Struct Source;                 //where did the Data come From/response goes to
} CITECWIFIProtocol_DataStruct;

// *****************************************************************************
// *****************************************************************************
// Section: Application Queue Pointers
// *****************************************************************************
// *****************************************************************************
/* These routines are called by drivers when certain events occur.
*/
QueueHandle_t CITECWIFIProtocol_SendQueue;
QueueHandle_t CITECWIFIProtocol_ReceiveQueue;
	
// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
/*******************************************************************************
  Function:
    void CITECWIFIProtocol_ClientInitialize ( void )

  Summary:
    Initialises the "Send" functionality via WIFI UDP Protocol

  Description:
    see Summary

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    This funktion is called automatically when the 
    CITECWIFIProtocol_ClientTask is started.
*/
//// </editor-fold>
void CITECWIFIProtocol_ClientInitialize ( void );

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
/*******************************************************************************
  Function:
    void CITECWIFIProtocol_ServerInitialize ( void )

  Summary:
    Initialises the "Receive" functionality via WIFI TCP Protocol

  Description:
    see Summary

  Precondition:
    None.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    This funktion is called automatically when the 
    CITECWIFIProtocol_ServerTask is started.
*/
//// </editor-fold>
void CITECWIFIProtocol_ServerInitialize ( void );

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
/*******************************************************************************
  Function:
    void CITECWIFIProtocol_ClientTasks ( void )

  Summary:
    Task to handle requests for Outgoing Data

  Description:
    This Task will only act if Data to send via the
    UDP Protocol in a WIFI Network. Data will be send to either a
    predefined fixed IP or back to the orgin of a command.
    A Port to send to needs to be set in the Headder File.

  Precondition:
    CITECWIFIProtocol_ClientInitialize needs to be called first.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    This Funktion is thought to run in a FreeRTOS environment as a Task.
*/
//// </editor-fold>
void CITECWIFIProtocol_ClientTasks ( void );

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
/*******************************************************************************
  Function:
    void CITECWIFIProtocol_ServerTasks ( void )

  Summary:
    Task to handle requests for Incomming Data

  Description:
    This Task will only act if Data was received via the
    TCP Protocol in a WIFI Network. A Port where to listen needs
    to be set in the Headder file first.
    Received Data is put in a queue to be read when ever nessesary.

  Precondition:
    CITECWIFIProtocol_ServerInitialize needs to be called first.

  Parameters:
    None.

  Returns:
    None.

  Remarks:
    This Funktion is thought to run in a FreeRTOS environment as a Task.
*/
//// </editor-fold>
void CITECWIFIProtocol_ServerTasks( void );

// <editor-fold defaultstate="collapsed" desc="Funktion description"> 
/*******************************************************************************
  Function:
    int8_t CITECWIFIProtocol_Send (CITECWIFIProtocol_DataStruct* data)

  Summary:
    Funktion to request a Data Send. Can be called everywehere
    and anytime. 

  Description:
    Funktion will send the address of a Data Type to the ClienTask
    to start a send sequenz of the data. 

  Precondition:
    CITECWIFIProtocol_ServerInitialize needs to be called first.

  Parameters:
    @param CITECWIFIProtocol_DataStruct* data: Pointer to the Datastruct which should
    be send.

  Returns:
    int8_t Errorcodes, 0 on succsess

  Remarks:
    To reduce the requeired space and time the function works only with
    pointers to the original Data. Make shure that is not corrupted
    while data is beenig send.
*/
//// </editor-fold>
int8_t CITECWIFIProtocol_Send(CITECWIFIProtocol_DataStruct* data);


#endif /* _CITEC_WIFIUDPTCP_H */
/*******************************************************************************
 End of Filec
 */

