/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    CITEC_WIFIUSPTCP.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "CITEC_WIFIUDPTCP.h"


// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the CITEC_WIFIUDPTCP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/
//global holds information about the available Wifi Devices
CITECWIFIProtocol_DeviceStruct CITECWIFIProtocol_Device;
//Queue which holds incomming data from all sources, place to put incomming Data
extern QueueHandle_t CITECSerialReceive_Queue;

UDP_SOCKET_INFO udpInfo;                //udp socket
IPV4_ADDR RemoteAddr;                   //current IP adress of communication partner
SemaphoreHandle_t xSemaphore_IPAddress; //semaphore to check for WIFI connection              

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void CITECWIFIProtocol_ClientInitialize ( void )

  Remarks:
    See prototype in CITEC_WIFIUDPTCP.h.
 */
void CITECWIFIProtocol_ClientInitialize ( void )
{    
    if(xSemaphore_IPAddress == NULL)
    {
        xSemaphore_IPAddress = xSemaphoreCreateBinary();
        if(xSemaphore_IPAddress == NULL)
        {
            configASSERT(0);
        }
    }
    
    CITECWIFIProtocol_SendQueue = xQueueCreate(CITECWIFIProtocol_SendQueueSize, sizeof(CITECWIFIProtocol_DataStruct*));
    if(CITECWIFIProtocol_SendQueue==NULL)
    {
        configASSERT(0);
    }
    
    CITECWIFIProtocol_Device.ClientSocket = INVALID_SOCKET;
    
    CITECWIFIProtocol_Device.ClientState = CITECWIFIProtocol_WAIT_INIT;
}

/******************************************************************************
  Function:
    void CITECWIFIProtocol_ClientTasks ( void )

  Remarks:
    See prototype in CITEC_WIFIUDPTCP.h.
 */

void CITECWIFIProtocol_ClientTasks ( void )
{
    bool closeCheck = false;
    uint16_t NoBFlush;
    CITECWIFIProtocol_DataStruct* Transmit;  //Pointer to Data to Transmit  
    SYS_STATUS          tcpipStat;
    uint16_t FailFlushCounter;
    
    //Initialize Task
    CITECWIFIProtocol_ClientInitialize();
    
    while(1)
    {
        /* Check the application's current state. */
        switch ( CITECWIFIProtocol_Device.ClientState )
        {
            case CITECWIFIProtocol_WAIT_INIT:
            {
                if (xSemaphoreTake(xSemaphore_IPAddress, portMAX_DELAY) != pdTRUE )
                {//wait for a WIFI connection endlessly
                    configASSERT(0);
                }
                xSemaphoreGive(xSemaphore_IPAddress);   //Give again for Server Task

                tcpipStat = TCPIP_STACK_Status(sysObj.tcpip);
                if(tcpipStat < 0)
                {   // Error 
                    configASSERT(0);
                    while(1);
                }
                else if (tcpipStat == SYS_STATUS_BUSY)
                {
                    Nop();
                }
                else if(tcpipStat == SYS_STATUS_READY)
                {
                    // now that the stack is ready we can check the 
                    // available interfaces 
                    CITECWIFIProtocol_Device.ClientState = CITECWIFIProtocol_WAITING_FOR_COMMAND;                
                }

                break;
            }//case CITECWIFIProtocol_WAIT_INIT:
            
            case CITECWIFIProtocol_WAITING_FOR_COMMAND:
            {    
                if(xQueueReceive(CITECWIFIProtocol_SendQueue, &Transmit, portMAX_DELAY)==pdPASS)    
                {//wait for Data to send
                    
                    #ifdef CITECWIFIProtocol_DestinationIP
                    //send to pedefined destination
                    if (TCPIP_Helper_StringToIPAddress(CITECWIFIProtocol_DestinationIP, &RemoteAddr) != true)
                    {
                        configASSERT(0);
                    }                  
                    #else
                    //send to variable ip, usually coresponding receive ip
                    RemoteAddr = Transmit->Source.Adress.v4Add;
                    #endif
                    
                    if(CITECWIFIProtocol_Device.ClientSocket == INVALID_SOCKET)
                        CITECWIFIProtocol_Device.ClientSocket = TCPIP_UDP_ClientOpen(IP_ADDRESS_TYPE_IPV4, CITECWIFIProtocol_DestinationPort,(IP_MULTI_ADDRESS*) &RemoteAddr);

                    
                    if (CITECWIFIProtocol_Device.ClientSocket == INVALID_SOCKET)
                    { // TCPIP_UDP_ClientOpen ist fehgeschlagen
                        configASSERT(0);
                        break;
                    }

                    TCPIP_UDP_SocketInfoGet(CITECWIFIProtocol_Device.ClientSocket, &udpInfo);

                    TCPIP_UDP_Bind(CITECWIFIProtocol_Device.ClientSocket, IP_ADDRESS_TYPE_IPV4, 7501, (IP_MULTI_ADDRESS*) &(udpInfo.remoteIPaddress));
                    
                    CITECWIFIProtocol_Device.ClientState = CITECWIFIProtocol_WAIT_FOR_CONNECTION;
                }

                break;
            }//case CITECWIFIProtocol_WAITING_FOR_COMMAND:

            case CITECWIFIProtocol_WAIT_FOR_CONNECTION:
            {                 
                if (!TCPIP_UDP_IsConnected(CITECWIFIProtocol_Device.ClientSocket))
                {//check if we are still connected bevore actually sending data   
                    configASSERT(0);
                    closeCheck = TCPIP_UDP_Close(CITECWIFIProtocol_Device.ClientSocket);
                    if (closeCheck == false)
                    {
                        configASSERT(0);
                    }
                    CITECWIFIProtocol_Device.ClientState = CITECWIFIProtocol_WAITING_FOR_COMMAND;
                    break;
                }//if not connected

                CITECWIFIProtocol_Device.ClientState = CITECWIFIProtocol_TCPIP_Flush;

            }//case CITECWIFIProtocol_WAIT_FOR_CONNECTION:

            case CITECWIFIProtocol_TCPIP_Flush:
            {                             
                    while(TCPIP_UDP_PutIsReady(CITECWIFIProtocol_Device.ClientSocket) < Transmit->size)
                    {
                        //block until ther is enough space in the output buffer 
                        Nop();
                    }
                
                    if (TCPIP_UDP_ArrayPut(CITECWIFIProtocol_Device.ClientSocket, (uint8_t*)Transmit->data, Transmit->size) <= Transmit->size)
                    {//put data in output buffer
                        CITECWIFIProtocol_Device.ClientState = CITECWIFIProtocol_WAITING_FOR_COMMAND;
                    }

                    TCPIP_UDP_SocketInfoGet(CITECWIFIProtocol_Device.ClientSocket, &udpInfo);
                    //Output data
                    NoBFlush = TCPIP_UDP_Flush(CITECWIFIProtocol_Device.ClientSocket);

                    if (NoBFlush < Transmit->size)
                    {//not all data was send at once
                        Nop();
                    }
                    if ( NoBFlush == 0)
                    {//nothing was send
                        FailFlushCounter++;
                        Nop();
                        //<invalid socket> oder 
                        if (CITECWIFIProtocol_Device.ClientSocket == INVALID_SOCKET)
                        {
                            configASSERT(0);
                            Nop();
                        }
                        //<invalid remote address> oder 
                        TCPIP_UDP_SocketInfoGet(CITECWIFIProtocol_Device.ClientSocket, &udpInfo);
                        if ((udpInfo.remoteIPaddress.v4Add.v[0] != RemoteAddr.v[0]) ||
                            (udpInfo.remoteIPaddress.v4Add.v[1] != RemoteAddr.v[1]) ||
                            (udpInfo.remoteIPaddress.v4Add.v[2] != RemoteAddr.v[2]) ||
                            (udpInfo.remoteIPaddress.v4Add.v[3] != RemoteAddr.v[3])   )
                        {
                            configASSERT(0);
                            Nop();
                        }   
                        //<no route to the remote host could be found>

                        if (!TCPIP_UDP_IsConnected(CITECWIFIProtocol_Device.ClientSocket))
                        {
                            configASSERT(0);
                        }
                    }
                    else 
                    {

                        FailFlushCounter = 0;
                        TCPIP_UDP_Discard(CITECWIFIProtocol_Device.ClientSocket);  //clear receive buffer
                        if(TCPIP_UDP_Close(CITECWIFIProtocol_Device.ClientSocket))
                            CITECWIFIProtocol_Device.ClientSocket=INVALID_SOCKET;  //needs to be closed once in a while to empty resources
                                
                        
                        CITECWIFIProtocol_Device.ClientState = CITECWIFIProtocol_WAITING_FOR_COMMAND;
                    }
                    
                break;
            }//case CITECWIFIProtocol_TCPIP_Flush:

            default:
                configASSERT(0);
            break;
        }//switch ( CITECWIFIProtocol_Device.ClientState )
        
        vTaskDelay(1);  //enable other Task to run
    }//while
    

}

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in CITEC_WIFIUDPTCP.h.
 */

void CITECWIFIProtocol_ServerInitialize ( void )
{
    if(xSemaphore_IPAddress == NULL)
    {
        xSemaphore_IPAddress = xSemaphoreCreateBinary();
        if(xSemaphore_IPAddress == NULL)
        {
            configASSERT(0);
        }
    }
   
    CITECWIFIProtocol_ReceiveQueue = xQueueCreate(CITECWIFIProtocol_ReceiveQueueSize, sizeof(CITECWIFIProtocol_DataStruct));
    if(CITECWIFIProtocol_ReceiveQueue==NULL)
    {
        configASSERT(0);
    }
    
    /* Place the App state machine in its initial state. */
    CITECWIFIProtocol_Device.ServerState = CITECWIFIProtocol_WAIT_INIT;
}


/******************************************************************************
  Function:
    void CITECWIFIProtocol_ServerTasks ( void )

  Remarks:
    See prototype in CITEC_WIFIUDPTCP.h.
 */

void CITECWIFIProtocol_ServerTasks ( void )
{
    SYS_STATUS          tcpipStat;
    const char          *netName, *netBiosName;
    static IPV4_ADDR    dwLastIP[2] = { {-1}, {-1} };
    IPV4_ADDR           ipAddr;
    int                 i, nNets;
    TCPIP_NET_HANDLE    netH;
    CITECWIFIProtocol_DataStruct Receive;      //hold Received Data
    TCP_SOCKET_INFO tcpInfo;

    //Initialize the Task
    CITECWIFIProtocol_ServerInitialize();
    SYS_CMD_READY_TO_READ();
    
    while(1)
    {
        switch(CITECWIFIProtocol_Device.ServerState)
        {
            case CITECWIFIProtocol_WAIT_INIT:
            {
                if (xSemaphoreTake(xSemaphore_IPAddress, portMAX_DELAY) != pdTRUE )
                {//endles wait for Wifi connection
                    configASSERT(0);
                }
                xSemaphoreGive(xSemaphore_IPAddress);   //Give again for Client Task
                
                tcpipStat = TCPIP_STACK_Status(sysObj.tcpip);
                if(tcpipStat < 0)
                {   // some error occurred
                    SYS_CONSOLE_MESSAGE(" APP: TCP/IP stack initialization failed!\r\n");
                    CITECWIFIProtocol_Device.ServerState = CITECWIFIProtocol_ERROR;
                }
                else if(tcpipStat == SYS_STATUS_READY)
                {
                    // now that the stack is ready we can check the
                    // available interfaces
                    nNets = TCPIP_STACK_NumberOfNetworksGet();
                    for(i = 0; i < nNets; i++)
                    {

                        netH = TCPIP_STACK_IndexToNet(i);
                        netName = TCPIP_STACK_NetNameGet(netH);
                        netBiosName = TCPIP_STACK_NetBIOSName(netH);

    #if defined(TCPIP_STACK_USE_NBNS)
                        SYS_CONSOLE_PRINT("    Interface %s on host %s - NBNS enabled\r\n", netName, netBiosName);
    #else
                        SYS_CONSOLE_PRINT("    Interface %s on host %s - NBNS disabled\r\n", netName, netBiosName);
    #endif  // defined(TCPIP_STACK_USE_NBNS)

                    }
                    CITECWIFIProtocol_Device.ServerState = CITECWIFIProtocol_WAIT_FOR_IP;

                }//else SYS_STATUS_READY
                break;
            }//case CITECWIFIProtocol_WAIT_INIT:
            
            case CITECWIFIProtocol_WAIT_FOR_IP:
            {
                // if the IP address of an interface has changed
                // display the new value on the system console
                nNets = TCPIP_STACK_NumberOfNetworksGet();

                for (i = 0; i < nNets; i++)
                {
                    netH = TCPIP_STACK_IndexToNet(i);
                    if(!TCPIP_STACK_NetIsReady(netH))
                    {
                        break;    // interface not ready yet!
                    }
                    ipAddr.Val = TCPIP_STACK_NetAddress(netH);
                    if(dwLastIP[i].Val != ipAddr.Val)
                    {
                        dwLastIP[i].Val = ipAddr.Val;

                        SYS_CONSOLE_MESSAGE(TCPIP_STACK_NetNameGet(netH));
                        SYS_CONSOLE_MESSAGE(" IP Address: ");
                        SYS_CONSOLE_PRINT("%d.%d.%d.%d \r\n", ipAddr.v[0], ipAddr.v[1], ipAddr.v[2], ipAddr.v[3]);
                    }
                    CITECWIFIProtocol_Device.ServerState = CITECWIFIProtocol_OPENING_SERVER;
                }
                break;
            }//case CITECWIFIProtocol_WAIT_FOR_IP:    
            
            case CITECWIFIProtocol_OPENING_SERVER:
            {
                SYS_CONSOLE_PRINT("Waiting for Client Connection on port: %d\r\n", CITECWIFIProtocol_ServerPort);
                CITECWIFIProtocol_Device.ServerSocket = TCPIP_TCP_ServerOpen(IP_ADDRESS_TYPE_IPV4, CITECWIFIProtocol_ServerPort, 0);
                if (CITECWIFIProtocol_Device.ServerSocket == INVALID_SOCKET)
                {
                    SYS_CONSOLE_MESSAGE("Couldn't open server socket\r\n");
                    break;
                }
                CITECWIFIProtocol_Device.ServerState = CITECWIFIProtocol_WAIT_FOR_CONNECTION;
                
                break;
            }//case CITECWIFIProtocol_OPENING_SERVER:
            

            case CITECWIFIProtocol_WAIT_FOR_CONNECTION:
            {
                if (!TCPIP_TCP_IsConnected(CITECWIFIProtocol_Device.ServerSocket))
                {
                    break;
                }
                else
                {
                    // We got a connection
                    CITECWIFIProtocol_Device.ServerState = CITECWIFIProtocol_SERVING_CONNECTION;
                    SYS_CONSOLE_MESSAGE("Received a connection\r\n");
                }
                break;    
            }//case CITECWIFIProtocol_WAIT_FOR_CONNECTION:
            

            case CITECWIFIProtocol_SERVING_CONNECTION:
            {
                if (!TCPIP_TCP_IsConnected(CITECWIFIProtocol_Device.ServerSocket))
                {
                    CITECWIFIProtocol_Device.ServerState = CITECWIFIProtocol_CLOSING_CONNECTION;
                    SYS_CONSOLE_MESSAGE("Connection was closed\r\n");
                    break;
                }
                int16_t wMaxGet, wCurrentChunk;
                uint16_t w;
                // Figure out how many bytes have been received and how many we can transmit.
                wMaxGet = TCPIP_TCP_GetIsReady(CITECWIFIProtocol_Device.ServerSocket);	// Get TCP RX FIFO byte count
                //wMaxPut = TCPIP_TCP_PutIsReady(CITEC_TCPServer_AppData.ServerSocket);	// Get TCP TX FIFO free space
                // Make sure we don't take more bytes out of the RX FIFO than we can put into the TX FIFO
                if(UDP_DATAPACKET_SIZE < wMaxGet)
                        wMaxGet = UDP_DATAPACKET_SIZE;

                // Process all bytes that we can
                // This is implemented as a loop, processing up to sizeof(AppBuffer) bytes at a time.
                // This limits memory usage while maximizing performance.  Single byte Gets and Puts are a lot slower than multibyte GetArrays and PutArrays.
                wCurrentChunk = UDP_DATAPACKET_SIZE;
                for(w = 0; w < wMaxGet; w += UDP_DATAPACKET_SIZE)
                {
                    // Make sure the last chunk, which will likely be smaller than sizeof(AppBuffer), is treated correctly.
                    if(w + UDP_DATAPACKET_SIZE > wMaxGet)
                        wCurrentChunk = wMaxGet - w;

                    // Transfer the data out of the TCP RX FIFO and into our local processing buffer.
                    TCPIP_TCP_SocketInfoGet(CITECWIFIProtocol_Device.ServerSocket , &tcpInfo);
                    Receive.size=TCPIP_TCP_ArrayGet(CITECWIFIProtocol_Device.ServerSocket, (uint8_t*)Receive.data, wCurrentChunk);
                    Receive.Source.Adress = tcpInfo.remoteIPaddress;
                    
                    if(xQueueSend(CITECWIFIProtocol_ReceiveQueue ,&Receive, 0)==pdFAIL)
                        SYS_DEBUG_BreakPoint();                   
                }
                break;
            }//case CITECWIFIProtocol_SERVING_CONNECTION:
            
            case CITECWIFIProtocol_CLOSING_CONNECTION:
            {
                // Close the socket connection.
                TCPIP_TCP_Close(CITECWIFIProtocol_Device.ServerSocket);
                CITECWIFIProtocol_Device.ServerSocket = INVALID_SOCKET;
                CITECWIFIProtocol_Device.ServerState = CITECWIFIProtocol_WAIT_FOR_IP;

                break;
            }//case CITECWIFIProtocol_CLOSING_CONNECTION:
            
            default:
                break;
        }//switch
        
        vTaskDelay(1);
    }//while
}




int8_t CITECWIFIProtocol_Send(CITECWIFIProtocol_DataStruct* data)
{
    if(CITECWIFIProtocol_SendQueue==NULL)
        return -2;
   //add security here
    if(data->size>UDP_DATAPACKET_SIZE)
        return -1;  
    
    if(xQueueSend(CITECWIFIProtocol_SendQueue, &data, pdMS_TO_TICKS(WAITFORSENDSPACE_MS))==pdFAIL)
        configASSERT(0);
    
    return 0;
}



/*******************************************************************************
 End of File
 */

