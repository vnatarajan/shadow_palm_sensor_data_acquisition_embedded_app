/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "CITEC_WIFIUDPTCP.h"

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */
/** Descriptive Data Item Name

  @Summary
    Brief one-line summary of the data item.
    
  @Description
    Full description, explaining the purpose and usage of data item.
    <p>
    Additional description in consecutive paragraphs separated by HTML 
    paragraph breaks, as necessary.
    <p>
    Type "JavaDoc" in the "How Do I?" IDE toolbar for more information on tags.
    
  @Remarks
    Any additional remarks
 */



/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */

/** 
  @Function
    int ExampleLocalFunctionName ( int param1, int param2 ) 

  @Summary
    Brief one-line description of the function.

  @Description
    Full description, explaining the purpose and usage of the function.
    <p>
    Additional description in consecutive paragraphs separated by HTML 
    paragraph breaks, as necessary.
    <p>
    Type "JavaDoc" in the "How Do I?" IDE toolbar for more information on tags.

  @Precondition
    List and describe any required preconditions. If there are no preconditions,
    enter "None."

  @Parameters
    @param param1 Describe the first parameter to the function.
    
    @param param2 Describe the second parameter to the function.

  @Returns
    List (if feasible) and describe the return values of the function.
    <ul>
      <li>1   Indicates an error occurred
      <li>0   Indicates an error did not occur
    </ul>

  @Remarks
    Describe any special behavior not described above.
    <p>
    Any additional remarks.

  @Example
    @code
    if(ExampleFunctionName(1, 2) == 0)
    {
        return 3;
    }
 */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

// *****************************************************************************

/** 
  @Function
    int ExampleInterfaceFunctionName ( int param1, int param2 ) 

  @Summary
    Brief one-line description of the function.

  @Remarks
    Refer to the example_file.h interface header for function usage details.
 */
void appTask(void)
{
    extern CITECWIFIProtocol_DeviceStruct CITECWIFIProtocol_Device;
    
    static CITECWIFIProtocol_DataStruct outData, retData;
    CITECWIFIProtocol_DataStruct inData;
    
    //wait until wifi is connected to a network
    while(CITECWIFIProtocol_Device.ClientState!=CITECWIFIProtocol_WAITING_FOR_COMMAND||CITECWIFIProtocol_SendQueue==NULL)
        vTaskDelay(10);
    while(CITECWIFIProtocol_Device.ServerState!=CITECWIFIProtocol_WAIT_FOR_CONNECTION||CITECWIFIProtocol_ReceiveQueue==NULL)
        vTaskDelay(10);
    
    //make ip address to adress type, send data here there
    //use define in headder to send all data to same address
    TCPIP_Helper_StringToIPAddress("192.168.178.21", &outData.Source.Adress);
    
    while(1)
    {
        int i;
        for(i=0; i<UDP_DATAPACKET_SIZE; )
        {//Output counting array
            outData.data[i]=i;
            outData.size=++i;
            
            //Send Data
            CITECWIFIProtocol_Send(&outData);
            
            //check if Data was received and delay
            if(xQueueReceive(CITECWIFIProtocol_ReceiveQueue, &inData, 20)==pdPASS)
            {//return received String
                memcpy(retData.data, inData.data, inData.size);
                retData.size = inData.size;
                retData.Source = inData.Source;     //send back to received IP
                CITECWIFIProtocol_Send(&retData);
            }
        }        
    }
}


/* *****************************************************************************
 End of File
 */
