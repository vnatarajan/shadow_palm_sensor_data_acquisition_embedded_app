##############################################################################
## REVISIONS (newer Revisions on top)
## Date         Author             Description
## 2017-12-xx   <TechFak Account>  x
##############################################################################
## Abstract
## <write some lines explaining the functionality of the library>
##############################################################################
## Usage & library interface
## <explain the required procedure for usage (e.g., initialisation,
##  FreeRTOS task and queue requirements and list the library interface -
##  the queues, functions and variables required for interaction>
##############################################################################
## Example
## <write some lines explaining the functionality of the Example program,
## especially the hardware setup required to run it>
##############################################################################
