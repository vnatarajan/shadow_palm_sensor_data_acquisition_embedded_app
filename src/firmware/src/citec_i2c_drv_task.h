/* ************************************************************************** */
/** Hardware communication library for I2C for use with FreeRTOS

  @Company
 * University of Bielefeld 
 * CITEC - Neuroinformatics Group
 * Tobias Schwank
 * 04.09.2019

  @File Name
    i2c_drv_task.h

  @Summary
 * This library is used to control one i2c hardware module from different tasks
 * which want to send data via the i2c module.

  @Description
 * The library can be used to write data via i2c to other ic's without 
 * the risk of a collision through other tasks.
 * Only one task can write to the i2c buffer at one time. 
 * Every task can write a request structure to the i2c_drv_task and then has to 
 * for a received semaphore or for incomming data. 
/* ************************************************************************** */

#ifndef _I2C_DRV_TASK_H    /* Guard against multiple inclusion */
#define _I2C_DRV_TASK_H

#include "system_definitions.h"
#include "semphr.h"

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
    
    // Structure that holds the hardware module variables
    typedef struct 
    {
        DRV_HANDLE              handleI2C0;
        DRV_I2C_BUFFER_HANDLE   I2C0BufferHandle;
        DRV_I2C_BUFFER_EVENT    I2C0BufferEvent;
        SYS_STATUS              i2c0_status;
    }I2C_DRV_DATA;
    
    // Value that has to be read out
    uint8_t xReadValues[8];
    
    // writebuffer for the parameters of I2C_WriteRegVal())
    uint8_t xBuffer[2];
    
    bool bI2C_TANSACTION_FINISHED;
    bool bI2C_ERROR;
    
    // Global variable, because the I2C Bus is only available for the 
    // for the task, which holds the semaphore
    I2C_DRV_DATA i2cData;

    // Semaphore for the I2C Callback function 
    SemaphoreHandle_t xSemaphoreI2C0;
    SemaphoreHandle_t xSemaphoreI2CRequestReceived;
    SemaphoreHandle_t xSemaphoreI2C_ERROR;
    
    BaseType_t xI2CQueueStatus;
    // Queue to hold the incomming jobs
    QueueHandle_t xQueue_Jobs_I2CTask_Rx;
    
    // Queue to send the sensor data to the sensor tasks
    QueueHandle_t xDataQueue_MPL115A2_I2CTask_Tx;


    void I2C_Init ( void );
    void I2C_Tasks ( void );
    
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _I2C_DRV_TASK_H */

/* *****************************************************************************
 End of File
 */
