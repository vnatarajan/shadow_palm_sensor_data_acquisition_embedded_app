#ifndef _CITEC_TOF_GE_IMU_APP_H
#define _CITEC_TOF_GE_IMU_APP_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system_definitions.h"
#include "CITEC_USBCDC.h"

    
#include "imu.h"
#include "tof.h"
#include "grid_eye.h"

typedef struct{
    int16_t pixels[64];
    uint16_t pressure[60];
    double tof1;
    double tof2;
	struct quaternion qn;
}sensor_fusion_h;

typedef enum
{
	APP_STATE_INIT=0,
	APP_STATE_SERVICE_TASKS,
} APP_STATES;


typedef struct
{
    APP_STATES state;
    CDC_DataType cdcData; 
} APP_DATA;

void APP_Initialize ( void );
void APP_Tasks( void );
#endif // _CITEC_TOF_GE_IMU_APP_H 

