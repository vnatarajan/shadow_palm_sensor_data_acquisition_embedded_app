#ifndef APP_DELAY_H
#define APP_DELAY_H
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"



extern void app_delay(unsigned long int ms);
extern void DelayUs(unsigned long int usDelay);
#endif
