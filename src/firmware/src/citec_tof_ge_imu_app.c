#include "CITEC_BMP3xx.h"
#include "citec_tof_ge_imu_app.h"
#include "mplab_i2c_bitbanging.h"
#include "app_delay.h"
#include "FreeRTOSConfig.h"
#define ENABLE_TOF
#define ENABLE_IMU
#define ENABLE_GRID_EYE
#define ENABLE_SENSORS
#define ENABLE_LED_ALERTS
#define ENABLE_USB_SERIAL_COM
#define STR_SENSOR_DISABLED  "Sensors Disabled \r\n"
#define STR_SENSOR_DATA_NULL "Sensors Data Null \r\n"
int ictr;
APP_DATA appData;
CDC_DataType data;
char *p_sensor_data_encoded=NULL;
char *p_sensor_data_encoded2=NULL;
bool appInitialized = false;

/* size is 512 bytes */
#define USB_DATA_BUFFER USBCDC_BUFFER_SIZE

char sensor_data_encoded[USB_DATA_BUFFER];
char sensor_data_encoded2[USB_DATA_BUFFER];

void clear_buf(char *buf, int size)
{   
    int i;
    
    for(i=0; i<size; i++){
        buf[i] = '\0';
    }

}


/* Assumption Sensor value 
     * 
     *      decimal point suffix        = 4 char
     *      decimal point prefix        = 4 char
     *      decimal point character     = 1 char
     *      comma                       = 1 char
     *                                   --------
     *                                   10 character per sensor
     * 
     * 
     * Trailing Chars:
     * 
     *      semicolan char  = 1 char
     *      \n              = 1 char
     *      \0              = 1 char
     *                       -------
     *                        3 characters for the entire encoded string
     * 
     * Total chars/bytes required
     * 
     *      Grid Eye        = 64 * 2  = 128                       
     *      TOF1            = 1  * 10 =  10
     *      TOF2            = 1  * 10 =  10
     *      IMU             = 4  * 10 =  40
     *      Trailing Char   =             3
     *                                --------
     *                                  191
     *                                ------- 
     * 
     * 
     */

void append_sensor_val1(char *out, double sval, uint8_t last_value)
{
    char t_sens_data[USB_DATA_BUFFER];
    int i;
    
    if(!out) return;
    
    for(i=0;i<sizeof(t_sens_data); i++){
        t_sens_data[i] = '\0';
    }
    
    for(i=0;i<sizeof(t_sens_data); i++){
        t_sens_data[i] = out[i];
    }
    
    
    if(last_value){
        snprintf(out, USB_DATA_BUFFER, "%s%.4f,\n", t_sens_data, sval);
    }else{
        snprintf(out, USB_DATA_BUFFER, "%s%.4f,", t_sens_data, sval);
    }   
    
}

void append_str(char *out, char *in)
{
    char t_sens_data[USB_DATA_BUFFER];
    int i;

    if(!out) return;
    if(!in) return;

    for(i=0;i<sizeof(t_sens_data); i++){
        t_sens_data[i] = '\0';
    }
    
    for(i=0;i<sizeof(t_sens_data); i++){
        t_sens_data[i] = out[i];
    }
    
    
    snprintf(out, USB_DATA_BUFFER, "%s%s", t_sens_data, in);
    
}


void append_sensor_val2(char *out, int16_t sval, uint8_t last_value)
{
    char t_sens_data[USB_DATA_BUFFER];
    int i;
    
    for(i=0;i<sizeof(t_sens_data); i++){
        t_sens_data[i] = '\0';
    }
    
    for(i=0;i<sizeof(t_sens_data); i++){
        t_sens_data[i] = out[i];
    }
    
    
    if(last_value){
        snprintf(out, USB_DATA_BUFFER, "%s%d,\n", t_sens_data, sval);
    }else{
        snprintf(out, USB_DATA_BUFFER, "%s%d,", t_sens_data, sval);
    }   
    
}

void append_sensor_val3(char *out, uint16_t sval, uint8_t last_value)
{
    char t_sens_data[USB_DATA_BUFFER];
    int i;
    
    for(i=0;i<sizeof(t_sens_data); i++){
        t_sens_data[i] = '\0';
    }
    
    for(i=0;i<sizeof(t_sens_data); i++){
        t_sens_data[i] = out[i];
    }
    
    
    if(last_value){
        snprintf(out, USB_DATA_BUFFER, "%s%i,\n", t_sens_data, sval);
    }else{
        snprintf(out, USB_DATA_BUFFER, "%s%i,", t_sens_data, sval);
    }   
    
}

char *encode_sensor_data(sensor_fusion_h *s)
{
    int i;

    if(!s) return NULL;

    clear_buf(sensor_data_encoded, USB_DATA_BUFFER);
    
    append_str(sensor_data_encoded, "GE#");
    for(i=0;i<64;i++){
        append_sensor_val2(sensor_data_encoded, s->pixels[i], 0);
    }

    append_str(sensor_data_encoded, "TOF#");
    append_sensor_val1(sensor_data_encoded, s->tof1, 0); 
    append_sensor_val1(sensor_data_encoded, s->tof2, 0); 

    append_str(sensor_data_encoded, "IMU#");
    append_sensor_val1(sensor_data_encoded, (double) s->qn.w, 0);
    append_sensor_val1(sensor_data_encoded, (double) s->qn.x, 0);
    append_sensor_val1(sensor_data_encoded, (double) s->qn.y, 0);
    append_sensor_val1(sensor_data_encoded, (double) s->qn.z, 1);
   
    return sensor_data_encoded;
}

char *encode_sensor_data2(sensor_fusion_h *s)
{
    int i;

    if(!s) return NULL;

    clear_buf(sensor_data_encoded2, USB_DATA_BUFFER);
    
    append_str(sensor_data_encoded2, "PR#");
    for(i=0;i<NUMBER_SENSORS;i++){
        append_sensor_val3(sensor_data_encoded2, s->pressure[i], 0);
    }
 
    return sensor_data_encoded2;
}

void usb_serial_send_sensor_data(char *data, int size)
{
#ifdef ENABLE_USB_SERIAL_COM
        if(!data) return;
    	
        
        strncpy(appData.cdcData.ca_Message, data, size); 
        appData.cdcData.u32_Length=size;
                
                
        /* Publish data to USB serial */
        if(cdcData.b_isConfigured){
        	CITEC_USBCDC_WriteData(&appData.cdcData);
            CITEC_USBCDC_SendNow();
    	    cdcData.b_isReadComplete = true;
            cdcData.b_isDataAvailable = false;
            cdcData.b_isWriteComplete = true;
            cdcData.b_wasDataWritten = false;
        }

        
#endif
}

void bmp_init()
{
    CITEC_BMP3XX_Init();
}

void sensor_init_tof_ge_imu()
{
 	#ifdef ENABLE_SENSORS
            i2c_init();
            
    	    #ifdef ENABLE_GRID_EYE
            	grid_eye_init();
    	    #endif

    	    #ifdef ENABLE_TOF
            	tof_init();
    	    #endif
            
    	    #ifdef ENABLE_IMU
                imu_init();
    	    #endif
            
	    bmp_init();
	#endif    
}

int bmp_read_n_normalize(uint16_t *pressure)
{
    static uint16_t p_offset[NUMBER_SENSORS];
    static int ctr = 0;
    int ret, i;

    if(!pressure) return 0;

    if(ctr>10){

         for(i=0;i<NUMBER_SENSORS;i++){
	    /*hack to get absolute value*/

	    if(p_offset[i] >= Barometer[i].Pressure){
	    	pressure[i] = p_offset[i] - Barometer[i].Pressure;
	    }else{
	    	pressure[i] = Barometer[i].Pressure - p_offset[i];
	    }	

        }

        ret = 1; 
    }else{
        /*store highest of the base pressure in idle state*/
        for(i=0;i<NUMBER_SENSORS;i++){
            if(Barometer[i].Pressure > p_offset[i]){
                p_offset[i] = Barometer[i].Pressure;
                pressure[i] = 0;
            }
        }
        ret = 0;
        ctr = ctr + 1;
    }


    return ret;
}


void bmp_read(uint16_t *pressure)
{
    if(!pressure) return;
    int i;

    /*
    for(i=0;i<NUMBER_SENSORS;i++{
        pressure[i] = 0x0;
    }
    */

    for(i=0;i<NUMBER_SENSORS;i++){
        CITEC_BMPXX_DeviceTask();
    }

    bmp_read_n_normalize(pressure);

}

void sensor_read_tof_ge_imu(sensor_fusion_h *sensor_data)
{
	#ifdef ENABLE_SENSORS

    	    #ifdef ENABLE_GRID_EYE
            	grid_eye_read(&sensor_data->pixels[0]);
            #endif            

    	    #ifdef ENABLE_TOF
            	sensor_data->tof1= tof_read(ADDRESS_VL6180X);
            	sensor_data->tof2= tof_read(ADDRESS_VL6180X_1);
    	    #endif

    	    #ifdef ENABLE_IMU
            	sensor_data->qn = imu_read();
    	    #endif

            bmp_read(&sensor_data->pressure[0]); 

    	    p_sensor_data_encoded = encode_sensor_data(sensor_data);
    	    p_sensor_data_encoded2 = encode_sensor_data2(sensor_data);
            
	#endif    
}


void clear_sens_data(sensor_fusion_h *s){
    int i;
    for(i=0;i<64;i++){
        s->pixels[i] = 0.0;
    }
    
    s->qn.w = 0.0;
    s->qn.x = 0.0;
    s->qn.y = 0.0;
    s->qn.z = 0.0;
    
    s->tof1 = 0.0;
    s->tof2 = 0.0;
    

}


void CITEC_TOF_GE_IMU_APP_Tasks ( void )
{

    sensor_fusion_h local_sensor_data;
    clear_sens_data(&local_sensor_data);


    switch ( appData.state )
    {
        case APP_STATE_INIT:
        {

        
            if (!appInitialized)
            {
                sensor_init_tof_ge_imu();
                appInitialized = true;
            }
            
            appData.state = APP_STATE_SERVICE_TASKS;
            
            break;
        }

        case APP_STATE_SERVICE_TASKS:
        {
            
            
            sensor_read_tof_ge_imu(&local_sensor_data);           
	        
            #ifdef ENABLE_SENSORS
            
	        if(p_sensor_data_encoded != NULL ){
                usb_serial_send_sensor_data(p_sensor_data_encoded, USB_DATA_BUFFER);
            }else{
                usb_serial_send_sensor_data(STR_SENSOR_DATA_NULL, sizeof(STR_SENSOR_DATA_NULL));
            }

            if(p_sensor_data_encoded2 != NULL ){
                usb_serial_send_sensor_data(p_sensor_data_encoded2, USB_DATA_BUFFER);
            }else{
                usb_serial_send_sensor_data(STR_SENSOR_DATA_NULL, sizeof(STR_SENSOR_DATA_NULL));
            }

	    #else
             usb_serial_send_sensor_data(STR_SENSOR_DISABLED, (int) sizeof(STR_SENSOR_DISABLED));    
        #endif 
            
            appData.state = APP_STATE_SERVICE_TASKS;
            
            break;
        }

        default:
        {
            break;
        }
    }
             
}

