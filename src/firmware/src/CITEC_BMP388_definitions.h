/*************************************************************************
*        B M P 3 8 8 - S E N S O R   R E G I S T E R 
*
* File Name         : citec_BMP388.h
* Title             : BMP388 Register definitions
* Autor             : Bielefeld University - CITEC - Tobias Schwank
* Created           : 12.01.2019
* Version           : 1.00
* Target MCU        : -
*
* DESCRIPTION
* This file contains all register definitions, which are needed to 
* read and write the Bosch pressure sensor BMP388.
*
*************************************************************************/

#ifndef _CITEC_BMP388_DEFINITIONS_H
#define _CITEC_BMP388_DEFINITIONS_H

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

// Device Address of the sensor
#define BMP388_ADDR     0xEE        // 8-bit device address without R/W
#define BMP388_RW_BIT   0x80         
#define BMP388_DUMMY    0x00

// ============================================
//  R E G I S T E R  D E F I N I T I O N S 
// ============================================

#define BMP388_CHIP_ID         0x00      // Register contains the chip identification code.
#define BMP388_ERR_REG         0x02      // Sensor Error conditions are reported in this register.
#define BMP388_STATUS          0x03      // Sensor Status flags are stored in this register.
#define BMP388_DATA_0          0x04      // Pressure value [23:16]
#define BMP388_DATA_1          0x05      // Pressure value [15:8]
#define BMP388_DATA_2          0x06      // Pressure value [7:0]
#define BMP388_DATA_3          0x07      // Temperatur value MSB [23:16]
#define BMP388_DATA_4          0x08      // Temperatur value LSB [15:8]
#define BMP388_DATA_5          0x09      // Temperatur value XLSB [7:0]
#define BMP388_SENSORTIME_0    0x0C      // Sensor time [23:16]
#define BMP388_SENSORTIME_1    0x0D      // Sensor time [15:8]
#define BMP388_SENSORTIME_2    0x0E      // Sensot time [7:0]
#define BMP388_SENSORTIME_3    0x0F      // No description available.
#define BMP388_EVENT           0x10      // Register contains the sensor status flags.
#define BMP388_INT_STATUS      0x11      // Show interrupt status and is cleared after reading.
#define BMP388_FIFO_LENGTH_0   0x12      // FIFO fill level LSB
#define BMP388_FIFO_LENGTH_1   0x13      // FIFO fill level MSB
#define BMP388_FIFO_DATA       0x14      // FIFO data output register
#define BMP388_FIFO_WTM_0      0x15      // FIFO watermark LSB
#define BMP388_FIFO_WTM_1      0x16      // FIFO watermark MSB
#define BMP388_FIFO_CONFIG_1   0x17      // Register contains the FIFO frame content configuration.
#define BMP388_FIFO_CONFIG_2   0x18      // Register extends the "FIFO_CONFIG_1" register.
#define BMP388_INT_CTRL        0x19      // Interrupt configuration.
#define BMP388_IF_CONF         0x1A      // Serial Interface control register.
#define BMP388_PWR_CTRL        0x1B      // Register enables/disables pressure measurements and sets the mode.
#define BMP388_OSR             0x1C      // Register controls the oversampling settings.
#define BMP388_ODR             0x1D      // Register controls the output data rates.
#define BMP388_CONFIG          0x1F      // Register controls the IIR filter coefficients.
#define BMP388_CMD             0x7F      // Command register.

// ============================================
//  S T A T U S  B I T  P O S I T I O N S 
// ============================================

// ============================================
//  B I T  P O S I T I O N S  A N D  M A S K S
// ============================================

// CHIP_ID masks
#define BMP388_CHIPID_FIX_gm  0xF0      // Chip ID fixed group mask
#define BMP388_CHIPID_FIX_gp  4         // Chip ID fixed group position
#define BMP388_CHIPID_NVM_gm  0x0F      // Chip ID Non Volatile Memory group mask
#define BMP388_CHIPID_NVM_gp  0         // Chip ID Non Volatile Memory group position

// ERR_REG masks
#define BMP388_FATAL_ERR_bm  (1<<0)     // Fatal Error bit mask
#define BMP388_FATAL_ERR_bp  0          // Fatal Error bit position
#define BMP388_CMD_ERR_bm  (1<<1)       // Command execution Error bit mask
#define BMP388_CMD_ERR_bp  1            // Command execution Error bit position
#define BMP388_CONF_ERR_bm  (1<<2)      // Configuration Error bit mask
#define BMP388_CONF_ERR_bp  2           // Configuration Error bit position

// STATUS masks
#define BMP388_CMD_RDY_bm  (1<<4)       // CMD decoder status bit mask
#define BMP388_CMD_RDY_bp  4            // CMD decoder status bit position
#define BMP388_DRDY_PRES_bm  (1<<5)     // Data ready for pressure bit mask
#define BMP388_DRDY_PRES_bp  5          // Data ready for presure bit position
#define BMP388_DRDY_TEMP_bm  (1<<6)     // Data ready for temperatur bit mask
#define BMP388_DRDY_TEMP_bp  6          // Data ready for temperatur bit position

// EVENT masks
#define BMP388_POR_DETECTED_bm  (1<<0)  // bit mask
#define BMP388_POR_DETECTED_bp  0       // bit position

// INT_STATUS masks
#define BMP388_FWM_INT_bm  (1<<0)       // FIFO watermark interrupt bit mask
#define BMP388_FWM_INT_bp  0            // FIFO watermark interrupt bit position
#define BMP388_FFULL_INT_bm  (1<<1)     // FIFO full interrupt bit mask
#define BMP388_FFULL_INT_bp  1          // FIFO full interrupt bit position
#define BMP388_DRDY_bm  (1<<2)          // Data ready interrupt bit mask
#define BMP388_DRDY_bp  2               // Data ready interrupt bit position

// FIFO_LENGTH_0 masks
#define BMP388_FIFO_LENGTH_0_gm  0xFF   // FIFO fill level LSB group mask
#define BMP388_FIFO_LENGTH_0_gp  0      // FIFO fill level LSB group position

// FIFO_LENGTH_1 masks
#define BMP388_FIFO_LENGTH_1_gm  0x01   // FIFO fill level MSB group mask
#define BMP388_FIFO_LENGTH_1_gp  0      // FIFO fill level MSB group position

// FIFO_WTM_0 masks
#define BMP388_FIFO_WTM_0_gm  0xFF      // FIFO watermark LSB group mask
#define BMP388_FIFO_WTM_0_gp  0         // FIFO watermark LSB group position

// FIFO_WTM_1 masks
#define BMP388_FIFO_WTM_1_gm  0x01      // FIFO watermark MSB group mask
#define BMP388_FIFO_WTM_1_gp  0         // FIFO watermark MSB group position

// FIFO_CONFIG_1 masks
#define BMP388_FIFO_MODE_bm  (1<<0)     // FIFO enable bit mask
#define BMP388_FIFO_MODE_bp  0          // FIFO enable bit position
#define BMP388_FIFO_STOP_ON_FULL_bm  (1<<1) // FIFO stop on full enable bit mask
#define BMP388_FIFO_STOP_ON_FULL_bp  1  // FIFO stop on full enable bit position
#define BMP388_FIFO_TIME_EN_bm  (1<<2)  // FIFO sensortime enable bit mask
#define BMP388_FIFO_TIME_EN_bp  2       // FIFO sensortime enable bit position
#define BMP388_FIFO_PRESS_EN_bm  (1<<3) // FIFO pressure storage enable bit mask
#define BMP388_FIFO_PRESS_EN_bp  3      // FIFO pressure storage enable bit position
#define BMP388_FIFO_TEMP_EN_bm  (1<<4)  // FIFO temperatur storage enable bit mask
#define BMP388_FIFO_TEMP_EN_bp  4       // FIFO temperatur storage enable bit position

// FIFO_CONFIG_2 masks
#define BMP388_FIFO_SUBSAMPLING_gm  0x07// FIFO downsampling selection group mask
#define BMP388_FIFO_SUBSAMPLING_gp  0   // FIFO downsampling selection group position
#define BMP388_FIFO_SUBSAMPLING0_bm  (1<<0) // FIFO downsampling selection bit 0 mask
#define BMP388_FIFO_SUBSAMPLING0_bp  0  // FIFO downsampling selection bit 0 position
#define BMP388_FIFO_SUBSAMPLING1_bm  (1<<1) // FIFO downsampling selection bit 1 mask
#define BMP388_FIFO_SUBSAMPLING1_bp  1  // FIFO downsampling selection bit 1 position
#define BMP388_FIFO_SUBSAMPLING2_bm  (1<<2) // FIFO downsampling selection bit 2 mask
#define BMP388_FIFO_SUBSAMPLING2_bp  2  // FIFO downsampling selection bit 2 position
#define BMP388_DATA_SELECT_gm  0x18     // Data filter enable group mask
#define BMP388_DATA_SELECT_gp  3        // Data filter enable group position 
#define BMP388_DATA_SELECT0_bm  (1<<3)  // Data filter enable bit 0 mask
#define BMP388_DATA_SELECT0_bp  3       // Data filter enable bit 0 position
#define BMP388_DATA_SELECT1_bm  (1<<4)  // Data filter enable bit 1 mask
#define BMP388_DATA_SELECT1_bp  4       // Data filter enable bit 1 position

// INT_CTRL masks
#define BMP388_INT_OD_bm  (1<<0)        // Output configuration bit mask
#define BMP388_INT_OD_bp  0             // Output configuration bit position
#define BMP388_INT_LEVEL_bm  (1<<1)     // Int pin level bit mask
#define BMP388_INT_LEVEL_bp  1          // Int pin level bit position
#define BMP388_INT_LATCH_bm  (1<<2)     // Latch interrupt enable bit mask
#define BMP388_INT_LATCH_bp  2          // Latch interrupt enable bit position
#define BMP388_FWTM_EN_bm  (1<<3)       // FIFO watermark reached interrupt enable bit mask
#define BMP388_FWTM_EN_bp  3            // FIFO watermark reached interrupt enable bit position
#define BMP388_FFULL_EN_bm  (1<<4)      // FIFO full interrupt enable bit mask
#define BMP388_FFULL_EN_bp  4           // FIFO full interrupt enable bit position
#define BMP388_DRDY_EN_bm  (1<<6)       // Data ready interrupt enable bit mask
#define BMP388_DRDY_EN_bp  6            // Data ready interrupt enable bit position

// IF_CONFIG masks
#define BMP388_SPI3_bm  (1<<0)          // SPI Interface Mode select bit mask
#define BMP388_SPI3_bp  0               // SPI Interface Mode select bit position
#define BMP388_I2C_WDT_EN_bm  (1<<1)    // I2C watchdog timer enable bit mask
#define BMP388_I2C_WDT_EN_bp  1         // I2C watchdog timer enable bit position
#define BMP388_I2C_WDT_SEL_bm  (1<<2)   // I2C watchdog timer period bit mask
#define BMP388_I2C_WDT_SEL_bp  2        // I2C watchdog timer period bit position

// PWR_CTRL masks
#define BMP388_PRESS_EN_bm  (1<<0)      // Pressure sensor enable bit mask
#define BMP388_PRESS_EN_bp  0           // Pressure sensor enable bit position
#define BMP388_TEMP_EN_bm  (1<<1)       // Temperatur sensor enable bit mask
#define BMP388_TEMP_EN_bp  1            // Temperatur sensor enable bit position
#define BMP388_MODE_gm  0x30            // Mode select group mask
#define BMP388_MODE_gp  4               // Mode select group position
#define BMP388_MODE0_bm  (1<<4)         // Mode select bit 0 mask
#define BMP388_MODE0_bp  4              // Mode select bit 0 position
#define BMP388_MODE1_bm  (1<<5)         // Mode select bit 1 mask
#define BMP388_MODE1_bp  5              // Mode select bit 1 position

#define BMP388_MODE_SLEEP_gc  (0x00<<4)  // Sleep mode select
#define BMP388_MODE_FORCED_gc  (0x01<<4) // Forced mode select
#define BMP388_MODE_NORMAL_gc  (0x03<<4) // Normal mode select

// OSR masks
#define BMP388_OSR_P_gm  0x07           // Pressure oversampling group mask
#define BMP388_OSR_P_gp  0              // Pressure oversampling group position
#define BMP388_OSR_P0_bm  (1<<0)        // Pressure oversampling bit 0 mask
#define BMP388_OSR_P0_bp  0             // Pressure oversampling bit 0 position
#define BMP388_OSR_P1_bm  (1<<1)        // Pressure oversampling bit 1 mask
#define BMP388_OSR_P1_bp  1             // Pressure oversampling bit 1 position
#define BMP388_OSR_P2_bm  (1<<2)        // Pressure oversampling bit 2 mask
#define BMP388_OSR_P2_bp  2             // Pressure oversampling bit 2 position
#define BMP388_OSR4_T_gm  0x38          // Temperatur oversampling group mask
#define BMP388_OSR4_T_gp  3             // Temperatur oversampling group position
#define BMP388_OSR4_T0_bm  (1<<3)       // Temperatur oversampling bit 0 mask
#define BMP388_OSR4_T0_bp  3            // Temperatur oversampling bit 0 position
#define BMP388_OSR4_T1_bm  (1<<4)       // Temperatur oversampling bit 1 mask
#define BMP388_OSR4_T1_bp  4            // Temperatur oversampling bit 1 position
#define BMP388_OSR4_T2_bm  (1<<5)       // Temperatur oversampling bit 2 mask
#define BMP388_OSR4_T2_bp  5            // Temperatur oversampling bit 2 position

#define BMP388_OSR_P_x1_gc  (0x00<<0)   // No oversampling
#define BMP388_OSR_P_x2_gc  (0x01<<0)   // 2x oversampling
#define BMP388_OSR_P_x4_gc  (0x02<<0)   // 4x oversampling
#define BMP388_OSR_P_x8_gc  (0x03<<0)   // 8x oversampling
#define BMP388_OSR_P_x16_gc  (0x04<<0)  // 16x oversampling
#define BMP388_OSR_P_x32_gc  (0x05<<0)  // 32x oversampling

#define BMP388_OSR4_T_x1_gc  (0x00<<3)  // No oversampling
#define BMP388_OSR4_T_x2_gc  (0x01<<3)  // 2x oversampling
#define BMP388_OSR4_T_x4_gc  (0x02<<3)  // 4x oversampling
#define BMP388_OSR4_T_x8_gc  (0x03<<3)  // 8x oversampling
#define BMP388_OSR4_T_x16_gc  (0x04<<3) // 16x oversampling
#define BMP388_OSR4_T_x32_gc  (0x05<<3) // 32x oversampling

// ODR masks
#define BMP388_ODR_SEL_gm  0x1F         // Subvision factor select group mask
#define BMP388_ODR_SEL_bp  0            // Subvision factor select group position
#define BMP388_ODR_SEL0_bm  (1<<0)      // Subvision factor select bit 0 mask
#define BMP388_OSR_SEL0_bp  0           // Subvision factor select bit 0 position
#define BMP388_ODR_SEL1_bm  (1<<1)      // Subvision factor select bit 1 mask 
#define BMP388_ODR_SEL1_bp  1           // Subvision factor select bit 1 position
#define BMP388_ODR_SEL2_bm  (1<<2)      // Subvision factor select bit 2 mask
#define BMP388_ODR_SEL2_bp  2           // Subvision factor select bit 2 position
#define BMP388_ODR_SEL3_bm  (1<<3)      // Subvision factor select bit 3 mask
#define BMP388_ODR_SEL3_bp  3           // Subvision factor select bit 3 position
#define BMP388_ODR_SEL4_bm  (1<<4)      // Subvision factor select bit 4 mask
#define BMP388_ODR_SEL4_bp  4           // Subvision factor select bit 4 position

// (ODR) Output Data Rate select        // Frequenzy  ,    sampling period
#define BMP388_ODR_200_gc  (0x00<<0)    // 200    Hz,   5ms
#define BMP388_ODR_100_gc  (0x01<<0)    // 100    Hz,   10ms
#define BMP388_ODR_50_gc  (0x02<<0)     // 50   Hz,   20ms
#define BMP388_ODR_25_gc  (0x03<<0)     // 25   Hz,   40ms
#define BMP388_ODR_12p5_gc  (0x04<<0)   // 12.5   Hz,   80ms
#define BMP388_ODR_6p25_gc  (0x05<<0)   // 6.25   Hz,   160ms
#define BMP388_ODR_3p1_gc  (0x06<<0)    // 3.1    Hz,   320ms
#define BMP388_ODR_1p5_gc  (0x06<<0)    // 1.5    Hz,   640ms
#define BMP388_ODR_0p78_gc  (0x07<<0)   // 0.78   Hz,   1.280s
#define BMP388_ODR_0p39_gc  (0x08<<0)   // 0.39   Hz,   2.560s
#define BMP388_ODR_0p2_gc  (0x09<<0)    // 0.2    Hz,   5.120s
#define BMP388_ODR_0p1_gc  (0x0A<<0)    // 0.1    Hz,   10.24s
#define BMP388_ODR_0p05_gc  (0x0B<<0)   // 0.05   Hz,   20.48s
#define BMP388_ODR_0p02_gc  (0x0C<<0)   // 0.02   Hz,   40.96s
#define BMP388_ODR_0p01_gc  (0x0D<<0)   // 0.01   Hz,   81.92s
#define BMP388_ODR_0p006_gc  (0x0E<<0)  // 0.006    Hz,   163.84s
#define BMP388_ODR_0p003_gc  (0x0F<<0)  // 0.003    Hz,   327.68s
#define BMP388_ODR_0p0015_gc  (0x1F<<0) // 0.0015  Hz,    655.36s

// CONFIG masks
#define BMP388_IIR_FILTER_gm  0x0E      // IIR Filter coefficients group mask
#define BMP388_IIR_FILTER_gp  1         // IIR Filter coefficients group position
#define BMP388_IIR_FILTER0_bm  (1<<0)   // IIR Filter coefficients bit 0 mask
#define BMP388_IIR_FILTER0_bp  1        // IIR Filter coefficients bit 0 position
#define BMP388_IIR_FILTER1_bm  (1<<1)   // IIR Filter coefficients bit 1 mask
#define BMP388_IIR_FILTER1_bp  2        // IIR Filter coefficients bit 1 position
#define BMP388_IIR_FILTER2_bm  (1<<2)   // IIR Filter coefficients bit 2 mask
#define BMP388_IIR_FILTER2_bp  3        // IIR Filter coefficients bit 2 position

#define BMP388_IIR_COEF_0_gc  (0x00<<1)  // filter coefficient is 0 -> bypass-mode
#define BMP388_IIR_COEF_1_gc  (0x01<<1)  // filter coefficient is 1
#define BMP388_IIR_COEF_3_gc  (0x02<<1)  // filter coefficient is 3
#define BMP388_IIR_COEF_7_gc  (0x03<<1)  // filter coefficient is 7
#define BMP388_IIR_COEF_15_gc  (0x04<<1) // filter coefficient is 15
#define BMP388_IIR_COEFF_31_gc  (0x05<<1)// filter coefficient is 31
#define BMP388_IIR_COEFF_63_gc  (0x06<<1)// filter coefficient is 63
#define BMP388_IIR_COEFF_127_gc  (0x07<<1)// filter coefficient is 127

// CMD masks
// CMD register accepts only the following 4 values to be written.
#define BMP388_NOP_gc  0x00             // reserved. No command.
#define BMP388_EXTMODE_EN_MIDDLE  0x34  // See extmode_en_last
#define BMP388_FIFO_FLUSH  0xB0         // Clears all data. Config is leaved unchanged.
#define BMP388_SOFTRESET  0xB6          // Triggers a software reset. All register get their default value.
    
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _CITEC_BMP388_DEFINITIONS_H */

// ***** END OF FILE ******************************************************
