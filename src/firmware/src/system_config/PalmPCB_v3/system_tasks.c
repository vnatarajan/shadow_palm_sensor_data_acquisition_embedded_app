/*******************************************************************************
 System Tasks File

  File Name:
    system_tasks.c

  Summary:
    This file contains source code necessary to maintain system's polled state
    machines.

  Description:
    This file contains source code necessary to maintain system's polled state
    machines.  It implements the "SYS_Tasks" function that calls the individual
    "Tasks" functions for all the MPLAB Harmony modules in the system.

  Remarks:
    This file requires access to the systemObjects global data structure that
    contains the object handles to all MPLAB Harmony module objects executing
    polled in the system.  These handles are passed into the individual module
    "Tasks" functions to identify the instance of the module to maintain.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_config.h"
#include "system_definitions.h"
#include "citec_PalmPCB_v3.h"


// *****************************************************************************
// *****************************************************************************
// Section: Local Prototypes
// *****************************************************************************
// *****************************************************************************


 
static void _SYS_Tasks ( void );
void _SYS_TMR_Tasks(void);
void _USB_Tasks(void);
static void _CITEC_PALMPCB_V3_Tasks(void);
static void _APP_Tasks(void);


// *****************************************************************************
// *****************************************************************************
// Section: System "Tasks" Routine
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void SYS_Tasks ( void )

  Remarks:
    See prototype in system/common/sys_module.h.
*/

void SYS_Tasks ( void )
{

    if(xTaskCreate((TaskFunction_t) _SYS_TMR_Tasks, 
                "_SYS_TMR_Tasks", 1024, NULL, 1, NULL) != pdPASS)
        SYS_ASSERT(false, "Not enough heap to create _SYS_TMR_Tasks!");

    if(xTaskCreate((TaskFunction_t) _USB_Tasks, 
                "_USB_Tasks", 1024, NULL, 1, NULL) != pdPASS)
        SYS_ASSERT(false, "Not enough heap to create _USB_Tasks!");

      /* Create OS Thread for CDC Tasks. */
    if(xTaskCreate((TaskFunction_t) CITEC_USBCDC_RxTask,
                "CITEC_USBCDC_RxTask",
                USBCDCDeviceTask_Size, NULL, USBCDCSendTask_Prio, NULL) != pdPASS)
       SYS_ASSERT(false, "Not enough heap to create CITEC_USBCDC_RxTask!");
   
    /* Create OS Thread for Send tasks. */
    if(xTaskCreate((TaskFunction_t) CITEC_USBCDC_TxTask,
                "CITEC_USBCDC_TxTask",
                USBCDCDeviceTask_Size, NULL, USBCDCSendTask_Prio, NULL) != pdPASS)
       SYS_ASSERT(false, "Not enough heap to create CITEC_USBCDC_TxTask!");

    /* Create OS Thread for SerialProtocol tasks. */
//    if(xTaskCreate((TaskFunction_t) CITEC_SerialProtocol_Task,
//                "CITEC_SerialProtocol_Task",
//                1024, NULL, 4, NULL) != pdPASS)
//       SYS_ASSERT(false, "Not enough heap to create CITEC_SerialProtocol_Task!");


//    if(xTaskCreate((TaskFunction_t) _CITEC_PALMPCB_V3_Tasks,
//                "CITEC_PALMPCB_V3 Tasks", 1024, NULL, 1, NULL) == pdFAIL)
//        SYS_ASSERT(false, "Failed to create _CITEC_PALMPCB_V3_Tasks");
    
  
    /* Create OS Thread for APP Tasks. */
        xTaskCreate((TaskFunction_t) _APP_Tasks,
                    "APP Tasks",
                    1024, NULL, 1, NULL);
    /**************
     * Start RTOS * 
     **************/
    vTaskStartScheduler(); /* This function never returns. */
    
}
/*******************************************************************************
  Function:
    void _SYS_Tasks ( void )

  Summary:
    Maintains state machines of system modules.
*/

void _SYS_TMR_Tasks(void)
{
    while(1)
    {
        SYS_TMR_Tasks(sysObj.sysTmr);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
 }
 
 

void _USB_Tasks(void)
{
    while(1)
    {
        /* USBHS Driver Task Routine */ 
         DRV_USBHS_Tasks(sysObj.drvUSBObject);
         
        
        /* USB Device layer tasks routine */ 
        USB_DEVICE_Tasks(sysObj.usbDevObject0);
 

        vTaskDelay(50 / portTICK_PERIOD_MS);
    }
 }

static void _APP_Tasks(void)
{
    while(1)
    {
        CITEC_TOF_GE_IMU_APP_Tasks();
        //vTaskDelay(1000 / portTICK_PERIOD_MS); /*viki_comment: removed this delay*/
}
}
/*******************************************************************************
  Function:
    void _CITEC_PALMPCB_V3_Tasks ( void )

  Summary:
    Maintains state machine of CITEC_PALMPCB_V3.
*/

/*static void _CITEC_PALMPCB_V3_Tasks(void)
{
    while(1)
    {
        //CITEC_PALMPCB_V3_Tasks();
        
//        MISO2Toggle();
//        MISO6Toggle();
//        MOSI6Toggle();
//        SCK6Toggle();
        
        //PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_A, PORTS_BIT_POS_0);
        //CS25Toggle();
        
        LED2Toggle();
        vTaskDelay(pdMS_TO_TICKS(250));
    }
}*/


/*******************************************************************************
 End of File
 */
