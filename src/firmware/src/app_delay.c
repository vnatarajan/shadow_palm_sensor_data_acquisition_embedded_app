#include "app_delay.h"
#include "system_definitions.h"

#define GetSystemClock() (SYS_CLK_FREQ)
#define ms_SCALE   (GetSystemClock()/2000)
#define us_SCALE            (GetSystemClock()/2000000)

static uint32_t ReadCoreTimer(void);
static uint32_t ReadCoreTimer()
{
    volatile uint32_t timer;

    // get the count reg
    asm volatile("mfc0   %0, $9" : "=r"(timer));

    return(timer);
}

void app_delay(unsigned long int ms){
#if 0
      register unsigned int startCntms = ReadCoreTimer();
      register unsigned int waitCntms = ms * ms_SCALE;

      //while( ReadCoreTimer() - startCntms < waitCntms );
#else
      vTaskDelay(pdMS_TO_TICKS(ms));
#endif
}

void DelayUs(unsigned long int usDelay )
{
#if 0
    register unsigned int startCnt = ReadCoreTimer();
    register unsigned int waitCnt = usDelay * us_SCALE;
    while( ReadCoreTimer() - startCnt < waitCnt );
#else
    vTaskDelay(pdMS_TO_TICKS(usDelay/1000));
#endif
}


