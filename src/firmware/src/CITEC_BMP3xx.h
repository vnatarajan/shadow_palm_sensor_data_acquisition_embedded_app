/*******************************************************************************
  MPLAB Harmony Application Header File

  Company:
    Bielefeld University CITEC - Neuroinformatics Group
    Tobias Schwank

  File Name:
    CITEC_BMP3xx.h

  Summary:
    

  Description:
    
*******************************************************************************/

#ifndef _CITEC_BMP3XX_H
#define _CITEC_BMP3XX_H

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_definitions.h"
#include "CITEC_BMP3xx_config.h"
#include "CITEC_BMP388_definitions.h"
#include "citec_spi_drv_task.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

extern "C" {

#endif
// DOM-IGNORE-END 
    
#define MIN_SUPPORTED_PERIOD_MS 6
#define DEFAULT_PERIOD_MS       10
extern int SensorCount;
typedef struct 
    {
        uint8_t TxBuffer[2];
        uint8_t RxBuffer[5];
        uint8_t ErrReg;
        uint8_t StatusReg;
        uint16_t Pressure;
        uint16_t Tempreture;
        
        uint8_t data_lsb, data_xlsb, data_msb;
        
        SPI_MODULE_ID SPI_Module;
        PORTS_CHANNEL CS_Channel;
        PORTS_BIT_POS CS_Bit_Pos;
    }BAROMETER;
    
    extern BAROMETER Barometer[NUMBER_SENSORS];
    SerialDataUpdateStruct UpdateData;
    uint16_t ui16_BMP3xx_period;
    bool b_BMP3xx_running;
    
    extern void CITEC_BMP3XX_Init( void );
    bool CITEC_BMP3XX_RunRequest (bool b_runRequest);
    bool CITEC_BMP3XX_SetPeriod (uint16_t ui16_period);
    extern void CITEC_BMPXX_DeviceTask( void );
    void PrintValuesASCII( void );
    void PrintValuesRAW( void );

    // SPI communication functions
    bool SPISetRegVal(uint8_t RegAddr, uint8_t RegVal);
    bool SPIReadnBytes(size_t nBytes);
    bool CheckSPIFinished();

#include "system/debug/sys_debug.h"

#ifndef SYS_ASSERT
#define SYS_ASSERT(test, message) if(test==false){SYS_MESSAGE(message); SYS_DEBUG_BreakPoint();}
#endif

#endif /* _CITEC_BMP3XX_H */

/*******************************************************************************
 End of File
 */

