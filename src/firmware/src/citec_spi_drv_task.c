/* ************************************************************************** */
/* SPI Initialization

  @Company
    University of Bielefeld
    CITEC - Neuroinformatics Group
    Tobias Schwank
 
  @Date
    August 27th, 2017
 
  @File Name
    citec_spi_drv_task.c

  @Summary
    This file includes the source code of the spi_drv_task.h 

 */
/* ************************************************************************** */

#include "citec_spi_drv_task.h"


SPI_DRV_DATA spiTaskData;
SPIJOB_RETURN spiJobReturnData;
SPIJOB_JOBDATA spiJobData;

bool CITEC_SPI_Init ( void )
{
    xQueue_SPIJob = xQueueCreate( 1, sizeof(SPIJOB_JOBDATA) );
    if( xQueue_SPIJob == NULL )
    {
        SYS_ASSERT(false, "Failed to create SPI job queue.");
        return false;
    }
    
    xQueue_SPIJob_Return = xQueueCreate( 1, sizeof(SPIJOB_RETURN) );
    if( xQueue_SPIJob_Return == NULL )
    {
        SYS_ASSERT(false, "Failed to create SPI job return queue.");
        return false;
    }

    spiTaskData.handleSPI0 = DRV_HANDLE_INVALID;
    spiTaskData.handleSPI1 = DRV_HANDLE_INVALID;
    spiTaskData.handleSPI2 = DRV_HANDLE_INVALID;
    spiTaskData.handleSPI3 = DRV_HANDLE_INVALID;
    spiTaskData.handleSPI4 = DRV_HANDLE_INVALID;
    spiTaskData.handleSPI5 = DRV_HANDLE_INVALID;
    
    bool bSPI0 = false, bSPI1 = false, bSPI2 = false, bSPI3 = false, bSPI4 = false, bSPI5 = false; //, bSPI6 = false;
    if (spiTaskData.handleSPI0 == DRV_HANDLE_INVALID)
    {
        spiTaskData.handleSPI0 = DRV_SPI_Open(0, DRV_IO_INTENT_READWRITE);
            
        if(spiTaskData.handleSPI0 != DRV_HANDLE_INVALID)
            bSPI0 = true;
    }
    if (spiTaskData.handleSPI1 == DRV_HANDLE_INVALID)
    {
        spiTaskData.handleSPI1 = DRV_SPI_Open(1, DRV_IO_INTENT_READWRITE);
            
        if(spiTaskData.handleSPI1 != DRV_HANDLE_INVALID)
            bSPI1 = true;
    }
    if (spiTaskData.handleSPI2 == DRV_HANDLE_INVALID)
    {
        spiTaskData.handleSPI2 = DRV_SPI_Open(2, DRV_IO_INTENT_READWRITE);
            
        if(spiTaskData.handleSPI2 != DRV_HANDLE_INVALID)
            bSPI2 = true;
    }
    if (spiTaskData.handleSPI3 == DRV_HANDLE_INVALID)
    {
        spiTaskData.handleSPI3 = DRV_SPI_Open(3, DRV_IO_INTENT_READWRITE);
            
        if(spiTaskData.handleSPI3 != DRV_HANDLE_INVALID)
            bSPI3 = true;
    }
    if (spiTaskData.handleSPI4 == DRV_HANDLE_INVALID)
    {
        spiTaskData.handleSPI4 = DRV_SPI_Open(4, DRV_IO_INTENT_READWRITE);
            
        if(spiTaskData.handleSPI4 != DRV_HANDLE_INVALID)
            bSPI4 = true;
    }
    if (spiTaskData.handleSPI5 == DRV_HANDLE_INVALID)
    {
        spiTaskData.handleSPI5 = DRV_SPI_Open(5, DRV_IO_INTENT_READWRITE);
            
        if(spiTaskData.handleSPI5 != DRV_HANDLE_INVALID)
            bSPI5 = true;
    }
        
    // Check if all SPI Modules were initialized
    if(bSPI0 && bSPI1 && bSPI2 && bSPI3 && bSPI4 && bSPI5)
        return true;
    else
        return false;
}

/* *****************************************************************************
 End of File
 */
