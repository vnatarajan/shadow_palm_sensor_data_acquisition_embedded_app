/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Bielefeld University CITEC - Neuroinformatics Group
    Tobias Schwank

  @File Name
    CITEC_BMP3xx_config.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _CITEC_BMP3XX_CONFIG_H    /* Guard against multiple inclusion */
#define _CITEC_BMP3XX_CONFIG_H

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
    
// Task specific definitions used to create the task.
#define BMP3XXTASK_SIZE                4096u
#define BMP3XXTASK_PRIO                1u
    
// Define the number of sensors in total
// The PalmPCBv3 carries 60 sensors
#define NUMBER_SENSORS 60
// If testing without little finger unit, change the value to 50 sensors
#define SENSNUM_TO_READ 60
#define SENS_CNT_RST 0
    
// SPI 1 and SPI 3 !CS definitions
#define CS1_CNL PORT_CHANNEL_D
#define CS1_POS PORTS_BIT_POS_12
#define CS2_CNL PORT_CHANNEL_D
#define CS2_POS PORTS_BIT_POS_13
#define CS3_CNL PORT_CHANNEL_D
#define CS3_POS PORTS_BIT_POS_4
#define CS4_CNL PORT_CHANNEL_D
#define CS4_POS PORTS_BIT_POS_5
#define CS5_CNL PORT_CHANNEL_F
#define CS5_POS PORTS_BIT_POS_0
#define CS6_CNL PORT_CHANNEL_G
#define CS6_POS PORTS_BIT_POS_0
#define CS7_CNL PORT_CHANNEL_A
#define CS7_POS PORTS_BIT_POS_6
#define CS8_CNL PORT_CHANNEL_A
#define CS8_POS PORTS_BIT_POS_7
#define CS9_CNL PORT_CHANNEL_E
#define CS9_POS PORTS_BIT_POS_0
#define CS10_CNL PORT_CHANNEL_E
#define CS10_POS PORTS_BIT_POS_1
#define CS11_CNL PORT_CHANNEL_G
#define CS11_POS PORTS_BIT_POS_14
#define CS12_CNL PORT_CHANNEL_G
#define CS12_POS PORTS_BIT_POS_12
#define CS13_CNL PORT_CHANNEL_G
#define CS13_POS PORTS_BIT_POS_13
    
// SPI 2 and SPI 4 !CS definitions
#define CS14_CNL PORT_CHANNEL_E
#define CS14_POS PORTS_BIT_POS_2
#define CS15_CNL PORT_CHANNEL_E
#define CS15_POS PORTS_BIT_POS_3
#define CS16_CNL PORT_CHANNEL_E
#define CS16_POS PORTS_BIT_POS_4
#define CS17_CNL PORT_CHANNEL_G
#define CS17_POS PORTS_BIT_POS_15
#define CS18_CNL PORT_CHANNEL_A
#define CS18_POS PORTS_BIT_POS_5
#define CS19_CNL PORT_CHANNEL_E
#define CS19_POS PORTS_BIT_POS_5
#define CS20_CNL PORT_CHANNEL_E
#define CS20_POS PORTS_BIT_POS_6
#define CS21_CNL PORT_CHANNEL_E
#define CS21_POS PORTS_BIT_POS_7
#define CS22_CNL PORT_CHANNEL_C
#define CS22_POS PORTS_BIT_POS_2
#define CS23_CNL PORT_CHANNEL_C
#define CS23_POS PORTS_BIT_POS_3
#define CS24_CNL PORT_CHANNEL_G
#define CS24_POS PORTS_BIT_POS_9
//#define CS25_CNL PORT_CHANNEL_A
//#define CS25_POS PORTS_BIT_POS_0
#define CS25_CNL PORT_CHANNEL_E
#define CS25_POS PORTS_BIT_POS_8

// SPI 6 !CS definitions
#define CS101_CNL PORT_CHANNEL_C
#define CS101_POS PORTS_BIT_POS_14
#define CS102_CNL PORT_CHANNEL_C
#define CS102_POS PORTS_BIT_POS_13
#define CS103_CNL PORT_CHANNEL_D
#define CS103_POS PORTS_BIT_POS_9
#define CS104_CNL PORT_CHANNEL_A
#define CS104_POS PORTS_BIT_POS_15
#define CS105_CNL PORT_CHANNEL_A
#define CS105_POS PORTS_BIT_POS_14
#define CS106_CNL PORT_CHANNEL_F
#define CS106_POS PORTS_BIT_POS_5
#define CS107_CNL PORT_CHANNEL_F
#define CS107_POS PORTS_BIT_POS_4
#define CS108_CNL PORT_CHANNEL_A
#define CS108_POS PORTS_BIT_POS_4
#define CS109_CNL PORT_CHANNEL_A
#define CS109_POS PORTS_BIT_POS_3
#define CS110_CNL PORT_CHANNEL_C
#define CS110_POS PORTS_BIT_POS_15
#define CS111_CNL PORT_CHANNEL_D
#define CS111_POS PORTS_BIT_POS_14

    // *****************************************************************************
    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
    // *****************************************************************************

    

    // *****************************************************************************
    // *****************************************************************************
    // Section: Interface Functions
    // *****************************************************************************
    // *****************************************************************************

    

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _CITEC_BMP3XX_CONFIG_H */

/* *****************************************************************************
 End of File
 */
