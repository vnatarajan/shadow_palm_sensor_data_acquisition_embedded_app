/* ************************************************************************** */
/** SPI Task 

  @Company
    University of Bielefeld
    CITEC - Neuroinformatics Group
    Tobias Schwank
 
  @Datum
    February 6th 2019
 
  @File Name
    citec_spi_drv_task.h

  @Summary
    This file includes the definitions used by the source file
    citec_spi_drv_task.c

 */
/* ************************************************************************** */

#ifndef _SPI_DRV_TASK_H    /* Guard against multiple inclusion */
#define _SPI_DRV_TASK_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "system_definitions.h"

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
    
// Defines the prototype to which Chip Select functions must conform.
//typedef void (*CSFunction)( void * );
    
// Task specific definitions used to create the task.
#define SPITASK_SIZE                1024u
#define SPITASK_PRIO                2u

// SPIJOB_MAX_BUFFER_SIZE defines the maximum
// number of bytes to be received or send at once.
#define SPIJOB_MAX_BUFFER_SIZE 64

// Enumeration for Error indication.
typedef enum
{
    SUCCESSFUL,
    TIMEOUT,
    ERROR,
} SPIJOB_RESULT;
    
// Enumeration for possible SPI jobs to request by other tasks.
typedef enum
{
    SPI_WRITE,      // Just transmit data (e.g. Set register value.)
    SPI_READ,       // Just read data (e.g. Read value from sensor.)
    SPI_WRITE_READ, // Perform a Write and Read operation.
} SPIJOB_JOBS;

// Structure to hold SPITask data to control the SPI bus.
typedef struct
{
    /* SPI Driver Handle  */
    DRV_HANDLE handleSPI0;
    DRV_HANDLE handleSPI1;
    DRV_HANDLE handleSPI2;
    DRV_HANDLE handleSPI3;
    DRV_HANDLE handleSPI4;
    DRV_HANDLE handleSPI5;
    //DRV_HANDLE handleSPI6; There is only max 6 SPI-buses
    /* SPI Buffer Handle */
    DRV_SPI_BUFFER_HANDLE drvSPIBufferHandle;
    DRV_SPI_BUFFER_HANDLE drvSPIBufferHandleCB;
    DRV_SPI_BUFFER_HANDLE drvSPIJobHandle;
} SPI_DRV_DATA;
    
// Structure used by xQueue_SPIJob_Return to transmitt received data back 
// to requesting task.
// NOTE! Every Task needs to create its own Queue to receive the returned
// data and needs to send the according return Queue address to the SPI task.
// This method should enable waiting for finished SPI transmission by waiting 
// for incomming QueueElements on each task without triggering the wrong task.
typedef struct
{
    SPIJOB_RESULT result; 
    uint8_t RxBuffer[SPIJOB_MAX_BUFFER_SIZE];
} SPIJOB_RETURN;
    
// Structure used by xQueue_SPIJob to transmit data between task 
typedef struct 
{
    // Holds the wanted action to perform on the SPI bus.
    SPIJOB_JOBS RequestedJob;
        
    // Source ID indicates which task was sending the data.
    uint8_t SourceID;
        
    // QueueAddress is used to return the incomming data to the
    // task which has requested the data.
    uint32_t ReturnQueueAddress;
        
    // TxBuffer is the source of the data to be send.
    uint8_t TxBuffer[SPIJOB_MAX_BUFFER_SIZE];
        
    // BytesToSend is the ammount of data for the transmission.
    // SPI Task will recognize the end of the transmission with this value.
    uint8_t BytesToSend;
        
    // This two variables holds the position and channel of
    // the !CS pin to start and stop the transmission.
    // e.g. RB15 -> Module = 0, Channel = B, Pos = 15
    // NOTE! The function with correct definitions for every pin should be 
    // generated in system_config.h by MHC.
    PORTS_MODULE_ID CSPinModuleID;  // Normally it should be 0
    PORTS_CHANNEL CSPinChannel;     // Pin channel e.g. A, B, C, ...
    PORTS_BIT_POS CSPinNumber;      // Pin Number 0-15 
} SPIJOB_JOBDATA;

BaseType_t xSPIQueueStatus;
    
// Queue to hold the incoming jobs
QueueHandle_t xQueue_SPIJob;
QueueHandle_t xQueue_SPIJob_Return; // Wirklich n�tig?? Sollen ja die Tasks machen...
    
bool CITEC_SPI_Init ( void );
    
#include "system/debug/sys_debug.h"

#ifndef SYS_ASSERT
#define SYS_ASSERT(test, message) if(test==false){SYS_MESSAGE(message); SYS_DEBUG_BreakPoint();}
#endif

#endif /* _SPI_DRV_TASK_H */

/* *****************************************************************************
 End of File
 */
