/* ************************************************************************** */
/** Hardware communication library for I2C for use with FreeRTOS

  @Company
 * Universtity of Bielefeld
 * CITEC - Neuroinformatics Group
 * Tobias Schwank
 * 04.09.2019

  @File Name
 * i2c_drv_task.c

  @Summary
 * This file includes the source code of the i2c_drv_task.h file.

  @Description
    -
 */
/* ************************************************************************** */

#include "citec_i2c_drv_task.h"

// The callback function of the I2C module 
// The function is triggered, if the queued buffer transmittion has finished.
void I2CBufferEventFunction( DRV_I2C_BUFFER_EVENT event,
                             DRV_I2C_BUFFER_HANDLE handle, 
                             uintptr_t context)
{
    
    BaseType_t xHigherPriorityTaskWoken;
    
    xHigherPriorityTaskWoken = pdFALSE;
    
    // Check the queued buffer transaction 
    // Has it finished correctly?
    switch(event)
    {
        case DRV_I2C_BUFFER_EVENT_COMPLETE:  
            bI2C_TANSACTION_FINISHED = true;
            
            xSemaphoreGiveFromISR(xSemaphoreI2C0, &xHigherPriorityTaskWoken);
            
        break;

        case DRV_I2C_BUFFER_EVENT_ERROR:
            bI2C_TANSACTION_FINISHED = true;
        break;

        default:
            break;
    }
    
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}

// Function to init the i2c port 
void I2C_Init ( void )
{
    i2cData.handleI2C0 = DRV_HANDLE_INVALID;
    bool bI2CInit0 = false;
    bool bInit = false;
    
    // Create the binray semaphore and check if its non-null
    xSemaphoreI2C0 = xSemaphoreCreateBinary();
    if(xSemaphoreI2C0 == NULL)
    {
        // Error: Couldn't create binary semaphore. Probably due to insufficient memory.
        configASSERT(0);
    }

    // Retry the init until it is successful
    while(!bInit/*!bI2CInit && !bTMRInit*/)
    {
        if (i2cData.handleI2C0 == DRV_HANDLE_INVALID)
        {
            i2cData.handleI2C0 = DRV_I2C_Open(0, DRV_IO_INTENT_EXCLUSIVE);

            if(i2cData.handleI2C0 != DRV_HANDLE_INVALID)
                bI2CInit0 = true;
        }

        // If all the drivers run, do further inits
        if(bI2CInit0)
        {
            // Client registers an event handler with driver. This is done once.
            DRV_I2C_BufferEventHandlerSet( i2cData.handleI2C0, 
                                            I2CBufferEventFunction,
                                            i2cData.I2C0BufferEvent );

            bInit = true;

            // After this call, only the ISR will give the semaphore
            xSemaphoreGive(xSemaphoreI2C0);
        }
    }
}

/* *****************************************************************************
 End of File
 */
