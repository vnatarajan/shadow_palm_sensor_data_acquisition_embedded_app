/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    citec_palmpcb_v3.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "citec_PalmPCB_v3.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

CITEC_PALMPCB_V3_DATA citec_palmpcb_v3Data;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

// period is given in 100 us units
void CITEC_PALMPCB_V3_SetPeriod (uint16_t ui16_period)
{
    //only accept period of full milliseconds or more
    if (ui16_period >= 10)
       citec_palmpcb_v3Data.ui16_period = ui16_period/10;
    else
       citec_palmpcb_v3Data.ui16_period = 1;
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void CITEC_PALMPCB_V3_Initialize ( void )

  Remarks:
    See prototype in citec_palmpcb_v3.h.
 */

void CITEC_PALMPCB_V3_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    citec_palmpcb_v3Data.state = CITEC_PALMPCB_V3_STATE_INIT;
}


/******************************************************************************
  Function:
    void CITEC_PALMPCB_V3_Tasks ( void )

  Remarks:
    See prototype in citec_palmpcb_v3.h.
 */

void CITEC_PALMPCB_V3_Tasks ( void )
{
    uint8_t I2C_TxBuffer[5];
    
    /* Check the application's current state. */
    switch ( citec_palmpcb_v3Data.state )
    {
        /* Application's initial state. */
        case CITEC_PALMPCB_V3_STATE_INIT:
        {
            bool appInitialized = true;
       
        
            if (appInitialized)
            {
            
                citec_palmpcb_v3Data.state = CITEC_PALMPCB_V3_STATE_IDLE;
            }
            break;
        }

        case CITEC_PALMPCB_V3_STATE_SERVICE_TASKS:
        {
            
            /******************************************************************
             *            I 2 C   -  A C K N O W L E D G E   T E S T
             * 
             * I2C Test of all sensors, placed on the board which are using I2C. 
             * The task is totest, if the sensors are answering to request by 
             * the master. In case of ACK, the sensors are working and no further 
             * rework is to do on and the sensor will work for future use.
             * 
             * For current work, it is enough to test on ACK, because after 
             * molding the barometers in silicon, no rework is possible and 
             * broken sensors cannot be fixed.
             */
            
            int i = 0;
            for (i=0; i<(sizeof(I2C_TxBuffer)/sizeof(I2C_TxBuffer[0])); i++) {
                I2C_TxBuffer[i] = 0x00;
            }
            // Speak to the IMU
            DRV_I2C_Transmit(i2cData.handleI2C0, 0x50, &I2C_TxBuffer, 1, NULL);
            vTaskDelay(pdMS_TO_TICKS(1));
            
            // Speak to the GridEye
            DRV_I2C_Transmit(i2cData.handleI2C0, 0xD0, &I2C_TxBuffer, 1, NULL);
            vTaskDelay(pdMS_TO_TICKS(1));

            // Speak to the ToF Nr-1, but first we need to shut down ToF Nr-2.
            TOF2_GPIO0Off();
            DRV_I2C_Transmit(i2cData.handleI2C0, 0x52, &I2C_TxBuffer, 1, NULL);
            TOF2_GPIO0On();
            vTaskDelay(pdMS_TO_TICKS(1));
            
            // Speak to the ToF Nr-1, but first we need to shut down ToF Nr-1.
            TOF1_GPIO0Off();
            DRV_I2C_Transmit(i2cData.handleI2C0, 0x52, &I2C_TxBuffer, 1, NULL);
            TOF1_GPIO0On();
            vTaskDelay(pdMS_TO_TICKS(1));
            
            
            break;
        }

        /* TODO: implement your application state machine.*/
        case CITEC_PALMPCB_V3_STATE_IDLE:
        {
            break;
        }

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}

 

/*******************************************************************************
 End of File
 */
