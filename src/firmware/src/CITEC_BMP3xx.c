/*******************************************************************************
 Application Code for the use of BMP388 sensor.
  
  Company:
    Bielefeld University CITEC - Neuroinformatics Group
    Tobias Schwank
  
  File Name:
    CITEC_BMP38xx.c

  Summary:
    This source file contains the functions to read data from the Bosch BMP388
    pressure and temperatur sensor via SPI with FreeRTOS.

  Description:
    
 *******************************************************************************/

#include "CITEC_BMP3xx.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

int SensorCount;
BAROMETER Barometer[NUMBER_SENSORS];
// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void CITEC_BMP3XX_RunRequest (bool b_runRequest)
    void CITEC_BMP3XX_SetPeriod (uint16_t ui16_period)

  Remarks:
    See prototype in app.h.
*/

TickType_t xLastExecutionTime;
uint8_t SensValBuf[SENSNUM_TO_READ*2];
uint8_t SensMissedBuf[SENSNUM_TO_READ];

bool CITEC_BMP3XX_RunRequest (bool b_runRequest)
{
    if (b_BMP3xx_running != b_runRequest)
        b_BMP3xx_running = b_runRequest;
    return true;
}

// period is given in 100 us units
bool CITEC_BMP3XX_SetPeriod (uint16_t ui16_period)
{
    //only accept period of full milliseconds or more
    if (ui16_period >= 10*MIN_SUPPORTED_PERIOD_MS) // cannot be smaller than 6 ms currently
    {
       ui16_BMP3xx_period = ui16_period/10;
       return true;
    }
    else
    {
       ui16_BMP3xx_period = MIN_SUPPORTED_PERIOD_MS;
       return false;
    }
}

void bmp_post_init()
{
    SensorCount = SENS_CNT_RST;

    vTaskDelay(pdMS_TO_TICKS(10)); // 10ms delay
    
    // Initialize the variable used by the call to vTaskDelayUntil().
    xLastExecutionTime = xTaskGetTickCount();
    memset(SensMissedBuf, 0, SENSNUM_TO_READ);
}



// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void CITEC_BMP3XX_Init ( void )
{
    ui16_BMP3xx_period = DEFAULT_PERIOD_MS; // 10 ms by default
    b_BMP3xx_running = false;

    CITEC_BMP3XX_RunRequest(true);
    CITEC_BMP3XX_SetPeriod(DEFAULT_PERIOD_MS); /*10ms*/

    // Set the Pin channel and position of each CS 
    // and store it in its instance of data
    // 
    // The task will later call each sensor in a row with its own CS pin.
    // <editor-fold defaultstate="collapsed" desc="Barometer instance to CS mapping">
    Barometer[0].CS_Channel = CS1_CNL;
    Barometer[0].CS_Bit_Pos = CS1_POS;
    Barometer[0].SPI_Module = SPI_ID_1;
    Barometer[1].CS_Channel = CS1_CNL;
    Barometer[1].CS_Bit_Pos = CS1_POS;
    Barometer[1].SPI_Module = SPI_ID_3;
    Barometer[2].CS_Channel = CS2_CNL;
    Barometer[2].CS_Bit_Pos = CS2_POS;
    Barometer[2].SPI_Module = SPI_ID_1;
    Barometer[3].CS_Channel = CS2_CNL;
    Barometer[3].CS_Bit_Pos = CS2_POS;
    Barometer[3].SPI_Module = SPI_ID_3;
    Barometer[4].CS_Channel = CS3_CNL;
    Barometer[4].CS_Bit_Pos = CS3_POS;
    Barometer[4].SPI_Module = SPI_ID_1;
    Barometer[5].CS_Channel = CS3_CNL;
    Barometer[5].CS_Bit_Pos = CS3_POS;
    Barometer[5].SPI_Module = SPI_ID_3;
    Barometer[6].CS_Channel = CS4_CNL;
    Barometer[6].CS_Bit_Pos = CS4_POS;
    Barometer[6].SPI_Module = SPI_ID_1;
    Barometer[7].CS_Channel = CS4_CNL;
    Barometer[7].CS_Bit_Pos = CS4_POS;
    Barometer[7].SPI_Module = SPI_ID_3;
    Barometer[8].CS_Channel = CS5_CNL;
    Barometer[8].CS_Bit_Pos = CS5_POS;
    Barometer[8].SPI_Module = SPI_ID_1;
    Barometer[9].CS_Channel = CS5_CNL;
    Barometer[9].CS_Bit_Pos = CS5_POS;
    Barometer[9].SPI_Module = SPI_ID_3;
    Barometer[10].CS_Channel = CS6_CNL;
    Barometer[10].CS_Bit_Pos = CS6_POS;
    Barometer[10].SPI_Module = SPI_ID_1;
    Barometer[11].CS_Channel = CS6_CNL;
    Barometer[11].CS_Bit_Pos = CS6_POS;
    Barometer[11].SPI_Module = SPI_ID_3;
    Barometer[12].CS_Channel = CS7_CNL;
    Barometer[12].CS_Bit_Pos = CS7_POS;
    Barometer[12].SPI_Module = SPI_ID_1;
    Barometer[13].CS_Channel = CS7_CNL;
    Barometer[13].CS_Bit_Pos = CS7_POS;
    Barometer[13].SPI_Module = SPI_ID_3;
    Barometer[14].CS_Channel = CS8_CNL;
    Barometer[14].CS_Bit_Pos = CS8_POS;
    Barometer[14].SPI_Module = SPI_ID_1;
    Barometer[15].CS_Channel = CS8_CNL;
    Barometer[15].CS_Bit_Pos = CS8_POS;
    Barometer[15].SPI_Module = SPI_ID_3;
    Barometer[16].CS_Channel = CS9_CNL;
    Barometer[16].CS_Bit_Pos = CS9_POS;
    Barometer[16].SPI_Module = SPI_ID_1;
    Barometer[17].CS_Channel = CS9_CNL;
    Barometer[17].CS_Bit_Pos = CS9_POS;
    Barometer[17].SPI_Module = SPI_ID_3;
    Barometer[18].CS_Channel = CS10_CNL;
    Barometer[18].CS_Bit_Pos = CS10_POS;
    Barometer[18].SPI_Module = SPI_ID_1;
    Barometer[19].CS_Channel = CS10_CNL;
    Barometer[19].CS_Bit_Pos = CS10_POS;
    Barometer[19].SPI_Module = SPI_ID_3;
    Barometer[20].CS_Channel = CS11_CNL;
    Barometer[20].CS_Bit_Pos = CS11_POS;
    Barometer[20].SPI_Module = SPI_ID_1;
    Barometer[21].CS_Channel = CS11_CNL;
    Barometer[21].CS_Bit_Pos = CS11_POS;
    Barometer[21].SPI_Module = SPI_ID_3;
    Barometer[22].CS_Channel = CS12_CNL;
    Barometer[22].CS_Bit_Pos = CS12_POS;
    Barometer[22].SPI_Module = SPI_ID_1;
    Barometer[23].CS_Channel = CS12_CNL;
    Barometer[23].CS_Bit_Pos = CS12_POS;
    Barometer[23].SPI_Module = SPI_ID_3;
    Barometer[24].CS_Channel = CS13_CNL;
    Barometer[24].CS_Bit_Pos = CS13_POS;
    Barometer[24].SPI_Module = SPI_ID_1;
    Barometer[25].CS_Channel = CS13_CNL;
    Barometer[25].CS_Bit_Pos = CS13_POS;
    Barometer[25].SPI_Module = SPI_ID_3;
    Barometer[26].CS_Channel = CS14_CNL;
    Barometer[26].CS_Bit_Pos = CS14_POS;
    Barometer[26].SPI_Module = SPI_ID_4;
    Barometer[27].CS_Channel = CS14_CNL;
    Barometer[27].CS_Bit_Pos = CS14_POS;
    Barometer[27].SPI_Module = SPI_ID_2;
    Barometer[28].CS_Channel = CS15_CNL;
    Barometer[28].CS_Bit_Pos = CS15_POS;
    Barometer[28].SPI_Module = SPI_ID_4;
    Barometer[29].CS_Channel = CS15_CNL;
    Barometer[29].CS_Bit_Pos = CS15_POS;
    Barometer[29].SPI_Module = SPI_ID_2;
    Barometer[30].CS_Channel = CS16_CNL;
    Barometer[30].CS_Bit_Pos = CS16_POS;
    Barometer[30].SPI_Module = SPI_ID_4;
    Barometer[31].CS_Channel = CS16_CNL;
    Barometer[31].CS_Bit_Pos = CS16_POS;
    Barometer[31].SPI_Module = SPI_ID_2;
    Barometer[32].CS_Channel = CS17_CNL;
    Barometer[32].CS_Bit_Pos = CS17_POS;
    Barometer[32].SPI_Module = SPI_ID_4;
    Barometer[33].CS_Channel = CS17_CNL;
    Barometer[33].CS_Bit_Pos = CS17_POS;
    Barometer[33].SPI_Module = SPI_ID_2;
    Barometer[34].CS_Channel = CS18_CNL;
    Barometer[34].CS_Bit_Pos = CS18_POS;
    Barometer[34].SPI_Module = SPI_ID_4;
    Barometer[35].CS_Channel = CS18_CNL;
    Barometer[35].CS_Bit_Pos = CS18_POS;
    Barometer[35].SPI_Module = SPI_ID_2;
    Barometer[36].CS_Channel = CS19_CNL;
    Barometer[36].CS_Bit_Pos = CS19_POS;
    Barometer[36].SPI_Module = SPI_ID_4;
    Barometer[37].CS_Channel = CS19_CNL;
    Barometer[37].CS_Bit_Pos = CS19_POS;
    Barometer[37].SPI_Module = SPI_ID_2;
    Barometer[38].CS_Channel = CS20_CNL;
    Barometer[38].CS_Bit_Pos = CS20_POS;
    Barometer[38].SPI_Module = SPI_ID_4;
    Barometer[39].CS_Channel = CS20_CNL;
    Barometer[39].CS_Bit_Pos = CS20_POS;
    Barometer[39].SPI_Module = SPI_ID_2;
    Barometer[40].CS_Channel = CS21_CNL;
    Barometer[40].CS_Bit_Pos = CS21_POS;
    Barometer[40].SPI_Module = SPI_ID_4;
    Barometer[41].CS_Channel = CS21_CNL;
    Barometer[41].CS_Bit_Pos = CS21_POS;
    Barometer[41].SPI_Module = SPI_ID_2;
    Barometer[42].CS_Channel = CS22_CNL;
    Barometer[42].CS_Bit_Pos = CS22_POS;
    Barometer[42].SPI_Module = SPI_ID_4;
    Barometer[43].CS_Channel = CS22_CNL;
    Barometer[43].CS_Bit_Pos = CS22_POS;
    Barometer[43].SPI_Module = SPI_ID_2;
    Barometer[44].CS_Channel = CS23_CNL;
    Barometer[44].CS_Bit_Pos = CS23_POS;
    Barometer[44].SPI_Module = SPI_ID_4;
    Barometer[45].CS_Channel = CS23_CNL;
    Barometer[45].CS_Bit_Pos = CS23_POS;
    Barometer[45].SPI_Module = SPI_ID_2;
    Barometer[46].CS_Channel = CS24_CNL;
    Barometer[46].CS_Bit_Pos = CS24_POS;
    Barometer[46].SPI_Module = SPI_ID_4;
    Barometer[47].CS_Channel = CS24_CNL;
    Barometer[47].CS_Bit_Pos = CS24_POS;
    Barometer[47].SPI_Module = SPI_ID_2;
    Barometer[48].CS_Channel = CS25_CNL;
    Barometer[48].CS_Bit_Pos = CS25_POS;
    Barometer[48].SPI_Module = SPI_ID_4;
    Barometer[49].CS_Channel = CS25_CNL;
    Barometer[49].CS_Bit_Pos = CS25_POS;
    Barometer[49].SPI_Module = SPI_ID_2;
    
    Barometer[50].CS_Channel = CS101_CNL;
    Barometer[50].CS_Bit_Pos = CS101_POS;
    Barometer[50].SPI_Module = SPI_ID_6;
    Barometer[51].CS_Channel = CS102_CNL;
    Barometer[51].CS_Bit_Pos = CS102_POS;
    Barometer[51].SPI_Module = SPI_ID_6;
    Barometer[52].CS_Channel = CS103_CNL;
    Barometer[52].CS_Bit_Pos = CS103_POS;
    Barometer[52].SPI_Module = SPI_ID_6;
    Barometer[53].CS_Channel = CS104_CNL;
    Barometer[53].CS_Bit_Pos = CS104_POS;
    Barometer[53].SPI_Module = SPI_ID_6;
    Barometer[54].CS_Channel = CS105_CNL;
    Barometer[54].CS_Bit_Pos = CS105_POS;
    Barometer[54].SPI_Module = SPI_ID_6;
    Barometer[55].CS_Channel = CS106_CNL;
    Barometer[55].CS_Bit_Pos = CS106_POS;
    Barometer[55].SPI_Module = SPI_ID_6;
    Barometer[56].CS_Channel = CS107_CNL;
    Barometer[56].CS_Bit_Pos = CS107_POS;
    Barometer[56].SPI_Module = SPI_ID_6;
    Barometer[57].CS_Channel = CS108_CNL;
    Barometer[57].CS_Bit_Pos = CS108_POS;
    Barometer[57].SPI_Module = SPI_ID_6;
    Barometer[58].CS_Channel = CS109_CNL;
    Barometer[58].CS_Bit_Pos = CS109_POS;
    Barometer[58].SPI_Module = SPI_ID_6;
    Barometer[59].CS_Channel = CS110_CNL;
    Barometer[59].CS_Bit_Pos = CS110_POS;
    Barometer[59].SPI_Module = SPI_ID_6;
    // </editor-fold>
    
    int i = 0;
    for (i=0; i<SENSNUM_TO_READ; i++)
    {
        // Set the !CS to HIGH to stop the transmission.
        PLIB_PORTS_PinSet(PORTS_ID_0, Barometer[i].CS_Channel, Barometer[i].CS_Bit_Pos);
    }
    
    SensorCount = SENS_CNT_RST;
    while(SensorCount < SENSNUM_TO_READ)
    {
        // Setup the BMP388 sensor in forced mode, meaning it will be triggered
        // and polled in sync with a precise timing for sending out to USB_CDC
        // only press measurement enabled. no temperature
        SPISetRegVal(BMP388_PWR_CTRL,   (BMP388_MODE_FORCED_gc | BMP388_PRESS_EN_bm));
        // Setup the BMP388 Sensor with the configuration for maximum frequency
        // to be read out. This means the sensor runs at lowest possible precision.
        SPISetRegVal(BMP388_OSR,        (BMP388_OSR4_T_x1_gc   | BMP388_OSR_P_x1_gc));
        // Select next chip
        SensorCount++;
    }

    /*Post Init*/
    bmp_post_init();
}

/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void CITEC_BMPXX_DeviceTask ( void )
{
    bool bSPIStatus;

    // On each cycle, a single sensor is read out
        if (b_BMP3xx_running)
        {
            // Write the register address 0x02 to read from + R/W Bit (0x80))
            Barometer[SensorCount].TxBuffer[0] = BMP388_DUMMY;
            Barometer[SensorCount].TxBuffer[1] = (BMP388_ERR_REG | BMP388_RW_BIT);

            // Read all bytes at once and check if the data was new or old.
            // For normal use of the sensor within the timing limits (max. 200Hz),
            // the data should be always refreshed/new.
            bSPIStatus = SPIReadnBytes(5);

            // Immediately retrigger the same sensor for future read-out
            SPISetRegVal(BMP388_PWR_CTRL, (BMP388_MODE_FORCED_gc |BMP388_PRESS_EN_bm));

            // Read the Error and Status register
            if(bSPIStatus)
            {
                // Save the received values to the destination variables.
                // ErrReg contains the Sensor Error message storage and indicates configuration errors of the sensor.
                // StatusReg contains the Sensor status flags and indicates runtime errors and conversion ready flags.
                Barometer[SensorCount].ErrReg = Barometer[SensorCount].RxBuffer[0];
                Barometer[SensorCount].StatusReg = Barometer[SensorCount].RxBuffer[1];

                // We are interested in the status flags of the sensor which indicate, if we can read out the next measurement.
                // Therfore delete all other status flags and compare with the result.
                Barometer[SensorCount].StatusReg &= (BMP388_CMD_RDY_bm | BMP388_DRDY_PRES_bm);
                if(Barometer[SensorCount].StatusReg == (BMP388_CMD_RDY_bm | BMP388_DRDY_PRES_bm))
                {   // -> Sensor has completed the last measurement.

                    // Extract the barometer values out of the array. 
                    // BMP388 value is split into 3 bytes of data.
                    // Caused by low precision due to maximum readout frequency,
                    // the first byte of the 24-bit data (data_xlsb) is ignored and not further used.
                    Barometer[SensorCount].data_xlsb = Barometer[SensorCount].RxBuffer[2];
                    Barometer[SensorCount].data_lsb = Barometer[SensorCount].RxBuffer[3];
                    Barometer[SensorCount].data_msb = Barometer[SensorCount].RxBuffer[4];
                    Barometer[SensorCount].Pressure = (uint16_t)((Barometer[SensorCount].data_msb << 8)| Barometer[SensorCount].data_lsb);
                }
                else
                {
                    if (!(Barometer[SensorCount].StatusReg & BMP388_DRDY_PRES_bm))
                    {
                        SensMissedBuf[SensorCount]++;
                        if (SensMissedBuf[SensorCount] > 1)
                            configASSERT(0); // too many value were not fresh   
                    }
                }
            }
            // Print out the new value to PC
            //PrintValuesASCII();

            // Increase or reset the SensorCount variable to read the next Sensor
            // or the first Sensor.
            if(SensorCount >= SENSNUM_TO_READ-1)
            {
                SensorCount = SENS_CNT_RST;


                int i = 0, j = 0;
                // Copy all the data values of each sensor into the BarometerDataBuffer
                while(i < SENSNUM_TO_READ)
                {
                    // Store the value of the current sensor into the BarometerDataBuffer
                    SensValBuf[j] = Barometer[i].data_lsb;
                    SensValBuf[j+1] = Barometer[i].data_msb;

                    j+=2;   // Increase the DataBuffer counter by two
                    i++;    // Increment the sensor counter
                }
                UpdateData.ui8_sensorDatagramID = 1;    // Barometer Array is DatagramID 1
                UpdateData.pui8_data = SensValBuf;       // Set the pointer to the buffer

                /*
                    Conversion time of a single BMP388 is 4.6 ms, so a 5 ms waiting 
                    between two read-outs seemed sufficient but is not, 6 ms works
                    This is a periodic task.  Block until it is time to run again. 
                    The task will execute every 10 ms.
                */
                
                //viki_comment 
                //vTaskDelayUntil( &xLastExecutionTime, pdMS_TO_TICKS( ui16_BMP3xx_period ));
            }else{
                SensorCount++;
            }
        }else{
            vTaskDelayUntil( &xLastExecutionTime, pdMS_TO_TICKS( ui16_BMP3xx_period ));
        }
}

// This function is called to send the Barometer data as ASCII string via USB CDC 
// to the host PC.
// This function needs to be called for each sensor seperately.
void PrintValuesASCII( void )
{
    // Create a temporary buffer to store the received value into.
    char BarometerStringBuffer[SENSNUM_TO_READ*5];
    CDC_DataType data;
    
    if(SensorCount >= SENSNUM_TO_READ-1)
    {
        data.u32_Length = sprintf(BarometerStringBuffer, "%i:%i\n",SensorCount+1, Barometer[SensorCount].Pressure);
    }
    else
        data.u32_Length = sprintf(BarometerStringBuffer, "%i:%i ",SensorCount+1, Barometer[SensorCount].Pressure);
    
    
    strncpy(data.ca_Message, BarometerStringBuffer, data.u32_Length); //Store string (this can be int values or any other kind of data as well)
    CITEC_USBCDC_WriteData(&data);
}

// This function is called to send the barometer data as RAW string via USB CDC to the PC.
// It is called after a complete readout of all sensors and sends the data as one packet.
void PrintValuesRAW( void )
{
    char BarometerDataBuffer[SENSNUM_TO_READ*2];
    CDC_DataType data;
    
    // Datasize for USB is the number of sensor in total times 2 for each data packet.
    data.u32_Length = (SENSNUM_TO_READ+1)*2;
    
    // Set the header which is the GUI application parsing for
    BarometerDataBuffer[0] = 0xFC;
    BarometerDataBuffer[1] = 0xA5;
    
    // Init a counter to send all data at once
    uint8_t i, j;
    i = 0;
    j = 2;  // Startvalue 2 is caused by the header bytes
    
    // Copy all the data values of each sensor into the BarometerDataBuffer1
    while(i <= SENSNUM_TO_READ-1)
    {
        // Store the value of the current sensor into the BarometerDataBuffer
        BarometerDataBuffer[j] = Barometer[i].data_msb;
        BarometerDataBuffer[j+1] = Barometer[i].data_lsb;
        
        j+=2;   // Increase the DataBuffer counter by two.
        i++;    // Increment the sensor counter variable i.
    }
    
    // Copy the buffer into the USBCDC object and let USBCDCSend() transmit the data to the PC.
    memcpy(data.ca_Message, BarometerDataBuffer, data.u32_Length); //Store string (this can be int values or any other kind of data as well)
    CITEC_USBCDC_WriteData(&data);
}

// SPISetRegVal
//
// Description: Use this function to set parameter in slave registers.
//              by using the SPI interface.
// 
bool SPISetRegVal(uint8_t RegAddr, uint8_t RegVal)
{
    uint8_t val = 0;
    bool bSPIResult = true;
    
    // Set the !CS to LOW to start the transmission. 
    PLIB_PORTS_PinClear(PORTS_ID_0, Barometer[SensorCount].CS_Channel, 
                                    Barometer[SensorCount].CS_Bit_Pos);
    
    switch(Barometer[SensorCount].SPI_Module)
    {
        case SPI_ID_1:
            // Write the given parameters directly into the
            // SPI Buffer register. Data is shifted out immediately.
            SPI1BUF = RegAddr;
            // Check if the transmission didn't went wrong.
            // If it went wrong, Stop and return.
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            // Store the value which came in.
            val = SPI1BUF;
            // Write the value in to buffer, which should
            // be written in the register of the slave.
            SPI1BUF = RegVal;
            // Check if the transmission didn't went wrong.
            // If it went wrong, Stop and return.
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            // Store the received data in temp register in give it back to calling function
            val = SPI1BUF;
            break;
        case SPI_ID_2:
            SPI2BUF = RegAddr;
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            val = SPI2BUF;
            SPI2BUF = RegVal;
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            val = SPI2BUF;
            break;
        case SPI_ID_3:
            SPI3BUF = RegAddr;
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            val = SPI3BUF;
            SPI3BUF = RegVal;
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            val = SPI3BUF;
            break;
        case SPI_ID_4:
            SPI4BUF = RegAddr;
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            val = SPI4BUF;
            SPI4BUF = RegVal;
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            val = SPI4BUF;
            break;
        case SPI_ID_6:
            SPI6BUF = RegAddr;
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            val = SPI6BUF;
            SPI6BUF = RegVal;
            if(!CheckSPIFinished())
            {
                // Transaction has failed. Stop current transmission.
                bSPIResult = false;
                break;
            }
            val = SPI6BUF;
            break;
        case SPI_ID_5:
        default:
            SYS_ASSERT(false, "SPI Module isn't active");
            break;
    }
    
    // Set the !CS to HIGH to stop the transmission.
    PLIB_PORTS_PinSet(PORTS_ID_0, Barometer[SensorCount].CS_Channel, 
                                  Barometer[SensorCount].CS_Bit_Pos);

    return true;
}

// SPIReadnBytes
//
// Description: Use this function to read data from a slave.
// 
// Returns true if success, false otherwise
bool SPIReadnBytes(size_t nBytes)
{
    // Set the !CS to LOW to start the transmission. 
    PLIB_PORTS_PinClear(PORTS_ID_0, Barometer[SensorCount].CS_Channel, 
                                    Barometer[SensorCount].CS_Bit_Pos);
    
    int iCount = 0;
    uint32_t RxByte = 0x00;
    bool bSPIResult = true;
    switch(Barometer[SensorCount].SPI_Module)
    {
        case SPI_ID_1:
            // Read the status register of the sensor.
            for(iCount=0; iCount<nBytes+2; iCount++)
            {
                if(iCount == 0)
                {
                    // Write the register address to the active sensor
                    SPI1BUF = Barometer[SensorCount].TxBuffer[1];
                    // Wait for a finished SPI communication
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI 
                    RxByte = SPI1BUF;
                }
                else
                {
                    // Write the register address 0x02 to read from + R/W Bit (0x80))
                    SPI1BUF = Barometer[SensorCount].TxBuffer[0];
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI1BUF;
                    
                    // Store the value in the RX Buffer of the sensor instance,
                    // without the first two header bytes.
                    if(iCount >= 2)
                    {
                        // Store the received byte
                        Barometer[SensorCount].RxBuffer[iCount-2] = RxByte;
                    }
                }
            }
            break;
        case SPI_ID_2:
            // Read the ststus register of the sensor.
            for(iCount=0; iCount<nBytes+2; iCount++)
            {
                if(iCount == 0)
                {
                    // Write the register address to the active sensor
                    SPI2BUF = Barometer[SensorCount].TxBuffer[1];
                    // Wait for a finished SPI communication
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI2BUF;
                }
                else
                {
                    // Write the register address 0x02 to read from + R/W Bit (0x80))
                    SPI2BUF = Barometer[SensorCount].TxBuffer[0];
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI2BUF;
                    
                    // Store the value in the RX Buffer of the sensor instance,
                    // without the first two header bytes.
                    if(iCount >= 2)
                    {
                        // Store the received byte
                        Barometer[SensorCount].RxBuffer[iCount-2] = RxByte;
                    }
                }
            }
            break;
        case SPI_ID_3:
            // Read the ststus register of the sensor.
            for(iCount=0; iCount<nBytes+2; iCount++)
            {
                if(iCount == 0)
                {
                    // Write the register address to the active sensor
                    SPI3BUF = Barometer[SensorCount].TxBuffer[1];
                    // Wait for a finished SPI communication
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI3BUF;
                }
                else
                {
                    // Write the register address 0x02 to read from + R/W Bit (0x80))
                    SPI3BUF = Barometer[SensorCount].TxBuffer[0];
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI3BUF;
                    
                    // Store the value in the RX Buffer of the sensor instance,
                    // without the first two header bytes.
                    if(iCount >= 2)
                    {
                        // Store the received byte
                        Barometer[SensorCount].RxBuffer[iCount-2] = RxByte;
                    }
                }
            }
            break;
        case SPI_ID_4:
            // Read the ststus register of the sensor.
            for(iCount=0; iCount<nBytes+2; iCount++)
            {
                if(iCount == 0)
                {
                    // Write the register address to the active sensor
                    SPI4BUF = Barometer[SensorCount].TxBuffer[1];
                    // Wait for a finished SPI communication
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI4BUF;
                }
                else
                {
                    // Write the register address 0x02 to read from + R/W Bit (0x80))
                    SPI4BUF = Barometer[SensorCount].TxBuffer[0];
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI4BUF;
                    
                    // Store the value in the RX Buffer of the sensor instance,
                    // without the first two header bytes.
                    if(iCount >= 2)
                    {
                        // Store the received byte
                        Barometer[SensorCount].RxBuffer[iCount-2] = RxByte;
                    }
                }
            }
            break;
        case SPI_ID_6:
            // Read the ststus register of the sensor.
            for(iCount=0; iCount<nBytes+2; iCount++)
            {
                if(iCount == 0)
                {
                    // Write the register address to the active sensor
                    SPI6BUF = Barometer[SensorCount].TxBuffer[1];
                    // Wait for a finished SPI communication
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI6BUF;
                }
                else
                {
                    // Write the register address 0x02 to read from + R/W Bit (0x80))
                    SPI6BUF = Barometer[SensorCount].TxBuffer[0];
                    if(!CheckSPIFinished())
                    {
                        // Transaction has failed. Stop current transmission.
                        bSPIResult = false;
                        break;
                    }
                    // Store the received value of the SPI module
                    RxByte = SPI6BUF;
                    
                    // Store the value in the RX Buffer of the sensor instance,
                    // without the first two header bytes.
                    if(iCount >= 2)
                    {
                        // Store the received byte
                        Barometer[SensorCount].RxBuffer[iCount-2] = RxByte;
                    }
                }
            }
            break;
        case SPI_ID_5:
        default:
            SYS_ASSERT(false, "Wrong SPI Module was called");
            break;
    }
    
    // Set the !CS to HIGH to stop the transmission.
    PLIB_PORTS_PinSet(PORTS_ID_0, Barometer[SensorCount].CS_Channel, 
                                  Barometer[SensorCount].CS_Bit_Pos);

    // Return the result of the transaction
    if(!bSPIResult)
        return false;
    else
        return true;
}

// Function called to check a started SPI 
// communication.
// Returns true if successfull and false if an error occured.
bool CheckSPIFinished()
{
    // Maximum number of cycles, the task should wait for
    // a completed spi transmission
    // Value should be 3 times bigger than the worst transmission
    // time for one byte of data.
    const uint16_t MAX_ITERATOR_COUNT = 1000;
    uint16_t IteratorCounter = 0;
    
    // Check status bit of the SPI module to poll for a complete
    // transmission of one byte. 
    // 
    switch(Barometer[SensorCount].SPI_Module)
    {
        case SPI_ID_1:
            while(SPI1STATbits.SPIBUSY && IteratorCounter < MAX_ITERATOR_COUNT)
            {
                IteratorCounter++;
            }

            if(IteratorCounter >= MAX_ITERATOR_COUNT)
            {
                SYS_ASSERT(false, "SPI 1 Transmission Timeout.");
                return false;
            }
            break;
        case SPI_ID_2:
            while(SPI2STATbits.SPIBUSY && IteratorCounter <= MAX_ITERATOR_COUNT)
            {
                IteratorCounter++;
            }

            if(IteratorCounter >= MAX_ITERATOR_COUNT)
            {
                SYS_ASSERT(false, "SPI 2 Transmission Timeout.");
                return false;
            }
            break;
        case SPI_ID_3:
            while(SPI3STATbits.SPIBUSY && IteratorCounter <= MAX_ITERATOR_COUNT)
            {
                IteratorCounter++;
            }

            if(IteratorCounter >= MAX_ITERATOR_COUNT)
            {
                SYS_ASSERT(false, "SPI 3 Transmission Timeout.");
                return false;
            }
            break;
        case SPI_ID_4:
            while(SPI4STATbits.SPIBUSY && IteratorCounter <= MAX_ITERATOR_COUNT)
            {
                IteratorCounter++;
            }

            if(IteratorCounter >= MAX_ITERATOR_COUNT)
            {
                SYS_ASSERT(false, "SPI 4 Transmission Timeout.");
                return false;
            }
            break;
        case SPI_ID_6:
            while(SPI6STATbits.SPIBUSY && IteratorCounter <= MAX_ITERATOR_COUNT)
            {
                IteratorCounter++;
            }

            if(IteratorCounter >= MAX_ITERATOR_COUNT)
            {
                SYS_ASSERT(false, "SPI 6 Transmission Timeout.");
                return false;
            }
            break;
        case SPI_ID_5:
        default:
            SYS_ASSERT(false, "Wrong SPI Module was called");
            break;
    }
    return true;
}

/*******************************************************************************
 End of File
 */
