This is the firmware source for the ShadowHand Palm Sensor.
Currently only supports tactile sensors, uses our own "SerialProtocol" communication.

The firmware sources are tested with Microchip MPLAB X v5.40, XC32 v2.50 and Harmony v2.06.


= Setting up the build environment =

For downloading and setting up the programming environment, please see:
1) https://www.microchip.com/mplab/mplab-x-ide
2) https://www.microchip.com/mplab/compilers
3) https://www.microchip.com/mplab/mplab-harmony/mplab-harmony-v2

Please check out the code to your harmony folder structure under
\third_party\unibi\apps\ShadowHand-PalmSensor\<project name>
As project name you could use e.g. ShadowPalm.

As an example, under Windows, the MPLAB project would need to go under:
C:\microchip\harmony\v2_06\third_party\unibi\apps\ShadowHand-PalmSensor\ShadowPalm\firmware

The code is expecting PIC32libs repository checked out under:
\third_party\unibi\PIC32libs

Please also see further info about the required folder structure for UniBi PIC32 projects:
https://ni.www.techfak.uni-bielefeld.de/wiki/index.php/ROBOTIK:PIC32HarmonyDevelopments

The code is making use of FreeRTOS extensions for multi-tasking and thread safe communication.
https://www.freertos.org/
https://www.freertos.org/wp-content/uploads/2018/07/161204_Mastering_the_FreeRTOS_Real_Time_Kernel-A_Hands-On_Tutorial_Guide.pdf


= Running and testing the code =

The USB CDC connection is expecting 115200 baud setting on the USB host side.


= Limitations of the code =

Does not use ToF, IMU, Grid-Eye sensors yet.
