## Shadow Palm Sensor Data Acquisition Embedded App

This app is based on MPLAB Harmony Embedded Framework. The application reads the data from the following sensors,

    1. Grid Eye 
    2. ToF ( Time Of Flight )
    3. IMU

These sensors are mounted on Shadow Palm Hardware, Refer ( Fig 1 )

![](fig1.png)

The acquired sensor data is published to USB serial interface. The published data can be intuitively viewed by shadow_palm_demo_ui.

## Shadow Palm Demo UI Application

The demo ui application can be downloaded from,

    https://gitlab.ub.uni-bielefeld.de/vnatarajan/shadow_palm_demo_ui.git


## Integration Status:

1. All sensors are working properly and printing the sensor reading  over USD CDC COM.


## How to run the embedded code on remote system?

#### [ Step 1 ]  Environment Setup ( Remote - Real Palm Hardware)

    Make sure you have the following environment in the remote system,

	1) Harmony Version => v2.06
	2) XC32 Compiler Version => 1.44
	3) MPLAB X IDE Version => 5.5
    4) Target Device : PIC32MZ2048EFM100
    5) OS: Windows 10

#### [ Step 2 ] Code and Library Location ( Remote Environment ) 
    
In the remote environment ( setup by Risto for testing ), We have to copy the code as follows

     Copy From (1): The contents of "<this_repo>/src/*"
     To   : C:\microchip\harmony\v2_06\third_party\unibi\apps\ShadowHand-PalmSensor\ShadowPalm\

     Copy From (2): The contents of "<this_repo>/lib/*"
     To   : C:\microchip\harmony\v2_06\third_party\unibi\PIC32libs\

#### [ Step 3 ] Open MPLAB X IDE
    
    - Open MPLAB X IDE
    - Navigate to : File -> Open Project -> 
        -> "C:\microchip\harmony\v2_06\third_party\unibi\apps\ShadowHand-PalmSensor\ShadowPalm\firmware\ShadowHand-PalmSensor.X" 
    - In the Project Window, Right Click on "ShadowHand-PalmSensor" -> Set as main project

#### [ Step 4 ] Compile the program in MPLAB X IDE ( Remote Machine )

    -  Menu: Production -> Build Main Project ( F11 )

#### [ Step 5 ] Run the program in the palm hardware in debug mode ( Remote Machine )


Start Debugging Session:

    -  Menu: Debug -> Debug Main Project
            -> If there is a popup ( Select ICD )
            -> By now if the debug is successfully started, you can seed 'Debug-> Finish Debugger Session option' enabled]
            -> In the ICD3 Tab at the bottom, you can see messages like,

                Programming ...
                .
                .
                .
                Programming/Verify complete
                .
                .
                .
                Running 

            -> If the debugger started successfully, you can connect to COM3 port via teraterm @ 115200 ( Baud Rate )

Stop Debugging Session, To Stop Debugging Session,

    - Menu, Debug-> Finish Debugger Session option

Note:

    In remote machine device is connected via ICD3

    If you get any ICD Connection error, Try stopping previous debug session and start again (or) restart the MPLAB X IDE


## Code Reference:

    - Main Embedded App Location ( src/firmware/src )

        [ citec_tof_ge_imu_app.c ]

    -  Sensor Lib Location

        [ ToF ( lib/I2C/VL6180x/      )  ]
        [ IMU ( lib/I2C/BNO055_IMU/   )  ]
        [ GridEye ( lib/I2C/Grid_eye/ )  ]

## Delay Fix:

    - After removing all unused delays we can make the embedded code push data evey 40 milli seconds to USB Serial. Check dump.txt for reference

## Image Reference


    Barometer-based Tactile Skin for Anthropomorphic Robot Hand  

      Risto Koiva, Tobias Schwank, Guillaume Walck, Martin Meier, 
      Robert Haschke and Helge Ritter


